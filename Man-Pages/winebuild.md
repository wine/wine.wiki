---
title: winebuild - Wine dll builder
---

## SYNOPSIS

**winebuild**
[_options_]&nbsp;[_inputfile_...]

## DESCRIPTION

**winebuild**
generates the assembly files that are necessary to build a Wine dll,
which is basically a Win32 dll encapsulated inside a Unix library.

**winebuild**
has different modes, depending on what kind of file it is asked to
generate. The mode is specified by one of the mode options specified
below. In addition to the mode option, various other command-line
option can be specified, as described in the **OPTIONS** section.

## MODE OPTIONS

You have to specify exactly one of the following options, depending on
what you want winebuild to generate.

* **--dll**<br>
  Build an assembly file from a .spec file (see **SPEC FILE SYNTAX**
  for details), or from a standard Windows .def file. The .spec/.def
  file is specified via the **-E** option. The resulting file must be
  assembled and linked to the other object files to build a working Wine
  dll.  In this mode, the
  _input files_
  should be the list of all object files that will be linked into the
  final dll, to allow
  **winebuild**
  to get the list of all undefined symbols that need to be imported from
  other dlls.
* **--exe**<br>
  Build an assembly file for an executable. This is basically the same as
  the **--dll** mode except that it doesn't require a .spec/.def file as input,
  since an executable need not export functions. Some executables however
  do export functions, and for those a .spec/.def file can be specified via
  the **-E** option. The executable is named from the .spec/.def file name if
  present, or explicitly through the **-F** option. The resulting file must be
  assembled and linked to the other object files to build a working Wine
  executable, and all the other object files must be listed as
  _input files._
* **--def**<br>
  Build a .def file from a spec file. The .spec file is specified via the
  **-E** option. This is used when building dlls with a PE (Win32) compiler.
* **--implib**<br>
  Build a .a import library from a spec file. The .spec file is
  specified via the **-E** option. If the output library name ends
  in .delay.a, a delayed import library is built.
* **--staticlib**<br>
  Build a .a static library from object files.
* **--resources**<br>
  Generate a .o file containing all the input resources. This is useful
  when building with a PE compiler, since the PE binutils cannot handle
  multiple resource files as input. For a standard Unix build, the
  resource files are automatically included when building the spec file,
  so there's no need for an intermediate .o file.
* **--builtin**<br>
  Mark a PE module as a Wine builtin module, by adding the "Wine builtin
  DLL" signature string after the DOS header.
* **--fixup-ctors**<br>
  Fixup constructors after a module has been built. This should be done
  on the final .so module if its code contains constructors, to ensure
  that Wine has a chance to initialize the module before the
  constructors are executed.

## OPTIONS


* **--as-cmd=**_as-command_<br>
  Specify the command to use to compile assembly files; the default is
  **as**.
* **-b,&nbsp;--target=**_cpu-manufacturer_[**-**_kernel_]**-**_os_<br>
  Specify the target CPU and platform on which the generated code will
  be built. The target specification is in the standard autoconf format
  as returned by config.sub.
* **-B&nbsp;**_directory_<br>
  Add the directory to the search path for the various binutils tools
  like **as**, **nm** and **ld**.
* **--cc-cmd=**_cc-command_<br>
  Specify the C compiler to use to compile assembly files; the default
  is to instead use the assembler specified with **--as-cmd**.
* **--data-only**<br>
  Build a module that contains only data and resources, and no
  executable code.  With this option, **winebuild** directly outputs a
  PE file, instead of an assembly or object file.
* **-d,&nbsp;--delay-lib=**_name_<br>
  Set the delayed import mode for the specified library, which must be
  one of the libraries imported with the **-l** option. Delayed mode
  means that the library won't be loaded until a function imported from
  it is actually called.
* **-D&nbsp;**_symbol_<br>
  Ignored for compatibility with the C compiler.
* **-e,&nbsp;--entry=**_function_<br>
  Specify the module entry point function; if not specified, the default
  is
  **DllMain**
  for dlls, and
  **main**
  for executables (if the standard C
  **main**
  is not defined,
  **WinMain**
  is used instead). This is only valid for Win32 modules.
* **-E,&nbsp;--export=**_filename_<br>
  Specify a .spec file (see **SPEC FILE SYNTAX** for details),
  or a standard Windows .def file that defines the exports
  of the DLL or executable that is being built.
* **--external-symbols**<br>
  Allow linking to external symbols directly from the spec
  file. Normally symbols exported by a dll have to be defined in the dll
  itself; this option makes it possible to use symbols defined in
  another Unix library (for symbols defined in another dll, a
  _forward_
  specification must be used instead).
* **-f&nbsp;**_option_<br>
  Specify a code generation option. Currently **-fPIC** and
  **-fasynchronous-unwind-tables** are supported. Other options are
  ignored for compatibility with the C compiler.
* **--fake-module**<br>
  Create a fake PE module for a dll or exe, instead of the normal
  assembly or object file. The PE module contains the resources for the
  module, but no executable code.
* **-F,&nbsp;--filename=**_filename_<br>
  Set the file name of the module. The default is to use the base name
  of the spec file (without any extension).
* **-h, --help**<br>
  Display a usage message and exit.
* **-H,&nbsp;--heap=**_size_<br>
  Specify the size of the module local heap in bytes (only valid for
  Win16 modules); default is no local heap.
* **-I&nbsp;**_directory_<br>
  Ignored for compatibility with the C compiler.
* **-k, --kill-at**<br>
  Remove the stdcall decorations from the symbol names in the
  generated .def file. Only meaningful in **--def** mode.
* **-K&nbsp;**_flags_<br>
  Ignored for compatibility with the C compiler.
* **--large-address-aware**<br>
  Set a flag in the executable to notify the loader that this
  application supports address spaces larger than 2 gigabytes.
* **--ld-cmd=**_ld-command_<br>
  Specify the command to use to link the object files; the default is
  **ld**.
* **-m16, -m32, -m64**<br>
  Generate respectively 16-bit, 32-bit or 64-bit code.
* **-march=**_option_**,&nbsp;-mcpu=**_option_**,&nbsp;-mfpu=**_option_<br>
  Set code generation options for the assembler.
* **-mno-cygwin**<br>
  Build a library that uses the Windows runtime instead of the Unix C
  library.
* **-M,&nbsp;--main-module=**_module_<br>
  When building a 16-bit dll, set the name of its 32-bit counterpart to
  _module_. This is used to enforce that the load order for the
  16-bit dll matches that of the 32-bit one.
* **-N,&nbsp;--dll-name=**_dllname_<br>
  Set the internal name of the module. It is only used in Win16
  modules. The default is to use the base name of the spec file (without
  any extension). This is used for KERNEL, since it lives in
  KRNL386.EXE. It shouldn't be needed otherwise.
* **--nm-cmd=**_nm-command_<br>
  Specify the command to use to get the list of undefined symbols; the
  default is **nm**.
* **--nxcompat=**_yes_|_no_<br>
  Specify whether the module is compatible with no-exec support. The
  default is yes.
* **-o,&nbsp;--output=**_file_<br>
  Set the name of the output file (default is standard output). If the
  output file name ends in .o, the text output is sent to a
  temporary file that is then assembled to produce the specified .o
  file.
* **--prefer-native**<br>
  Specify that the native DLL should be preferred if available at run
  time. This can be used on modules that are mostly unimplemented.
* **-r,&nbsp;--res=**_rsrc.res_<br>
  Load resources from the specified binary resource file. The
  _rsrc.res_ file can be produced from a source resource file with
  [**wrc**](./wrc)
  (or with a Windows resource compiler).<br>
  This option is only necessary for Win16 resource files, the Win32 ones
  can simply listed as
  _input files_
  and will automatically be handled correctly (though the
  **-r**
  option will also work for Win32 files).
* **--safeseh**<br>
  Mark object files as SEH compatible.
* **--save-temps**<br>
  Do not delete the various temporary files that **winebuild** generates.
* **--subsystem=**_subsystem_[**:**_major_[**.**_minor_]]<br>
  Set the subsystem of the executable, which can be one of the following:<br>
  **console**
  for a command line executable,<br>
  **windows**
  for a graphical executable,<br>
  **native**
  for a native-mode dll,<br>
  **wince**
  for a ce dll.<br>
  The entry point of a command line executable is a normal C **main**
  function. A **wmain** function can be used instead if you need the
  argument array to use Unicode strings. A graphical executable has a
  **WinMain** entry point.<br>
  Optionally a major and minor subsystem version can also be specified;
  the default subsystem version is 4.0.
* **--syscall-table=**_id_<br>
  Set the system call table id, between 0 and 3. The default is 0, the
  ntdll syscall table. Only useful in modules that define syscall entry
  points.
* **-u,&nbsp;--undefined=**_symbol_<br>
  Add _symbol_ to the list of undefined symbols when invoking the
  linker. This makes it possible to force a specific module of a static
  library to be included when resolving imports.
* **-v, --verbose**<br>
  Display the various subcommands being invoked by
  **winebuild**.
* **--version**<br>
  Display the program version and exit.
* **-w, --warnings**<br>
  Turn on warnings.
* **--without-dlltool**<br>
  Generate import library without using dlltool.

## SPEC FILE SYNTAX


### General syntax

A spec file should contain a list of ordinal declarations. The general
syntax is the following:

_ordinal functype_
[_flags_]&nbsp;_exportname_&nbsp;**(**&nbsp;[_args..._]_&nbsp;_**)**&nbsp;[_handler_]<br>
_ordinal&nbsp;_**variable**
[_flags_]&nbsp;_exportname_&nbsp;**(**&nbsp;[_data..._]_&nbsp;_**)**<br>
_ordinal&nbsp;_**extern**
[_flags_]&nbsp;_exportname_&nbsp;[_symbolname_]<br>
_ordinal&nbsp;_**stub**
[_flags_]&nbsp;_exportname_&nbsp;[&nbsp;**(**_args..._**)**&nbsp;]<br>
_ordinal&nbsp;_**equate**
[_flags_]&nbsp;_exportname&nbsp;data_<br>
**#&nbsp;**_comments_

Declarations must fit on a single line, except if the end of line is
escaped using a backslash character. The
**#**
character anywhere in a line causes the rest of the line to be ignored
as a comment.

_ordinal_
specifies the ordinal number corresponding to the entry point, or '@'
for automatic ordinal allocation (Win32 only).

_flags_
is a series of optional flags, preceded by a '-' character. The
supported flags are:

* **-norelay**<br>
  The entry point is not displayed in relay debugging traces (Win32
  only).
* **-noname**<br>
  The entry point will be exported by ordinal instead of by name. The
  name is still available for importing.
* **-ret16**<br>
  The function returns a 16-bit value (Win16 only).
* **-ret64**<br>
  The function returns a 64-bit value (Win32 only).
* **-register**<br>
  The function uses CPU register to pass arguments.
* **-private**<br>
  The function cannot be imported from other dlls, it can only be
  accessed through GetProcAddress.
* **-ordinal**<br>
  The entry point will be imported by ordinal instead of by name. The
  name is still exported.
* **-thiscall**<br>
  The function uses the
  _thiscall_
  calling convention (first parameter in %ecx register on i386).
* **-fastcall**<br>
  The function uses the
  _fastcall_
  calling convention (first two parameters in %ecx/%edx registers on
  i386).
* **-syscall**<br>
  The function is an NT system call. A system call thunk will be
  generated, and the actual function will be called by the
  _\_\_wine_syscall_dispatcher_ function that will be generated on the
  Unix library side.
* **-import**<br>
  The function is imported from another module. This can be used instead
  of a
  _forward_
  specification when an application expects to find the function's
  implementation inside the dll.
* **-arch=**[**!**]_cpu_[**,**_cpu_]<br>
  The entry point is only available on the specified CPU
  architecture(s). The names **win32** and **win64** match all
  32-bit or 64-bit CPU architectures respectively. In 16-bit dlls,
  specifying **-arch=win32** causes the entry point to be exported
  from the 32-bit wrapper module. A CPU name can be prefixed with
  **!** to exclude only that specific architecture.

### Function ordinals

Syntax:<br>
_ordinal functype_
[_flags_]&nbsp;_exportname_&nbsp;**(**&nbsp;[_args..._]_&nbsp;_**)**&nbsp;[_handler_]<br>

This declaration defines a function entry point.  The prototype defined by
_exportname_&nbsp;**(**&nbsp;[_args..._]_&nbsp;_**)**
specifies the name available for dynamic linking and the format of the
arguments. '@' can be used instead of
_exportname_
for ordinal-only exports.

_functype_
should be one of:

* **stdcall**<br>
  for a normal Win32 function
* **pascal**<br>
  for a normal Win16 function
* **cdecl**<br>
  for a Win16 or Win32 function using the C calling convention
* **varargs**<br>
  for a Win16 or Win32 function using the C calling convention with a
  variable number of arguments

_args_
should be one or several of:

* **word**<br>
  (16-bit unsigned value)
* **s_word**<br>
  (16-bit signed word)
* **long**<br>
  (pointer-sized integer value)
* **int64**<br>
  (64-bit integer value)
* **int128**<br>
  (128-bit integer value)
* **float**<br>
  (32-bit floating point value)
* **double**<br>
  (64-bit floating point value)
* **ptr**<br>
  (linear pointer)
* **str**<br>
  (linear pointer to a null-terminated ASCII string)
* **wstr**<br>
  (linear pointer to a null-terminated Unicode string)
* **segptr**<br>
  (segmented pointer)
* **segstr**<br>
  (segmented pointer to a null-terminated ASCII string).
  .HP
  Note: The 16-bit and segmented pointer types are only valid for Win16
  functions.

_handler_
is the name of the actual C function that will implement that entry
point in 32-bit mode. The handler can also be specified as
_dllname_**.**_function_
to define a forwarded function (one whose implementation is in another
dll). If
_handler_
is not specified, it is assumed to be identical to
_exportname._

This first example defines an entry point for the 32-bit GetFocus()
call:

* @ stdcall GetFocus() GetFocus

This second example defines an entry point for the 16-bit
CreateWindow() call (the ordinal 100 is just an example); it also
shows how long lines can be split using a backslash:

* 100 pascal CreateWindow(ptr ptr long s_word s_word s_word &nbsp;   s_word word word word ptr) WIN_CreateWindow

To declare a function using a variable number of arguments, specify
the function as
**varargs**
and declare it in the C file with a '...' parameter for a Win32
function, or with an extra VA_LIST16 argument for a Win16 function.
See the wsprintf* functions in user.exe.spec and user32.spec for an
example.

### Variable ordinals

Syntax:<br>
_ordinal&nbsp;_**variable**
[_flags_]&nbsp;_exportname_&nbsp;**(**&nbsp;[_data..._]_&nbsp;_**)**

This declaration defines data storage as 32-bit words at the ordinal
specified.
_exportname_
will be the name available for dynamic
linking.
_data_
can be a decimal number or a hex number preceded by "0x".  The
following example defines the variable VariableA at ordinal 2 and
containing 4 ints:

* 2 variable VariableA(-1 0xff 0 0)

This declaration only works in Win16 spec files. In Win32 you should
use
**extern**
instead (see below).

### Extern ordinals

Syntax:<br>
_ordinal&nbsp;_**extern**
[_flags_]&nbsp;_exportname_&nbsp;[_symbolname_]

This declaration defines an entry that simply maps to a C symbol
(variable or function). It only works in Win32 spec files.
_exportname_
will point to the symbol
_symbolname_
that must be defined in the C code. Alternatively, it can be of the
form
_dllname_**.**_symbolname_
to define a forwarded symbol (one whose implementation is in another
dll). If
_symbolname_
is not specified, it is assumed to be identical to
_exportname._

### Stub ordinals

Syntax:<br>
_ordinal&nbsp;_**stub**
[_flags_]&nbsp;_exportname_&nbsp;[&nbsp;**(**_args..._**)**&nbsp;]

This declaration defines a stub function. It makes the name and
ordinal available for dynamic linking, but will terminate execution
with an error message if the function is ever called.

### Equate ordinals

Syntax:<br>
_ordinal&nbsp;_**equate**
[_flags_]&nbsp;_exportname&nbsp;data_

This declaration defines an ordinal as an absolute value.
_exportname_
will be the name available for dynamic linking.
_data_
can be a decimal number or a hex number preceded by "0x".

### Api sets

Syntax:<br>
**apiset&nbsp;**_apiset_dll&nbsp;_**=&nbsp;**_target.dll&nbsp;[host.dll:target.dll]_

This declaration defines that the _apiset_dll_ (of the form
api-ms-\*) resolves to the _target_ dll. Optionally other targets
can be specified to resolve differently for specific host dlls. For
example:

* api-ms-win-core-processenvironment-l1-1-0 = kernelbase.dll<br>
  api-ms-win-core-processthreads-l1-1-0 = kernel32.dll \.br
    kernel32.dll:kernelbase.dll

If apisets are defined, a corresponding .apiset section will be
generated in the PE binary. This requires building the module with the
--data-only option.

## AUTHORS

**winebuild**
has been worked on by many people over the years. The main authors are
Robert J. Amstadt, Alexandre Julliard, Martin von Loewis, Ulrich
Weigand and Eric Youngdale. Many other people have contributed new features
and bug fixes. For a complete list, see the git commit logs.

## BUGS

It is not yet possible to use a PE-format dll in an import
specification; only Wine dlls can be imported.

Bugs can be reported on the
[**Wine bug tracker**](https://bugs.winehq.org).

## AVAILABILITY

**winebuild**
is part of the Wine distribution, which is available through WineHQ,
the
[**Wine development headquarters**](https://www.winehq.org/).

## SEE ALSO

[**wine**](./wine),
[**winegcc**](./winegcc),
[**wrc**](./wrc),<br>
[**Wine documentation and support**](https://www.winehq.org/help).
