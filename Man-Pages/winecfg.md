---
title: winecfg - Wine Configuration Editor
---

## SYNOPSIS

**winecfg**
[_options_]

## DESCRIPTION

**winecfg**
is the Wine configuration editor. It allows you to change several settings, such as DLL load order
(native versus builtin), enable a virtual desktop, setup disk drives, and change the Wine audio driver,
among others. Many of these settings can be made on a per application basis, for example, preferring native
riched20.dll for wordpad.exe, but not for notepad.exe.

If no option is given launch the graphical version of
**winecfg**.

## OPTIONS


* **-v** [_version_], **/v** [_version_]<br>
  Set global Windows version to
  _version_
  and exit.
  If
  _version_
  is not specified, display the current global Windows version and exit.
* **-?**, **/?**<br>
  Display help, valid versions for
  _version_
  and exit.

## BUGS

Bugs can be reported on the
[**Wine bug tracker**](https://bugs.winehq.org).

## AVAILABILITY

**winecfg**
is part of the Wine distribution, which is available through WineHQ,
the
[**Wine development headquarters**](https://www.winehq.org/).

## SEE ALSO

[**wine**](./wine),<br>
[**Wine documentation and support**](https://www.winehq.org/help).
