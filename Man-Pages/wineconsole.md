---
title: wineconsole - Wine console manager
---

## SYNOPSIS

**wineconsole**
[_command_]

## DESCRIPTION

**wineconsole**
is the Wine console manager, used to run console commands and applications. It allows running the
console in a newly made window.

## BUGS

Bugs can be reported on the
[**Wine bug tracker**](https://bugs.winehq.org).

## AVAILABILITY

**wineconsole**
is part of the Wine distribution, which is available through WineHQ,
the
[**Wine development headquarters**](https://www.winehq.org/).

## SEE ALSO

[**wine**](./wine),<br>
[**Wine documentation and support**](https://www.winehq.org/help).
