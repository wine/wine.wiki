---
title: wineserver - the Wine server
---

<small>
&nbsp;[:flag_fr: Français](fr/Man-Pages/wineserver)
&nbsp;[:flag_de: Deutsch](de/Man-Pages/wineserver)
</small>

-----


## SYNOPSIS

**wineserver**
[_options_]

## DESCRIPTION

**wineserver**
is a daemon process that provides to Wine roughly the same services
that the Windows kernel provides on Windows.

**wineserver**
is normally launched automatically when starting [**wine**](./wine), so you
shouldn't have to worry about it. In some cases however, it can be
useful to start **wineserver** explicitly with different options, as
explained below.

## OPTIONS


* **-d**[_n_], **--debug**[**=**_n_]<br>
  Set the debug level to
  _n_.
  0 means no debugging information, 1 is the normal level, and 2 is for
  extra verbose debugging. If
  _n_
  is not specified, the default is 1. The debug output will be sent to
  stderr. [**wine**](./wine) will automatically enable normal level debugging
  when starting **wineserver** if the +server option is set in the
  **WINEDEBUG** variable.
* **-f**, **--foreground**<br>
  Make the server remain in the foreground for easier debugging, for
  instance when running it under a debugger.
* **-h**, **--help**<br>
  Display a help message.
* **-k**[_n_], **--kill**[**=**_n_]<br>
  Kill the currently running
  **wineserver**,
  optionally by sending signal _n_. If no signal is specified, sends
  a **SIGINT** first and then a **SIGKILL**.  The instance of **wineserver**
  that is killed is selected based on the **WINEPREFIX** environment
  variable.
* **-p**[_n_], **--persistent**[**=**_n_]<br>
  Specify the **wineserver** persistence delay, i.e. the amount of
  time that the server will keep running when all client processes have
  terminated. This avoids the cost of shutting down and starting again
  when programs are launched in quick succession. The timeout _n_ is
  in seconds, the default value is 3 seconds. If _n_ is not
  specified, the server stays around forever.
* **-v**, **--version**<br>
  Display version information and exit.
* **-w**, **--wait**<br>
  Wait until the currently running
  **wineserver**
  terminates.

## ENVIRONMENT


* **WINEPREFIX**<br>
  If set, the content of this variable is taken as the name of the directory where
  **wineserver**
  stores its data (the default is _\$HOME/.wine_). All
  **wine**
  processes using the same
  **wineserver**
  (i.e.: same user) share certain things like registry, shared memory
  and kernel objects.
  By setting
  **WINEPREFIX**
  to different values for different Wine processes, it is possible to
  run a number of truly independent Wine sessions.

## FILES


* **~/.wine**<br>
  Directory containing user specific data managed by
  **wine**.
* **/tmp/.wine-**_uid_<br>
  Directory containing the server Unix socket and the lock
  file. These files are created in a subdirectory generated from the
  **WINEPREFIX** directory device and inode numbers.

## AUTHORS

The original author of
**wineserver**
is Alexandre Julliard. Many other people have contributed new features
and bug fixes. For a complete list, see the git commit logs.

## BUGS

Bugs can be reported on the
[**Wine bug tracker**](https://bugs.winehq.org).

## AVAILABILITY

**wineserver**
is part of the Wine distribution, which is available through WineHQ,
the
[**Wine development headquarters**](https://www.winehq.org/).

## SEE ALSO

[**wine**](./wine),<br>
[**Wine documentation and support**](https://www.winehq.org/help).
