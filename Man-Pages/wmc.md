---
title: wmc - Wine Message Compiler
---

## SYNOPSIS

**wmc**
[_options_]&nbsp;[_inputfile_]

## DESCRIPTION

**wmc**
compiles messages from
**inputfile**
into FormatMessage[AW] compatible format encapsulated in a resourcescript
format.
**wmc**
outputs the data either in a standard _.bin_ formatted binary
file, or can generated inline resource data.

**wmc**
takes only one _inputfile_ as argument (see **BUGS**). The
_inputfile_ normally has extension _.mc_. The messages are read from
standard input if no inputfile is given. If the outputfile is not specified
with **-o**, then **wmc** will write the output to _inputfile.{rc,h}_.
The outputfile is named _wmc.tab.{rc,h}_ if no inputfile was given.

## OPTIONS


* **-c**<br>
  Set 'custom-bit' in message-code values.
* **-d**<br>
  NON-FUNCTIONAL; Use decimal values in output
* **-D**<br>
  Set debug flag. This results is a parser trace and a lot of extra messages.
* **-h**,&nbsp;**--help**<br>
  Print an informative usage message and exits.
* **-H&nbsp;**_file_<br>
  Write headerfile to _file_. Default is _inputfile.h_.
* **-i**<br>
  Inline messagetable(s). This option skips the generation of all _.bin_ files
  and writes all output into the _.rc_ file. This encoding is parsable with
  wrc(1).
* **--nls-dir=**_directory_<br>
  Specify the directory to search for the NLS files containing the
  codepage mapping tables.
* **-o**,&nbsp;**--output**=_file_<br>
  Output to _file_. Default is _inputfile.rc_.
* **-O**,&nbsp;**--output-format**=_format_<br>
  Set the output format. Supported formats are **rc** (the default),
  **res**, and **pot**.
* **-P**,&nbsp;**--po-dir**=_directory_<br>
  Enable the generation of resource translations based on po files
  loaded from the specified directory. That directory must follow the
  gettext convention, in particular in must contain one _.po_ file for
  each language, and a LINGUAS file listing the available languages.
* **-u**<br>
  Assume that the input file is in Unicode or UTF‑8 format and skip
  codepage conversions.
* **-v**<br>
  Show all supported codepages and languages.
* **-V**,&nbsp;**--version**<br>
  Print version end exit.
* **-W**,&nbsp;**--pedantic**<br>
  Enable pedantic warnings.

## EXTENSIONS

The original syntax is extended to support codepages more smoothly. Normally,
codepages are based on the DOS codepage from the language setting. The
original syntax only allows the destination codepage to be set. However, this
is not enough for non-DOS systems which do not use unicode source-files.

A new keyword **Codepages** is introduced to set both input and output
codepages to anything one wants for each language. The syntax is similar to
the other constructs:

Codepages '=' '(' language '=' cpin ':' cpout ... ')'

The _language_ is the numerical language-ID or the alias set with
LanguageNames. The input codepage _cpin_ and output codepage
_cpout_ are the numerical codepage IDs. There can be multiple mappings
within the definition and the definition may occur more than once.

## AUTHORS

**wmc**
was written by Bertho A. Stultiens.

## BUGS

The message compiler should be able to have multiple input files and combine
them into one output file. This would enable the splitting of languages into
separate files.

Decimal output is completely lacking. Don't know whether it should be
implemented because it is a, well, non-informative format change. It is
recognized on the commandline for some form of compatibility.

Bugs can be reported on the
[**Wine bug tracker**](https://bugs.winehq.org).

## AVAILABILITY

**wmc**
is part of the Wine distribution, which is available through WineHQ,
the
[**Wine development headquarters**](https://www.winehq.org/).

## SEE ALSO

[**wine**](./wine),
[**wrc**](./wrc),<br>
[**Wine documentation and support**](https://www.winehq.org/help).
