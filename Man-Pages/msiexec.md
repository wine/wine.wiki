---
title: msiexec - Wine MSI Installer
---

## SYNOPSIS

**msiexec**
_command_
{_required parameter_}
[_optional parameter_]...

## DESCRIPTION

**msiexec**
is the Wine MSI installer, which is command line
compatible with its Microsoft Windows counterpart.

## INSTALL OPTIONS


* **/i&nbsp;**{_package_|_productcode_}&nbsp;[_property_=_foobar_]<br>
  Install {package|productcode} with property=foobar.
* **/a&nbsp;**{_package_|_productcode_}&nbsp;[_property_=_foobar_]<br>
  Install {package|productcode} in administrator (network) mode.
* **/x&nbsp;**{_package_|_productcode_}&nbsp;[_property_=_foobar_]<br>
  Uninstall {package|productcode} with property=foobar.
* **/uninstall&nbsp;**{_package_|_productcode_}&nbsp;[_property_=_foobar_]<br>
  Same as **/x**.

## REPAIR OPTIONS


* **/f**&nbsp;[**p**|**o**|**e**|**d**|**c**|**a**|**u**|**m**|**s**|**v**]&nbsp;{_package_|_productcode_}<br>
  Repair an installation. Default options are \'omus\'
* **p**<br>
  Reinstall the file if it is missing.
* **o**<br>
  Reinstall the file if it is missing or if any older version is installed.
* **e**<br>
  Reinstall the file if it is missing, or if the installed version is equal or older.
* **d**<br>
  Reinstall the file if it is missing or a different version is installed.
* **c**<br>
  Reinstall the file if it is missing or the checksum does not match.
* **a**<br>
  Reinstall all files.
* **u**<br>
  Rewrite all required user registry entries.
* **m**<br>
  Rewrite all required machine registry entries.
* **s**<br>
  Overwrite any conflicting shortcuts.
* **v**<br>
  Recache the local installation package from the source installation package.

## PATCHING


* **/p&nbsp;**{_patch_}&nbsp;[_property_=_foobar_]<br>
  Apply _patch_. This should not be used with any of the above options.

## UI CONTROL


* **/q**[**n**|**b**|**r**|**f**]<br>
  These options allow changing the behavior of the UI when installing MSI packages.
* **/q**<br>
  Show no UI.
* **/qn**<br>
  Same as **/q**.
* **/qb**<br>
  Show a basic UI.
* **/qr**<br>
  Shows a reduced user UI.
* **/qf**<br>
  Shows a full UI.

## LOGGING


* **/l**[**\***][**i**|**w**|**e**|**a**|**r**|**u**|**c**|**m**|**o**|**p**|**v**][**+**|**!**]&nbsp;{_logfile_}<br>
  Enable logging to _logfile_. Defaults are \'iwearmo\'.
* **\***<br>
  Enable all logging options except \'v\' and \'x\'.
* **i**<br>
  Log status messages.
* **w**<br>
  Log nonfatal warnings.
* **e**<br>
  Log all error messages.
* **a**<br>
  Log start of actions.
* **r**<br>
  Log action specific records.
* **u**<br>
  Log user requests.
* **c**<br>
  Log initial UI parameters.
* **m**<br>
  Log out of memory errors.
* **o**<br>
  Log out of diskspace messages.
* **p** <br>
  Log terminal properties.
* **v** <br>
  Verbose logging.
* **x** <br>
  Log extra debugging messages.
* **+** <br>
  Append logging to existing file.
* **!** <br>
  Flush each line to log.

## OTHER OPTIONS


* **/h**<br>
  Show help.
* **/j**[**u**|**m**] {_package_|_productcode_} [**/t** _transform_] [**/g** _languageid_]<br>
  Advertise _package_ optionally with **/t** _transform_ and **/g** _languageid_.
* **/y**<br>
  Register MSI service.
* **/z**<br>
  Unregister MSI service.
* **/?**<br>
  Same as **/h**.


## BUGS

Bugs can be reported on the
[**Wine bug tracker**](https://bugs.winehq.org).

## AVAILABILITY

**msiexec**
is part of the Wine distribution, which is available through WineHQ,
the
[**Wine development headquarters**](https://www.winehq.org/).

## SEE ALSO

[**wine**](./wine),<br>
[**Wine documentation and support**](https://www.winehq.org/help).
