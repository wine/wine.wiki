---
title: regedit - Wine registry editor
---

## SYNOPSIS

**regedit**
[_text&nbsp;file_]

## DESCRIPTION

**regedit**
is the Wine registry editor, designed to be compatible with its Microsoft Windows counterpart.
If called without any options, it will start the full GUI editor.

The switches are case-insensitive and can be prefixed either by '-' or '/'.

## OPTIONS


* **-E** _file_ [_regpath_]<br>
  Exports the content of the specified registry key to the specified _file_. It exports
  the whole registry if no key is specified.
* **-D**&nbsp;_regpath_<br>
  Deletes the specified registry key.
* **-S**<br>
  Run regedit silently (ignored, CLI mode is always silent). This exists for compatibility with Windows regedit.
* **-V**<br>
  Run regedit in advanced mode (ignored). This exists for compatibility with Windows regedit.
* **-L**&nbsp;_location_<br>
  Specifies the location of the system.dat registry file (ignored). This exists for compatibility with Windows regedit.
* **-R**&nbsp;_location_<br>
  Specifies the location of the user.dat registry file (ignored). This exists for compatibility with Windows regedit.
* **-?**<br>
  Prints a help message. Any other options are ignored.
* **-C**&nbsp;_file.reg_<br>
  Create registry from _file.reg_ (unimplemented).

## BUGS

Bugs can be reported on the
[**Wine bug tracker**](https://bugs.winehq.org).

## AVAILABILITY

**regedit**
is part of the Wine distribution, which is available through WineHQ,
the
[**Wine development headquarters**](https://www.winehq.org/).

## SEE ALSO

[**wine**](./wine),<br>
[**Wine documentation and support**](https://www.winehq.org/help).
