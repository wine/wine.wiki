---
title: wine - run Windows programs on Unix
---

<small>
&nbsp;[:flag_fr: Français](fr/Man-Pages/wine)
&nbsp;[:flag_de: Deutsch](de/Man-Pages/wine)
&nbsp;[:flag_pl: Polski](pl/Man-Pages/wine)
</small>

-----


## SYNOPSIS

**wine**
_program_ [_arguments_]<br>
**wine --help**<br>
**wine --version**

For instructions on passing arguments to Windows programs, please see the
**PROGRAM/ARGUMENTS**
section of the man page.

## DESCRIPTION

**wine**
loads and runs the given program, which can be a DOS, Windows
3.x, Win32 or Win64 executable (on 64-bit systems).

For debugging wine, use
**winedbg**
instead.

For running CUI executables (Windows console programs), use
**wineconsole**
instead of
**wine**.
This will display the output in a separate window. Not using
**wineconsole**
for CUI programs will only provide very limited console support, and your
program might not function properly.

When invoked with
**--help**
or
**--version**
as the only argument,
**wine**
will simply print a small help message or its version respectively and exit.

## PROGRAM/ARGUMENTS

The program name may be specified in DOS format
(_C:\\\\WINDOWS\\\\SOL.EXE_)
or in Unix format
(_/msdos/windows/sol.exe_).
You may pass arguments to the program being executed by adding them to the
end of the command line invoking
**wine**
(such as: _wine notepad C:\\\\TEMP\\\\README.TXT_).
Note that you need to '\\' escape special characters (and spaces) when invoking Wine via
a shell, e.g.

wine C:\\\\Program\\ Files\\\\MyPrg\\\\test.exe

It can also be one of the Windows executables shipped with Wine, in
which case specifying the full path is not mandatory, e.g. _wine_
explorer or _wine notepad_.


## ENVIRONMENT

**wine**
makes the environment variables of the shell from which it
is started accessible to the Windows/DOS processes started. So use the
appropriate syntax for your shell to enter environment variables you need.

* **WINEPREFIX**<br>
  If set, the contents of this variable is taken as the name of the directory where
  Wine stores its data (the default is
  _\$HOME/.wine_).
  This directory is also used to identify the socket which is used to
  communicate with the
  **wineserver**.
  All
  **wine**
  processes using the same
  **wineserver**
  (i.e.: same user) share certain things like registry, shared memory,
  and config file.
  By setting
  **WINEPREFIX**
  to different values for different
  **wine**
  processes, it is possible to run a number of truly independent
  **wine**
  processes.
* **WINESERVER**<br>
  Specifies the path and name of the
  **wineserver**
  binary. If not set, Wine will look for a file named "wineserver" in
  the path and in a few other likely locations.
* **WINEDEBUG**<br>
  Turns debugging messages on or off. The syntax of the variable is
  of the form
  [_class_][**+**|**-**]_channel_[,[_class2_][**+**|**-**]_channel2_]

  _class_
  is optional and can be one of the following:
  **err**,
  **warn**,
  **fixme**,
  or
  **trace**.
  If
  _class_
  is not specified, all debugging messages for the specified
  channel are turned on.  Each channel will print messages about a particular
  component of Wine.
  The following character can be either **+** or **-** to switch the specified
  channel on or off respectively.  If there is no
  _class_
  part before it, a leading **+** can be omitted. Note that spaces are not
  allowed anywhere in the string.

  Examples:
    * WINEDEBUG=warn+all<br>
  will turn on all warning messages (recommended for debugging).<br>
    * WINEDEBUG=warn+dll,+heap<br>
  will turn on DLL warning messages and all heap messages.  <br>
    * WINEDEBUG=fixme-all,warn+cursor,+relay<br>
  will turn off all FIXME messages, turn on cursor warning messages, and turn
  on all relay messages (API calls).<br>
    * WINEDEBUG=relay<br>
  will turn on all relay messages. For more control on including or excluding
  functions and dlls from the relay trace, look into the
  **HKEY_CURRENT_USER\\\\Software\\\\Wine\\\\Debug**
  registry key.

  For more information on debugging messages, see the
  _Running Wine_
  chapter of the Wine User Guide.

* **WINEDLLPATH**<br>
  Specifies the path(s) in which to search for builtin dlls and Winelib
  applications. This is a list of directories separated by ":". In
  addition to any directory specified in
  **WINEDLLPATH**,
  Wine will also look in the installation directory.
* **WINEDLLOVERRIDES**<br>
  Defines the override type and load order of dlls used in the loading
  process for any dll. There are currently two types of libraries that can be loaded
  into a process address space: native windows dlls
  (_native_) and Wine internal dlls (_builtin_).
  The type may be abbreviated with the first letter of the type
  (_n_ or _b_).
  The library may also be disabled (''). Each sequence of orders must be separated by commas.

  Each dll may have its own specific load order. The load order
  determines which version of the dll is attempted to be loaded into the
  address space. If the first fails, then the next is tried and so
  on. Multiple libraries with the same load order can be separated with
  commas. It is also possible to use specify different loadorders for
  different libraries by separating the entries by ";".

  The load order for a 16-bit dll is always defined by the load order of
  the 32-bit dll that contains it (which can be identified by looking at
  the symbolic link of the 16-bit .dll.so file). For instance if
  _ole32.dll_ is configured as builtin, _storage.dll_ will be loaded as
  builtin too, since the 32-bit _ole32.dll_ contains the 16-bit
  _storage.dll_.

  Examples:
    * WINEDLLOVERRIDES="comdlg32,shell32=n,b"<br><br>
  Try to load comdlg32 and shell32 as native windows dll first and try
  the builtin version if the native load fails.
    * WINEDLLOVERRIDES="comdlg32,shell32=n;c:\\\\foo\\\\bar\\\\baz=b"<br><br>
  Try to load the libraries comdlg32 and shell32 as native windows dlls. Furthermore, if
  an application request to load _c:\\foo\\bar\\baz.dll_ load the builtin library _baz_.
    * WINEDLLOVERRIDES="comdlg32=b,n;shell32=b;comctl32=n;oleaut32="<br><br>
  Try to load comdlg32 as builtin first and try the native version if
  the builtin load fails; load shell32 always as builtin and comctl32
  always as native; oleaut32 will be disabled.

* **WINEPATH**<br>
  Specifies additional path(s) to be prepended to the default Windows
  **PATH**
  environment variable. This is a list of Windows-style directories
  separated by ";".

  For a permanent alternative, edit (create if needed) the
  **PATH**
  value under the
  **HKEY_CURRENT_USER\\\\Environment**
  registry key.

* **WINEARCH**<br>
  Specifies the Windows architecture to support. It can be set to
  **win32**
  (support only 32-bit applications), to
  **win64**
  (support both 64-bit applications and 32-bit ones), or to
  **wow64**
  (support 64-bit applications and 32-bit ones, using a 64-bit host
  process in all cases).<br>
  The architecture supported by a given Wine prefix is set at prefix
  creation time and cannot be changed afterwards. When running with an
  existing prefix, Wine will refuse to start if
  **WINEARCH**
  doesn't match the prefix architecture. It is possible however to
  switch freely between **win64** and **wow64** with an existing
  64-bit prefix.
* **WINE_D3D_CONFIG**<br>
  Specifies Direct3D configuration options. It can be used instead of
  modifying the
  **HKEY_CURRENT_USER\\\\Software\\\\Wine\\\\Direct3D**
  registry key. The value is a comma- or semicolon-separated list
  of key-value pairs.
  For example:
    * WINE_D3D_CONFIG="renderer=vulkan;VideoPciVendorID=0xc0de"<br>

  If an individual setting is specified in both
  the environment variable and the registry, the former takes precedence.

* **DISPLAY**<br>
  Specifies the X11 display to use.
* OSS sound driver configuration variables:<br>
* **AUDIODEV**<br>
  Set the device for audio input / output. Default
  _/dev/dsp_.
* **MIXERDEV**<br>
  Set the device for mixer controls. Default
  _/dev/mixer_.
* **MIDIDEV**<br>
  Set the MIDI (sequencer) device. Default
  _/dev/sequencer_.

## FILES


* _wine_<br>
  The Wine program loader.
* _wineconsole_<br>
  The Wine program loader for CUI (console) applications.
* _wineserver_<br>
  The Wine server
* _winedbg_<br>
  The Wine debugger
* _\$WINEPREFIX/dosdevices_<br>
  Directory containing the DOS device mappings. Each file in that
  directory is a symlink to the Unix device file implementing a given
  device. For instance, if COM1 is mapped to _/dev/ttyS0_ you'd have a
  symlink of the form _\$WINEPREFIX/dosdevices/com1_ -\> _/dev/ttyS0_.<br>
  DOS drives are also specified with symlinks; for instance if drive D:
  corresponds to the CDROM mounted at _/mnt/cdrom_, you'd have a symlink
  _\$WINEPREFIX/dosdevices/d:_ -\> _/mnt/cdrom_. The Unix device corresponding
  to a DOS drive can be specified the same way, except with '::' instead
  of ':'. So for the previous example, if the CDROM device is mounted
  from _/dev/hdc_, the corresponding symlink would be
  _\$WINEPREFIX/dosdevices/d::_ -\> _/dev/hdc_.

## AUTHORS

Wine is available thanks to the work of many developers. For a listing
of the authors, please see the file
_AUTHORS_
in the top-level directory of the source distribution.

## COPYRIGHT

Wine can be distributed under the terms of the LGPL license. A copy of the
license is in the file
_COPYING.LIB_
in the top-level directory of the source distribution.

## BUGS


A status report on many applications is available from the
[**Wine Application Database**](https://appdb.winehq.org).
Please add entries to this list for applications you currently run, if
necessary.

Bugs can be reported on the
[**Wine bug tracker**](https://bugs.winehq.org).

## AVAILABILITY

The most recent public version of
**wine**
is available through WineHQ, the
[**Wine development headquarters**](https://www.winehq.org/).

## SEE ALSO

[**wineserver**](./wineserver),
[**winedbg**](./winedbg),<br>
[**Wine documentation and support**](https://www.winehq.org/help).
