---
title: regsvr32 - Wine DLL Registration Server
---

## SYNOPSIS

**regsvr32**
[**/u**] [**/s**] [**/n**] [**/i**[**:**_cmdline_]] _dllname_

## DESCRIPTION

**regsvr32**
is the Wine dll registration server, designed to be compatible with its Microsoft Windows counterpart.
By default, it will register the given dll.

## COMMANDS


* **/u**<br>
  Unregister the specified dll.
* **/s**<br>
  Run regsvr32 silently (will not show any GUI dialogs).
* **/i**<br>
  Call DllInstall passing it an optional _cmdline_. When used with **/u** calls DllUninstall.
* **/n**<br>
  Do not call DllRegisterServer; this option must be used with **/i**.

## BUGS

Bugs can be reported on the
[**Wine bug tracker**](https://bugs.winehq.org).

## AVAILABILITY

**regsvr32**
is part of the Wine distribution, which is available through WineHQ,
the
[**Wine development headquarters**](https://www.winehq.org/).

## SEE ALSO

[**wine**](./wine),<br>
[**Wine documentation and support**](https://www.winehq.org/help).
