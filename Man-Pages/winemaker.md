---
title: winemaker - generate a build infrastructure for compiling Windows programs on Unix
---

<small>
&nbsp;[:flag_fr: Français](fr/Man-Pages/winemaker)
&nbsp;[:flag_de: Deutsch](de/Man-Pages/winemaker)
</small>

-----


## SYNOPSIS

**winemaker**
[
**--nobanner** ] [ **--backup** | **--nobackup** ] [ **--nosource-fix**
]<br>
  [
**--lower-none** | **--lower-all** | **--lower-uppercase**
]<br>
  [
**--lower-include** | **--nolower-include** ]&nbsp;[ **--mfc** | **--nomfc**
]<br>
  [
**--guiexe** | **--windows** | **--cuiexe** | **--console** | **--dll** | **--lib**
]<br>
  [
**-D**_macro_[=_defn_] ] [_&nbsp;_**-I**_dir_ ]&nbsp;[ **-P**_dir_ ] [ **-i**_dll_ ] [ **-L**_dir_ ] [ **-l**_library_
]<br>
  [
**--nodlls** ] [ **--nomsvcrt** ] [ **--interactive** ] [ **--single-target** _name_
]<br>
  [
**--generated-files** ] [ **--nogenerated-files** ]<br>
  [
**--wine32** ]<br>
  _work_directory_ | _project_file_ | _workspace_file_


## DESCRIPTION


**winemaker**
is a perl script designed to help you bootstrap the
process of converting your Windows sources to Winelib programs.

In order to do this **winemaker** can perform the following operations:

- rename your source files and directories to lowercase in the event they
got all uppercased during the transfer.

- perform DOS to Unix (CRLF to LF) conversions.

- scan the include statements and resource file references to replace the
backslashes with forward slashes.

- during the above step **winemaker** will also perform a case insensitive search
of the referenced file in the include path and rewrite the include statement
with the right case if necessary.

- **winemaker** will also check other more exotic issues like _#pragma pack_
usage, use of _afxres.h_ in non MFC projects, and more. Whenever it
encounters something out of the ordinary, it will warn you about it.

- **winemaker** can also scan a complete directory tree at once, guess what are
the executables and libraries you are trying to build, match them with
source files, and generate the corresponding _Makefile_.

- finally **winemaker** will generate a global _Makefile_ for normal use.

- **winemaker** knows about MFC-based project and will generate customized files.

- **winemaker** can read existing project files. It supports dsp, dsw, vcproj and sln files.


## OPTIONS


* **--nobanner**<br>
  Disable the printing of the banner.
* **--backup**<br>
  Perform a backup of all the modified source files. This is the default.
* **--nobackup**<br>
  Do not backup modified source files.
* **--nosource-fix**<br>
  Do no try to fix the source files (e.g. DOS to Unix
  conversion). This prevents complaints if the files are readonly.
* **--lower-all**<br>
  Rename all files and directories to lowercase.
* **--lower-uppercase**<br>
  Only rename files and directories that have an all uppercase name.
  So _HELLO.C_ would be renamed but not _World.c_.
* **--lower-none**<br>
  Do not rename files and directories to lower case. Note
  that this does not prevent the renaming of a file if its extension cannot
  be handled as is, e.g. ".Cxx". This is the default.
* **--lower-include**<br>
  When the file corresponding to an include statement (or other form of file reference
  for resource files) cannot be found, convert that filename to lowercase. This is the default.
* **--nolower-include**<br>
  Do not modify the include statement if the referenced file cannot be found.
* **--guiexe** | **--windows**<br>
  Assume a graphical application when an executable target or a target of
  unknown type is found. This is the default.
* **--cuiexe** | **--console**<br>
  Assume a console application when an executable target or a target of
  unknown type is found.
* **--dll**<br>
  Assume a dll when a target of unknown type is found, i.e. when **winemaker** is unable to
  determine whether it is an executable, a dll, or a static library,
* **--lib**<br>
  Assume a static library when a target of unknown type is found, i.e. when **winemaker** is
  unable to determine whether it is an executable, a dll, or a static library,
* **--mfc**<br>
  Specify that the targets are MFC based. In such a case **winemaker** adapts
  the include and library paths accordingly, and links the target with the
  MFC library.
* **--nomfc**<br>
  Specify that targets are not MFC-based. This option disables use of MFC libraries
  even if **winemaker** encounters files _stdafx.cpp_ or _stdafx.h_ that would cause it
  to enable MFC automatically if neither **--nomfc** nor **--mfc** was specified.
* **-D**_macro_[**=**_defn_]<br>
  Add the specified macro definition to the global list of macro definitions.
* **-I**_dir_<br>
  Append the specified directory to the global include path.
* **-P**_dir_<br>
  Append the specified directory to the global dll path.
* **-i**_dll_<br>
  Add the Winelib library to the global list of Winelib libraries to import.
* **-L**_dir_<br>
  Append the specified directory to the global library path.
* **-l**_library_<br>
  Add the specified library to the global list of libraries to link with.
* **--nodlls**<br>
  Do not use the standard set of Winelib libraries for imports.
  That is, any DLL your code uses must be explicitly passed with **-i** options.
  The standard set of libraries is: _odbc32.dll_, _odbccp32.dll_, _ole32.dll_,
  _oleaut32.dll_ and _winspool.drv_.
* **--nomsvcrt**<br>
  Set some options to tell **winegcc** not to compile against msvcrt.
  Use this option if you have cpp-files that include _\<string\>_.
* **--interactive**<br>
  Use interactive mode. In this mode **winemaker** will ask you to
  confirm the list of targets for each directory, and then to provide directory and
  target specific options.
* **--single-target** _name_<br>
  Specify that there is only one target, called _name_.
* **--generated-files**<br>
  Generate the _Makefile_. This is the default.
* **--nogenerated-files**<br>
  Do not generate the _Makefile_.
* **--wine32**<br>
  Generate a 32-bit target. This is useful on wow64 systems.
  Without that option the default architecture is used.


## EXAMPLES


Here is a typical **winemaker** use:

\$ winemaker --lower-uppercase -DSTRICT .

The above tells **winemaker** to scan the current directory and its
subdirectories for source files. Whenever if finds a file or directory which
name is all uppercase, it should rename it to lowercase. It should then fix
all these source files for compilation with Winelib and generate _Makefile_s.
The **-DSTRICT** specifies that the **STRICT** macro must be set when compiling
these sources. Finally a _Makefile_ will be created.

The next step would be:

\$ make

If at this point you get compilation errors (which is quite likely for a
reasonably sized project) then you should consult the Winelib User Guide to
find tips on how to resolve them.

For an MFC-based project you would have to run the following commands instead:

\$ winemaker --lower-uppercase --mfc .<br>
\$ make

For an existing project-file you would have to run the following commands:

\$ winemaker myproject.dsp<br>
\$ make



## TODO / BUGS

In some cases you will have to edit the _Makefile_ or source files manually.

Assuming that the windows executable/library is available, we could
use **winedump** to determine what kind of executable it is (graphical
or console), which libraries it is linked with, and which functions it
exports (for libraries). We could then restore all these settings for the
corresponding Winelib target.

Furthermore **winemaker** is not very good at finding the library containing the
executable: it must either be in the current directory or in the
**LD_LIBRARY_PATH**.

**winemaker** does not support message files and the message compiler yet.

Bugs can be reported on the
[**Wine bug tracker**](https://bugs.winehq.org).

## AUTHORS

François Gouget for CodeWeavers<br>
Dimitrie O. Paun<br>
André Hentschel

## AVAILABILITY

**winemaker**
is part of the Wine distribution, which is available through WineHQ,
the
[**Wine development headquarters**](https://www.winehq.org/).

## SEE ALSO

[**wine**](./wine),<br>
[**Wine documentation and support**](https://www.winehq.org/help).
