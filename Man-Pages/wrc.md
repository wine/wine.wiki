---
title: wrc - Wine Resource Compiler
---

## SYNOPSIS

**wrc**
[_options_]&nbsp;[_inputfile_...]

## DESCRIPTION

**wrc**
compiles resources from _inputfile_
into win16 and win32 compatible binary format.

The source file is preprocessed with a builtin ANSI-C compatible
preprocessor before the resources are compiled. See **PREPROCESSOR**
below.

**wrc**
takes a series of _inputfile_ as argument. The resources are read from
standard input if no inputfile is given. If the output file is not
specified with **-o**, then **wrc** will write the output to
_inputfile.res_ with _.rc_ stripped, or to _wrc.tab.res_ if
no inputfile was given.

## OPTIONS


* **-D**, **--define**=_id_[**=**_val_]<br>
  Define preprocessor identifier _id_ to (optionally) value _val_.
  See also
  **PREPROCESSOR**
  below.
* **--debug**=_nn_<br>
  Set debug level to _nn_. The value is a bitmask consisting of
  1=verbose, 2=dump internals, 4=resource parser trace, 8=preprocessor
  messages, 16=preprocessor scanner and 32=preprocessor parser trace.
* **-E**<br>
  Preprocess only. The output is written to standard output if no
  outputfile was selected. The output is compatible with what gcc would
  generate.
* **-h**, **--help**<br>
  Prints a summary message and exits.
* **-i**, **--input**=_file_<br>
  The name of the input file. If this option is not used, then **wrc**
  will use the first non-option argument as the input file name. If there
  are no non-option arguments, then **wrc** will read from standard input.
* **-I**, **--include-dir**=_path_<br>
  Add _path_ to the list of directories to search for includes. It
  is allowed to specify **-I** multiple times. Include files are
  searched in the order in which the **-I** options were specified.<br>
  The search is compatible with gcc, in which '\<\>' quoted filenames are
  searched exclusively via the **-I** set path, whereas the '""' quoted
  filenames are first tried to be opened in the current directory. Also
  resource statements with file references are located in the same way.
* **-J**, **--input-format**=_format_<br>
  Sets the input format. Valid options are 'rc' or 'rc16'. Setting the
  input to 'rc16' disables the recognition of win32 keywords.
* **-l**, **--language**=_lang_<br>
  Set default language to _lang_. Default is the neutral language 0
  (i.e. "LANGUAGE 0, 0").
* **-m16, -m32, -m64**<br>
  Generate resources for 16-bit, 32-bit or 64-bit platforms respectively.
  The only difference between 32-bit and 64-bit is whether
  the \_WIN64 preprocessor symbol is defined.
* **--nls-dir=**_dir_<br>
  Specify the directory to search for the NLS files containing the
  codepage mapping tables.
* **--nostdinc**<br>
  Do not search the standard include path, look for include files only
  in the directories explicitly specified with the **-I** option.
* **--no-use-temp-file**<br>
  Ignored for compatibility with _windres_.
* **-o**, **-fo**, **--output**=_file_<br>
  Write output to _file_. Default is **inputfile.res**
  with **.rc** stripped or **wrc.tab.res** if input is read
  from standard input.
* **-O**, **--output-format**=_format_<br>
  Sets the output format. The supported formats are **po**, **pot**,
  **res**, and **res16**.  If this option is not specified, the
  format defaults to **res**.<br>
  In **po** mode, if an output file name is specified it must match a
  known language name, like **en_US.po**; only resources for the
  specified language are output. If no output file name is specified, a
  separate _.po_ file is created for every language encountered in the
  input.
* **--pedantic**<br>
  Enable pedantic warnings. Notably redefinition of #define statements can
  be discovered with this option.
* **--po-dir=**_dir_<br>
  Enable the generation of resource translations based on mo files
  loaded from the specified directory. That directory must follow the
  gettext convention, in particular it must contain one _.mo_ file for
  each language, and a LINGUAS file listing the available languages.
* **-r**<br>
  Ignored for compatibility with _rc_.
* **--preprocessor**=_program_<br>
  This option may be used to specify the preprocessor to use, including any
  leading arguments. If not specified, **wrc** uses its builtin processor.
  To disable preprocessing, use **--preprocessor=cat**.
* **--sysroot=**_dir_<br>
  Prefix the standard include paths with _dir_.
* **--utf8**, **-u**<br>
  Set the default codepage of the input file to UTF‑8.
* **-U**, **--undefine**=_id_<br>
  Undefine preprocessor identifier _id_.  Please note that only macros
  defined up to this point are undefined by this command. However, these
  include the special macros defined automatically by _wrc_.
  See also
  **PREPROCESSOR**
  below.
* **--use-temp-file**<br>
  Ignored for compatibility with _windres_.
* **-v**, **--verbose**<br>
  Turns on verbose mode (equivalent to **-d 1**).
* **--version**<br>
  Print version and exit.

## PREPROCESSOR

The preprocessor is ANSI-C compatible with some of the extensions of
the gcc preprocessor.

The preprocessor recognizes these directives: #include, #define (both
simple and macro), #undef, #if, #ifdef, #ifndef, #elif, #else, #endif,
#error, #warning, #line, # (both null- and line-directive), #pragma
(ignored), #ident (ignored).

The preprocessor sets by default several defines:<br>
- RC_INVOKED      set to 1<br>
- \_\_WRC_\_         Major version of wrc<br>
- \_\_WRC_MINOR_\_   Minor version of wrc<br>
- \_\_WRC_PATCHLEVEL_\_   Patch level

Win32 compilation mode also sets \_WIN32 to 1.

Special macros \_\_FILE_\_, \_\_LINE_\_, \_\_TIME_\_ and \_\_DATE_\_ are also
recognized and expand to their respective equivalent.

## LANGUAGE SUPPORT

Language, version and characteristics can be bound to all resource types that
have inline data, such as RCDATA. This is an extension to Microsoft's resource
compiler, which lacks this support completely. Only VERSIONINFO cannot have
version and characteristics attached, but languages are propagated properly if
you declare it correctly before the VERSIONINFO resource starts.

Example:

    1 RCDATA DISCARDABLE
    LANGUAGE 1, 0
    VERSION 312
    CHARACTERISTICS 876
    {
    	1, 2, 3, 4, 5, "and whatever more data you want"
    	'00 01 02 03 04 05 06 07 08'
    }

## AUTHORS

**wrc**
was written by Bertho A. Stultiens and is a nearly complete rewrite of
the first wine resource compiler (1994) by Martin von Loewis.
Additional resource types were contributed by Ulrich Czekalla and Albert
den Haan. Many cleanups by Dimitrie O. Paun in 2002-2003.
Bugfixes have been contributed by many Wine developers.

## BUGS

- The preprocessor recognizes variable argument macros, but does not
expand them correctly.<br>
- Error reporting should be more precise, as currently the column and
line number reported are those of the next token. <br>
- Default memory options should differ between win16 and win32.

There is no support for:<br>
- RT_DLGINCLUDE, RT_VXD, RT_PLUGPLAY and RT_HTML (unknown format)<br>
- PUSHBOX control is unsupported due to lack of original functionality.

Fonts are parsed and generated, but there is no support for the
generation of the FONTDIR yet. The user must supply the FONTDIR
resource in the source to match the FONT resources.

Bugs can be reported on the
[**Wine bug tracker**](https://bugs.winehq.org).

## AVAILABILITY

**wrc**
is part of the Wine distribution, which is available through WineHQ,
the
[**Wine development headquarters**](https://www.winehq.org/).

## SEE ALSO

[**wine**](./wine),<br>
[**Wine documentation and support**](https://www.winehq.org/help).
