---
title: winepath - convert Unix paths to/from Win32 paths
---

## SYNOPSIS

**winepath**
_option_ {_path_}

## DESCRIPTION

**winepath**
is a tool to convert a Unix path to/from a Win32 (short/long) path
compatible with its Microsoft Windows counterpart.

If more than one option is given then the input paths are output in
all formats specified, in the order long, short, Unix, Windows.
If no option is given the default output is Unix format.

## OPTIONS


* **-u**,**&nbsp;--unix**<br>
  converts a Windows path to a Unix path.
* **-w**,**&nbsp;--windows**<br>
  converts a Unix path to a long Windows path.
* **-l**,**&nbsp;--long**<br>
  converts the short Windows path of an existing file or directory to the long
  format.
* **-s**,**&nbsp;--short**<br>
  converts the long Windows path of an existing file or directory to the short
  format.
* **‑0**<br>
  separate output with \\0 character, instead of a newline.
* **-h**,**&nbsp;--help**<br>
  shows winepath help message and exit.

## BUGS

Bugs can be reported on the
[**Wine bug tracker**](https://bugs.winehq.org).

## AVAILABILITY

**winepath**
is part of the Wine distribution, which is available through WineHQ,
the
[**Wine development headquarters**](https://www.winehq.org/).

## SEE ALSO

[**wine**](./wine),<br>
[**Wine documentation and support**](https://www.winehq.org/help).
