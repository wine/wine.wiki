---
title: widl - Wine Interface Definition Language (IDL) compiler
---

## SYNOPSIS

**widl**
[_options_] _IDL_file_<br>
**widl**
[_options_] **--dlldata-only** _name1_ [_name2_...]

## DESCRIPTION

When no options are used the program will generate a header file, and possibly
client and server stubs, proxy and dlldata files, a typelib, and a UUID file,
depending on the contents of the IDL file.  If any of the options **-c**,
**-h**, **-p**, **-s**, **-t**, **-u** or **--local-stubs** is given,
**widl**
will only generate the requested files, and no others.  When run with
**--dlldata-only**, widl will only generate a dlldata file, and it will
contain a list of the names passed as arguments.  Usually the way this file
is updated is that each time
**widl**
is run, it reads any existing dlldata file, and if necessary regenerates it
with the same list of names, but with the present proxy file included.

When run without any arguments,
**widl**
will print a help message.


## OPTIONS


**General options:**

* **-V**<br>
  Print version number and exit.
* **-o, --output=**_name_<br>
  Set the name of the output file. When generating multiple output
  files, this sets only the base name of the file; the respective output
  files are then named _name_.h, _name_\_p.c, etc.  If a full
  file name with extension is specified, only that file is generated.
* **-b, --target=**_cpu-manufacturer_[_-kernel_]_-os_<br>
  Set the target architecture when cross-compiling. The target
  specification is in the standard autoconf format as returned by
  **config.sub**.
* **-m32, -m64, --win32, --win64**<br>
  Force the target architecture to 32-bit or 64-bit.
* **--sysroot=**_dir_<br>
  Prefix the standard include paths with _dir_.
* **--nostdinc**<br>
  Do not search standard include paths like /usr/include and
  /usr/local/include.

**Header options:**

* **-h**<br>
  Generate header files. The default output filename is _infile_**.h**.
* **--oldnames**<br>
  Use old naming conventions.

**Type library options:**

* **-t**<br>
  Generate a type library. The default output filename is
  _infile_**.tlb**.  If the output file name ends in **.res**, a
  binary resource file containing the type library is generated instead.
* **-L** _path_<br>
  Add a directory to the library search path for imported typelibs. The
  option can be specified multiple times.
* **--oldtlb**<br>
  Generate a type library in the old format (SLTG format).

**UUID file options:**

* **-u**<br>
  Generate a UUID file. The default output filename is _infile_**\_i.c**.

**Proxy/stub generation options:**

* **--align=**_n_**, --packing=**_n_<br>
  Set the structure packing to _n_. Supported values are 2, 4, and 8.
* **-c**<br>
  Generate a client stub file. The default output filename is _infile_**\_c.c**.
* **-Os**<br>
  Generate inline stubs.
* **-Oi**<br>
  Generate old-style interpreted stubs.
* **-Oif, -Oic, -Oicf**<br>
  Generate new-style fully interpreted stubs. This is the default.
* **-p**<br>
  Generate a proxy. The default output filename is _infile_**\_p.c**.
* **--prefix-all=**_prefix_<br>
  Prefix to put on the name of both client and server stubs.
* **--prefix-client=**_prefix_<br>
  Prefix to put on the name of client stubs.
* **--prefix-server=**_prefix_<br>
  Prefix to put on the name of server stubs.
* **-s**<br>
  Generate a server stub file. The default output filename is
  _infile_**\_s.c**.


* **--winrt**<br>
  Enable Windows Runtime mode.
* **--ns_prefix**<br>
  Prefix namespaces with ABI namespace.

**Registration script options:**

* **-r**<br>
  Generate a registration script. The default output filename is
  _infile_**\_r.rgs**. If the output file name ends in **.res**, a
  binary resource file containing the script is generated instead.

**Dlldata file options:**

* **--dlldata-only** _name1_ [_name2_...]<br>
  Regenerate the dlldata file from scratch using the specified proxy
  names. The default output filename is **dlldata.c**.

**Preprocessor options:**

* **-I** _path_<br>
  Add a directory to the include search path. Multiple include
  directories are allowed.
* **-D** _id_[**=**_val_]<br>
  Define preprocessor macro _id_ with value _val_.
* **-E**<br>
  Preprocess only.
* **-N**<br>
  Do not preprocess input.

**Debug options:**

* **-W**<br>
  Enable pedantic warnings.
* **-d** _n_<br>
  Set debug level to the non negative integer _n_.  If
  prefixed with **0x**, it will be interpreted as an hexadecimal
  number.  For the meaning of values, see the **DEBUG** section.

**Miscellaneous options:**

* **-app_config**<br>
  Ignored, present for midl compatibility.
* **--acf=**_file_<br>
  Use specified application configuration file.
* **--local-stubs=**_file_<br>
  Generate empty stubs for call_as/local methods in an object interface and
  write them to _file_.


## DEBUG

Debug level _n_ is a bitmask with the following meaning:
  - 0x01 Tell which resource is parsed (verbose mode)
  - 0x02 Dump internal structures
  - 0x04 Create a parser trace (yydebug=1)
  - 0x08 Preprocessor messages
  - 0x10 Preprocessor lex messages
  - 0x20 Preprocessor yacc trace

## BUGS

Bugs can be reported on the
[**Wine bug tracker**](https://bugs.winehq.org).

## AUTHORS

**widl**
was originally written by Ove Kåven.  It has been improved by Rob Shearman,
Dan Hipschman, and others.  For a complete list, see the git commit logs.
This man page was originally written by Hannu Valtonen and then updated by
Dan Hipschman.

## AVAILABILITY

**widl**
is part of the Wine distribution, which is available through WineHQ,
the
[**Wine development headquarters**](https://www.winehq.org/).

## SEE ALSO

[**Wine documentation and support**](https://www.winehq.org/help).
