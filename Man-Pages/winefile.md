---
title: winefile - Wine File Manager
---

## SYNOPSIS

**winefile** _path_

## DESCRIPTION

**winefile**
is the Wine file manager, with a similar design to early Microsoft Windows explorer.

## USAGE

You can pass winefile either a Unix or Win32 path, for example:

- Using a Unix path:  **winefile /mnt/hda1**

- Using a Win32 path: **winefile C:\\\\windows\\\\&nbsp;**


## BUGS

Bugs can be reported on the
[**Wine bug tracker**](https://bugs.winehq.org).

## AVAILABILITY

**winefile**
is part of the Wine distribution, which is available through WineHQ,
the
[**Wine development headquarters**](https://www.winehq.org/).

## SEE ALSO

[**wine**](./wine),<br>
[**Wine documentation and support**](https://www.winehq.org/help).
