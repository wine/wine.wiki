---
title: wineboot - perform Wine initialization, startup, and shutdown tasks
---

## SYNOPSIS

**wineboot**
[_options_]

## DESCRIPTION

**wineboot**
performs the initial creation and setup of a WINEPREFIX for wine(1). It can also perform a simulated
reboot or shutdown to any applications running within the WINEPREFIX.

## OPTIONS


* **-h**,**&nbsp;--help**<br>
  Display help message.
* **-e**,**&nbsp;--end-session**<br>
  End the current session cleanly.
* **-f**,**&nbsp;--force**<br>
  Force exit for processes that don't exit cleanly
* **-i**,**&nbsp;--init**<br>
  Initialize the WINEPREFIX.
* **-k**,**&nbsp;--kill**<br>
  Kill running processes without any cleanup.
* **-r**,**&nbsp;--restart**<br>
  Restart only, don't do normal startup operations.
* **-s**,**&nbsp;--shutdown**<br>
  Shutdown only, don't reboot.
* **-u**,**&nbsp;--update**<br>
  Update the WINEPREFIX.

## BUGS

Bugs can be reported on the
[**Wine bug tracker**](https://bugs.winehq.org).

## AVAILABILITY

**wineboot**
is part of the Wine distribution, which is available through WineHQ,
the
[**Wine development headquarters**](https://www.winehq.org/).

## SEE ALSO

[**wine**](./wine),<br>
[**Wine documentation and support**](https://www.winehq.org/help).
