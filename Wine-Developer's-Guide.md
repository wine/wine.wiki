## Table of Contents

### I. Developing Wine

1.  [Building Wine](Building-Wine)
2.  [Running Wine](Wine-User's-Guide)
3.  [Debugging Wine](Wine-Developer's-Guide/Debugging-Wine)
4.  [Debug Logging](Wine-Developer's-Guide/Debug-Logging)
5.  [Other Debugging
    Techniques](Wine-Developer's-Guide/Other-Debugging-Techniques)
6.  [Coding Practice](Wine-Developer's-Guide/Coding-Practice)
7.  [Writing Conformance
    Tests](Wine-Developer's-Guide/Writing-Conformance-Tests)
8.  [Submitting Patches](Submitting-Patches)
9.  [Documenting
    Wine](Wine-Developer's-Guide/Documenting-Wine)

### II. Wine Architecture

1.  [Overview](Wine-Developer's-Guide/Architecture-Overview)
2.  [Kernel modules](Wine-Developer's-Guide/Kernel-modules)
3.  [Windowing
    system](Wine-Developer's-Guide/Windowing-system)
4.  [COM in Wine](Wine-Developer's-Guide/COM-in-Wine)
5.  [Wine and OpenGL](Wine-Developer's-Guide/Wine-and-OpenGL)
6.  [Outline of DirectDraw
    Architecture](Wine-Developer's-Guide/Outline-of-DirectDraw-Architecture)
7.  [Wine and
    Multimedia](Wine-Developer's-Guide/Wine-and-Multimedia)

### III. Miscellaneous references

1.  [Tips for Vim users](Vim-Tips)
2.  [Running Wine under Valgrind](Wine-and-Valgrind)
3.  [A list of windows messages, by
    value](Wine-Developer's-Guide/List-of-Windows-Messages)
4.  [The format of a ternary raster operation
    code](Wine-Developer's-Guide/Ternary-Raster-Ops)
5.  [Windows Graphics Programming: Win32 GDI and
    DirectDraw](https://books.google.com/books?id=-O92IIF1Bj4C) by Feng
    Yuan

## Authors

Uwe Bonnes; Jonathan Buzzard; Zoran Dzelajlija; Klaas van Gend; Francois
Gouget; Jon Griffiths; Albert den Haan; Mike Hearn; André Hentschel; Ove
Kaaven; Tony Lambregts; Marcus Meissner; Gerard Patel; Dimitrie Paun;
Michele Petrovski; Eric Pouech; Douglas Ridgway; John Sheets; Lionel
Ulmer; Ulrich Weigand; Morten Welinder; Jeremy White
