Currently, we have almost no public relations management at all,
something that should change for the better. We are also mainly
"preaching to the choir" about Wine right now since most articles about
us are on online IT sites (and most of those primarily focus on Linux,
not BSD, Solaris, or macOS).

We would like much more coverage on traditional Window-ish sites too,
and most importantly, in mainstream outlets such as magazines and
newspapers to inform the general public about our project. Anyone
interested in helping could probably approach this in one of several
ways:

- Writing articles about Wine for (small or large) blogs, websites, and
  newspapers
- Demonstrating Wine at Linux-User Group meetings, computer shows,
  conferences, and FOSS install-fests
- If you have contacts with the press, possibly arranging written or
  televised interviews and reports on Wine
- Simple word of mouth

While more experience with running, testing, or developing Wine never
hurts, anyone with a basic understanding of the project and how to use
Wine can help spread the word.
