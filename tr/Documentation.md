---
title: Belgelendirme
---

<small>
&nbsp;[:flag_gb: English](Documentation)
&nbsp;[:flag_fr: Français](fr/Documentation)
&nbsp;[:flag_de: Deutsch](de/Documentation)
&nbsp;[:flag_nl: Nederlands](nl/Documentation)
&nbsp;[:flag_pl: Polski](pl/Documentation)
&nbsp;[:flag_kr: 한국어](ko/Documentation)
</small>

-----

## Guides

- **[Wine Kullanıcı Rehberi](Wine-User's-Guide)**

  Wine, Windows uygulamaları çalıştırmak üzere nasıl kullanılır ve yapılandırılır.

- **[Winelib Kullanıcı Rehberi](Winelib-User's-Guide)**

  Windows uygulamaları Linux'a nasıl bağlanır.

- **[Wine Geliştirici Rehberi](Wine-Developer's-Guide)**

  Wine üzerinde işlemler nasıl yapılır.

- **[Wine Installation and Configuration](Wine-Installation-and-Configuration)**

  Bize bir şeylerin nasıl yapıldığı hakkında yardım etmek isteyenler için kısa bir rehbet.

## Command reference

- **[Commands](Commands)**

  Documentation about the commands available with Wine.

- **[Man Sayfaları](Man-Pages)**

  Manual pages for the Wine commands and tools.
