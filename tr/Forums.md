---
title: Önemli Posta Listeleri / Forumlar
---

<small>
&nbsp;[:flag_gb: English](Forums)
&nbsp;[:flag_fr: Français](fr/Forums)
&nbsp;[:flag_pt: Português](pt/Forums)
&nbsp;[:flag_nl: Nederlands](nl/Forums)
&nbsp;[:flag_pl: Polski](pl/Forums)
&nbsp;[:flag_kr: 한국어](ko/Forums)
&nbsp;[:flag_cn: 简体中文](zh_CN/Forums)
</small>

-----


## Forumlar

Wine için posta listeleri ve forumlar bulunuyor. Kullanıcılar için en
kullanışlı olanlar aşağıdakilerdir:

- [WineHQ Forumları](https://forum.winehq.org)
- Ubuntu kullanıcıları ayrıca [Ubuntu Forumları Wine
  Alanı](https://ubuntuforums.org/forumdisplay.php?f=313) sayfasını
  ziyaret etmek isteyebilirler.

Eğer etkin başka bir Wine forumu biliyorsanız - özellikle İngilizce
olmayan - bize bildirin ve listeye ekleyelim.

:information_source: Eğer yeni sürümlerden haberdar olmak isterseniz
[Wine Haber posta
listesi](https://list.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/)ne
abone olun.

## Tüm WineHQ Posta Listeleri

WineHQ yama göndermek, Git gönderilerini izlemek ve Wine hakkında
tartışmak için posta listelerine sahiptir.

:information_source: Listeye gönderi yapmadan önce abone olmalısınız, aksi takdirde
posta listesi yazılımı iletinize muhtemel gereksiz posta olarak
inceleyecektir.

- **<wine-announce@winehq.org>**\
    \[[Abone ol/iptal
    et](https://list.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/)\]
    \[[Arşiv](https://list.winehq.org/mailman3/hyperkitty/list/wine-announce@winehq.org/)\]\
    Sadece sürümleri ve Wine ile WineHQ haberlerini yayınlamak için
    düşük trafikli (2 ileti/ay) salt okunur bir liste
- **<wine-devel@winehq.org>**\
    \[[Abone ol/iptal
    et](https://list.winehq.org/mailman3/postorius/lists/wine-devel.winehq.org/)\]
    \[[Arşiv](https://list.winehq.org/mailman3/hyperkitty/list/wine-devel@winehq.org/)\]\
    Wine geliştirmesi, WineHQ, yamalar ve Wine geliştiricileri ile
    alakalı diğer şeylerle ilgili orta trafikli (50/gün) bir açık liste
- **<wine-commits@winehq.org>**\
    \[[Abone ol/iptal
    et](https://list.winehq.org/mailman3/postorius/lists/wine-commits.winehq.org/)\]
    \[[Arşiv](https://list.winehq.org/mailman3/hyperkitty/list/wine-commits@winehq.org/)\]\
    A medium traffic (25/day) read only list which tracks commits to the
    Git tree.
- **<wine-releases@winehq.org>**\
    \[[Abone ol/iptal
    et](https://list.winehq.org/mailman3/postorius/lists/wine-releases.winehq.org/)\]
    \[[Arşiv](https://list.winehq.org/mailman3/hyperkitty/list/wine-releases@winehq.org/)\]\
    Her resmi Wine sürümünde büyük diff dosyasını almak için salt okunur
    düşük trafikli (2/ay) bir liste
- **<wine-bugs@winehq.org>**\
    \[[Abone ol/iptal
    et](https://list.winehq.org/mailman3/postorius/lists/wine-bugs.winehq.org/)\]
    \[[Arşiv](https://list.winehq.org/mailman3/hyperkitty/list/wine-bugs@winehq.org/)\]\
    [Hata İzleme Veritabanı](https://bugs.winehq.org/) üzerinde etkinliği
    takip etmek için kullanılan yüksek trafikli (100/gün) salt okunur
    bir liste.
