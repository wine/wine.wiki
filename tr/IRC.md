---
title: Canlı Sohbet Desteği
---

<small>
&nbsp;[:flag_gb: English](IRC)
&nbsp;[:flag_fr: Français](fr/IRC)
&nbsp;[:flag_de: Deutsch](de/IRC)
&nbsp;[:flag_es: Español](es/IRC)
&nbsp;[:flag_pt: Português](pt/IRC)
&nbsp;[:flag_nl: Nederlands](nl/IRC)
&nbsp;[:flag_pl: Polski](pl/IRC)
&nbsp;[:flag_kr: 한국어](ko/IRC)
&nbsp;[:flag_cn: 简体中文](zh_CN/IRC)
</small>

-----


[Libera.​Chat](https://libera.chat/) Wine IRC kanalına ev sahipliği
yapar. [HexChat](https://hexchat.github.io/) gibi IRC programları
kullanarak sohbet odasına erişebilirsiniz. Aşağıda listelenen ayarları
kullanın.

> **Sunucu:** irc.libera.chat\
> **Port:** 6697\
> **Kanal:** #winehq

Eğer Firefox veya IRC url\'ler destekleyen herhangi bir başka tarayıcı
kullanıyorsanız, [#winehq](irc://irc.libera.chat/winehq) üzerine tıklayarak
sohbete katılabilirsiniz.
