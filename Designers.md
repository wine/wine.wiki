## Design Tasks for Wine

Many aspects of the Wine project require skills other than writing or
programming. This may include drawing icons or graphics for use in Wine,
maintaining the appearance of the website, and for those comfortable
with web programming, better integrating the parts of the website.

The following features and ideas have come up specifically:

- [Create Icons](Create-Icons) for Windows filetypes and
  applications.
- [Create Fonts](Create-Fonts) to stand in for the fonts used
  by Windows, particularly TrueType fonts.
- [Web Design Tasks](Web-Design-Tasks) has a list of proposed
  improvements to WineHQ and the sub-sites.
