There are a lot of Wine users and this page attempts to estimate how
many there are. It isn't just an academic question because who our users
are should shape our priorities. How many are gamers? How many use Wine
for a single application? How many care more about improving performance
on apps that work already, and how many want more apps to work?

Now that we directly package Wine [Downloads](Download) for
several popular distributions, we can count statistics ourselves (not
sure if we do though), but even then, there are many ways to estimate
Wine's popularity.

## Popcons

Several distributions offer popularity contest (popcon) packages, which
report package install statistics for users who have checked the box to
"send anonymous package usage data." Besides the raw figures for users
with Wine installed, proportions can be estimated by taking a ratio of
Wine installations to the installations of a core package for the
distro. However, we suspect that popcons miscount actual Wine users for
various reasons, such as self-selection bias (are popcon users more or
less likely to also use Wine?)

Wine popcon pages for various distributions:

- [Ubuntu popcon results (sorted by install
  count)](https://popcon.ubuntu.com/by_inst)
- [Debian popcon results for Wine-based
  packages](https://qa.debian.org/popcon.php?package=wine)

## Surveys

Maybe we could setup our own survey through
[SurveyMonkey](https://www.surveymonkey.com/) to get some new data?

- At the end of 2007, DesktopLinux.com did a survey about the Linux
  market. In their results, 31.5% of 38,500 reported using Wine to run
  Windows applications.

## Every ~~Linux~~ ~~Unix~~ Computer User is a Potential Wine User

Work backwards from estimates of global computer and OS users (gstatcounter, Garnett, et. al).

## Website Traffic as a Proxy Statistic

- While it introduces a layer of indirection, we can treat things like
  [web traffic to WineHQ](https://www.winehq.org/webalizer/index.html)
  as a rough estimate of overall interest in the project.

- Use Google Trends
