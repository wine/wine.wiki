---
title: Wine
---

<small>
&nbsp;[:flag_fr: Français](fr/home)
&nbsp;[:flag_de: Deutsch](de/home)
&nbsp;[:flag_nl: Nederlands](nl/home)
&nbsp;[:flag_kr: 한국어](ko/home)
&nbsp;[:flag_cn: 简体中文](zh_CN/home)
</small>

-----

[Wine](https://winehq.org) enables Linux, Mac, FreeBSD, and Solaris users
to run Windows applications without a copy of Microsoft Windows. Wine is
[free software](https://www.gnu.org/philosophy/free-sw.html) under
constant development. [Other platforms](Compatibility) may
benefit as well.

## Most popular links

- [Frequently Asked Questions](FAQ): If you're having a general
  problem with Wine, please read the FAQ!

- [Wine documentation](Documentation): If the FAQ didn't help, try
  reading the manual! :smiley:

- [The Wine Application Database](https://appdb.winehq.org): For help
  with a particular app, look up its entry in the AppDB.

- [Wine Users support forum](https://forum.winehq.org): For asking
  questions that you can't find an answer to in the above.

- [Known Issues](Known-Issues): Check here before you report a bug.

- [winetricks](Winetricks): A useful tool for using common workarounds
  to current deficiencies in Wine.

- [macOS](MacOS): Running Wine on macOS.

## About the Wine project

- A brief overview of [Wine Features](Wine-Features) and goals.

- Some short articles about the [Importance Of
  Wine](Importance-of-Wine).

- An overview of Wine [Project Organization](Project-Organization).

- [Who's Who](Who's-Who) in the Wine project, and
  [Acknowledgements](Acknowledgements) for major contributors.

- The [history of the Wine project](Wine-History) and recent
  [News](News).

- Information about Wine's [Licensing](Licensing).

## Contribute

- User support: Help users by answering questions on the [User's
  Forum](https://forum.winehq.org) or [IRC](IRC).

- [Developers](Developers): If you want to help with Wine or build it
  from source.

- [AppDB Maintainers](https://gitlab.winehq.org/winehq/appdb/-/wikis/Maintainer-Guidelines): If you
  are (or want to become) an AppDB Maintainer.

- [Debuggers](Bugs) can help by testing Wine and narrowing
  down problems, or help triage bugs reported by other users.

- [Writers](Writers) can contribute by documenting the program,
  maintaining the wiki, and translating various parts of the project.

- [Designers](Designers) can, among other tasks, draw icons for
  programs, or improve the style and function of the website.

- [Wine Development Fund](https://www.winehq.org/donate): Please consider
  making a donation to support the Wine project.

- [Public Relations](Public-Relations): ways to help spread the word
  about Wine.

## Other useful links

- A list of [Commands](Commands) for all the little tools that come
  with Wine.

- [Third Party Applications](Third-Party-Applications) and
  "unofficial" tools that might come in handy.

- [Useful Registry Keys](Useful-Registry-Keys) and various settings
  for configuring Wine.

- Instructions for [Regression Testing](Regression-Testing).

- [Applications which officially test against Wine as a
  platform](Apps-That-Support-Wine).

## More information

- The Wine project [Source Code](Source-Code) can be downloaded or
  browsed online.

- You can browse archived discussions or subscribe to the Wine [mailing
  lists](Forums).

- Information on development progress and the [status of
  Wine](Wine-Status).

- Assorted files such as site icons and images, and database dumps of
  Bugzilla, AppDB, and the wiki can be downloaded from WineHQ's
  [download server](https://dl.winehq.org/wine/).

- View [WineHQ site
  statistics](https://www.winehq.org/webalizer/index.html) and rough
  estimates of Wine's [Usage Statistics](Usage-Statistics).
