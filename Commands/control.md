**control.exe** is Wine's implementation of the Windows Control Panel.

Wine does *not* currently implement many of the control panel applets
(.cpl files) typically found on a Windows system. If the requested
applet cannot be found, control will silently exit. It should however be
possible to load applets installed by third party software by running
"wine control" and clicking the appropriate icon or specifying the .cpl
file on the command line.

## Usage

`wine control [filename.cpl | COLOR | DATE/TIME | DESKTOP | INTERNATIONAL | KEYBOARD | MOUSE | PORTS | PRINTERS]`

See also:

- [winecfg](Commands/winecfg)
- [Useful Registry Keys](Useful-Registry-Keys).
