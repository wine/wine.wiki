**msiexec** is the *Wine MSI installer*, which is command line
compatible with its Microsoft Windows counterpart.

## Usage

### Install a product

Install a product via *packagename* or *productcode* with an optional
*property* *foobar*:

`msiexec {package|productcode} [property=foobar]]`

`msiexec /i {package|productcode} [property=foobar]]`

Install a product via *packagename* or *productcode* in administrator
(network) mode with an optional *property* *foobar*:

`msiexec /a package [property=foobar]]`

### Repair an installation

Repair an installation via *packagename* or *productcode*. Default
options are *omus*:

`msiexec /f[p|o|e|d|c|a|u|m|s|v] {package|productcode}`

#### Reinstall a file if it is missing

`msiexec /fp {package|productcode}`

#### Reinstall a file if it is missing or if any older version is installed

`msiexec /fo {package|productcode}`

#### Reinstall a file if it is missing, or if the installed version is equal or older

`msiexec /fe {package|productcode}`

#### Reinstall a file if it is missing or a different version is installed

`msiexec /fd {package|productcode}`

#### Reinstall a file if it is missing or the checksum does not match

`msiexec /fc {package|productcode}`

#### Reinstall all files

`msiexec /fa {package|productcode}`

#### Rewrite all required user registry entries

`msiexec /fu {package|productcode}`

#### Rewrite all required machine registry entries

`msiexec /fm {package|productcode}`

#### Overwrite any conflicting shortcuts

`msiexec /fs {package|productcode}`

#### Recache the local installation package from the source installation package

`msiexec /fv {package|productcode}`

### Uninstall a product

Uninstall a product via *packagename* or *productcode* with an optional
*property* *foobar*:

`msiexec /uninstall {package|productcode} [property]`
`msiexec /x {package|productcode} [property]`

### Advertise a product

Advertise package optionally with */t transform* and */g languageid*.
This is usefully for *installation on demand*:

`msiexec /j[u|m] package [/t transform] [/g languageid]`
`msiexec {u|m} package [/t transform] [/g languageid]`

### Apply a patch

Apply a patch. This should not be used with any of the *Repair* or
*Reinstall* options:

`msiexec /p patchpackage [property]`
`msiexec /p patchpackage /a package [property]`

### Modifiers for UI control

These options allow changing the behavior of the *U*ser *I*nterface when
installing MSI packages:

`msiexec /q{|n|b|r|f|n+|b+|b-}`

#### Show no UI

`msiexec /q`

`msiexec /qn`

#### Show a basic UI

`msiexec /qb`

#### Shows a reduced user UI

`msiexec /qr`

#### Shows a full UI

`msiexec /qf`

### Modifiers for logging

Enable logging to *logfile*. Defaults are *iwearmo*.

`msiexec /l[*][i|w|e|a|r|u|c|m|o|p|v|][+|!] logfile`

#### Enable all logging options except *v* and *x*

`msiexec /l* logfile`

#### Log status messages

`msiexec /li logfile`

#### Log nonfatal warnings

`msiexec /lw logfile`

#### Log all error messages

`msiexec /le logfile`

#### Log start of actions

`msiexec /la logfile`

#### Log action specific records

`msiexec /lr logfile`

#### Log user requests

`msiexec /lu logfile`

#### Log initial UI parameters

`msiexec /lc logfile`

#### Log out of memory errors

`msiexec /lm logfile`

#### Log out of diskspace messages

`msiexec /lo logfile`

#### Log terminal properties

`msiexec /lp logfile`

#### Verbose logging

`msiexec /lv logfile`

#### Log extra debugging messages

`msiexec /lx logfile`

#### Append logging to existing file

`msiexec /l+ logfile`

#### Flush each line to log

`msiexec /l! logfile`

### Register MSI service

`msiexec /y module`

### Unregister MSI service

`msiexec /z module`

### Display usage / help and copyright

`msiexec {/h|/?}`

**Notes:**

- Use of Product codes on commandline has not yet been implemented.
- Generally a product will ship with an executable (.exe file) which
  provides a more user friendly mechanism for installing the product.
  Where possible you should use the .exe provided by the application
  developer. The file name will usually contain "setup" or "install" in
  its filename.

## Examples

- Install the package *msxml3.msi* from the actual directory:

`msiexec /i msxml3.msi`

- Install the package *thebat_32_v10-3-3-16_nau.msi* from the users
  download directory without an UI:

`msiexec /qn /i /home/user/Downloads/thebat_32_v10-3-3-16_nau.msi`
