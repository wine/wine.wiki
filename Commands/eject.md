**eject** is a Wine command. It is used to free, unlock, and eject an
optical device when Wine is using it.

Note: in order to eject, the drive must be correctly set up in
[winecfg](Commands/winecfg) and must be invoked with **wine**
preceding it.

## Usage

```
wine eject [-u] [-a] [-h] [x:]...
   -a  Eject all the CD drives we find
   -h  Display this help message
   -u  Unmount only, don't eject the CD
   x:  Eject drive x:
```

Useful when trying to use multidisk installers:

1.  `wine /media/cdrom/setup.exe` (or whatever it's called)

1.  When the installer prompts for the 2nd disc, go to another terminal
    and run `wine eject` if just pressing the eject button doesn't let
    go of the disc

Alternate suggestions to run `wine /media/cdrom/setup.exe` don't
always work because wine uses a different method of accessing the
contents of the CD in that case that doesn't survive a CD change
untouched. Case in point is the Game "NEXUS - The Jupiter Incident".
