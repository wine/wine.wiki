**expand** extracts a file stored in a cabinet file (.cab).

## Usage

`wine expand source_file destination_file`

*source_file* - file name of the cabinet file

*destination_file* - path and filename of file to be extracted.

*Note:* Wine's **expand** does not support all the command line options
available from the Microsoft version of the tool.

There is a third party tool called `cabextract` available on many Linux
distributions which supports many advanced features not yet supported by
**expand**. Cabextract is free software under the GPL.

## Examples

If you have a file called *cow.cab* which contains a file named
*moo.txt* you could extract it with:

`wine expand cow.cab moo.txt`

## See Also

- <https://www.cabextract.org.uk/>
