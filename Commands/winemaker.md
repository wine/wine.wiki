

## USAGE

    Usage: winemaker [--nobanner] [--backup|--nobackup] [--nosource-fix]
                     [--lower-none|--lower-all|--lower-uppercase]
                     [--lower-include|--nolower-include] [--mfc|--nomfc]
                     [--guiexe|--windows|--cuiexe|--console|--dll|--lib]
                     [-Dmacro[=defn]] [-Idir] [-Pdir] [-idll] [-Ldir] [-llibrary]
                     [--nodlls] [--nomsvcrt] [--interactive] [--single-target name]
                     [--generated-files|--nogenerated-files]
                     [--wine32]
                     work_directory|project_file|workspace_file

## DESCRIPTION

**Winemaker** is a perl script designed to help you bootstrap the
process of converting your Windows sources to Winelib programs. In order
to do this winemaker can perform the following operations:

- rename your source files and directories to lowercase in the event
  they got all uppercased during the transfer.
- perform Dos to Unix line-ending (CRLF to LF) conversions.
- scan the include statements and resource file references to replace
  the backslashes with forward slashes.
- during the above step winemaker will also perform a case-insensitive
  search of the referenced file in the include path and rewrite the
  include statement with the right case if necessary.
- winemaker will also check other more exotic issues like ’#pragma pack’
  usage, use of "afxres.h" in non-MFC projects, and more. Whenever it
  encounters something out of the ordinary, winemaker will warn you
  about it.
- winemaker can also scan a complete directory tree at once, guess what
  are the executables and libraries you are trying to build, match them
  with source files, and generate the corresponding Makefile.in files.
- finally winemaker will generate a global Makefile for normal use.
- winemaker knows about MFC-based project and will generate customized
  files.
- winemaker can read existing project files. It supports dsp, dsw,
  vcproj and sln files.

## OPTIONS

--nobanner


Disables the printing of the banner.

--backup


Directs winemaker to perform a backup of all the source files in which
it makes changes. This is the default.

--nobackup


Tells winemaker not to backup modified source files.

--nosource-fix


Directs winemaker not to try fixing the source files (e.g. Dos to Unix
conversion). This prevents complaints if the files are read-only.

--lower-all


Tells winemaker to rename all files and directories to lowercase.

--lower-uppercase


Tells winemaker to only rename files and directories that have an all
uppercase name. So "HELLO.C" would be renamed but not "World.c".

--lower-none


Tells winemaker not to rename files and directories to lowercase. Note
that this does not prevent the renaming of a file if its extension
cannot be handled as is, e.g. ".Cxx". This is the default.

--lower-include


Tells winemaker that if it does not find the file corresponding to an
include statement (or other form of file reference for resource files),
then it should convert that filename to lowercase. This is the default.

--nolower-include


Tells winemaker not to modify the include statement if it cannot find
the referenced file.

--guiexe \| --windows


Specifies that whenever winemaker finds an executable target, or a
target of unknown type, it should assume that it is a graphical
application. This is the default.

--cuiexe \| --console


Specifies that whenever winemaker finds an executable target, or a
target of unknown type, it should assume that it is a console
application.

--dll


This option tells winemaker that whenever it finds a target of unknown
type, i.e. for which it does not know whether it is an executable or a
library, it should assume it is a library.

--nodlls


This option tells winemaker not to use the standard set of winelib
libraries for imports. That is, any DLL your code uses must be
explicitly passed to winemaker with -i options. The standard set of
libraries is: advapi32.dll, comdlg32.dll, gdi32.dll, kernel32.dll,
odbc32.dll, ole32.dll, oleaut32.dll, shell32.dll, user32.dll,
winspool.drv.

--mfc


Specifies that the targets are MFC based. In such a case winemaker the
include and library paths accordingly, and links the target with the MFC
library.

--nomfc


Specifies that targets are not MFC-based. This option disables use of
MFC libraries even if winemaker encounters files "stdafx.cpp" or
"stdafx.h" that would cause it to enable MFC automatically if neither
--nomfc nor --mfc was specified.

--nomsvcrt


Set some options to tell winegcc not to compile against msvcrt. Use this
option if you have cpp-files that include <string>.

-Dmacro\[=defn\]


Adds the specified macro definition to the global list of macro
definitions.

-Idir


Appends the specified directory to the global include path.

-Pdir


Appends the specified directory to the global dll path.

-idll


Adds the Winelib library to the global list of Winelib libraries to
import.

-Ldir


Appends the specified directory to the global library path.

-llibrary


Adds the specified library to the global list of libraries to link with.

--interactive


Puts winemaker in interactive mode. In this mode winemaker will ask you
to confirm each directory’s list of targets, and then to provide
directory and target specific options.

--single-target name


Specifies that there is only one target, and that it is called "name".

--generated-files


Tells winemaker to generate the build the Makefile. This is the default.

--nogenerated-files


Tells winemaker not to generate the Makefile.

--wine32


Tells winemaker to generate a 32-bit target. This is useful on wow64
systems. Without that option the default architecture is used.

## EXAMPLES

Here is a typical winemaker use:

`$ winemaker --lower-uppercase -DSTRICT .`

The above tells winemaker to scan the current directory and its
subdirectories for source files. Whenever if finds a file or directory
which name is all uppercase, it should rename it to lowercase. It should
then fix all these source files for compilation with Winelib and
generate Makefiles. The ’-DSTRICT’ specifies that the STRICT macro must
be set when compiling these sources. Finally winemaker will create a
Makefile.

The next step would be:

`$ make`

If at this point you get compilation errors (which is quite likely for a
reasonably-sized project) then you should consult [the Winelib User
Guide](Winelib-User's-Guide) to find tips on how to resolve them.

For an MFC-based project you would have to run the following commands
instead:

`$ winemaker --lower-uppercase --mfc .`

`$ make`

For an existing project-file you would have to run the following
commands:

`$ winemaker --lower-all myproject.dsp`

`$ make`

#### Using STL

If there are errors like

`/usr/include/c++/4.3/cwchar:153: error: ‘::fwide’ has not been declared`

`/usr/include/c++/4.3/cwchar:160: error: ‘::mbsinit’ has not been declared  `

`/usr/include/c++/4.3/cwchar:169: error: ‘::vfwscanf’ has not been declared `

when including <string>, add the commandline option `--nomsvcrt` to
winemaker

## TODO / BUGS

In some cases, you will have to edit the Makefile or source files by
yourself.

Assuming that the Windows executable/library is available, we could use
winedump to determine what kind of executable it is (graphical or
console), which libraries it is linked with, and which functions it
exports (for libraries). We could then restore all these settings for
the corresponding Winelib target.

Furthermore, winemaker is not very good at finding the library
containing the executable: it must either be in the current directory or
in the LD_LIBRARY_PATH.

Winemaker does not support message files and the message compiler yet.

## SEE ALSO

- [Winelib User's Guide](Winelib-User's-Guide)
- [Source code for
  winemaker](https://gitlab.winehq.org/wine/wine/-/blob/HEAD/tools/winemaker/winemaker)
- [winemaker man page](Man-Pages/winemaker)

# AUTHORS

- Francois Gouget for CodeWeavers.
- Dimitrie O. Paun
- André Hentschel
