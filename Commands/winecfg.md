**winecfg** is a GUI configuration tool for Wine, designed to make
life a little easier than editing the registry.

The goal of this document is to describe features of winecfg which may
be less than intuitive. It should be noted that changes to winecfg have
been proposed to increase its overall usability (user friendliness).
That said, this document may very well be much shorter in the future.

**Tip:** Although winecfg is a great configuration tool, some more
advanced settings can be only be changed by editing the registry (i.e.
with the [regedit](Commands/regedit) tool). You can find some useful
registry keys [here](Useful-Registry-Keys). As always, one should
exercise care when editing the registry.

## Using winecfg

There are two ways you can use winecfg to change settings. You can
change settings for all applications ("Default Settings") or you can
change settings for a specific application (which overrides the default
settings). When you start winecfg, the *Applications* tab is shown with
"Default Settings" selected. With "Default Settings" selected, changes
made affect all applications.

To change settings only for a specific application:

1.  In the Application tab, click the "Add application..." button.
2.  Browse to and highlight the application then click the "Open"
    button.
3.  With your application's filename still highlighted in the
    Application tab, changes only (i.e. in other tabs) affect that
    specific application.

**Tip:** When editing per application settings, the application name
appears in the winecfg title bar.

### Windows Version

This setting changes the version reported to applications that ask for
it. Generally, this setting does not change Wine's behavior. If an
application never asks for this version information, it is likely that
changing this setting will have no effect on the application's
performance or functioning.

Applications which ask for version information might do so in order to
make decisions about which Windows features it should use. For example,
if a feature is only available in newer versions of Windows then the
application would want to avoid using that feature if running on older
Windows versions. (Note: Better designed applications will try to detect
the features in question rather than relying on the operating system's
version.)

This setting is provided because some applications will refuse to run if
the reported version does not meet their stated system requirements. In
some cases, changing this setting may also work around bugs in Wine by
causing the application to use different feature sets to get its work
done. If changing this setting breaks or “fixes” your application, it
may be a [bug](Bugs).

Versions of Windows prior to XP are only available for selection in
winecfg in 32 bit wineprefixes. This is because those versions of
Windows did not exist in 64 bit. If you are on a 64 bit system, you will
have to [create a 32 bit
wineprefix](FAQ#how-do-i-create-a-32-bit-wineprefix-on-a-64-bit-system)
to be able to set Wine to those versions.

### Libraries

In this tab you can override the default way Wine loads DLLs (Dynamic
Link Libraries). Sometimes Wine ships with a DLL, which is not fully
implemented or contains bugs which haven't yet been worked out. In some
cases you can work around these kinds of problems by using a library
provided with an application or copied from a Windows installation.

Builtin means: Provided by Wine.

Native means: Not provided by Wine (eg installed by an application or
copied from a Windows installation).

**Attention:**

- You only need to provide overrides for libraries provided by Wine.
- Overridable Libraries provided by Wine *should* be listed in the
  drop-down list.
- Although DLL overrides can sometimes solve problems, they can also
  potentially cause larger problems which prevent Wine and your
  applications from working altogether. You should override a DLL only
  if absolutely necessary.
- Wine HQ cannot provide support for native DLLs. If you use them, you
  may be on your own. In particular, if you use an override do not
  submit a bug report based on that configuration. You could potentially
  waste developer time, have your bug marked as invalid, and be no
  closer to solving the problem with an improved Wine.
- The following libraries should ***never*** be overridden (If you try,
  you'll get a dialog box telling you not to--*don't do it!*):

` advapi32, capi2032, ddraw, gdi32, glu32, icmp, iphlpapi, kernel32, mswsock, ntdll, opengl32, stdole2.tlb, stdole32.tlb, twain_32, unicows, user32, vdmdbg, w32skrnl, winealsa.drv, wined3d, winejoystick.drv, wineoss.drv, wineps.drv, winex11.drv, winmm, wintab32, wnaspi32, wow32, ws2_32, wsock32 `

- Do not blindly copy files from a Windows installation to a Wine
  system32 folder and create overrides.

If all of the above didn't scare you off, you can add an override by
typing the name of the library or selecting it from the drop down list
and then click Add. At this point, the library should now be listed
under "Existing Overrides" and highlighted. Click the "Edit" button and
select how you would like the override to work.

The following load orders (overrides) are selectable:

- **Builtin (Wine)** - Use only the library version provided by Wine
  (fail if not found).
- **Native (Windows)** - Use only the native version of the library
  (fail if not found).
- **Builtin then Native** - Try to load the library provided by Wine
  first, then try native if that fails. (This is the default behavior)
- **Native then Builtin** - Try to load a native version first, then try
  builtin if that fails. (Probably the one you want if you are going to
  use an override)
- **Disable** - If an application tries to load the library, it will
  fail.

You can prefix the DLL name with an asterisk (\*) when entering it in;
in that case the override will apply when it is loaded from any
directory (instead of just system32). That way, you can force a builtin
DLL to be used even if an application ships its own redistributable.

### Graphics

#### Window Settings

The settings in this section are generally best edited on a per
application bases. Changing a setting here may fix one application and
break another at the same time.

- **Allow the window manager to decorate the windows.** -- This setting
  has to do with the borders and Title bars of windows created by
  applications. If this option is enabled your Window manager (eg. KDE)
  will draw them and your applications will look a little more native to
  your desktop environment.
- **Allow the window manager to control the windows** -- When unchecked
  windows are disconnected from your window manager. They will not show
  up in the window list (ie in the alt+tab list or on your task bar) and
  are not decorated.
- **Emulate a virtual desktop** -- With this setting enabled, created
  windows are confined to a single window (the "virtual" desktop
  window). You should select a desktop size smaller than what you run X
  in. Windows within a virtual desktop are not decorated or managed by
  your window manager.

#### Screen Resolution (DPI Setting)

It is possible to set this value too high which results in winecfg
(amongst other things) being too large to be usable. To edit this value
manually create a file named logpixels.reg with the following contents:

    REGEDIT4

    [HKEY_LOCAL_MACHINE\System\CurrentControlSet\Hardware Profiles\Current\Software\Fonts]
    "LogPixels"=dword:00000060

Next, import it into the registry with:

`regedit logpixels.reg`

Restart winecfg and note the value is now 96.

Note: if winecfg is not run under a virtual desktop and the windows are
controlled by your window manager, you may be able to move the window by
pressing alt/meta and dragging the mouse any where in the window to move
it (thus granting you access to the DPI slider).

### Desktop Integration

In this tab you adjust the appearance of applications (Theming) and
change some default shell folders. You can, for example, make your
Windows applications look more like your favorite KDE or Gnome desktop
theme.

### Drives

In this tab you can control how Windows drive letters are mapped to Unix
paths. Generally you will not need to change anything here unless
directed to do so.

**Do *not* change C: to point to an actual Windows installation**. Doing
so may render your Windows installation unusable. Wine is not designed
to interact with a Windows installation; it is designed to be
independent of one.

### Audio

In this tab you'll find settings related to the configuration of your
[Sound](Sound) system. Although Wine supports multiple sound drivers,
you should only use ONE at a time. The supported sound drivers are:
PULSEAUDIO, ALSA, and OSS.

### About

This tab proudly shows the Wine logo, it's version, a URL to [Wine
HQ](https://www.winehq.org), and a [GNU LGPL notice](Licensing).

It also has a couple of text boxes to allow you to set the Owner and
Organization.

## See Also

- [Configuring Wine chapter of the Wine User's
  Guide](Wine-User's-Guide#configuring-wine)
- [Useful Registry Keys](Useful-Registry-Keys) - Useful Registry Keys
  that can't be changed (yet) with winecfg.
- [regedit](Commands/regedit) - A tool for editing the registry
