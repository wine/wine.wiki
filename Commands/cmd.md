**cmd** is command-line access into Wine, similar to Windows' *cmd*.

Once invoked in a terminal/console (by using `wine cmd`), the current
directory is displayed using DOS format. The system root is displayed as
"Z:" and normal Unix forward slashes ( / ) are replaced by DOS
backslashes ( \\ ) .

Wine's command prompt has some settings available through the
right-click menu: simply right-click anywhere in the console.
