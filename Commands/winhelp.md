**winhelp** views a Microsoft help file (.hlp)

## Usage

`winhelp [filename.hlp]`

Attempts to open *filename.hlp* for viewing.

**Tip:** for .chm files try `wine hh file.chm`

## See Also

- [hh](Commands/hh) - HTML Help (.chm) viewer.
