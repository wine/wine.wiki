**winebrowser** attempts to open a URL in the native operating
system's default protocol handler.

### Usage

`winebrowser URL`

where *URL* is the URL to open. For example:

    winebrowser https://www.winehq.org
    winebrowser ftp://ftp.winehq.org
    winebrowser irc://irc.libera.chat:6697/#winehq
    winebrowser file://c:\\windows\\win.ini

On a KDE system the first two might open in Konqueror, the third in
Konversation, and the last in KWrite.
