**wine start** starts a program or opens a document in the program
normally used for files with that suffix.

### Usage

    start [options] program_filename [...]
    start [options] document_filename

    Options:
    "title"        Specifies the title of the child windows.
    /d directory   Start the program in the specified directory.
    /b             Don't create a new console for the program.
    /i             Start the program with fresh environment variables.
    /min           Start the program minimized.
    /max           Start the program maximized.
    /low           Start the program in the idle priority class.
    /normal        Start the program in the normal priority class.
    /high          Start the program in the high priority class.
    /realtime      Start the program in the realtime priority class.
    /abovenormal   Start the program in the abovenormal priority class.
    /belownormal   Start the program in the belownormal priority class.
    /node n        Start the program on the specified NUMA node.
    /affinity mask Start the program with the specified affinity mask.
    /wait          Wait for the started program to finish, then exit with its
                   exit code.
    /unix          Use a Unix filename and start the file like windows
                   explorer.
    /ProgIDOpen    Open a document using the specified progID.
    /?             Display this help and exit.
