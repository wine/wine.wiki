**winemenubuilder** is used to replicate the Windows desktop and start
menu into the native desktop's copies. Desktop entries are merged
directly into the native desktop. The Windows Start Menu corresponds
to a Wine entry within the native "start" menu and replicates the
whole tree structure of the Windows Start Menu. Currently it does not
differentiate between the user's desktop/start menu and the "All
Users" copies.

This program will read a Windows shortcut file using the `IShellLink`
interface, then create a KDE/Gnome menu entry for the shortcut.

## Usage

` wine winemenubuilder [ -w ] <shortcut.lnk>`

If the -w parameter is passed, and the shortcut cannot be created, this
program will wait for the parent process to finish and then try again.
This covers the case when a shortcut is created before the executable
containing its icon.

## See also

- [FAQ entry on disabling
  winemenubuilder](FAQ#how-can-i-prevent-wine-from-changing-the-filetype-associations-on-my-system-or-adding-unwanted-menu-entriesdesktop-links)
