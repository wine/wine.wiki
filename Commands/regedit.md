**regedit** is Wine's registry editor, similar in
appearance and function to Window's regedit. When invoked as \`wine
regedit\` with no arguments, the program runs in GUI mode.

**Note:** Although Wine stores the registry as text files (specifically
`$WINEPREFIX/*.reg`), you should use the regedit tool for making changes
to the registry, just like you would on Windows. This is because of,
amongst other things, the special encoding used to store keys.

## Usage

    wine regedit filename
    wine regedit /E filename [regpath]
    wine regedit /D regpath

    filename - registry file name
    regpath - name of the registry key

    When called without any switches, adds the content of the specified file to the registry

    Switches:
        /E - exports contents of the specified registry key to the specified
            file. Exports the whole registry if no key is specified.
        /D - deletes specified registry key
        /S - silent execution, can be used with any other switch.
            Default. The only existing mode, exists for compatibility with Windows regedit.
        /V - advanced mode, can be used with any other switch.
            Ignored, exists for compatibility with Windows regedit.
        /L - location of system.dat file. Can be used with any other switch.
            Ignored. Exists for compatibility with Windows regedit.
        /R - location of user.dat file. Can be used with any other switch.
            Ignored. Exists for compatibility with Windows regedit.
        /? - print this help. Any other switches are ignored.
        /C - create registry from file. Not implemented.

    The switches are case-insensitive, can be prefixed either by '-' or '/'.
    This program is command-line compatible with Microsoft Windows regedit

## User Keys Backup

The user can save whatever particular settings he/she wishes, in case of
~/.wine being removed or created on a new system.

For backing up software settings:

1.  Run *regedit* , navigate to `HKEY_CURRENT_USER\Software`, and click
    on the folder.
2.  Click Registry -\> Export Registry File . Save as *myoptions.reg*
    but be sure to click the "Selected Branch" button before OK
3.  Or to combine multiple individual folders (in the HKCU\Software dir)
    into a single .reg file:
    1.  Navigate to `HKEY_CURRENT_USER\Software` and click on the
        pertinent program's folder
    2.  Click File -\> Export , making sure the "Selected Branch" button
        is selected
    3.  Save the files as *myoptions.reg myoptions2.reg myoptions3.reg*
        and so on, for each top-level folder
    4.  In the terminal/console, cd to the directory of the saved files
    5.  Run *cat myoptions.reg myoptions2.reg myoptions3.reg \> foo &&
        cp foo myoptions.reg && rm foo*
4.  Registry backup is finished! Restore the keys into the registry any
    time by running *regedit myoptions.reg*

## See Also

- [Useful Registry Keys](Useful-Registry-Keys) - a collection of
  registry keys for further tweaking of Wine
