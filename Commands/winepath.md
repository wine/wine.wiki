**winepath** is the helper program to convert file paths between UNIX
and Windows.

## Usage

    Usage: winepath [OPTION] [PATH]...
    Convert PATH(s) to Unix or Windows long or short paths.

      -u, --unix    converts a Windows path to a Unix path
      -w, --windows converts a Unix path to a long Windows path
      -l, --long    converts the short Windows path of an existing file or
                    directory to the long format
      -s, --short   converts the long Windows path of an existing file or
                    directory to the short format
      -h, --help    output this help message and exit
      -v, --version output version information and exit

If more than one option is given then the input paths are output in all
formats specified, in the order long, short, Unix, Windows.

If no option is given the default is Unix format.

## Examples

    $ winepath c:\\
    /home/vitaliy/.wine/dosdevices/c:/

    $ winepath "c:\program files"
    /home/vitaliy/.wine/dosdevices/c:/Program Files

    $ winepath -w ~
    z:\home\vitaliy

    $ winepath -w /media/dvd/
    d:\

To launch documents from inside Windows apps, create a small shell
script called e.g. /usr/bin/run_prog containing

    #!/bin/sh
    $1 "`wine winepath -u "$2"`"

And then add a registry entry like this:

    [HKEY_CLASSES_ROOT\xcelfile\Shell\Open\command]
    @="/bin/sh run_prog /usr/bin/oocalc \"%1\""

## See also

- [winenative](https://www.winehq.org/pipermail/wine-users/2008-January/028728.html) -
  Tres Finocchiaro's script that uses winepath to launch Linux apps
  from inside Windows apps
