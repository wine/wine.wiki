**hh** is Wine's HTML help viewer (for help files with the .chm
extension)

## Usage

`wine hh filename.chm`

## See also

- [winhelp](Commands/winhelp) - Help file viewer for .hlp files
