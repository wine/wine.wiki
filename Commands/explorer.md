**explorer.exe** does one of two things:

- Provides an alternative method of starting
  [winefile](Commands/winefile). Simply put, it accepts parameters
  similar to the explorer provided by Windows then uses them to call
  winefile.
- Creates a named "virtual desktop" which programs can run under.

## Usage

```
 wine explorer [/n] [/e,] [/root,object] [/select,object]
   /n              Opens in single-paned view for each selected items. This is default
   /e,             Uses Windows Explorer View
   /root,object    Specifies the root level of the view
   /select,object  Opens parent folder and selects (highlights) specified object
```

OR

```
 wine explorer /desktop=name[,widthxheight] [program]
   name            name of the desktop (used when launching other programs under the same desktop)
   [,WidthxHeight] specifies the size of the desktop to create (example 800x600)
   program         If supplied explorer will attempt to launch the program.
```

Example:

`wine explorer /desktop=foo,800x600 winemine`

Requesting the desktop named "shell" starts a virtual desktop which
includes a start menu and remains open even if no programs are running
inside of it:

`wine explorer /desktop=shell`

## See Also

- [winefile](Man-Pages/winefile) - A file manager for wine.
