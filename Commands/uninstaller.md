**uninstaller** is a GUI uninstaller for all setup programs that put an
uninstall entry in the registry, e.g. InstallShield or the WISE
installer. It's similar in function to "Add/Remove Programs" in Windows,
except much simpler.

An actual Add/Remove Programs control panel applet (appwiz.cpl) was
added to Wine 1.1.2, and the existing uninstaller was replaced with a
stub to launch this control panel in Wine 1.1.3.

The uninstaller does not work with programs installed with an .msi file.

## Usage

`wine uninstaller`
