---
title: Where to start
---
Wine is, of course, a huge project. So where to start? Does Wine have a Good First Bug list?

Unfortunately not; the easy bugs tend to get fixed quickly.

Instead, the best place to start is find something you're motivated to fix; find something you want to work that currently doesn't, and hack the code until it does.

This isn't as overwhelming as it sounds, because Wine isn't one huge application - it's more like hundreds of small pieces. You don't need to understand them all, or how they work together, to get started.

You should, however, be familiar with C programming. If you're not, the easiest place to start in Wine is writing a test for your favorite unimplemented feature. Alternatively, read a tutorial or two, or otherwise write code outside Wine.