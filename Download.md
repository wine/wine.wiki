<small>
&nbsp;[:flag_nl: Nederlands](nl/Download)
&nbsp;[:flag_cn: 简体中文](zh_CN/Download)
</small>

-----

## Supported Wine

<table><tbody><tr>
<td><a href="https://www.codeweavers.com/crossover">

![Crossover logo](/media/Crossover-logo.png)

</a></td>
<td>
[**CrossOver**](https://www.codeweavers.com/products) is a polished
version of Wine provided by
[**CodeWeavers**](https://www.codeweavers.com). CrossOver makes it
easier to use Wine and CodeWeavers provides excellent technical
support to its users. All purchases of CrossOver are used to directly
fund the developers working on Wine. So CrossOver is both a great way
to get support in using Wine and to support the Wine
Project.<br><br>

CodeWeavers provides fully functional [**trial versions of
CrossOver**](https://www.codeweavers.com/crossover).
<br><br>
<small>
This endorsement is the primary recognition that CodeWeavers
has requested in exchange for hosting the Wine web site.
</small></td>
</tr>
</tbody>
</table>

## Wine Binary Packages

[**Release announcements**](https://www.winehq.org/news/)

[**Installation and configuration how-to**](Wine-Installation-and-Configuration)

### WineHQ Binary Packages

*These packages are built and supported by WineHQ. Please report any
problems with them in [WineHQ's bugzilla](https://bugs.winehq.org).*

<table><tbody><tr>
<td rowspan="2">![Wine logo](/media/Wine-logo.png){width=60%}</td>
<td>

[**Ubuntu**](Debian-Ubuntu) - WineHQ binary packages for Ubuntu 20.04, 22.04,
24.04 and 24.10

[**Debian**](Debian-Ubuntu) - WineHQ binary packages for Debian Bullseye,
Bookworm and Trixie

[**Fedora**](Fedora) - WineHQ binary packages for Fedora 40 and 41
</td><td>
<small>
<strong>Maintainers:</strong>
[Rosanne DiMesio](mailto:dimesio@earthlink.net),
[Marcus Meissner](mailto:meissner@suse.de)
</small></td></tr>
<tr><td>

[**macOS**](MacOS) - WineHQ binary packages for macOS 10.15.4 and
later
</td><td>
<small>
<strong>Maintainer:</strong>
[Dean Greer](mailto:gcenx83@gmail.com)
</small></td>
</tbody></table>

### Distro Binary Packages

*These packages are built and supported by the distros. Please report
any problems with them to the package maintainer.*

<table>
<tbody>
<tr><td>![Suse logo](/media/Suse-logo.png)</td><td>

[**SUSE**](https://en.opensuse.org/Wine#Repositories) - release binary
and source .rpms and daily snapshot RPMs for all openSUSE versions
(Leap and Tumbleweed) and SUSE Linux Enterprise 12 and 15
</td><td>
<small>
<strong>Maintainer:</strong>
[Marcus Meissner](mailto:meissner@suse.de)
</small></td></tr>
<tr><td>![Slackware logo](/media/Slackware-logo.png)</td><td>

[**Slackware**](https://sourceforge.net/projects/wine/files/Slackware%20Packages) -
binary txz (Slackware 15.0), and tgz (for older versions)
</td><td>
<small>
<strong>Maintainer:</strong>
[Simone Giustetti](mailto:studiosg@giustetti.net)
</small></td></tr>
<tr><td>![FreeBSD logo](/media/FreeBSD-logo.png)</td><td>

[**FreeBSD**](https://www.freshports.org/emulators/wine/) - source for
FreeBSD 5.3 or later
</td><td>
<small>
<strong>Maintainer:</strong>
[Gerald Pfeifer](mailto:gerald@pfeifer.com)
</small></td></tr>
</tbody>
</table>

## 3rd Party Tools

Sometimes a customization of Wine can cause an application to work, but
this change cannot be incorporated into Wine for some reason. To help
overcome current deficiencies in Wine, various third party applications
for end users have been made over the years. These applications are
entirely unsupported by the Wine developers, however you may find them
useful. For more information, see our wiki page on [Third Party
Applications](Third-Party-Applications).

## Wine Source Downloads

[**WineHQ download server**](https://dl.winehq.org/wine/source/) - our
official source release site.

**[Git](Git-Wine-Tutorial)** - instructions for building Wine from git.

See [Building Wine](Building-Wine) for help on configuring
and building Wine.

[**Wine-staging repository**](https://gitlab.winehq.org/wine/wine-staging)
