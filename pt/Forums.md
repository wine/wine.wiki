---
title: Principais listas de Email / Fórums
---

<small>
&nbsp;[:flag_gb: English](Forums)
&nbsp;[:flag_fr: Français](fr/Forums)
&nbsp;[:flag_nl: Nederlands](nl/Forums)
&nbsp;[:flag_pl: Polski](pl/Forums)
&nbsp;[:flag_tr: Türkçe](tr/Forums)
&nbsp;[:flag_kr: 한국어](ko/Forums)
&nbsp;[:flag_cn: 简体中文](zh_CN/Forums)
</small>

-----


## Fórums

O Wine tem várias listas de email e fórums. Aqui estão as mais úteis
para os usuários:

- Os [Fórums do WineHQ](https://forum.winehq.org)
- Usuários do Ubuntu talvez queiram visitar a [sessão do Wine nos
  Fóruns do Ubuntu](https://ubuntuforums.org/forumdisplay.php?f=313).

Se você conhece algum outro fórum do Wine ativo - especialmente um de
idioma diferente do inglês - por favor nos avise na lista.

:information_source: Caso você queira ser notificado dos novos
lançamentos, inscreva-se na [Lista de anúncios do
Wine](https://list.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/).

## Todas listas de Email do WineHQ

O WineHQ tem listas de email para submeter patches, acompanhamento de
commits no Git, e discussões sobre o Wine.

:information_source: Você precisa estar inscrito nas listas antes de
postar nelas, do contrário o seu email será tratado como um possível
spam pelo software da lista de emails.

- **<wine-announce@winehq.org>**\
   \[[(Des-)Inscrever](https://list.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/)\]
    \[[Arquivo](https://list.winehq.org/mailman3/hyperkitty/list/wine-announce@winehq.org/)\]\
    Uma lista de baixo tráfego (2/mês) de somente leitura para anunciar
    lançamentos e outras notícias importantes do Wine e WineHQ.
- **<wine-devel@winehq.org>**\
    \[[(Des-)Inscrever](https://list.winehq.org/mailman3/postorius/lists/wine-devel.winehq.org/)\]
    \[[Arquivo](https://list.winehq.org/mailman3/hyperkitty/list/wine-devel@winehq.org/)\]\
    Uma lista aberta de tráfego médio (50/dia) para discutir
    desenvolvimento do Wine, WineHQ, patches e qualquer outra coisa de
    interesse a desenvolvedores do Wine.
- **<wine-commits@winehq.org>**\
    \[[(Des-)Inscrever](https://list.winehq.org/mailman3/postorius/lists/wine-commits.winehq.org/)\]
    \[[Arquivo](https://list.winehq.org/mailman3/hyperkitty/list/wine-commits@winehq.org/)\]\
    Uma lista aberta de tráfego médio (25/dia) de somente leitura que
    lista os commits feitas na árvore Git.
- **<wine-releases@winehq.org>**\
    \[[(Des-)Inscrever](https://list.winehq.org/mailman3/postorius/lists/wine-releases.winehq.org/)\]
    \[[Arquivo](https://list.winehq.org/mailman3/hyperkitty/list/wine-releases@winehq.org/)\]\
    Uma lista de baixo tráfego (2/mês) de somente leitura para receber
    um grande arquivo diff para cada novo lançamento do Wine.
- **<wine-bugs@winehq.org>**\
    \[[(Des-)Inscrever](https://list.winehq.org/mailman3/postorius/lists/wine-bugs.winehq.org/)\]
    \[[Arquivo](https://list.winehq.org/mailman3/hyperkitty/list/wine-bugs@winehq.org/)\]\
    Uma lista de alto tráfego (100/dia) de somente leitura para
    monitorar a atividade na [Banco de Dados de Bugs](https://bugs.winehq.org/).
