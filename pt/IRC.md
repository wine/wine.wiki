---
title: Chat para suporte em tempo real
---

<small>
&nbsp;[:flag_gb: English](IRC)
&nbsp;[:flag_fr: Français](fr/IRC)
&nbsp;[:flag_de: Deutsch](de/IRC)
&nbsp;[:flag_es: Español](es/IRC)
&nbsp;[:flag_nl: Nederlands](nl/IRC)
&nbsp;[:flag_pl: Polski](pl/IRC)
&nbsp;[:flag_tr: Türkçe](tr/IRC)
&nbsp;[:flag_kr: 한국어](ko/IRC)
&nbsp;[:flag_cn: 简体中文](zh_CN/IRC)
</small>

-----


O [Libera.​Chat](https://libera.chat/) hospeda o canal de IRC do Wine. A
sala de chat pode ser acessada usando algum programa com suporte a IRC,
como por exemplo o [HexChat](https://hexchat.github.io/). Use as
configurações abaixo.

> **Servidor:** irc.libera.chat\
> **Porta:** 6697\
> **Canal:** #winehq

Caso você use o Firefox ou algum outro navegador com suporte a urls IRC,
você pode entrar no chat clicando em [#winehq](irc://irc.libera.chat/winehq).
