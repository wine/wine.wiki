The following applications list Wine as a supported platform.

- If you have a favorite application that works perfectly in Wine, you
  may wish to suggest that its developer lists Wine as a supported
  platform. If the developer hasn't (yet), don't add the application to
  this list; please add a test report to the
  [AppDB](https://appdb.winehq.org/) instead.
- If you are an open source, freeware or shareware developer, consider
  officially supporting Wine – if your app works perfectly in Wine it's
  almost certain to be trouble-free on all varieties of Windows. And
  you'll increase your market base.

| Application                                                                                    | AppDB Entry                                                                                                                                           |
| ---------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------- |
| [1by1](http://mpesch3.de1.cc/1by1.html)                                                        | [3049](https://appdb.winehq.org/objectManager.php?sClass=application&iId=3049)                                                                        |
| [AceMoney](http://www.mechcad.net)                                                             | [2248](https://appdb.winehq.org/objectManager.php?sClass=application&iId=2248)                                                                        |
| [Alchemy Mindworks Software](http://www.mindworkshop.com/alchemy/linux.html)                   | [Vendor entry:931](https://appdb.winehq.org/objectManager.php?bIsQueue=false&bIsRejected=false&sClass=vendor&iId=931&sAction=view&sTitle=View+Vendor) |
| [AudioMulch Interactive Music Studio](http://www.audiomulch.com/faq.htm)                       | |[7879](https://appdb.winehq.org/objectManager.php?sClass=application&iId=7879)                                                                       |
| [Avid Metafuze](http://www.avid.com/metafuze/)                                                 | [9256](https://appdb.winehq.org/appview.php?iAppId=9256)                                                                                              |
| [clrmamepro](http://www.clrmame.com/)                                                          | [5692](https://appdb.winehq.org/objectManager.php?sClass=application&iId=5692)                                                                        |
| [Coccinella](http://coccinella.im/v/windows/)                                                  | [5870](https://appdb.winehq.org/objectManager.php?sClass=application&iId=5870)                                                                        |
| [dBpoweramp Music Converter](http://www.dbpoweramp.com/dmc.htm)                                | [1622](https://appdb.winehq.org/objectManager.php?sClass=application&iId=1622)                                                                        |
| [DNA Baser](http://www.dnabaser.com)                                                           | [9279](https://appdb.winehq.org/appview.php?iAppId=9279)                                                                                              |
| [DVDFab](http://www.dvdfab.com/download.htm)                                                   | [4356](https://appdb.winehq.org/appview.php?iAppId=4356)                                                                                              |
| [6478](https://appdb.winehq.org/appview.php?iAppId=6478)                                       |                                                                                                                                                       |
| [Enterprise Architect](http://www.sparxsystems.com.au/support/faq/ea_on_linux.html)            | [3964](https://appdb.winehq.org/objectManager.php?sClass=application&iId=3964)                                                                        |
| [Eureqa](http://ccsl.mae.cornell.edu/eureqa)                                                   | [10739](https://appdb.winehq.org/objectManager.php?sClass=application&iId=10739)                                                                      |
| [EVE Online](http://support.eve-online.com/Pages/KB/Article.aspx?id=499)                       | [2249](https://appdb.winehq.org/objectManager.php?sClass=application&iId=2249)                                                                        |
| [FEMM](http://femm.foster-miller.net)                                                          | [9233](https://appdb.winehq.org/objectManager.php?sClass=application&iId=9233)                                                                        |
| [NCH Fling](http://www.nch.com.au/software/linux.html)                                         | [7166](https://appdb.winehq.org/objectManager.php?sClass=application&iId=7166)                                                                        |
| [Front Panel Designer](http://www.frontpanelexpress.com/en/download/front-panel-designer.html) | [16956](https://appdb.winehq.org/objectManager.php?sClass=version&iId=9601&iTestingId=16956)                                                          |
| [GenoPro](http://www.genopro.com/newsletters/2007/05/#Wine)                                    | [8822](https://appdb.winehq.org/objectManager.php?sClass=application&iId=8822)                                                                        |
| [expected tech](http://www.expectedtech.com)                                                   | [5751](https://appdb.winehq.org/objectManager.php?sClass=application&iId=5751)                                                                        |
| [Imgburn](http://www.imgburn.com)                                                              | [4625](https://appdb.winehq.org/objectManager.php?sClass=application&iId=4625)                                                                        |
| [Javelin](http://stepaheadsoftware.com/products/javelin/javelin.htm)                           | [5256](https://appdb.winehq.org/objectManager.php?sClass=application&iId=5256)                                                                        |
| [JWPce](http://www.physics.ucla.edu/~grosenth/jwpce.html)                                      | [810](https://appdb.winehq.org/appview.php?iAppId=810)                                                                                                |
| [Lotus Europa Knowledgebase](http://www.lotus-europa.com/knowledgebase.html)                   | (Missing)                                                                                                                                             |
| [LTSpice](http://www.linear.com/designtools/software/ltspice.jsp)                              | [2000](https://appdb.winehq.org/appview.php?iAppId=2000)                                                                                              |
| [Mappy Tile Map Editor](http://www.tilemap.co.uk/mappy.php)                                    | [9281](https://appdb.winehq.org/appview.php?iAppId=9281)                                                                                              |
| [MediaCoder](http://mediacoder.sourceforge.net/)                                               | [5313](https://appdb.winehq.org/appview.php?iAppId=5313)                                                                                              |
| [6785](https://appdb.winehq.org/appview.php?iAppId=6785)                                       |                                                                                                                                                       |
| [Neat Image](http://www.neatimage.com/index-ni32.html)                                         | [1531](https://appdb.winehq.org/appview.php?iAppId=1531)                                                                                              |
| [Neat Video](http://www.neatvideo.com/)                                                        | (Missing)                                                                                                                                             |
| [PCEditor](http://www.pceditor.de/)                                                            | [3900](https://appdb.winehq.org/objectManager.php?sClass=application&iId=3900)                                                                        |
| [Perfect Dark](http://www21.atwiki.jp/botubotubotubotu/)                                       | [6674](https://appdb.winehq.org/appview.php?iAppId=6674)                                                                                              |
| [Reaper](http://www.cockos.com/reaper/)                                                        | [3336](https://appdb.winehq.org/appview.php?iAppId=3336)                                                                                              |
| [RegexBuddy](http://www.regexbuddy.com/wine.html)                                              | [5589](https://appdb.winehq.org/objectManager.php?sClass=application&iId=5589)                                                                        |
| [RosAsm](http://www.rosasm.org/)                                                               | [7569](https://appdb.winehq.org/appview.php?iAppId=7569)                                                                                              |
| [SeiSee SEG-Y file viewer](http://www.dmng.ru/seisview/seisee.en.html)                         | (Missing)                                                                                                                                             |
| [SIMION](http://www.simion.com/info/Linux)                                                     | (Missing)                                                                                                                                             |
| [SIV](http://rh-software.com/)                                                                 | (Missing)                                                                                                                                             |
| [Spotify](http://www.spotify.com/en/download/other/)                                           | [8514](https://appdb.winehq.org/objectManager.php?sClass=application&iId=8514)                                                                        |
| [TreePad](http://www.treepad.com/linux/)                                                       | [696](https://appdb.winehq.org/appview.php?iAppId=696)                                                                                                |
| [The House At Desert Bridge](http://www.jonas-kyratzes.net/?page_id=95)                        | [9282](https://appdb.winehq.org/objectManager.php?sClass=application&iId=9282)                                                                        |
| [utorrent](http://www.utorrent.com/download.php)                                               | [6824](https://appdb.winehq.org/appview.php?iAppId=6824)                                                                                              |
| [VMLAB](http://www.amctools.com/download.htm)                                                  | [5452](https://appdb.winehq.org/appview.php?iAppId=5452)                                                                                              |
| [WavePad](http://nch.com.au/wavepad/index.html)                                                | [4276](https://appdb.winehq.org/objectManager.php?sClass=application&iId=4276)                                                                        |
| [WinTopo](http://www.wintopo.com/linux/)                                                       | (Missing)                                                                                                                                             |
| [xmlspy](http://www.altova.com/support_platform_linux.html)                                    | [505](https://appdb.winehq.org/appview.php?iAppId=505)                                                                                                |
| [zero . nine nine seven](http://www.zero997.com)                                               | [9474](https://appdb.winehq.org/appview.php?iAppId=9474)                                                                                              |

Apps that used to be in the above list:

- [Just Learn Morse Code](https://www.justlearnmorsecode.com/)
  ([4386](https://appdb.winehq.org/objectManager.php?sClass=application&iId=4386)):
  says it works in Wine, but does not say it is 'supported'.
- [Soldat](https://www.soldat.pl/)
  ([964](https://appdb.winehq.org/objectManager.php?sClass=application&iId=964)):
  barely mentions WineX, but doesn't say that it works.
- [StarMoney](https://www.starmoney.de)
  ([2250](https://appdb.winehq.org/appview.php?iAppId=2250)): ended
  support and
  [blamed](https://www.pro-linux.de/NB3/news/1/15259/starmoney-stellt-linux-variante-ein.html)
  Wine for Problems without talking to us.
