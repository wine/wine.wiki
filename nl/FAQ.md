<small>
&nbsp;[:flag_gb: English](FAQ)
&nbsp;[:flag_fr: Français](fr/FAQ)
&nbsp;[:flag_kr: 한국어](ko/FAQ)
</small>

-----

In deze FAQ (Veel gestelde vragen) staan de vragen en antwoorden over
het gebruik van Wine.
Voor de vragen over de ontwikkeling van Wine bekijk de [FAQ voor
ontwikkelaars](Developer-FAQ).

Heel vaak gestelde vragen:

- [Een programma uitvoeren in
  Wine](#hoe-voer-ik-na-de-installatie-een-programma-uit)
- [Wine in een terminal
  uitvoeren](#hoe-start-ik-een-windows-programma-met-de-opdrachtregel)
- [Een programma als root of met sudo
  uitvoeren](#moet-ik-wine-als-root-of-met-sudo-uitvoeren)
- [Een programma
  verwijderen](#hoe-kan-ik-windows-programmas-verwijderen)
- [Een logbestand
  maken](#hoe-krijg-ik-een-debug-logbestand-terminal-uitvoer)
- [Een Wineprefix
  maken](#kan-ik-ook-een-andere-plek-gebruiken-dan-wine)
- [Een 32-bit prefix maken op een 64-bit
  systeem](#hoe-maak-ik-een-32-bit-wineprefix-op-een-64-bit-systeem)

## Algemene vragen

### Wie is er verantwoordelijk voor Wine?

Wine wordt mogelijk gemaakt door allerlei mensen wereldwijd. Ook hebben
de bedrijven [CodeWeavers](https://www.codeweavers.com), Bordeaux,
TransGaming, Corel, Macadamian en Google meegeholpen. Bekijk ook
[medewerkers](Acknowledgements) en
[geschiedenis](Wine-History).

### Is Wine schadelijk voor Linux of andere open besturingssystemen?

Wine zorgt dat de bruikbaarheid van Linux groter wordt. Het wordt voor
gebruikers makkelijker om naar een open/ vrij besturingssysteem over te
stappen en voor Windows ontwikkelaars programma's voor Linux te maken.
Hierdoor stijgt het marktaandeel van Linux wat weer meer
(commerciële)ontwikkelaars naar Linux toehaalt.

### Is Wine een emulator? Hierover verschillen de meningen

Hier is een hoop verwarring over. Voor een groot deel komt dit door de
naam. Veel mensen denken ten onrechte dat Wine staat voor <u>Win</u>dows
<u>e</u>mulator.

Wanneer mensen denken aan een emulator wordt er vaak gedacht aan een
spelcomputer emulator of een virtuele computer. Wine is een
[compatibiliteitslaag](https://en.wikipedia.org/wiki/Compatibility_layer).
Het voert Windows programma's ongeveer op dezelfde manier uit als
Windows dat zelf zou doen. Omdat er niets nagebootst wordt, is geen
prestatie- of snelheidsverlies. Ook hoeft Wine niet opgestart te worden
om een Windows programma uit te voeren.

Wine kan gezien worden als een Windows emulator op dezelfde manier als
dat Windows 10 gebruikt kan worden als een Windows XP emulator. Beide
manieren vertalen de systeemoproepen, zodat het programma wordt
uitgevoerd. Als in Wine de Windows versie op Windows XP wordt gezet, is
dat hetzelfde als de optie "uitvoeren in comptabiliteitsmodus" in
Windows 10.

Andere dingen die Wine meer maken dan een emulator:

- Onderdelen van Wine kunnen ook op Windows worden gebruikt. Sommige
  virtuele computers gebruiken de omzetting van Direct3D naar OpenGL in
  plaats van de 3D hardware na te bootsen.
- De Winelib bibliotheek kan gebruikt worden om Windows programma's om
  te zetten naar andere besturingssystemen die Wine ondersteund.
  Hierdoor kunnen de programma's ook op processoren uitgevoerd worden
  die niet door Windows worden ondersteund.

"Wine is niet alleen een emulator" is nauwkeuriger. Als men Wine ziet
als enkel een emulator, worden alle andere dingen vergeten. De
"emulator" van Wine is eigenlijk gewoon een lader waarmee Windows
programma's kunnen communiceren met de vervangende API van Wine.

### Wat is het verschil tussen Wine, CrossOver, Proton en Cedega?

Wine is de basis. Hier gebeurd het meeste werk. Wine is niet perfect,
maar [heel veel mensen](Usage-Statistics) gebruiken met veel
succes de kale Wine versie om Windows programma's uit te voeren.

CrossOver is een product van CodeWeavers. Het is gebaseerd op Wine met
een paar aanpassingen. Waar Wine om de twee weken een nieuwe versie
uitbrengt, wordt een uitgave van CrossOver door en door getest. Dit om
achteruitgang in de ontwikkeling te voorkomen. Hierdoor blijven de
ondersteunde programma's werken. CodeWeavers heeft veel van de
Wine-ontwikkelaars in dienst en geeft op verschillende gebieden leiding
aan het project. Alle verbeteringen in Wine komen op den duur in
CrossOver terecht.

Proton is ontwikkeld door Valve voor het gebruik in hun eigen Steam
programma. Het bestaat uit verschillende stukken. Een afgeleidde van
Wine, DXVK (voor het vertalen van Direct3D 9/10/11 naar Vulkan),
VKD3D-Proton (de versie van VKD3D van Valve voor de vertaling van
Direct3D 12 naar Vulkan), FAudio en heel veel experimentele
veranderingen om de prestaties van computerspellen te verbeteren. Proton
is alleen gemaakt om in Steam te gebruiken. Omdat bijna alle onderdelen
open source zijn, is het ook mogelijk om niet-Steam software uit te
voeren. Net als bij CrossOver zullen alle veranderingen en verbeteringen
aan Wine uiteindelijk in Proton terecht komen.

Cedega bestaat niet meer. Het was een product van TransGaming. In 2002
gebruikte TransGaming de broncode van Wine om een eigen versie speciaal
voor computerspellen te maken. De ontwikkeling van Cedega en Wine zijn
altijd voor de grootste gedeelte apart van elkaar geweest. TransGaming
heeft maar heel weinig code aan Wine gegeven. Door het verschil in
licenties kwam er geen code van Wine in Cedega. Cedega kon beter omgaan
met verschillende kopieerbeveiligingen. Dit kwam omdat de broncode van
Cedega gesloten is daarom wilden verschillende
kopieerbeveiligingsbedrijven wel hun code delen. Wine is in de jaren
verder doorontwikkeld. Hierdoor werken veel spellen beter dan onder
Cedega. Ook is de Direct3D ondersteuning een stuk beter dan die van
Cedega.

Lees ook de [geschiedenis van Wine](Wine-History).

### Hoe werken de versienummers van Wine?

Lees [Wine van
WineHQ](nl/Wine-User's-Guide#wine-van-winehq).

### Moet ik de opdrachtregel gebruiken?

Gewoonlijk is het in Linux niet nodig om de opdrachtregel (de
commandolijn) te gebruiken. De meeste dingen zijn, net als bij Windows,
in de grafische gebruikersomgeving te doen. In veel gevallen is
rechts-klikken op het installatiebestand en kiezen voor "Openen met
Wine", of dubbelklikken voldoende. Geïnstalleerde programma's kunnen
worden geopend met een snelkoppeling.

Soms zijn er situaties waarin de opdrachtregel nodig is. De meest
voorkomende situatie is om de fouten te bekijken als een programma niet
goed werkt. Een andere situatie is om hulpprogramma's zoals `regedit` te
starten. Deze programma's hebben geen snelkoppeling. (Als het nodig is,
kunnen deze handmatig gemaakt worden).

Onder MacOS is dit niet zo. Hier moet wel alles via de opdrachtregel
gedaan worden, of er moet een [programma van
derden](Third-Party-Applications) gebruikt worden.

### Welke programma's werken goed met Wine?

Duizenden programma's en spellen werken goed. Als een vuistregel kan
worden gebruikt dat simpele of oude programma's vaak goed werken.
Complexe programma's en de laatste versie van een nieuw spel werken
minder goed of niet. Bekijk [de Wine Programma
Database](https://appdb.winehq.org) of het programma werkt. Programma's
met een zilveren, gouden of platina beoordeling werken goed. Bij brons
of vuilnis dan is Wine, voor de meeste gebruikers, nog niet goed genoeg.
Als er nog geen melding met een recente versie van Wine in de Programma
Database staat dan is simpelweg proberen de makkelijkste manier. Werkt
het niet? Dan kan het zijn dat de fout in Wine zit. Vraag hulp op het
[Wine Forum](https://forums.winehq.org/) als het nodig is.

### Op welke manieren kan ik bijdragen aan Wine?

Om de Wine-ontwikkelaars te helpen bij het bereiken van hun doelen kunt
u helpen met programmeren, documenteren, geld of met materieel.

Iedereen kan meehelpen door goede foutrapportages in te sturen aan onze
[Bugzilla](https://bugs.winehq.org/) en de ontwikkelaars te helpen met
eventuele vervolg vragen. Het is onmogelijk en niet praktisch voor de
ontwikkelaars om een kopie te hebben van elk mogelijk programma. Dus
hulp blijft nodig nadat de foutrapportage is ingestuurd. De ontwikkelaar
kan vragen om een oplossing voor het probleem uit te testen. Als de
oplossing werkt dan kan die worden opgenomen in Wine en wordt het
foutrapport gesloten. Uw hulp wordt gewaardeerd door iedereen én het
probleem is opgelost.

Een lijst met andere manieren om bij te dragen staat op de Startpagina
onder het kopje [Meedoen](nl/home#meedoen).

### Ik denk dat ik een fout heb gevonden. Hoe meld ik die?

Foutrapportages kunnen gemeld worden aan onze
[Bugzilla](https://bugs.winehq.org/). Om de rapportage zo goed mogelijk
te maken **lees het Wiki artikel over [fouten](Bugs)**.
Slechte foutrapportages kunnen als ONGELDIG worden bestempeld en worden
gesloten. Het probleem is dan nog niet opgelost. Goede rapportages zijn
essentieel om Wine beter te maken.

Maak geen foutrapport wanneer er een [programma van
derden](Third-Party-Applications) is gebruikt of als er DLL
is overschreven met een variant uit Windows.

### Waar kan ik nog meer hulp krijgen?

Naast deze [Startpagina](nl/home) bekijk de [Wine HQ
Documentatie](nl/Documentation) en het
[forum](https://forums.winehq.org/). Programmeurs die de Winelib
bibliotheek gebruiken en vragen hebben, kunnen kijken op
[wine-devel](nl/Forums).

Doorzoek de [Programma Database](https://appdb.winehq.org) voor hulp met
een specifiek programma. Hier delen gebruikers hun ervaringen, vragen,
tips en trucs.

Het IRC kanaal: [\#WineHQ](irc://irc.libera.chat:6697/#winehq) op
irc.libera.chat. Deskundige gebruikers en vaak ook ontwikkelaars van
Wine zijn hier te vinden. Bekijk [IRC](nl/IRC) voor belangrijke
informatie.

## Wine Installeren

### Wat zijn de systeemvereisten voor Wine?

Wine en het besturingssysteem samen nemen meestal minder schijfruimte in
dan Windows. Als er geen Windows programma wordt uitgevoerd dan heeft
Wine, naast schijfruimte, geen invloed op het systeem.

Als een programma of spel goed loopt op Windows dan zou dat met dezelfde
hardware en de goede, echte stuurprogramma's ook zo onder Wine moeten
zijn. De Linux open broncode stuurprogramma's voor grafische kaarten
(denk aan nouveau) zijn meestal niet goed genoeg om spellen te spelen.
Als er geen Linux stuurprogramma voor het apparaat is, werkt het ook
niet in Wine.

#### Werkt Wine op alle Unix bestandssystemen?

Bijna. Wine is geschreven om bestandssysteem onafhankelijk te zijn.
Windows programma's zouden moeten werken op vrijwel elk volledig UNIX
bestandssysteem. De uitzondering is dat niet alle bestandssystemen of
stuurprogramma's alle functies hebben die FAT32 of NTFS hebben. Eén
voorbeeld is het ntfsv3 stuurprogramma. Deze heeft geen ondersteuning
voor shared-write mmap. Deze functie die gebruikt wordt door Steam kan
dus niet nagedaan worden.

Wat ook kan gebeuren is dat enkele programma's *beter* werken op
bestandssystemen die niet hoofdletter gevoelig zijn.

#### Werkt Wine alleen met de X-server?

Tot voor kort stonden projecten zoals Wayland of andere serieuze
alternatieven voor de x11drv nog in de kinderschoenen. De ontwikkeling
van Wine was dus ook gericht op X. Echter is de gebruikersomgeving van
Wine zo gemaakt dat het ondersteunen van andere grafische systemen te
doen moet zijn.

### Welke versie van Wine moet ik gebruiken?

**Kort antwoord:** Gebruik de versie die voor het Windows programma het
beste werkt. Vaak zal dit de laatste ontwikkelingsversie zijn.

**Lang antwoord:** De ontwikkeling van Wine gaat snel. Bijna elke twee
weken komt er een nieuwe versie uit. Gewoonlijk zal de laatste versie de
beste zijn. Soms gebeurd het dat er een stukje code wordt veranderd om
het ene programma te laten werken, maar waardoor een ander programma
niet meer werkt. (Een achteruitgang in de ontwikkeling.) Ook kan het
voorkomen dat er een nieuwe functie aan Wine wordt toegevoegd. Deze
functie is misschien nog niet helemaal af of is nog niet uitvoerig
getest.

Als vuistregel kan worden gebruikt: begin met de Wine versie die meekomt
met het besturingssysteem. Werkt deze versie? Dan is er geen probleem.
Werkt deze niet? Installeer dan een nieuwere versie. Meestal is de
ontwikkelingsversie een goed idee, maar doorzoek eerst [de Programma
Database](https://appdb.winehq.org) en
[Bugzilla](https://bugs.winehq.org) of er nieuwe fouten gevonden zijn.
Een foutrapport op Bugzilla met de melding STAGED houdt in dat er een
oplossing is in de wine-staging versie. (Dit is de experimentele versie
van Wine). Installeer dan de laatste wine-staging versie.

De stabiele versie van Wine wordt weinig aangepast. Alleen herstelde
fouten die zeker weten geen nieuwe fouten veroorzaken worden ingevoegd.
Vandaar ook de term "stabiel". Gebruikers die de ontwikkelings- of
staging-versie gebruiken, kunnen hetzelfde effect bereiken door de
versie niet meer bij te werken. Bij het gebruik van de stabiele versie
is het alleen mogelijk om een melding te maken in de Programma Database.
Gebruikers die hulp zoeken op het forum of via IRC wordt gevraagd om
eerst de nieuwste ontwikkelingsversie te proberen.

De huidige stabiele, ontwikkeling en staging versies zijn te vinden op
de [WineHQ pagina's](https://www.winehq.org). Lees [Wine van
WineHQ](nl/Wine-User's-Guide#Wine-van-WineHQ) voor meer
informatie over de drie versies en hun versienummers.

### Hoe installeer ik Wine?

- Gebruik de kant en klare pakketten. Op de [Download](nl/Download)
  pagina staat meer informatie.
- [Bouw Wine](Building-Wine) vanaf de broncode.

### Ik krijg een foutmelding dat Wine niet geïnstalleerd kan worden, omdat sommige afhankelijkheden niet opgelost kunnen worden.

Is het probleem hiermee niet opgelost dan moet handmatig gekeken worden
welk pakket de boosdoener is. Probeer het pakket te installeren welke in
de foutmelding naar boven komt. Dit pakket zal waarschijnlijk een ander
pakket de schuld geven waarom het niet geïnstalleerd kan worden. Probeer
dan dat pakket te installeren. Net zo lang totdat de fout gevonden is.

Een veel voorkomend probleem op Ubuntu is het gebruik van PPA's. De
pakketten hieruit zijn niet van Ubuntu. Hierdoor kan een versienummer
van een pakket te nieuw zijn voor Wine. Of de PPA heeft alleen de
64-bits pakketten. De afhankelijkheden van Wine moeten *multiarch* zijn
en de versienummers van de 32 en 64-bits pakketten moeten precies
hetzelfde zijn. De makkelijkste oplossing is dan ook vaak om het pakket
uit de PPA om te ruilen door het pakket uit Ubuntu. Een andere oplossing
is om op zoek te gaan naar het missende pakket.

### Ik kan het Wine pakket dat door mijn distributie is gemaakt niet installeren.

WineHQ geeft alleen ondersteuning op de pakketten die te vinden zijn
op de [download](nl/Download) pagina. Neem contact op met de
distributie voor hulp bij hun pakketten.

### Kan ik verschillende versies van Wine naast elkaar installeren?

Ja, dat kan. Eén manier is om Wine zelf vanaf de broncode te bouwen.
Gebruik hierbij de `--prefix` optie om een andere installatiefolder op
te geven.

```sh
./configure prefix=pad_naar_de_installatie_map && make
```

En installeer het met:

```sh
sudo make install
```

Op Linux is het waarschijnlijk ook nodig om LD_LIBRARY_PATH te
veranderen

```sh
export LD_LIBRARY_PATH="$PREFIX/lib:$LD_LIBRARY_PATH
```

Op Debian en Ubuntu kunnen de stabiele-, ontwikkelings- en
staging-versie naast elkaar geïnstalleerd worden met de pakketten van
WineHQ. Een volledige installatie van Wine bestaat uit vier pakketten:

|                    |                                                                                               |
|--------------------|-----------------------------------------------------------------------------------------------|
| winehq-<soort>     | Het hoofdpakket. Zorgt voor het `wine` commando en installeert het pakket hieronder.          |
| wine-<soort>       | Zorgt voor de installatie in `/opt/wine-`<soort> en installeert de twee pakketten hier onder. |
| wine-<soort>-amd64 | Alle bestanden en afhankelijkheden die een 64-bits installatie nodig heeft.                   |
| wine-<soort>-i386  | Alle bestanden en afhankelijkheden die een 32-bits installatie nodig heeft.                   |

Door alleen de laatste drie pakketten te installeren, is er een werkende
Wine installatie. Alleen werkt het commando `wine` niet, maar moet het
volledige pad gebruikt worden. Bijvoorbeeld:

```sh
/opt/wine-devel/bin/wine programma.exe
```

Een andere manier om verschillende Wine versies naast elkaar te
gebruiken, is door het gebruik van software van derden.

Gebruik voor elke Wine versie of soort een eigen wineprefix.

### Is er een 64-bit versie van Wine

Sinds versie 1.2 is er op Linux een 64-bit versie. De meeste
distributies hebben hier pakketten van gemaakt of bekijk onze
[Download](nl/Download) pagina. Als Wine vanaf de broncode
wordt gebouwd, lees dan de [handleiding](Building-Wine) voor
de instructies hoe een WoW64 installatie gemaakt kan worden.

Een paar opmerkingen:

- De 32-bits Wine installatie werkt op 32 en 64-bits Linux/Unix
  installaties. 16 en 32-bit Windows programma's werken hierop.
- De 64-bits Wine installatie werkt alleen op 64-bits systemen. Om een
  nuttige installatie te hebben zijn ook de 32-bits bibliotheken nodig.
  (De meeste Windows installatie programma's zijn 32-bits). 32 en 64-bit
  Windows programma's werken hierop.
- Een pure 64-bits Wine installatie zonder de 32-bits bibliotheken wordt
  niet ondersteund.

### Waar kan ik oude versies van Wine vinden?

Oude versies van WineHQ staan op onze [WineHQ
server](https://dl.winehq.org/wine-builds).

### Hoe kan ik Wine installeren op een netbook (eeePC, Acer Aspire One, enz.)?

Bij het gebruik van Xandros, Linpus of een andere door de fabrikant
aangepaste Linux distributie zal u contact moeten leggen met fabrikant.
Is de distributie vervangen door een gangbare Linux distributie dan kunt
u de pakketten van de distributie gebruiken.

### Installatie op een Apple computer

#### Hoe installeer ik Wine op mijn Mac?

- **[WineHQ Paketten](MacOS)** zijn beschikbaar voor MacOS
  10.8 t/m 10.14.
- Maak en installeer Wine met [Homebrew](https://brew.sh) of
  [MacPorts](https://www.macports.org). Een installatie met MacPorts
  zorgt ook voor alle afhankelijkheden.
- Er zijn ook [programma's van
  derden](Third-Party-Applications) die Wine kunnen
  installeren. Bijvoorbeeld [CrossOver
  Mac](https://www.codeweavers.com/crossover/)
- Na een installatie van Linux op uw Mac, zijn de pakketten van WineHQ
  te gebruiken. Lees de [Download](nl/Download) pagina voor
  meer informatie.

#### Kan ik Wine ook op een oude Mac (zonder chip van Intel) installeren?

Nee dat kan niet. Oude Mac computers hebben een PowerPC processor. Deze
processor snapt geen x86 code. Hiervoor is een emulator nodig en Wine
heeft geen processor emulatie. Er is een poging gedaan door het DarWine
project, maar daar is al een paar jaar niks meer van gehoord.

## Wine in elkaar zetten

### Hoe bouw ik Wine vanaf de broncode?

Lees [Wine bouwen vanaf de broncode](Building-Wine).

### Hoe voeg ik een *patch* toe?

Hiervoor moet Wine vanaf de broncode worden opgebouwd. Zie hierboven.

## Verwijderen

### Hoe verwijder ik Wine?

**Het verwijderen van Wine zorgt er <u>niet</u> voor dat de Windows
programma's of instellingen gewist worden. Deze staan in de thuismap.**
Voor het verwijderen hiervan, lees [Hoe verwijder ik een virtuele
Windows
installatie?](#hoe-verwijder-ik-een-virtuele-windows-installatie).

- Is de installatie gedaan door een pakketbeheerder? Gebruik dan deze
  ook voor de verwijdering.
- Is de installatie gedaan met `make install`? Gebruik dan
  `make uninstall`.

### Hoe kan ik Windows programma's verwijderen?

Dit kan met de opdracht `wine uninstaller` of met *Sotware* in het
configuratiescherm van Wine. Helaas werkt dit nog niet altijd helemaal
goed. Hieronder meer informatie over het weghalen van **alle** Windows
programma's.

### Hoe maak ik de lijst Openen Met weer netjes?

Voer de volgende opdrachten uit om de "Openen met..."-lijst weer vrij te
maken van alle Windows programma's.

```sh
rm -f ~/.local/share/mime/packages/x-wine*
rm -f ~/.local/share/applications/wine-extension*
rm -f ~/.local/share/icons/hicolor/*/*/application-x-wine-extension*
rm -f ~/.local/share/mime/application/x-wine-extension*
```

### Hoe verwijder ik een virtuele Windows installatie?

De installatie van de virtuele Windows omgeving staat standaard in de
verborgen `$HOME/.wine` map. Verwijder deze map om alle geïnstalleerde
programma's en instellingen te verwijderen.
Een andere naam geven, om een reservekopie te hebben, kan natuurlijk
ook.

Hoewel nu all Windows programma's zijn verdwenen, staan alle
snelkoppelingen nog wel op de computer. Deze kunnen weggehaald worden
met:

```
rm -f ~/.config/menus/applications-merged/wine*
rm -rf ~/.local/share/applications/wine
rm -f ~/.local/share/desktop-directories/wine*
rm -f ~/.local/share/icons/????_*.{xpm,png}
rm -f ~/.local/share/icons/*-x-wine-*.{xpm,png} }}}
```

Ook is er een [optie om te zorgen dat Wine deze pictogrammen niet
maakt](#hoe-zorg-ik-ervoor-dat-wine-niet-de-bestandsassociaties-verandert-of-ongewenste-pictogrammen-maakt).

## Windows programma's installeren en uitvoeren

### Ik heb al veel programma's in Windows geïnstalleerd. Hoe voer ik deze uit met Wine?

Kort antwoord: [Installeer alle programma's opnieuw met
Wine](nl/Wine-User's-Guide#windows-programmas-installeren-en-uitvoeren).

Lang antwoord: Sommige programma's kunnen vanuit Windows naar Wine
worden gekopieerd. Dit is alleen aan te raden voor ervaren gebruikers.
Of als er bekent is dat het programma blijft werken ("Portable
software").

**Wine is niet gemaakt om samen te werken met een bestaande Windows
installatie. Gebruik de bestandsbeheerder om de bestanden die nodig zijn
te kopiëren van Windows naar Wine.**

Probeer niet een echte Windows C-schijf als C-schijf in Wine te
gebruiken. **Dit maakt Windows onherstelbaar kapot.** We hebben dit zo
moeilijk mogelijk gemaakt, zodat dit niet per ongeluk kan gebeuren. Lukt
dit toch, dan werkt Wine waarschijnlijk niet goed en Windows is zeker
onherstelbaar kapot. Wine overschrijft namelijk belangrijke Windows
bestanden met een Wine variant. Een herinstallatie van Windows is dan de
enige oplossing.

### Hoe installeer ik een programma in Wine?

Lees [Windows programma's installeren en
uitvoeren](nl/Wine-User's-Guide#windows-programmas-installeren-en-uitvoeren).

### Hoe voer ik na de installatie een programma uit?

Lees [Windows programma's installeren en
uitvoeren](nl/Wine-User's-Guide#windows-programmas-installeren-en-uitvoeren).

### Hoe start ik een Windows programma met de opdrachtregel?

Lees [Programma's uitvoeren vanaf de
opdrachtregel](nl/Wine-User's-Guide#windows-programmas-uitvoeren-vanaf-de-opdrachtregel).

### Hoe kan ik een opdrachtregelparameter gebruiken?

Lees [Opdrachtregelparameters
gebruiken](nl/Wine-User's-Guide#windows-programma-argumenten-doorgeven).

### Hoe kan ik een `.msi` bestand installeren?

Lees [.msi bestanden
uitvoeren](nl/Wine-User's-Guide#msi-bestanden-uitvoeren).

### Hoe kan ik een ClickOnce (`.application`) gebruiken?

Zoek eerst welke versie van het *.NET framework* nodig is voor het
programma. Gebruik *[winetricks](Winetricks)* om deze versie
te installeren. Daarna kan het programma uitgevoerd worden met:

```sh
cd naar_de_map_met_het_.application_bestand
wine start naam_programma.application
```

### Hoe start ik een Linux programma vanuit Windows?

Open het register (`regedit`) en voeg een punt toe aan de lijst van
extensies in
`HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment\PATHEXT`.

Dit moet na elke update van Wine opnieuw worden ingesteld.

Nu kunnen Linux programma's gestart worden met **wine cmd**,
bijvoorbeeld:

```sh
wine cmd /c /usr/bin/glxgears
```

of

```sh
wine cmd /c /bin/sh -c glxgears
```

Misschien is ook `winepath` nodig om een Windows-pad om te zetten in
een Linux-pad. (Lees ook: [Hoe kan ik een Linux programma koppelen aan
een bestandsformaat in
Wine?](#hoe-kan-ik-een-linux-programma-koppelen-aan-een-bestandsformaat-in-wine))

### Het installatieprogramma zegt dat ik geen schijfruimte meer heb.

Gewoonlijk is er ook geen ruimte meer. De C-schijf van Wine staat in de
thuismap. De partitie waar `/home` is moet voldoende ruimte beschikbaar
hebben. Controleer dit met:

```sh
dh -h ~
```

Als er minder dan 1 GB ruimte nodig is en de `df` meldt meer dan 1 GB
vrije ruimte, zet dan de Windows versie naar *Windows 98*. Er zit
namelijk een fout in oude installatieprogramma's (Windows 98-tijdperk)
waardoor ze niet goed om kunnen gaan met grote schijven.

Als een andere partitie nog wel genoeg ruimte heeft, dan kan daar een
andere wineprefix worden gemaakt. Lees [\#Hoe kan ik twee programma's
uitvoeren alsof ze op verschillende computers
staan?](#hoe-kan-ik-twee-programmas-uitvoeren-alsof-ze-op-verschillende-computers-staan)
voor meer informatie.

### Wanneer ik dubbel-klik op een `.exe` bestand gebeurd er niets.

Probeer eerst het programma te starten via een ander pictogram in het
Wine menu of op het bureaublad. Of klik-rechts op de `.exe` en kies voor
*Openen met Wine*.

Ook kan het zijn dat het programma niet goed opstart in Wine. Voer het
programma uit in de opdrachtregel om de fouten te bekijken. Lees
[Programma's uitvoeren vanaf de
opdrachtregel](nl/Wine-User's-Guide#windows-programmas-uitvoeren-vanaf-de-opdrachtregel)
voor meer informatie.

### Wanneer ik dubbel-klik op een `.exe` bestand komt de melding: The file programma.exe is not marked as executable..."

Als in het venster staat "Read about the executable bit", met een
verwijzing. Volg de verwijzing voor meer informatie.

Staat het programma op een harde schijf, selecteer het bestand met de
rechter muisknop en kies voor Eigenschappen/Rechten en selecteer
"Uitvoeren van het bestand toestaan".

Staat het programma op een CD-ROM, gebruik dan de opdrachtregel. Gebruik
`mount` om de CD-ROM opnieuw aan te koppelen met *alle bestanden
toestaan uitgevoerd te worden*.

(Verander /media/cdrom als dat nodig is)

```sh
mount -o remount,mode=0777,exec /media/cdrom
```

## Wine gebruiken

### Hoe start ik Wine?

Wine is geen programma dat los gestart kan worden. Lees [Windows
programma's installeren en
uitvoeren](nl/Wine-User's-Guide#Windows-programma's-installeren-en-uitvoeren)
voor meer informatie. Om een Windows bureaublad na te doen, gebruik:
`wine explorer /desktop=shell`

### Moet ik Wine als root of met sudo uitvoeren?

**Wine NOOIT als root uitvoeren!**
Op deze manier kan een Windows programma of virus alle onderdelen van de
computer bereiken. Met het sudo-commando komt daar ook nog bij dat de
toegang tot de bestanden in de Wineprefix overhoop wordt gehaald. Lees
hieronder hoe de bestandsrechten weer goed gezet kunnen worden. Gebruik
Wine altijd met een gewone gebruiker.

Alle programma's die met Wine worden uitgevoerd, worden als de Windows
gebruiker *Administrator* uitgevoerd. Mocht een programma toch een fout
geven over een gebrek aan rechten, maak een foutrapport. Het programma
uitvoeren als root helpt niet. (Er is al een
fout #40613 gemeld dat een
programma juist te veel rechten heeft.)

### Ik heb toch root of sudo gebruikt. Wat nu?

Alle bestandsrechten in `$HOME/.wine` moeten weer goed gezet worden. In
de verborgen map `$HOME/.wine` staat de virtuele Windows installatie, de
configuratie van Wine en het Windows register. Op de volgende manier
kunnen de rechten weer goed gezet worden:

```sh
cd $HOME
sudo chown -R $USER:$USER .wine
winecfg
```

### Welke versie van Wine gebruik ik?

De versie van Wine kan gevonden worden met `wine --version`. Of in
`winecfg` op het tabblad "Over Wine"

**TIP:** Op de pagina van [WineHQ](https://www.winehq.org) staat de
nieuwste versie. Elke twee weken komt een nieuwe versie uit. Hierdoor is
de versie die het besturingssysteem aanbied vaak al snel oud. Op de
[Download](nl/Download) pagina staat hoe de nieuwste versie
geïnstalleerd kan worden.

### Wineprefix

#### Waar is mijn C-schijf?

Wine gebruikt in plaats van de echte C-schijf een virtuele C-schijf. De
map waar deze staat heet een 'wineprefix'.

De standaard plek is in de thuismap. `~/.wine/drive_c` (Voor MacOS lees
[macOS FAQ](MacOS-FAQ).)

Lees ook over de [`WINEPREFIX`](nl/Wine-User's-Guide#wineprefix)
variabele om de standaard prefix te veranderen.

#### Kan ik ook een andere plek gebruiken dan ~/.wine?

Ja, dat kan. `~/.wine` is de standaard plek, maar iedere plek waar de
gebruiker eigenaar van is en volledige toegang tot heeft kan gebruikt
worden.

De prefix kan veranderd worden met de `WINEPREFIX` variabele.
Bijvoorbeeld:

```sh
export WINEPREFIX=~/nieuwe-wine-prefix
wine winecfg
```

De map *nieuwe-wine-prefix* mag niet bestaan. De nieuwe prefix in
`~/nieuwe-wine-prefix` wordt automatisch door Wine gemaakt.

Om weer de standaard wineprefix te gebruiken, gebruik het commando
`unset WINEPREFIX`. Of zet de `WINEPREFIX` variable naar `~/.wine`

Ook kan de `WINEPREFIX` variable elke keer los met Wine gebruikt worden.

```sh
WINEPREFIX=~/andere-wine-prefix wine winecfg
WINEPREFIX=~/andere-wine-prefix wine notepad
```

Een prefix kan gekopieerd, hernoemd, verplaatst of verwijderd worden
zonder dat een andere prefix hier last van heeft.

Elke keer als in de Wiki `~/.wine` of `$HOME/.wine` staat dan kan dit
vervangen worden door `WINEPREFIX=<map>`.

#### Hoe maak ik een 32-bit wineprefix op een 64-bit systeem?

Sommige programma's werken beter in een 32-bit prefix. Om een 32-bit
wineprefix te maken moet de `WINEARCH` variabele gebruikt worden.

```sh
WINEARCH=win32 WINEPREFIX=~/32-bit-prefix wine winecfg
```

Een bestaande 64-bit prefix kan niet veranderd worden in een 32-bit. Er
moet dus een nieuwe aangemaakt worden. Als de prefix eenmaal is
aangemaakt dan hoeft de variabele `WINEARCH` niet meer gebruikt te
worden. De variabele `WINEPREFIX` moet **wel** gebruikt worden om naar
de 32-bit prefix te verwijzen.

```sh
WINEPREFIX=~/32-bit-prefix wine setup.exe
```

#### Waarom is Windows XP de oudste versie in een 64-bit prefix?

Vanaf Wine 9.0-rc3 is het mogelijk om alle Windows versies te
selecteren. Het maakt niet meer uit of de wineprefix 32- of 64-bit is.

Voor oudere versies van Wine is het noodzakelijk om een 32-bit prefix
te maken. Lees [deze
handleiding](#hoe-maak-ik-een-32-bit-wineprefix-op-een-64-bit-systeem)
om een 32-bit prefix aan te maken. Hierna is het mogelijk om versies
van Windows ouder dan Windows XP te kiezen in `winecfg`.

#### Hoe kan ik twee programma's uitvoeren alsof ze op verschillende computers staan?

Voorbeeld: Er is een server en een gast. De één werkt niet als de andere
wordt uitgevoerd.

Met twee wineprefixes worden twee aparte Windows computers nagedaan.

Start het eerste programma op de standaard manier:

```sh
wine server.exe
````

Het tweede programma heeft zijn eigen prefix nodig. Gebruik dus de
`WINEPREFIX` variabele:

```sh
WINEPREFIX=~/gast-prefix wine gast.exe
```

De programma's "*server.exe*" en "*gast.exe*" kunnen zelfs hetzelfde
programma zijn. Dus het volgende werkt ook

```sh
wine server.exe
WINEPREFIX=~/gast-prefix wine server.exe
```

Nu worden er twee aparte server programma's uitgevoerd.

### Configuratie

#### Hoe krijg ik een virtueel bureaublad?

Dit kan met [winecfg](Commands/winecfg). Selecteer in het tabblad
"Grafisch" "Emuleer een virtueel bureaublad" en stel bij "Afmetingen"
de grootte in.

Dit kan ook op de opdrachtregel.

```sh
wine explorer /desktop=naam,1024x768 programma.exe
```

Hierbij is *naam* de naam van het bureaublad venster en *programma.exe*
de naam van het programma dat uitgevoerd moet worden. Wanneer als *naam*
*shell* gekozen wordt, dan wordt het bureaublad met taakbalk en start
menu getoond.

```sh
wine explorer /desktop=shell,1024x768
```

#### Hoe verander ik het register?

Het register van Wine staat opgeslagen in de `.reg` bestanden in de
hoofdmap van de wineprefix. Het wordt **niet** aangeraden om deze
bestanden met een tekstverwerker of het kladblok te veranderen. Gebruik
altijd `regedit`

```sh
wine regedit
```

Dit programma is gelijk aan de Windows variant. Ook het importeren en
exporteren van register bestanden wordt ondersteund. Probeer **niet**
het hele Windows register te importeren. Dit zal de wineprefix kapot
maken.

Ook wordt het commando `reg` ondersteund. Gebruik de `/?` parameter voor
meer informatie.

```sh
wine reg /?
```

#### Hoe kan ik een Linux programma koppelen aan een bestandsformaat in Wine?

Hiervoor zijn twee manieren. De eerste manier gebruikt `winebrowser` de
andere in een shell-script.

In het voorbeeld hieronder wordt `winebrowser` gebruikt om het standaard
PDF-programma op de computer te openen als in Wine een PDF bestand wordt
geopend.

Maak een tekstbestand aan met de onderstaande gegevens en sla deze op
als `pdf.reg`

```
[HKEY_CLASSES_ROOT\.pdf]
@="PDFfile"
"Content Type"="application/pdf"
[HKEY_CLASSES_ROOT\PDFfile\Shell\Open\command]
@="winebrowser \"%1\"" 
```

Importeer het `.reg` bestand in het register met

```sh
wine regedit pdf.reg
```

De andere manier is om een shell-script te maken die naar een ander
programma verwijst. Sla het onderstaande op als `run_linux_program` in
`$HOME/bin` (waarschijnlijk moet deze map worden aangemaakt).

```sh
#!/bin/sh $1
wine winepath -u "$2"
```

Vergeet niet om dit bestand uitvoerbaar te maken met
`chmod a+x $HOME/bin/run_linux_program`. En dat de `$HOME/bin` map in de
`$PATH` variabele staat (In Debian is dit standaard het geval als de
`$HOME/bin` map bestaat en de gebruiker opnieuw is aangemeld).

In dit voorbeeld laten we .pdf bestanden openen met het Linux programma
`acroread`. Sla het op als `pdf.reg` en importeer het met
`wine regedit pdf.reg`

```
[HKEY_CLASSES_ROOT\.pdf]
@="PDFfile"
"Content Type"="application/pdf"
[HKEY_CLASSES_ROOT\PDFfile\Shell\Open\command]
@="/bin/sh run_linux_program acroread \"%1\""
```

Het script kan voor andere programma's hergebruikt worden. Om
bijvoorbeeld .doc bestanden in LibreOffice (`soffice`) te openen,
gebruik:

```
[HKEY_CLASSES_ROOT\.doc]
@="DOCfile"
"Content Type"="application/msword"
[HKEY_CLASSES_ROOT\DOCfile\Shell\Open\command]
@="/bin/sh run_linux_program soffice \"%1\""
```

#### Hoe zorg ik ervoor dat Wine niet de bestandsassociaties verandert of ongewenste pictogrammen maakt?

Vanaf Wine 3.14 heeft `winecfg` de optie "Toestaan dat
bestandsassociaties worden gemaakt" in het tabblad "Desktop Integratie"

Voor oudere versies van Wine of als ook voorkomen moet worden dat er
pictogrammen worden gemaakt, schakel `winemenubuilder.exe` uit. Dit kan
op verschillende manieren:

- *Met winecfg:* Voordat er programma's worden geïnstalleerd, open
  `winecfg`. Ga naar het tabblad "Bibliotheken". Type onder "Nieuwe
  DLL aanpassing" `winemenubuilder.exe` (deze staat niet in de
  keuzelijst) en klik op "Toevoegen". Selecteer het in het vak
  "Bestaande aanpassingen" en klik op "Bewerken". Kies hier voor
  "Uitzetten", daarna op "OK" en vervolgens op "Toepassen".

- *Met een registerbestand:* De bovenstaande manier kan nogal lastig
  zijn als die vaak moet worden uitgevoerd. Een makkelijkere manier is
  om een tekstbestand te maken met een .reg extensie (bijvoorbeeld
  `stop-winemenubuilder.reg`) met de volgende inhoud:

  ```
  [HKEY_CURRENT_USER\Software\Wine\DllOverrides]
  "winemenubuilder.exe"=""
  ```
  Importeer dit in het register met

  ```sh
  wine regedit stop-winemenubuilder.reg
  ```

- *Met een variabele:* Gebruik de variabele `WINEDLLOVERRIDES` als een
  programma wordt geïnstalleerd. Bijvoorbeeld:

  ```sh
  WINEDLLOVERRIDES=winemenubuilder.exe=d wine setup.exe
  ```

  Mensen die vaak een nieuwe wineprefix aanmaken, kunnen
  `WINEDLLOVERRIDES=winemenubuilder.exe=d` aan .bashrc toevoegen.

#### Hoe schakel ik het crash venster uit?

Standaard staat het crash venster aan. Sommige gebruikers hebben een
programma die op de achtergrond een fout veroorzaakt, maar voor de rest
goed werkt. Dan kan het venster ongewenst zijn.

De makkelijkste manier om het venster uit te zetten is met
[winetricks](Winetricks):

```sh
sh winetricks nocrashdialog
```

Liever met de hand? Maak dan een tekstbestand aan met een .reg extensie
(bijvoorbeeld `crashdialog.reg`) met de volgende inhoud:

```
[HKEY_CURRENT_USER\Software\Wine\WineDbg]
"ShowCrashDialog"=dword:00000000
```

Importeer dit bestand in het Wine register met:

```sh
wine regedit crashdialog.reg
```

Om het venster weer aan te zetten, verander 00000000 in 00000001 en
importeer het bestand opnieuw.

Dit kan natuurlijk ook handmatig in `regedit`.

#### Hoe maak ik de lettertypen anti-aliased?

Sinds Wine 1.1.12 is er ondersteuning voor subpixel lettertype
rendering, maar misschien staat het niet aan. Gebruik `winetricks` en
kies één van de volgende opties: fontsmooth-gray, fontsmooth-rgb of
fontsmooth-bgr

#### Hoe verander ik de DPI (lettertypegrootte)?

Gebruik als eerste manier `winecfg` (tabblad "Grafisch"). Schuif de
"Schermresolutie" balk naar de gewenste grootte. De veranderingen worden
pas zichtbaar nadat `winecfg` opnieuw wordt opgestart.

Als `winecfg` niet bruikbaar is, lees dan [De lettertypes in Wine zijn
enorm groot](#de-lettertypes-in-wine-zijn-enorm-groot).

#### Hoe stel ik een proxy in?

Gebruik de `http_proxy` variabele om voor alle HTTP verbindingen een
proxy server te gebruiken. Op de meeste Linux systemen wordt dit
automatisch gedaan als de proxy wordt ingesteld met de Network Proxy
tool.

Ook kan het register worden gebruikt om een proxy in te stellen. Er zijn
verschillende plekken voor "wininet.dll" en "winhttp.dll".

Voor wininet, gebruik `regedit` en voeg de onderstaande waardes toe
aan de sleutel
`[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet
Settings]`:

```
"ProxyEnable"=dword:00000001
"ProxyServer"="proxy-server-address:port"
```

`wininet.dll` in Wine heeft nog geen "Automatische Proxy Configuratie
(PAC)" ondersteuning.

Voor winhttp, gebruik het programma `proxycfg.exe` om het register aan
te passen. Dit programma staat in de `system32` map van een Windows
installatie.

## Beperkingen en Risico's

### Kan Wine in tekenmodus (zonder GUI) worden uitgevoerd?

De ontwikkeling van Wine is voor het grootste gedeelte gericht op
grafische programma's, maar in beperkte mate is er ondersteuning door
middel van een "null" stuurprogramma. Wine gebruikt automatisch het
"null" stuurprogramma als de x11driver niet is geladen, maar zelfs dan
is Wine afhankelijk van de xorg bibliotheken.

Het "null" stuurprogramma werkt alleen voor "echte" console programma's
die geen enkele schermfunctie gebruiken.

Lees [Text programma's CUI: Console User
Interface)](nl/Wine-User's-Guide#tekst-mode-programmas)
voor meer informatie.

### Kan ik een programma voor meerdere gebruikers installeren?

Een wineprefix kan niet gedeeld worden met meerdere gebruikers. Het
register kan corrupt raken als verschillende wineservers hetzelfde
registerbestand gebruiken (fout #11112).

Een wineprefix kan wel gekopieerd worden. Installeer alles wat nodig is
in een prefix en kopieer deze prefix naar de thuismap van de
verschillende gebruikers. Op deze manier hoeven niet elke keer alle
programma's voor iedereen geïnstalleerd te worden.

### Kan ik Wine gebruiken om stuurprogramma's te installeren?

Nee. De hardware moet al door het besturingssysteem worden herkent.

### Kan ik een virus of malware via Wine krijgen?

Ja, dat kan helaas. Wine voert ook virussen, Trojaanse paarden en andere
vormen van malware uit.

Er zijn een paar dingen die het risico verkleinen:

- Voer geen bestanden uit van onbetrouwbare websites. [Een besmetting is
  al een keer
  voorgekomen.](https://www.winehq.org/pipermail/wine-devel/2007-January/053719.html)
- Klik niet zomaar op verwijzingen in e-mail programma's en webbrowsers.
- Gebruik geen root of sudo met
  [Wine](#moet-ik-wine-als-root-of-met-sudo-uitvoeren).
- Gebruik een virusscanner. Bijvoorbeeld
  [ClamAV](https://www.clamav.net). Helaas geeft geen enkele
  virusscanner 100% bescherming.
- Verdachte programma's kunnen worden uitgevoerd als een andere
  gebruiker of in een virtuele computer.
- De Z-schijf van Wine weghalen geeft niet echt meer veiligheid. Het
  houdt Windows programma's niet tegen om de harde schijf te lezen, maar
  voorkomt wel dat u makkelijk bij uw bestanden komt.

### Hoe goed kan Wine Windows programma's afschermen (sandbox)?

Een kort antwoord: niet. **Wine biedt geen enkele bescherming.** Overal
waar de gebruiker bij kan, kan ook een programma ook bij. Wine zal (en
kan niet) een programma stoppen om de bestanden op de computer van de
gebruiker overhoop te halen.

Om een programma goed af te schermen is AppArmor, SELinux of een
virtuele computer nodig.

De optie `sandbox` van [winetricks](Winetricks) haalt alleen
de verwijzingen naar de mappen van de gebruiker en die van de Z-schijf
weg. Dit biedt bescherming tegen fouten van de gebruiker en niet tegen
kwaadaardige programma's.

## Programma's

### Zijn er programma's of services die een conflicten geven met Wine?

Compiz, Beryl en soortgelijke kunnen problemen veroorzaken. Schakel deze
uit.

Ook zijn er fouten bekent met toetsenbord hulpprogramma's zoals IBUS,
`xneur` (`gxneur`, `kxneur`) en SCIM.

### Wordt .Net ondersteund? Moet ik .Net installeren?

Tegenwoordig zal Wine als er een prefix wordt gemaakt, vragen om
[wine-mono](Wine-Mono) te installeren. Voor veel (oude) .Net
programma's is dit voldoende.

Maak een foutrapport als wine-mono niet werkt en probeer .Net te
installeren met [winetricks](Winetricks). Vaak werkt .Net
beter in een 32-bit prefix.

Er zijn problemen met .Net 4.5 in Wine 6.0.

### Werkt DirectX? Moet ik DirectX installeren?

Tegenwoordig zit DirectX ingebouwd in Wine. Mocht de ingebouwde versie
niet werken gebruik dan [winetricks](Winetricks) om Windows
onderdelen van DirectX te installeren. Of probeer `dxvk` (DirectX naar
Vulkan).

### Werkt Internet Explorer?

Wine gebruikt de kern van Firefox om Internet Explorer na te doen.
(wine-gecko). In veel gevallen is dit voldoende. Oudere versies van
Internet Explorer (6 t/m 8) kunnen met
[winetricks](Winetricks) worden geïnstalleerd. Nieuwere
versies werken niet.

### Waarom werkt Wine niet met sommige kopieerbeveiligingen?

Kopieerbeveiligingen gebruiken smerige trucjes om te kijken of een
schijf echt is. Sommige van deze systemen zitten zo in elkaar dat Wine
bijna een rootkit zou moeten worden om ze te laten werken.
Waarschijnlijk wordt dit dus nooit ondersteund. Sommige gebruikers
gebruiken daarom een "crack" om de kopieerbeveiliging te omzeilen, maar
die manier kan Wine natuurlijk niet ondersteunen.

Lees de pagina [kopieerbeveiliging](Copy-Protection) voor
meer informatie.

### Ik heb een handleiding op een webpagina gevolgd en het werkt niet.

Op internet staan heel veel handleidingen. Vaak wordt de handleiding
niet meer bijgewerkt en is dus niet meer van toepassing voor de huidige
versie van Wine. Sommige oude trucjes om een programma beter te laten
werken, zorgen er nu voor dat het programma helemaal niet meer werkt.
(Office 2007 is hier een voorbeeld van). Alleen de handleidingen van
WineHQ worden ondersteund.

Probeer een programma altijd eerst uit zonder trucjes of winetricks en
vraag bij problemen hulp op het forum.

### Ik heb een ander programma gebruikt (bijvoorbeeld PlayOnLinux) en ik hulp nodig.

Vraag hulp bij de makers van het programma. Om hier hulp te krijgen
gebruik een "kale" versie van Wine. De verwijzingen op de [Programma's
van Derden](Third-Party-Applications) pagina zijn er alleen
voor het gemak, maar worden niet hier ondersteund.

### Mijn programma werkte wel met een oude versie van Wine, maar nu niet meer!

Dit is een achteruitgang in de ontwikkeling van Wine. [Probeer uit te
vinden welke verandering de fout heeft
veroorzaakt](Regression-Testing). Maak daarna een foutrapport
en gebruik *regression* als sleutelwoord. Deze rapportages krijgen extra
aandacht van de ontwikkelaars. Hoe sneller een achteruitgang wordt
gevonden, hoe makkelijker hij is om op te lossen.

### Ik wil echt heel graag dat mijn programma werkt. Hoe kan ik daar voor zorgen?

[CodeWeavers](https://www.codeweavers.com/) heeft betaalde
ondersteuning. Een andere manier is zelf aan de slag gaan met de
broncode of iemand inhuren.

### Ik schrijf programma's. Hoe detecteer ik Wine?

Hoewel dit wel kan, is dit een slecht idee. Lees de [Developer
FAQ](Developer-FAQ#how-can-i-detect-wine) voor meer
informatie.

## Prestaties

### Mijn 3D programma of spel is erg langzaam (FPS)

Meestal komt dit door een fout in de OpenGL 3D stuurprogramma's. Lees
[3D problemen](3D-Driver-Issues) voor meer informatie.

### Is Wine langzamer dan Windows?

Soms is Wine zelfs sneller dan Windows. Snelheid hangt van veel factoren
af. De stuurprogramma's spelen hierin een belangrijke rol. Als een
stuurprogramma slecht is geschreven dan daalt de snelheid. Ook kan een
fout in Wine de snelheid van een programma verlagen. Bij Wine gaat
echter correctheid boven snelheid.

Lees de [prestatie](Performance) pagina voor meer informatie.

### Er staat heel vaak "fixme:" in de terminal en Wine is langzaam

Deze regels kunnen genegeerd worden. De `fixme` regels hebben alleen
betekenis voor de Wine ontwikkelaars. Een `fixme` regel betekent *niet*
dat er een probleem is. Veel programma's werken gewoon. Wel kan het een
inzicht geven hoe een programma werkt.

Als er heel veel lijnen met `fime` continu snel langs kommen, dan kan
Wine een beetje sneller gemaakt worden door deze regels uit te zetten.
Dit kan door de `WINEDEBUG` variabele op `-all` te zetten. Bijvoorbeeld:

```sh
WINEDEBUG=-all wine programma.exe
```

Gevorderde gebruikers en ontwikkelaars die fouten in Wine willen
opsporen kunnen hier meer over lezen op de [Debug
Kanalen](Debug-Channels) en op de
[Ontwikkelaars](Developers) wiki pagina's. Dit is voorbeeld
hoe alleen de `fixme` berichten voor dsound en d3d9 uitgezet kunnen
worden:

```sh
WINEDEBUG=fixme-dsound,fixme-d3d9 wine programma.exe
```

## Problemen Oplossen

### Algemeen

#### Hoe krijg ik een debug logbestand (terminal uitvoer)

Voer het programma uit vanaf de opdrachtregel (lees [Programma's
uitvoeren vanaf de
opdrachtregel](nl/Wine-User's-Guide#windows-programmas-uitvoeren-vanaf-de-opdrachtregel)).

Als de uitvoer kort is kan deze gekopieerd en geplakt worden naar een
bestand. Anders moet de uitvoer doorgestuurd worden naar een bestand.
Bijvoorbeeld:

`wine programma.exe &> log.txt`

**Belangrijk**: De bovenstaande instructies zijn voldoende, tenzij er
anders gevraagd wordt.

**Belangrijk**: Als er een crash venster komt, klik op
"Afsluiten/Close". Anders is het log bestand niet compleet.

#### Hoe krijg ik een debug trace-bestand?

Gebruik dit **alleen** als er om gevraagd wordt. Anders is een gewone
terminal uitvoer voldoende. Voor een foutrapport is het vaak nodig om
verschillende debug kanalen toe te voegen (vaak `+relay,+seh`, maar er
kan gevraagd worden naar andere [kanalen](Debug-Channels). Om
een debug trace-bestand te maken, voer het volgende uit:

```sh
WINEDEBUG=+relay,+seh,+tid wine programma.exe >> log.txt 2>&1
```

Het `log.txt` kan als **bijlage** aan het foutrapport worden toegevoegd.
Het bestand moet gecomprimeerd worden als het groter is dan 1MB.

In sommige gevallen verdwijnt de fout als `WINEDEBUG` voor een kanaal
wordt gebruikt. Vermeld dit in de rapportage.

### Vastlopers en Crashes

#### Het programma is vastgelopen. Wat nu?

Als het programma vanaf de opdrachtregel met `wine programma.exe`* is
gestart, dan kan deze waarschijnlijk gestopt worden met `Ctrl+C`. Als
het programma met een snelkoppeling is gestart, open dan een terminal
venster en stop het programma met:

```sh
killall -9 programma.exe
```

Of om alle Windows programma's in één keer te stoppen:

```sh
wineserver -k
```

Of open de taakbeheerder van Wine `wine taskmgr` en stop het programma.

#### De hele computer is vastgelopen, start opnieuw op of sluit af als ik een spel in Wine speel!

Als de computer vastloopt en zelfs de muis niet meer reageert, dan is
het waarschijnlijk niet een probleem met Wine. Wine is een
gebruikersprogramma en moet niet in staat zijn de computer te laten
vastlopen. Mogelijk is er een groter probleem met de computer die Wine
aan het licht heeft gebracht. Denk hierbij aan slechte stuurprogramma's,
een slechte geheugenmodule of overgeklokte hardware.

Vaak zijn de stuurprogramma's van de grafische kaart het probleem. Als
de computer ook vastloopt met `glxgears` dan is het zeker een
stuurprogramma probleem. Dit kan komen doordat de kernel is bijgewerkt
zonder dat de stuurprogramma's van de grafische kaart zijn bijgewerkt.
Het opnieuw installeren van de stuurprogramma's moet dit verhelpen.

Als een laptop zichzelf opeens uitschakelt, dan kan oververhitting het
probleem zijn. Er kunnen problemen zijn met de Linux ACPI code die de
ventilatoren aanstuurt, andere laptops hebben gewoon koelingsproblemen.

Als dit allemaal niet helpt, vraag hulp op het forum. Geef de naam van
het programma, de Wine versie, de uitvoer van `cat /etc/issue` ,
`lspci | grep -i vga` en bij het gebruik van een Nvidia kaart
`cat /proc/driver/nvidia/version`. Misschien kan iemand helpen.

#### Alle programma's doen het niet!

Probeer eerst een ingebouwd Wine programma uit te voeren.

```sh
wine notepad
```

Werkt dit ook niet meer, dan is waarschijnlijk de
[wineprefix](#Wineprefix) stuk. Dit kan gebeuren als het
foutieve programma mee opstart met Wine. Lees [\#Hoe verwijder ik een
virtuele Windows
installatie?](#Hoe-verwijder-ik-een-virtuele-Windows-installatie?)
en installeer de programma's, behalve het foutieve programma opnieuw.

### Waarschuwingen en Foutmeldingen

#### Het programma meldt dat een DLL of lettertype ontbreekt.

Eigenlijk hoort ieder programma alle DLLs mee te leveren die het nodig
heeft (behalve dan de Windows DLLs). Soms wordt dit vergeten of de
makers gaan er vanuit dat de DLLs en lettertypes al op de computer
staan. De missende DLL of lettertype kan op verschillende manieren
geïnstalleerd worden.

- Download het van de makers (bijvoorbeeld Microsoft). Dit kan
  gemakkelijk met [winetricks](winetricks).
- Installeer een programma die wel de bestanden meelevert.
- Kopieer het bestand van een legale Windows installatie.

**Download geen losse DLL bestanden van websites!** Deze kunnen meer
kwaad dan goed doen. Ook met Wine.

Een paar veel voorkomende missende DLL bestanden zijn:

| DLL              | winetricks                        |
|------------------|-----------------------------------|
| msvbvm60.dll     | vb6run                            |
| mfc42.dll        | mfc42 of vcrun6                   |
| vcruntime140.dll | vcrun2015, vcrun2017 of vcrun2019 |

#### `Too many open files, ulimit -n probably needs to be increased`

(`Te veel open bestanden, ulinit -n moet mogelijk vergroot worden`)

Dit probleem kan voorkomen op oudere besturingssystemen. Op Ubuntu kan
dit aangepast worden in `/etc/security/limits.conf`. Verander de regel

```
* hard nofile 2048 (of welk getal er ook staat)
```

naar

```
* hard nofile 8192
```

(Het sterretje betekent voor iedereen)

Meld opnieuw aan en controleer de waarde met `ulimit -H -n`. De waarde
moet nu 8192 zijn.

Er is ook een andere manier. Deze werkt tijdelijk, maar kan
waarschijnlijk ook op Mac OS X gebruikt worden.

```
$ sudo bash
# ulimit -n 8192
# su gebruikersnaam
$ wine programma.exe
```

#### `preloader: Warning: failed to reserve range 00000000-60000000` of `winevdm: unable to exec 'application name': DOS memory range unavailable`

Lees meer hierover op Bugzilla, fout #12516.

De oorzaak hiervan is een Linux kernel instelling. Als
`cat /proc/sys/vm/mmap_min_addr` geen 0 teruggeeft, dan kan
`sysctl -w vm.mmap_min_addr=0` een tijdelijke oplossing bieden.

Lees [Preloader Page Zero
Problem](Preloader-Page-Zero-Problem) voor meer informatie.

#### `Failed to use ICMP (network ping), this requires special permissions`

Op \*NIX systemen heeft ICMP ping speciale rechten nodig. Normaal heeft
alleen de root gebruiker die. Gelukkig kunnen gewone gebruikers op
moderne Linux systemen ook toestemming krijgen.

```sh
sudo setcap cap_net_raw+epi /usr/bin/wine-preloader
```

**Let op**. Dit werkt alleen met een standaard Wine installatie. WineHQ
plaatst *wine* in /opt/wine-stable/, /opt/wine-devel/, of
/opt/wine-staging/. Wine die vanaf de broncode is gebouwd, staat in
/usr/local/bin. `wine64-preloader` is de naam van de 64-bit variant. Na
een update van Wine moet dit commando opnieuw gegeven worden.

Om de toestemming in te trekken

```sh
sudo setcap -r /usr/bin/wine-preloader
```

Vanaf Wine 5.7 kunnen Debian en Ubuntu gebruikers die de WineHQ
pakketten gebruiken dit instellen met `dpkg-reconfigure`. Standaard
staat deze optie uit, omdat er veiligheidsrisico's aanzitten en de
meeste programma's dit niet nodig hebben.

```sh
dpkg-reconfigure wine-<soort>-amd64 wine-<soort> wine-<soort>-i386
```

Beantwoord de drie vragen met *ja* (Vervang <soort> door devel, staging,
of stable).

Wine-staging heeft tegenwoordig een [oplossing](#8332) voor dit
probleem.

#### `err:virtual:map_image failed to set 60000020 protection on section .text, noexec filesystem?`

Dit kan komen doordat er een bestandssysteem is aangekoppeld met de
*user* of de *noexec* optie. Of door SELinux. Zorg dat het programma
niet op een exotisch bestandssysteem staat of schakel tijdelijk SELinux
uit.

#### `Broken NVIDIA RandR detected, falling back to RandR 1.0`

RandR is onder andere een manier op de schermresolutie aan te passen.
NVidia heeft dit in zijn stuurprogramma's niet helemaal geïmplementeerd.
Dit kan problemen geven in programma's die de resolutie proberen te
veranderen. (Lees bug #34348)

Het stuurprogramma van Nouveau heeft hier geen last van en is de
aanbevolen oplossing als de gebruiker zonder het NVidia stuurprogramma
kan. Als dit niet kan, is er de volgende oplossing.

Veel programma's proberen te starten in een lage resolutie als die
beschikbaar is. Als die er dus niet is, start het programma gelijk in de
goede resolutie. Dit kan ingesteld worden in `/etc/X11/xorg.conf` en
voeg de volgende regel in bij het *Screen* onderdeel.

```
Option         "metamodes" "1920x1080 +0+0"
```

Verander 1920x1080 in de gewenste resolutie.

Na het herstarten van de Xserver voer `xrandr` en `xrandr –q1` uit.
Beide opdrachten moeten alleen de gekozen resolutie laten zien. Nu kan
het programma, hopelijk met meer succes, getest worden.

### Grafisch

#### Het programma kan de resolutie of kleurdiepte niet veranderen

Voeg een regel toe aan `/etc/X11/xorg.conf` in het *Screen* onderdeel
met de gevraagde resolutie en kleurdiepte. Er kan ook een probleem zijn
met Xrandr.

#### Het programma wil 256 kleuren gebruiken, maar ik heb er miljoenen.

Het onvermogen om van 24bpp naar 8bpp te schakelen ligt aan de Xserver.
Lees [256 Color Mode](256-Color-Mode) voor oplossingen.

#### Na het spelen van een spel in een volledig scherm gaat de Xserver niet terug naar zijn oorspronkelijke resolutie

De makkelijkste oplossing is om handmatig de resolutie te veranderen met
de systeem instellingen van het systeem.

Of voer de onderstaande opdracht uit in een terminal venster

```sh
xrandr -s 0
```

#### Afbeeldingen in het spel zijn slecht, maar in de AppDB staan goede beoordelingen

- Installeer de laatste stuurprogramma's van de grafische kaart.
- De meeste beoordelingen op de AppDB zijn gemaakt met de *Nvidia*
  stuurprogramma's
- *ATI*/*AMD*/*Radeon* kaarten die de stuurprogramma's van de
  fabrikant gebruiken, hebben problemen in Wine. Lees Wine fout #7411
  voor meer informatie.
- *Intel*/*S3*/*Matrox* kaarten werken waarschijnlijk alleen bij oude
  spellen.
- *Nouveau* mist voor veel kaarten 3D ondersteuning.

#### Tekst mist of is corrupt

Dit kan komen door een missend lettertype of een conflict tussen
lettertypes.

Ook kan dit komen door een fout in `riched20.dll` van Wine. Installeer
de Windows versie met [winetricks](Winetricks)

```
winetricks riched20
```

#### De lettertypes in Wine zijn enorm groot

Soms kan met de ALT-toets en de muis het venster van `winecfg` heen en
weer bewogen worden. Op het tabblad *Grafisch* staat de schuifbalk
*Schermresolutie*. Veranderingen vinden pas plaats als `winecfg` wordt
herstart.

Als dit niet werkt probeer de volgende opdracht in een terminal (Dit is
één lange regel)

```sh
echo -e "[HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Hardware Profiles\\Current\\Software\\Fonts]\n\"LogPixels\"=dword:60\n" | wine regedit -
```

### Geluid

#### MP3 bestanden werken niet met Windows Media Speler

Om MP3 bestanden te kunnen afspelen met Windows Media Speler (WMP) of
programma's die WMP of zijn codecs gebruiken, moet de 32-bit versie van
`libmpg123` geïnstalleerd zijn. Ook moet bij Wine tijdens het compileren
de ondersteuning voor MP3 ingeschakeld zijn. Vroeger was dit nog wel
eens een probleem vanwege de licenties op MP3, maar tegenwoordig zijn de
patenten verlopen en is dit gewoonlijk geen probleem meer.

Mocht dit nog wel een probleem zijn dan kan de codec van Windows Media
Speler 9, l3cod­eca.acm, gebruikt worden. Kopieer l3codeca.acm naar de
/windows/system32 folder van de wineprefix (of installeer WMP9 met
winetricks) en maakt een symbolische link in dezelfde map naar het
bestand aan met de naam winemp3.acm. Wine kan dan de Windows codec
gebruiken om de MP3 bestanden af te spelen.

Dit is alleen nodig voor WMP en programma's die daar afhankelijk van
zijn (zoals PowerPoint). Programma's zoals Winamp of de VLC Speler
werken zonder deze omweg.

### Netwerken

#### DNS resolve werkt niet op 64-bit systemen

Wine heeft de 32-bit DNS bibliotheken nodig. Op Debian en Ubuntu zitten
deze in het `libnss-mdns:i386` pakket. Op andere systemen kan dit pakket
een andere naam hebben.

### Verwijderbare Media

#### Ik kan de CD/DVD niet uitwerpen

Probeer `wine eject`. Deze opdracht koppelt de schijf af en werpt hem
uit. De schijf moet als *cd-rom* omschreven staan in `winecfg` en
gebruik de schijf letter. Bijvoorbeeld:

```sh
wine eject d:
```

#### De CD/DVD is leeg of er missen bestanden

Sommige schijven zijn zo slecht geschreven en op een manier dat dit
alleen op \*NIX problemen geeft.

De `unhide` of `norock` `mount`-opties zijn voor deze schijven nodig.
Bekijk met `mount` waar de schijf is aangekoppeld en koppel deze opnieuw
aan met de nodige optie. Bijvoorbeeld:

```sh
sudo mount -o remount,unhide /dev/sr0 /mnt/cdrom
```

of

```sh
sudo mount -t iso9660 -o ro,unhide /dev/cdrom /media/cdrom0
```

### Toetsenbord en Muis

#### Sommige toetscombinaties werken niet

Zelfs in volledig scherm kan de vensterbeheerder (KDE/Gnome)
toetscombinaties afvangen. Toetscombinaties die door de vensterbeheerder
worden gebruikt kunnen niet meer in Wine gebruikt worden. Zet deze
toetscombinaties in de vensterbeheerder uit.

### Overige

#### Het programma werkt, maar tekstvakken werken niet of vreemd

Waarschijnlijk is dit een fout in de RICHED20.DLL van Wine. Probeer de
RICHED20.DLL van Microsoft te gebruiken. Deze kan geïnstalleerd worden
met `winetricks riched20`. Hopelijk lost dit het probleem op totdat de
ontwikkelaars van Wine de fout hebben opgelost.

#### Ik heb mijn Wine menu verwijderd en krijg het niet meer terug

In plaats van dat het menu verwijderd is, hebben veel menu-bewerkers op
de Unix desktop de optie om een menu item te verbergen. Bekijk eerst of
dit het geval is.

De menu informatie is opgeslagen in `~/.config/menus/applications.menu`.
Open `applications.menu` met een tekstverwerker en zoek naar een
gedeelte zoals:

```xml
<Menu>
    <Name>wine-wine</Name>
    <Deleted/>
</Menu>
```

Of naar:

```xml
<Menu>
    <Name>wine-wine</Name>
    <Menu>
        <Name>wine-Programs</Name>
        <Menu>
            <Name>wine-Programs-AutoHotkey</Name>
            <DirectoryDir>/home/user/.local/share/desktop-directories</DirectoryDir>
        </Menu>
    </Menu>
    <Deleted/>
</Menu>
```

Haal de regel met <Deleted/> weg en het Wine menu is weer zichtbaar.

#### 16-bit programma's werken niet

Lees ook Fout #36664.  Voor de 64-bit Linux kernels 3.15+, 3.14.6+,
3.10.42+, 3.13.22+, 3.4.92+ en 3.2.60+. Voer de volgende opdracht als
root uit, maar wees bewust van de veiligheidsrisico's:

```
# echo 1 > /proc/sys/abi/ldt16
```

Bij oudere kernels mist waarschijnlijk `/proc/sys/abi/ldt16`

#### Met SSH werkt de invoer niet

Als Wine over SSH wordt gebruikt, dan moet 'trusted X11 forwarding'
mode gebruikt worden. Bij OpenSSH betekent dit dat '-Y' in plaats van
'-X' gebruikt moet worden. Lees Fout #41476 voor voorbeelden.

#### Ik mis een vraag in de Nederlandse FAQ

Opgeloste problemen zijn niet vertaald. Ook problemen op oude
besturingssystemen staan niet in de Nederlandstalige versie. De Engelse
versie is [hier](FAQ) te vinden.
