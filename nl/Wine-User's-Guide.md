---
title: Gebruikershandleiding
---

<small>
&nbsp;[:flag_gb: English](Wine-User's-Guide)
</small>

-----

**Schrijvers van de Engelstalige versie:** Huw Davies; Steven Elliott;
Mike Hearn; André Hentschel; James Juran; Ove Kaaven; Alex Korobka;
Bruce Milner; Andreas Mohr; Dustin Navea; Eric Pouech; Scott Ritchie;
Adam Sacarny; John Sheets; Petr Tomasek; Brian Vincent

De Nederlandstalige versie van deze handleiding is aangepast voor Wine
6.x en hoger.

## Introductie

### Overzicht

#### Voor wie en waarvoor is deze pagina

De *Gebruikershandleiding van Wine* is een eenvoudige
installatiehandleiding en een uitgebreid naslagwerk. De handleiding is
voor de beginnende en de gevorderde Wine gebruiker. Er staan
stap-voor-stap installatie en configuratie instructies in, maar het is
ook een uitgebreide documentatie voor alle instelbare opties.

#### Overige vragen en commentaar

Als er na het lezen van deze pagina, de *[Veel gestelde
vragen](nl/FAQ)* pagina en andere documentatie nog steeds
vragen zijn, dan horen wij dat graag. Op de pagina *[Belangrijke
Mailinglijsten / Forums](nl/Forums)* staan de
verschillende mailinglijsten van Wine. Of neem contact op via
[IRC](nl/IRC). Beide manieren zijn een goede plek om
hulp te vragen of om ideeën op te doen.

Op de WineHQ Wiki pagina een fout gevonden? Geef dit door op
[Bugzilla](https://bugs.winehq.org/) of vraag toestemming om de wiki
zelf aan te passen.

#### Inhoudsoverzicht

Om Wine te kunnen gebruiken, moet deze helemaal goed geïnstalleerd
worden. Deze handleiding doorloopt de stappen van een Wine-loos systeem
naar een spiksplinternieuwe Wine installatie.

|                                                             |                                                                                                                               |
|-------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------|
| Stap 1: [Hoe kom ik aan Wine?](#wine-verkrijgen) | Op welke manieren is Wine te verkrijgen.                                                                                      |
| Stap 2: [Wine instellen](#wine-configureren)     | Op welke manier Wine kan worden aangepast aan de wensen van de gebruiker.                                                     |
| Stap 3: [Wine uitvoeren](#wine-uitvoeren)        | Welke stappen er genomen kunnen worden om programma's beter te laten draaien en geeft verwijzingen als er meer hulp nodig is. |

#### Snelle start

In het kort komt een volledige Wine installatie neer op:

1.  Gebruik een distributie die ondersteund wordt en [download
    Wine](nl/Download). Kant-en-klare pakketten zijn voor de
    beginnende gebruiker het makkelijkste.
2.  Configureer Wine met [`winecfg`](#wine-configureren) als
    dat nodig is.
3.  Test de installatie met het `Kladblok`: `wine notepad`
4.  Kijk in de [Programma Database](https://appdb.winehq.org/) of er
    aanvullende stappen nodig zijn voor het programma.
5.  Installeer en [voer het programma](#wine-uitvoeren) met
    `wine programma.exe` uit.

### Wat is Wine?

#### Windows en Linux

Programma's zijn gemaakt voor een zeker besturingssysteem. De meeste
programma's werken dus ook niet op systemen waarvoor ze niet gemaakt
zijn. Windows programma's kunnen niet op Linux worden uitgevoerd, omdat
er instructies in staan die Linux, totdat ze vertaalt zijn, niet
begrijpt. Andersom begrijpt Windows niet alle Linux instructies.
Vergelijk het met twee verschillende talen. Een paar woorden in het
Nederlands en het Frans zijn hetzelfde, maar zonder vertaling begrijpen
mensen elkaar niet.

Tegen dit probleem lopen gebruikers aan die Windows en Linux programma's
samen willen gebruiken. Een veel gebruikte oplossing is om de twee
besturingssystemen op de computer naast elkaar te installeren. Een
*dual-boot* systeem. Als een Windows programma nodig is, wordt Windows
gestart. Als een Linux programma nodig is, wordt de computer opnieuw
opgestart in Linux. Dit is erg omslachtig. Elke keer als er van
programma gewisseld wordt, moet de computer opnieuw worden opgestart.
Windows heeft ook het nadeel dat het veel geld kost, een aparte partitie
nodig heeft en niet in staat is om veel verschillende bestandssystemen
te kunnen lezen. Door dit laatste nadeel is het lastig om gegevens te
delen tussen de besturingssystemen.

#### Wat is Wine en op welke manier kan het mij helpen?

Wine maakt het mogelijk om Windows programma's op een \*NIX
besturingssysteem (in het bijzonder Linux) uit te voeren. In het hart
implementeert Wine de Windows *Application Programming Interface (API)*.
Het werkt als een soort van brug tussen Windows programma's en Linux.
Wine is een compatibiliteitslaag. Als een Windows programma een opdracht
uitvoert die Linux niet snapt, vertaalt Wine die opdracht naar één die
het besturingssysteem begrijpt.

Als er toegang is tot de broncode van een Windows programma, dan kan
Wine gebruikt worden om het programma te compileren voor Linux. Lees
hier meer over in de [handleiding van Winelib](Winelib-User's-Guide).

#### Functies in Wine

In de loop der tijd zijn er steeds meer functies aan Wine toegevoegd.
Een paar van deze functies zijn:

- Ondersteuning voor Win64, Win32 (Win 95/98,
  NT/2000/XP/2003/Vista/2008/7/8/8.1/10), Win16 (Win 3.1) en DOS
  programma's
- DLL-bestanden van andere kunnen gebruiken, waaronder die van Windows
- Op X11 gebaseerde grafische weergave. Hierdoor is met iedere
  X-terminal toegang op afstand mogelijk
- MacOS en Android ondersteuning
- Virtueel bureaublad
- DirectX ondersteuning
- Ondersteuning van verschillende geluidsstuurprogramma's, waaronder
  ALSA, OSS, PulseAudio en CoreAudio
- Ondersteuning voor andere invoerapparaten, waaronder die van tablets
- Printer ondersteuning via een Unix PostScript print service, zoals
  CUPS
- Ondersteuning van modems en seriële apparaten
- Ondersteuning van Winsock TCP/IP netwerken
- Ondersteuning van scanners, CD-schrijvers en andere apparaten via de
  ASPI interface (SCSI)
- Ondersteuning van verschillende talen
- Ingebouwde foutopsporing en trace logger om makkelijker problemen te
  kunnen oplossen

### Versies van Wine

#### Wine van WineHQ

Wine is een open broncode project. Met als gevolg dat er veel
verschillende versies te vinden zijn. WineHQ brengt drie officiële
versies uit met de Engelse namen **stable**, **development** en
**staging**.

- *stable* – **stabiel**
  Jaarlijks brengt WineHQ een nieuwe stabiele versie uit. Deze krijgt
  tussendoor (om de 10 tot 12 weken) kleine verbeteringen. Werkt het
  programma of spel goed in Wine? Dan is dit de versie om te gebruiken.
- *development*-**ontwikkeling**
  Om de twee weken komt een nieuwe ontwikkelingsversie uit. In deze
  versie worden fouten gerepareerd en nieuwe functies toegevoegd. Deze
  versie is voor mensen die de nieuwe functies nodig hebben of tegen een
  gerepareerde fout aanlopen. Werkt het spel of programma niet met de
  stabiele versie van Wine? Probeer dan altijd eerst de
  ontwikkelingsversie voordat er een foutrapport wordt gemaakt.
- *staging*-**???**
  Dit is de speeltuin van Wine. In staging zitten honderden oplossingen
  voor fouten die nog niet klaar zijn om in de ontwikkelingsversie
  terecht te komen. Vaak hebben gebruikers van spellen baat bij deze
  versie.

De reparaties zijn experimenteel en kunnen andere programma's kapot
maken of van de één op de andere versie niet meer werken. In de Bugzilla
worden fouten die gerepareerd zijn in deze versie aangeduid met STAGED.

Deze versies kunnen worden gedownload als kant-en-klaar pakket en als
broncode. De mensen die echt het nieuwste van het nieuwste willen
hebben, kunnen ook de broncode vanaf de Git opslagplaats downloaden.
Deze wordt vijf keer per week bijgewerkt.

Iedere versie van Wine heeft een labelnummer. Vanaf Wine 2.0 gaat de
nummering op de volgende manier:

**Pre-publicatie van de stabiele versie:**

wine-*x*.0-rc*n*

**Stabiele versie, nieuwe versie:**

wine-*x*.0

**Stabiele versie, update:**

wine-*x*.0.*z*

**Ontwikkelingsversie, update:**

wine-*x*.*y*

Waarbij:

- *n* is een getal dat aangeeft hoe vaak een pre-publicatie is
  uitgebracht.
- *x* is het hoofdversienummer. Deze gaat ieder jaar in januari één stap
  omhoog.
- *y* is het publicatienummer van de ontwikkelingsversie. Deze wordt
  iedere twee weken verhoogd. (**Staging** volgt ook dit schema met
  *-staging* als achtervoegsel)
- *z* is het publicatienummer van een update van de stabiele versie.
  Deze wordt verhoogd als de update klaar is volgens de ontwikkelaar.

Bekijk [hier](https://gitlab.winehq.org/wine/wine/-/tags) alle
versienummers.

Een versie die van [Git](Git-Wine-Tutorial) afkomstig is, krijgt ook
nog een label van `git-describe`. Een versienummer ziet er dan zo uit:

`wine-x.y-n-gccccccc`

Waarbij:

- *n* het aantal verandering is sinds *x*.*y*.
- *ccccccc* de eerste karakters zijn van de laatste verandering.

Voorbeelden:

| Label                    | Datum       | Verklaring                   |
|--------------------------|-------------|------------------------------|
| wine-4.0                 | 22 Jan 2019 | Nieuwe stabiele versie \|-as |
| wine-5.22-staging        | 21 Nov 2020 | Staging versie               |
| wine-5.0.3               | 10 Nov 2020 | Update stabiele versie       |
| wine-6.0-rc6             | 8 Jan 2021  | Zesde pre publicatie van 6.0 |
| wine-6.2-63-gc7f1c9d8635 |             | Een git versie van Wine 6.2  |

Lees het volgende hoofdstuk [Wine
verkrijgen](#wine-verkrijgen) voor meer informatie.

#### Andere Wine versies

Er zijn ook andere programma's die op Wine gebaseerd zijn.

- **[CodeWeavers CrossOver](https://www.codeweavers.com/)** is een
  commerciële versie van Wine die goede ondersteuning biedt. Veel
  ontwikkelaars van Wine zijn in dienst van CodeWeavers.
- **[Lutris](https://lutris.net/)** brengt voor sommige spellen een
  aangepaste versie van Wine uit.
- **[Valve](https://github.com/ValveSoftware/Proton)** brengt voor Steam
  een aangepaste versie van Wine uit.

### Alternatieven voor Wine

Er zijn ook andere manieren om een programma uit te voeren dan met Wine.
Overweeg één van deze opties als er problemen zijn met Wine.

#### Een systeem eigen programma

In plaats van een Windows programma te gebruiken, is er vaak ook de
mogelijkheid om een ander programma te gebruiken die speciaal voor het
besturingssysteem is gemaakt. Mediaspelers, instant messengers en
programma's voor het delen van bestanden hebben goede alternatieven. Ook
zijn er steeds meer programma's die een eigen Linux variant hebben.

#### Een ander besturingssysteem

De meest voor de hand liggende manier is natuurlijk Windows gebruiken.
Echter kunnen kosten, veiligheid, privacy, compatibiliteit en
systeemvereisten dit het tot een moeilijke keuze maken. (Daarom bestaat
Wine ook)

Een alternatief is het gebruik van [ReactOS](https://reactos.org). Dit
is een open broncode versie van Windows. Veel code van ReactOS en Wine
is hetzelfde, maar ReactOS heeft een eigen kernel waardoor het ook
mogelijk is om Windows stuurprogramma's te installeren. (Hoewel dit een
erg interessant project is, is het nog in de testfase.)

#### Virtuele Computer

In plaats van het besturingssysteem te vervangen, kan er ook gekozen
worden voor een virtuele computer. Op deze manier kunnen Linux en
Windows programma's naast elkaar uitgevoerd worden. Op een virtuele
computer is het niet alleen mogelijk om verschillende Windows versies te
installeren, maar ook andere besturingssystemen, zoals ReactOS.

Er zijn verschillende virtuele computers op de markt.

- Open broncode:


[Bochs](https://bochs.sourceforge.net/)

[VirtualBox](https://www.virtualbox.org)

[QEMU](https://www.qemu.org)

- Commercieel:


[VMware](https://www.vmware.com)

[Hyper-V](https://docs.microsoft.com/en-us/virtualization/)

Er zitten nogal wat nadelen aan het gebruik van een virtuele computer.
Het is namelijk een emulator (wat Wine dus niet is). Hierdoor kan er
(veel) snelheid verloren gaan. Ook zal een programma dat in een virtuele
computer wordt uitgevoerd niet volledig integreren met het
hoofdbesturingssysteem. Zo is het niet mogelijk om een Windows
snelkoppeling naast een Linux snelkoppeling in de taakbalk te hebben
staan. Beide staan immers op twee verschillende computers.

## Wine verkrijgen

### Wine installeren

[Als Wine de goede keuze is](#alternatieven-voor-wine), dan
is het tijd om het te installeren. Er zijn drie verschillende manier om
Wine van WineHQ te installeren. Elke manier heeft zijn voor- en nadelen.

#### Installeren met pakketten

Dit is de meest makkelijke manier om Wine te installeren. De pakketten
bieden een kant-en-klaar uitvoerbare Wine installatie speciaal gemaakt
voor een distributie naar keuze. De pakketten worden regelmatig getest
op functionaliteit en compleetheid.

De pakketten zijn gemakkelijk te downloaden. De instructies hiervoor
staan op onze [download](nl/Download) pagina. Omdat Wine zo
populair is, is Wine waarschijnlijk ook te downloaden via de kanalen van
de distributie. WineHQ brengt om de twee weken een nieuwe versie uit,
daardoor kunnen deze pakketten een paar versies achterlopen.

De Wine pakketten van WineHQ zijn eenvoudig te updaten. Veel
distributies kunnen met een paar muisklikken Wine updaten. Ook is het
mogelijk om zelf deze pakketten vanaf de broncode te bouwen, maar dat
valt buiten deze handleiding.

#### Installeren vanaf de broncode

Het kan voorkomen dat de kant-en-klare pakketten niet toereikend zijn.
Misschien zijn ze niet beschikbaar voor de distributie of het platform.
Ook kan het zijn dat de voorkeur uitgaat naar het zelf in elkaar zetten
van Wine met speciale compiler optimalisaties, met sommige functies
uitgeschakeld of omdat er een verandering moet worden ingebouwd. Als dit
het geval is, dan is dit de goede keuze.

Omdat de broncode openbaar is kan dit allemaal. De broncode kan voor
elke uitgave van Wine gedownload worden op de [download](nl/Download)
pagina. Het compileren en installeren van de broncode is lastiger dan
de kant-en-klare pakketten te gebruiken, maar we geven hiervoor een
uitgebreide handleiding.

#### Installeren vanaf Git

Als alleen het nieuwste van het nieuwste goed genoeg is of als u Wine
wilt meehelpen ontwikkelen, dan is de Wine broncode ook te downloaden
vanaf Git. Instructies hiervoor zijn te vinden in de [Git Wine
handleiding](Git-Wine-Tutorial).

Houd er rekening mee dat alle gebruikelijke waarschuwingen voor een
ontwikkelingsversie gelden. De broncode op Git is voor een groot deel
niet getest en is dus misschien niet in elkaar te zetten. Het is echter
wel de manier om te kijken of een programma in de volgende versie van
Wine werkt, of dat een doorgevoerde verandering echt een verbetering is.

### Wine installeren met pakketten

#### Nieuwe installatie

Wine installeren op een nieuw systeem is een koud kunstje. Download het
WineHQ pakket of voeg de WineHQ pakketbron toe aan het systeem en
installeer Wine. Gewoonlijk is het niet nodig om een oude versie weg te
halen. De Wine pakketten zijn zo gemaakt dat ze dit automatisch doen.
Een uitzondering hierop is als Wine vanaf de broncode is geïnstalleerd.
Dan moet wel handmatig Wine worden verwijderd. Lees [Wine verwijderen
vanaf de broncode](#wine-verwijderen-vanaf-de-broncode) voor
meer informatie.

#### Verschillende distributies

Wine werkt op heel veel verschillende Linux distributies en op andere
Unix-achtige systemen zoals Solaris en FreeBSD. Iedere distributie heeft
zijn eigen pakketbeheerder en manier om programma's te installeren.
Gelukkig is het installeren en verwijderen van Wine hetzelfde als het
installeren of verwijderen van een ander programma.

In deze handleiding wordt niet voor iedere distributie beschreven hoe
Wine precies moet worden geïnstalleerd. Selecteer daarvoor de gewenste
distributie.

### Wine installeren vanaf de broncode

Voordat wordt begonnen met het installeren vanaf de broncode, zorg
ervoor dat andere installaties van Wine zijn verwijderd en de broncode
is gedownload. Het installeren gebeurd via de terminal.

#### Afhankelijkheden installeren

Wine maakt gebuikt van veel open broncode bibliotheken. Hoewel veel van
deze bibliotheken, strikt genomen, niet nodig zijn om Wine in elkaar te
zetten, verliest Wine wel functionaliteit. In het verleden zijn er
gebruikers geweest die fouten hebben gemeld in Wine, omdat ze sommige
bibliotheken niet geïnstalleerd hadden toen ze Wine compileerden. Daarom
raden we aan om de kant-en-klare pakketten te gebruiken voor beginnende
gebruikers.

Om te zien welke ontwikkelingsbibliotheken er nodig zijn, voer
**`configure`** uit. Dit script zal melden welke bibliotheken er
ontbreken. Als er een belangrijke bibliotheek ontbreekt, installeer die
en voer **`configure`** opnieuw uit. Ook kan er nuttige informatie staan
in het bestand dat **`configure`** maakt (`include/config.h`). Hierin
staat waar **`configure`** naar zoekt, maar niet kan vinden.

#### Wine Compileren

Wanneer alle afhankelijkheden zijn geïnstalleerd, dan kan Wine in elkaar
worden gezet. Voer in een terminalvenster de volgende opdrachten uit:

```
./configure
make
```

Wine kan in de map waarin het gebouwd is, uitgevoerd worden. Het is dus
niet nodig om het te installeren. Maar dat kan wel. Voer daarvoor als
**`root`** het volgende commando uit:

`make install`

(Alleen het installeren gebeurt als **`root`**. Voer Wine als gewone
gebruiker uit!)

#### Wine verwijderen vanaf de broncode

Ga in een terminalvenster naar dezelfde map waar *`configure`* is
uitgevoerd om Wine te verwijderen. Voer dan als *`root`* het volgende
commando uit:

`make uninstall`

Wine wordt hiermee verwijderd van het systeem. Dit verwijdert niet de
Wine configuratie en de programma's die Wine heeft geïnstalleerd in de
thuismap. Deze kan met de hand worden verwijderd of hergebruikt worden
met een andere Wine versie.

## Wine uitvoeren

Dit hoofdstuk beschrijft alle aspecten omtrent het uitvoeren van Wine.

Als Wine zonder parameters wordt gebruikt, wordt er een help-tekst
getoond:

```
wine
Usage: wine PROGRAM [ARGUMENTS...]   Run the specified program
       wine --help                   Display this help and exit
       wine --version                Output version information and exit
```

De eerste parameter `PROGRAM` is het bestand dat met **wine** moet
worden uitgevoerd. Hierna kunnen de parameters van het Windows programma
`ARGUMENTS` worden opgegeven.

*`--help`* geeft hetzelfde menu weer.

*`--version`* geeft de Wine versie terug. Dit is een makkelijke manier
om te kijken welke versie van Wine is geïnstalleerd.

Als het Windows programma in de `PATH` variabele voorkomt, dan kan
alleen de naam worden gegeven. Als dit niet het geval is, dan moet het
volledige pad worden gebruikt. Lees [Windows programma's uitvoeren vanaf
de
opdrachtregel](#windows-programmas-uitvoeren-vanaf-de-opdrachtregel)
voor meer informatie.

### Windows programma's installeren en uitvoeren

Vaak is het mogelijk om een `.exe` bestand uit te voeren door een
dubbelklik met de muis of met **`Openen met...`** via de
rechtermuisknop.

Het uitvoeren van een `.exe` bestand is vaak alleen maar nodig voor een
installatiebestand (`setup.exe`) of een simpel programma zonder
installatiebestand. Bij het installeren van een programma maakt Wine
automatisch van Windows snelkoppelingen een snelkoppeling voor het
besturingssysteem. Dus als er wordt gevraagd een snelkoppeling op het
bureaublad te plaatsen, dan maakt Wine daar een snelkoppeling van die
gelijk kan worden gebruikt.

Als dit niet lukt, gebruik dan de opdrachtregel.

#### Windows programma's uitvoeren vanaf de opdrachtregel

Op deze manier is zichtbaar wat Wine doet. Hierdoor zijn ook eventuele
fouten zichtbaar.

Windows programma's hebben vaak bestanden nodig die in dezelfde map
staan als het `.exe` bestand. Verander daarom met **`cd`** eerst de
huidige map naar de map waar het `.exe` bestand staat. Voer daarna
**`wine`** uit met *alleen* het `.exe` bestand. Bijvoorbeeld:

```
cd ".wine/drive_c/Program Files (x86)/Microsoft Office/Office14"
wine PPTVIEW.EXE
```

Let op het gebruik van hoofd en kleine letters.

Als er een spatie in de map of bestandsnaam voorkomt, moeten er
(dubbele) aanhalingstekens omheen gezet worden of een backslash worden
gebruikt.

```
wine "Power Point.exe"
wine Power\ Point.exe
wine Power point.exe # Wine zal proberen het programma Power uit te voeren met als argument point.exe`
```

Voor niet grafische programma's (een CUI programma) lees de paragraaf
over [tekst programma's](#tekst-mode-programmas).

##### **wine start** gebruiken

Soms is het nodig om het volledige pad te gebruiken. Als bijvoorbeeld
een installatie vanaf meerdere CD-ROM's gebeurd. De eerste CD-ROM kan
namelijk niet worden uitgeworpen als de opdrachtregel nog in de map van
de CD staat.

Met **`wine start`** kan het volledige pad gebruikt worden.

- Gebruik de Windows namen tussen enkele aanhalingstekens:

`wine start 'C:\Program Files (x86)\Microsoft Office\Office14\PPTVIEW.EXE'`

- Of met dubbele aanhalingstekens, maar dan komt er een backslash bij:

`wine start "C:\\Program Files (x86)\\Microsoft Office\\Office14\\PPTVIEW.EXE"`

- Of met een Unix pad naam:

`wine start /unix ~/.wine/drive_c/Program\ Files\ (x86)/Microsoft\ Office/Office14/PPTVIEW.EXE`

óf

`wine start /unix "$HOME/.wine/drive_c/Program Files (x86)/Microsoft Office/Office14/PPTVIEW.EXE"`

##### Windows programma-argumenten doorgeven

Ook is het mogelijk om programma-argumenten door te geven aan een
Windows programma. Om bijvoorbeeld de intro over te slaan bij SimCity 4

`SimCity4.exe -intro:off`

Hetzelfde is in Wine mogelijk:

`wine SimCity4.exe -intro:off`

Dat is alles. Het is precies hetzelfde als op Windows, alleen met
**`wine`** ervoor. Mocht er een speciaal teken in het argument staan,
dan moet er een backslash voor gezet worden. Bijvoorbeeld:

` SimCity4.exe -UserDir:C:\Games\SimCity4\`

wordt:

`wine SimCity4.exe -UserDir:C:\\Games\\SimCity4\\`

##### `.msi` bestanden uitvoeren

Windows Installer `.msi` bestanden kunnen worden geïnstalleerd met
**`msiexec`** of met **`wine start`**. Bijvoorbeeld:

`wine msiexec /i installatie_programma.msi`

of

`wine start installatie_programma.msi`

### Basisgebruik: programma's en het configuratiescherm

Programma's worden met Wine op dezelfde manier geïnstalleerd als op
[Windows](#windows-programmas-installeren-en-uitvoeren). Als
de standaard instellingen worden gebruikt, dan worden 32-bit programma's
vaak geïnstalleerd in `Program Files (x86)` en 64-bit programma's in
`Program Files`. Als er snelkoppelingen worden gemaakt voor op het
bureaublad of in het Start Menu, dan maakt Wine die aan en kan het
programma met een klik op de snelkoppeling gestart worden. Als het
programma geen snelkoppeling heeft aangemaakt, dan moet het programma
met de opdrachtregel gestart worden. Onthoud dan goed waar het programma
geïnstalleerd wordt.

Programma's kunnen verwijderd worden met **`uninstaller`**. Deze kan op
meerdere manieren worden gebruikt. Eén manier is via *Software* in het
*Configuratiescherm*:

`wine control`

Dit scherm kan ook direct worden opgeroepen met:

`wine uninstaller`

Ook is het mogelijk om programma's met de opdrachtregel te verwijderen:

```
wine uninstaller --list
{0A7C8977-1185-5C3F-A4E7-7A90611227C3}|||Wine Mono Runtime
```

`wine uninstaller --remove {0A7C8977-1185-5C3F-A4E7-7A90611227C3}`

Mocht het gewenste programma niet in de lijst met geïnstalleerde
programma's staan, probeer dan `wine64 uninstaller` in plaats van
`wine uninstaller`.

Lees meer over de ingebouwde [Wine
programma's](Commands).

### Wine Verkenner/ Bestandsbeheer

Liever een grafische omgeving om bestanden uit te voeren? Dat kan! Wine
heeft een eigen *Verkenner*

`wine winefile`

of een moderne variant:

`wine explorer`

of een "echt" bureaublad met een taakbalk:

`wine explorer /desktop=shell,1024x786`

### Variabelen van Wine

#### `WINEDEBUG`=*kanaal*

Wine is niet perfect en veel Windows programma's werken dan ook nog niet
foutloos met Wine. (Veel Windows programma's werken ook niet foutloos
onder Windows, maar dat is een ander verhaal.) Om het makkelijker te
maken te zien wat er fout gaat heeft Wine verschillende debug kanalen.

Als een kanaal geactiveerd is, wordt de uitvoer naar de console gestuurd
waar `wine` is aangeroepen. Deze uitvoer kan dan naar een bestand toe
worden gestuurd om het rustig door te nemen. Er kan erg veel uitvoer
geproduceerd worden. Het `relay` kanaal geeft bijvoorbeeld een melding
wanneer een win32 functie wordt aangeroepen. Logbestanden van meerdere
honderden MB's zijn dan ook geen uitzondering. Het gebruik van de
foutopsporing kan Wine langzamer maken. Gebruik het dus alleen als het
echt nodig is.

Elk debug-kanaal heeft vier niveaus. Elk niveau geeft de ernst van de
fout aan. De vier niveaus zijn: `fixme`, `warn`, `err` en `trace`.

Om een kanaal aan te zetten gebruik `niveau+kanaal`. Om er één uit te
zetten gebruik `niveau-kanaal`. Om meerdere kanalen in te stellen,
gebruik een komma tussen de kanalen. Om bijvoorbeeld alle berichten van
het niveau `warn` te zien voor het kanaal `heap`, gebruik:

`WINEDEBUG=warn+heap wine programma.exe`

Als het niveau niet wordt gegeven, dan worden alle vier niveaus getoond.

`WINEDEBUG=heap wine programma.exe`

Voor alle log berichten behalve `relay`

`WINEDEBUG=+all,-relay wine programma.exe`

Lees meer op de [Debug kanalen](Debug-Channels) pagina en in
de [Ontwikkelaarshandleiding van Wine](Wine-Developer's-Guide).

#### `WINEDLLOVERRIDES`=*DLL aanpassing*

Helaas werken niet alle programma's goed met de ingebouwde
DLL-bestanden van Wine. Soms werken de DLL-bestanden van Windows
beter. In `winecfg` kan in het tabblad *Bibliotheken* een DLL
aanpassing worden ingevoerd. Dit kan ook vanaf de opdrachtregel met de
`WINEDLLOVERRIDES` variabele.

Voorbeelden:

Probeer eerst de Windows varianten van `comdlg32` en `shell32`. Mochten
die niet geladen kunnen worden, gebruik de in Wine ingebouwde variant:

`WINEDLLOVERRIDES="comdlg32,shell32=n,b" wine programma.exe`

De Windows varianten van `comdlg32` en `shell32` worden geladen. En als
het programma `c:\foo\bar\baz.dll` nodig heeft, gebruik dan de
ingebouwde variant van `baz`:

`WINEDLLOVERRIDES="comdlg32,shell32=n;c:\\foo\\bar\\baz=b" wine programma.exe`

Van `comdlg32` wordt eerst de ingebouwde variant geladen en als dat
mislukt de Windows variant. Van `shell32` wordt altijd de ingebouwde
variant geladen. Van `comctl32` de Windows variant en `oleaut32` wordt
nooit geladen.

`WINEDLLOVERRIDES="comdlg32=b,n;shell32=b;comctl32=n;oleaut32=" wine programma.exe`

Lees ook de paragraaf [DLL Aanpassingen](#dll-aanpassingen)
voor meer informatie.

#### `WINEARCH`=*architectuur*

Met deze variabele kan de Windows-architectuur voor de
[wineprefix](#wineprefix-map) worden ingesteld. Gebruik `win32` voor de
32-bit variant of `win64` voor de 64-bit (WoW64) variant. Standaard
maakt Wine de 64-bit variant aan. Als eenmaal de prefix is aangemaakt
kan de architectuur niet meer gewijzigd worden. Deze variabele hoeft
dus ook alleen bij het aanmaken van de prefix worden opgegeven. Mocht
de opgegeven architectuur niet overeen komen met de architectuur van
de prefix, dan wordt er een foutmelding getoond en wordt Wine gestopt.

Voorbeeld:

Maak een 32-bit wineprefix in `~/.wine32` aan:

`WINEARCH=win32 WINEPREFIX=~/.wine32 wineboot`

#### `WINEPREFIX`=*map*

Geeft de locatie van de wineprefix aan. Standaard is dit de verborgen
`.wine`-map in de thuismap. Er kunnen zoveel prefixen aangemaakt worden
als nodig is. De enige criteria zijn dat de map nog niet bestaat, dat de
gebruiker de eigenaar is van de bovenliggende map en dat de gebruiker
lees- en schrijfrechten heeft. Een map in de thuismap voldoet hier vaak
aan.

Voorbeeld:

Maak een wineprefix aan in `~/Downloads/Wine`:

`WINEPREFIX=~/Downloads/Wine wineboot`

#### `WINEDLLPATH`=*map*

Voeg een map toe waarin naar de ingebouwde DLL-bestanden wordt gezocht.
Als meerdere mappen moeten worden opgegeven, gebruik dan een dubbelepunt
tussen de mappen.

#### `WINEPATH`=*map*

Voeg een map toe aan de Windows-`PATH` variabele. Gebruik
Windows-mapnamen. Meerdere mappen worden door een puntkomma gescheiden.
Deze optie kan ook in het register onder `HKEY_CURRENT_USER/Environment`
worden gezet. Maak daarvoor de `PATH`-sleutel aan

#### OSS Audio-stuurprogramma

Gebruikers die het OSS audio-stuurprogramma gebruiken en meerdere
apparaten hebben, kunnen met de volgende variabelen het te gebruiken
apparaat instellen.

- `AUDIODEV`=*audio apparaat*
- `MIXERDEV`=*mixer apparaat*
- `MIDIDEV`=*MIDI apparaat*

Voorbeeld:

`AUDIODEV=/dev/dsp4 MIXERDEV=/dev/mixer1 MIDIDEV=/dev/midi3 wine programma.exe`

### `wineserver` opties

De **wineserver** wordt automatisch gestart als Wine opstart. Toch heeft
**wineserver** nuttige opdrachtregelopties. Deze kunnen worden gegeven
als de **wineserver** handmatig wordt gestart of in een script.

#### -d*n*, --debug=*n*

Zet het debug-niveau naar *n*. Een *0* betekent geen debug-informatie.
*1* is het standaard niveau en *2* is extra veel informatie.

##### -f, --foreground

Houd de *wineserver* op de voorgrond. Handig voor foutopsporing.

##### -h, --help

Laat de helptekst zien.

#### -k*n*, --kill=*n*

Stop de *wineserver*. Eventueel kan met *n* het kill-signaal worden
gegeven. Standaard is `SIGINT` en daarna `SIGKILL`. Welke *wineserver*
er moet worden gestopt, wordt bepaald door de `WINEPREFIX` variabele.

#### -p*n*, --persistent=*n*

Geeft aan hoe lang de *wineserver* moet blijven bestaan nadat alle
programma's zijn afgesloten. Dit kan handig zijn als er veel programma's
vlak na elkaar worden gestart en gestopt. De vertraging *n* is in
seconden. Als er geen cijfer wordt gegeven, blijft de *wineserver*
altijd bestaan.

##### -w, --wait

Wacht totdat de *wineserver* is afgesloten. Deze optie is vooral handig
in scripts.

#### Windows/DOS Variabelen

Windows en DOS variabelen kunnen ook via de opdrachtregel aan Wine
worden doorgeven.

`WINDOWSVAR=variabele wine programma.exe`

Bij sommige variabelen is dit niet mogelijk omdat \*NIX deze zelf ook
gebruikt. Deze kunnen worden ingesteld in het register onder
`HKEY_CURRENT_USER/Environment` of als `PATH` moet worden aangepast,
gebruik de `WINEPATH` en `WINEDLLPATH` variabelen.

#### Tekst mode programma's

Tekst mode programma's zijn programma's die alleen maar tekst laten
zien. In Windows worden dit ook wel *CUI (Console User Interface)*
programma's genoemd. In tegenstelling tot *GUI (Graphical User
Interface)* programma's die vensters en andere afbeeldingen laten zien.

Op Wine kunnen deze programma's op twee verschillende manier worden
uitgevoerd.

- **`wine`** *programma.exe*
- **`wineconsole`** *programma.exe*

De optie *`--backend=`* voor **`wineconsole`** bestaat niet meer.

**`wineconsole`** heeft zijn eigen configuratiemenu. Deze kan met de
rechtermuisknop worden opgeroepen. De volgende opties zijn beschikbaar:

| Configuratie optie           | Uitleg                                                                                                                                                                                                                                               |
|------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Cursorgrootte                | Het formaat van de cursor. De drie opties zijn: Klein (33% van de hoogte), Middel (66%) en Groot (100%).                                                                                                                                             |
| Pop-up menu                  | Het kan voorkomen dat het programma dat in **`wineconsole`** wordt uitgevoerd de rechtermuisknop nodig heeft. Dan kan met deze optie een toets worden toegevoegd. Het menu kan dan met de rechtermuisknop en `Control` of `Shift` worden opgeroepen. |
| Geschiedenis                 | Hoeveel commando's er onthouden moeten worden.                                                                                                                                                                                                       |
| Snel bewerken                | Aan: De linkermuisknop gebruiken om tekst te selecteren en te kopiëren. Uit: het programma bepaalt wat er gebeurd.                                                                                                                                   |
| Lettertype                   | Stel het lettertype, de tekst kleur, de achtergrond kleur en de tekst grootte in.                                                                                                                                                                    |
| Bufferzone en Venstergrootte | De venstergrootte stelt het formaat van het venster in. De bufferzone geeft aan hoeveel informatie er getoond kan worden. Als de bufferzone groter is dan het venster komen er schuifbalken om het venster.                                          |
| Einde programma              | Het scherm sluiten als het programma gestopt is.                                                                                                                                                                                                     |
| Modus                        | Gebruik de Emacs of de Windows modus om de cursor te verplaatsen.                                                                                                                                                                                    |

## Wine configureren

Veel gebruikte instellingen in Wine kunnen worden aangepast met
**`winecfg`**. Hieronder volgt eerst een stap-voor-stap handleiding door
alle instellingen met een uitleg over de beschikbare opties in
**`winecfg`**. Daarna volgt een deel over welke geavanceerde
instellingen en configuratie opties er gedaan kunnen worden in
**`regedit`**. Tot slot komen er instellingen aan de beurt die niet in
*`winecfg`* en *`regedit`* thuishoren.

### Winecfg gebruiken

Als de installatie van Wine goed gegaan is, is **`winecfg`** automatisch
geïnstalleerd. Vaak is het mogelijk om `winecfg` te starten met:

`winecfg`

Anders kan `winecfg` gestart worden met:

`wine winecfg`

#### Winecfg - Opdrachtregel

Vanaf de opdrachtregel kan `winecfg` de gebruikte Windows versie tonen
en wijzigen.

Om de standaard ingestelde Windows versie te tonen:

`winecfg /v`

Om de versie van de standaardinstelling te wijzigen:

`winecfg /v versie`

De geldige Windows versies zijn te vinden met:

`winecfg /?`

Mocht het bovenstaande niet werken, gebruik
`wine`**`64`**` winecfg /optie` als opdracht.

Zonder parameter wordt `winecfg` in de grafische modus uitgevoerd.

#### Winecfg – Grafisch

Als het programma gestart is zijn er zeven tabbladen zichtbaar:

- [Toepassingen](#toepassingen)
- [Bibliotheken](#bibliotheken)
- [Grafisch](#grafisch)
- [Desktop Integratie](#desktop-integratie)
- [Schijven](#schijven)
- [Geluid](#geluid)
- [Over Wine](#over-wine)

Instellingen in *Toepassingen* en *Bibliotheken* hebben de meeste
invloed op de programma's die uitgevoerd worden. De andere tabbladen
gaan meer over hoe Wine werkt.

De tabbladen *Toepassingen*, *Bibliotheken* en *Grafisch* zijn met
elkaar verbonden. Als onder *Toepassingen* *"Standaardinstellingen"* is
geselecteerd, dan gelden de instellingen in de andere twee tabbladen
voor alle programma's. Als er een specifiek programma geselecteerd is
dan gelden de tabbladen *Bibliotheken* en *Grafisch* alleen voor dat
programma. Op deze manier kan voor verschillende programma's andere
instellingen worden gebruikt.

------------------------------------------------------------------------

##### **Toepassingen**

Wine kan verschillende Windows versies nadoen. De grootste verschillen
zitten tussen *Windows 98*, *Windows XP* en *Windows 7*. Standaard staat
de versie op *Windows 7*. Sommige programma's controleren alleen de
Windows versie als ze opstarten. Andere programma's gebruiken andere
bibliotheken als ze onder een andere Windows versie draaien. Oudere
Windows programma's kunnen van slag raken als er een te nieuwe versie is
ingesteld. Als een programma niet werkt, probeer dan eerste een andere
Windows versie.

Om een Windows versie ouder dan *Windows XP* te kunnen kiezen, moet er
een 32-bit prefix worden gemaakt. Lees de [veel gestelde
vragen](nl/FAQ#hoe-maak-ik-een-32-bit-wineprefix-op-een-64-bit-systeem)
voor meer informatie.

Als *Standaardinstellingen* is geselecteerd dan is de standaard Windows
versie zichtbaar. Deze versie wordt voor alle programma's gebruikt. Om
een programma een andere Windows versie te laten gebruiken, voer de
volgende stappen uit:

1.  Kies *Toepassing toevoegen*
2.  Blader naar het programma
3.  Verander *Gebruik de standaardinstellingen* naar de gewenste Windows
    versie

------------------------------------------------------------------------

##### **Bibliotheken**

Windows programma's hebben DLL-bestanden nodig om uitgevoerd te kunnen
worden. Wine bootst veel deze bestanden na, de zogenaamde ingebouwde
DLL-bestanden. Deze zijn speciaal voor Wine gemaakt en hebben geen
Windows licentie nodig. Hoewel ze voor veel programma's perfect werken,
hebben sommige ingebouwde DLL-bestanden enkele tekortkomingen. Daarom
heeft Wine de mogelijkheid om Windows DLL-bestanden te gebruiken.

###### DLL Aanpassingen

De Windows versies van `kernel32.dll`, `gdi32.dll`, `user32.dll`, en
`ntdll.dll` werken **niet** in Wine. Deze DLL-bestanden hebben op
Windows toegang tot de kernel. Dit heeft Wine niet.

Het is niet altijd mogelijk om de ingebouwde DLL-bestanden te gebruiken.
Soms wordt een Windows versie aanbevolen om een programma werkend te
krijgen. Een andere keer wordt er door het installatieprogramma een
DLL-bestand overschreven in `C:\windows\system32`. Om het nieuwe
DLL-bestand te kunnen gebruiken, moet dit aan Wine worden doorgegeven.
Dit kan voor alle programma's (de *Standaardinstellingen* optie in het
*Toepassingen* tabblad) of alleen voor een specifiek programma dat in
het *Toepassingen* tabblad is aangeduid.

Om een DLL aanpassing te maken voor bijvoorbeeld `foo.dll`, type `foo`
in het veld *Nieuwe DLL aanpassing* en kies *Toevoegen*. Hierna kan het
gedrag van de DLL worden bepaald. Selecteer daarvoor de DLL onder
*Bestaande aanpassingen* en kies *Bewerken*. Standaard staat de
laadvolgorde op eerst *Windows dan Ingebouwd*, maar dit kan ook op
alleen *Windows* of alleen *Ingebouwd* worden ingesteld. Of eerst de
ingebouwde versie en dan pas de Windows versie. Of zelfs de DLL helemaal
uitschakelen.

Gewoonlijk worden de DLL-bestanden in de volgende volgorde geladen:

1.  De map waar het programma in staat
2.  De huidige map
3.  De Windows *system* map
4.  De Windows map
5.  De mappen in de *PATH* variabele

###### Missende DLL-bestanden

Wine kan onmogelijk alle DLL-bestanden die er bestaan inbouwen. Als er
een melding is over een missend DLL-bestand zoek dan eerst op bij welk
programma het DLL-bestand hoort. Vaak is het een onderdeel van een zo
genaamd *Redistributable*-pakket. Ook kan het voorkomen dat het bij een
ander programma hoort.

Bijvoorbeeld:

`err:module:import_dll Library MFC42.DLL (which is needed by C:\\Program Files\\Foo\\Bar.dll) not found`

*MFC42.DLL* is een onderdeel van *Visual C++ 6*. Dit pakket kan
gemakkelijk worden geïnstalleerd met
[winetricks](nl/Winetricks). Houd er rekening mee dat deze
pakketten hun eigen licentie hebben die niet onder Wine vallen.

Doorzoek de [Programma Database](https://appdb.winehq.org) om te kijken
of een DLL aanpassing nodig is voor een programma.

------------------------------------------------------------------------

##### **Grafisch**

Op dit tabblad staan vijf instellingen. Gewoonlijk zijn de
standaardinstelling goed, maar er zijn situaties waarin deze aangepast
moeten worden.

- *Indien volledig scherm, vang de muis automatisch af*


Deze optie kan gebruikt worden bij spellen die in een volledig scherm
worden gespeeld. De muis blijft dan in het scherm van het spel en kan
niet naar een andere monitor of virtueel bureaublad.

- *Laat de vensterbeheerder de vensters decoreren*


Sommige programma's hebben hun eigen knoppen in de vensterbalk staan.
Schakel deze optie uit om deze knoppen zichtbaar te maken.

- *Laat de vensterbeheerder de vensters beheren*


Hetzelfde als de optie hierboven en de vensters kunnen ook niet
verplaatst worden.

- *Maak een virtueel bureaublad*


Voer de Wine programma's uit in een virtueel bureaublad met de opgeven
afmetingen. Op deze manier kunnen programma's worden uitgevoerd die de
schermresolutie proberen aan te passen en daarin niet slagen.

- *Schermresolutie*


Pas de lettergrootte met de schuifbalk aan.

------------------------------------------------------------------------

##### **Desktop Integratie**

Wine ondersteunt Windows `.mmstyles` Thema bestanden. Hiermee kan het
uiterlijk van de programma's die in Wine uitgevoerd worden, worden
aangepast. Ook is het mogelijk om per *onderdeel* zelf de kleuren,
grootte of lettertype aan te passen.

Schakel het vakje *Toestaan dat bestandsassociaties worden gemaakt* uit
als niet gewenst is dat Windows programma's in Wine bestandsassociaties
van het besturingssysteem kunnen aanpassen. Op deze manier kan
bijvoorbeeld voorkomen worden dat `.docx` bestanden opeens met Microsoft
Office worden geopend. Dit kan ook met: `winetricks mimeassoc=off`

Standaard koppelt Wine sommige mappen van de virtuele Windows
installatie aan mappen van de gebruiker. Zo is de map
`C:\users\gebruiker\Mijn Documenten` in Wine de `~/Documenten`
map. Pas deze verwijzingen naar keuze aan of gebruik
`winetricks isolate_home` om ze allemaal leeg te maken.

------------------------------------------------------------------------

##### **Schijven**

Op dit tabblad kunnen virtuele Windows schijven worden gemaakt en
verwijderd. Wine maakt deze verwijzingen automatisch aan. De `C:`-schijf
wordt standaard aan `~/.wine/drive_c` gekoppeld en mocht er een DVD
zijn, dan wordt deze aan `D:` gekoppeld. Om alle bestanden op het
besturingssysteem toegankelijk te maken, wordt `/` aan de `Z:`-schijf
gekoppeld.

Onder *Uitgebreide Opties*, *"Soort"* kan ingesteld worden hoe de schijf
zich moet gedragen. Dit kan zijn als *Harde schijf*,
*Netwerkverbinding*, *Diskettestation* of als *CD-ROM*. Met *CD-ROM*
kan, voor sommige spellen die controleren of de CD aanwezig is, een
virtuele cd-rom speler worden gemaakt.

Bij *Naam* en *Serie-nr* kunnen desgewenst de naam en het serienummer
van de schijf worden ingevoerd. Deze worden opgeslagen in de bestanden
`.windows-label` en `.windows-serial` in de hoofdmap van de schijf. De
gebruiker heeft dus schrijftoegang nodig om deze in te stellen. Daarom
kan voor de `Z:`-schijf dit ook niet opgeslagen worden. De gebruiker
heeft immers geen schrijfrechten op `/`.

*Toon verborgen bestanden* Standaard worden de verborgen bestanden en
mappen (bestanden of mappen die beginnen met een punt) niet getoond in
Wine. Schakel dit vakje in om deze mappen en bestanden wel te tonen in
bijvoorbeeld `wine explorer`.

De schijven kunnen ook handmatig gemaakt worden. Het zijn namelijk
symbolische koppelingen in de `~/.wine/dosdevices` map:

```
~/.wine/dosdevices$ ls -l
lrwxrwxrwx 1 wineuser wineuser 10  8 mrt  2020 c: -> ../drive_c
lrwxrwxrwx 1 wineuser wineuser  8  8 mrt  2020 d:: -> /dev/sr0
lrwxrwxrwx 1 wineuser wineuser  1  8 mrt  2020 z: -> /
```

Om bijvoorbeeld de *Downloads* map te koppelen aan de `E:`-schijf,
gebruik:

`ln -s ~/Downloads ~/.wine/dosdevices/e:`

------------------------------------------------------------------------

##### **Geluid**

Op dit tabblad staan de audio instellingen van Wine. Het stuurprogramma
wordt automatisch door Wine gedetecteerd. Deze kan alleen in het
register of met `winetricks sound=stuurprogramma` worden veranderd.

Bij de overige instellingen kan worden gekozen welk apparaat er gebruikt
moet worden voor welke functie.

------------------------------------------------------------------------

##### **Over Wine**

Op dit tabblad staat de Wine versie die gebruikt wordt. Ook kan een naam
en organisatie worden opgegeven. Deze gegevens kunnen door sommige
programma's worden gebruikt.

------------------------------------------------------------------------

### Het register en `regedit`

Behalve de instellingen op het tabblad *Schijven* worden alle
veranderingen die in `winecfg` gemaakt worden in het register veranderd
en opgeslagen. In Windows is het register de centrale plaats waar
programma's hun configuratie en wijzigingen opslaan. Wine doet hetzelfde
en sommige instellingen die niet in `winecfg` staan, kunnen wel in het
register worden ingesteld. (Waarschijnlijk is de kans groter dat in het
register een verandering voor een programma moet worden gemaakt, dan dat
er een verandering voor Wine moet worden gemaakt.)

Dat Wine zijn instellingen opslaat in het register is in het begin nog
al controversieel geweest. Het was teveel als Windows. Toch zijn er
goede redenen om dit wel te doen. Er moest sowieso een register komen.
Windows programma's gaan er immers vanuit dat er een register is. Als de
instellingen van Wine op een andere manier moeten worden opgeslagen,
betekent dat er aparte code voor geschreven moet worden die hetzelfde
doet als wat de Win32 API al doet. Dus dan zijn er twee stukken code die
eigenlijk hetzelfde doen.

#### Structuur van het register

Het Windows register heeft een uitgebreide boomstructuur. Zelfs de
meeste Windows programmeurs zijn niet helemaal op de hoogte hoe het
register is uitgewerkt, met zijn verschillende plekken en de talloze
verbindingen er tussen. Een volledig overzicht wordt hier niet gegeven.
Daar zijn andere handleidingen voor. Wel de basis register sleutels:

- HKEY_LOCAL_MACHINE
Deze belangrijke hoofdsleutel (`system.dat` in Win9x) bevat alles met
betrekking tot de huidige Windows installatie. Dit wordt vaak afgekort
tot `HKLM`.

- HKEY_USERS
Deze belangrijke hoofdsleutel (`user.dat` in Win9x) bevat de
configuratie voor elke gebruiker.

- HKEY_CLASSES_ROOT
Dit is een verwijzing naar `HKEY_LOCAL_MACHINE\Software\Classes`. Het
bevat data die onder andere bestandsassociaties, OLE document handlers,
en COM classes beschrijft.

- HKEY_CURRENT_USER
Dit is een verwijzing naar `HKEY_USERS\your_username`. In andere
woorden: de persoonlijke configuratie.

#### Register bestanden

Het register wordt in Wine in drie verschillende bestanden in de
`~/.wine` map opgeslagen:

- `system.reg` Dit bestand bevat `HKEY_LOCAL_MACHINE`.
- `user.reg` Dit bestand bevat `HKEY_CURRENT_USER`.
- `userdef.reg` Dit bestand bevat `HKEY_USERS\.Default` (de standaard
gebruikersconfiguratie).

Deze bestanden worden automatisch aangemaakt als Wine voor de eerste
keer wordt gebruikt. De instellingen staan in `wine.inf` en worden door
`rundll32.exe` verwerkt en in het register verwerkt. Het register wordt
ook bijgewerkt als `wine.inf` wordt aangepast. Dit gebeurd bijvoorbeeld
als er van Wine versie of soort wordt veranderd.

Het wordt niet aangeraden om handmatig de bovenstaande bestanden te
wijzigen. Gebruik `regedit.exe`, `reg.exe` of een ander
registerbewerkingsprogramma.

#### `regedit` gebruiken

Een makkelijke manier om het register te openen en te bewerken is met
`wine regedit`. Dit programma is hetzelfde als zijn Windows variant. Na
het starten van het programma is de boomstructuur zichtbaar.

Door middel van van dubbel-klikken op of met het plusje voor de map, kan
door het register gebladerd worden. Met de rechtermuisknop kan een map
of sleutel worden bewerkt of verwijderd.

Veel instellingen van Wine worden opgeslagen in
`HKEY_CURRENT_USER\Software\Wine`. Hier staan ook de meeste wijzigingen
die in `winecfg` zijn gemaakt.

#### Systeembeheer tip

Het `system.reg` bestand kan gebruikt worden om de verschillende
wineprefixes van de gebruikers te beheren.

1.  Installeer en configureer alle programma's die de gebruikers in Wine
    mogen gebruiken.
2.  Kopieer de wineprefix naar de gebruikers.
3.  Maak een symbolische koppeling van de oorspronkelijke `system.reg`
    naar de `system.reg` van de gebruikers.

Het kan verleidelijk zijn om dit ook voor `user.reg` te doen, maar
iedere gebruiker moet schrijfrechten hebben op dit bestand.

#### Complete lijst van Registersleutels

Een lijst van [handige registersleutels](Useful-Registry-Keys) is in
de wiki te vinden.

### Overige aanpassingen

In deze paragraaf komen de overige aanpassingen die voor Wine gedaan
kunnen worden aan de orde. Ook staan er tips en trucs om het meeste uit
Wine te halen.

#### Seriële en parallelle poorten

Sinds Wine versie 2.8 worden de seriële en parallelle poorten
automatisch herkend. Op Windows worden de seriële poorten
`COMnummer` genoemd. De parallelle poorten `LPTnummer`. Linux,
MacOS en BSD gebruiken hun eigen namen, maar op ieder besturingssysteem
is het apparaat terug te vinden in `/dev`. De verwijzingen zijn terug te
vinden in `~/.wine/dosdevices`.

```
~/.wine/dosdevices$ ls -l
lrwxrwxrwx 1 wineuser wineuser 10  8 mrt  2020 c: -> ../drive_c
lrwxrwxrwx 1 wineuser wineuser 10  8 mrt  2020 com1 -> /dev/ttyS0
lrwxrwxrwx 1 wineuser wineuser 10  8 mrt  2020 com2 -> /dev/ttyS1
lrwxrwxrwx 1 wineuser wineuser 10  8 mrt  2020 com3 -> /dev/ttyS2
lrwxrwxrwx 1 wineuser wineuser 10  8 mrt  2020 com4 -> /dev/ttyS3
lrwxrwxrwx 1 wineuser wineuser 12  8 mrt  2020 com5 -> /dev/ttyUSB0
lrwxrwxrwx 1 wineuser wineuser  8  8 mrt  2020 d:: -> /dev/sr0
lrwxrwxrwx 1 wineuser wineuser  1  8 mrt  2020 z: -> /
```

Zorg er voor dat de gebruiker de benodigde rechten heeft om de poorten
te kunnen gebruiken. Op Linux is dat vaak voor de seriële poorten de
groep `sys` of `dialout` en lid van de groep `lp` voor de parallelle
poort.

In het register kunnen de standaard poorten veranderd worden. Maak met
**`wine regedit`** een nieuwe sleutel in
`HKEY_LOCAL_MACHINE\Software\Wine\Ports` aan. Met als naam de Windows
poort en als waarde het Unix pad. Om bijvoorbeeld `COM1` naar het eerste
USB apparaat te laten verwijzen, maak een sleutel aan met de naam `COM1`
met als waarde `/dev/ttyUSB0`. De volgende keer als Wine wordt gestart
worden dee veranderingen doorgevoerd. (Gebruik **`wineserver -k`** om
alle programma's in Wine te sluiten.)

#### Lettertypes

Kopieer de benodigde *TrueType* (`.ttf`) lettertypes naar
`c:\windows\fonts`.

Veel voorkomende lettertypes kunnen op verschillende Linux distributies
worden geïnstalleerd met het pakket `ttf-mscorefonts-installer`. Of
gebruik `winetricks corefonts` om de lettertypes alleen voor Wine te
installeren.

#### Printers

Wine maakt gebruik van CUPS om een printer te gebruiken. Als het
besturingssysteem CUPS gebruikt, installeer en configureer dan eerst de
printer en Wine zal de printer automatisch herkennen.

Als CUPS niet aanwezig is op het besturingssysteem, dan wordt het oude
BSD-print systeem gebruikt:

- Alle printers in `/etc/printcap` worden automatisch in Wine
  geïnstalleerd.
- Iedere printer heeft een PPD-bestand nodig (`generic.ppd` zit in
  Wine).
- Het `lpr` commando wordt gebruikt om een bestand af te drukken.

#### Scanners

De TWAIN API wordt in Windows voor een scanner gebruikt. Wine gebruikt
de ingebouwde TWAIN DLL om de gegevens door te geven aan de Linux SANE
bibliotheken. De scanner moet dus eerst geïnstalleerd en geconfigureerd
worden met SANE, voordat Wine hem kan gebruiken. Zorg er ook voor dat
het `xscanimage` programma geïnstalleerd is.

#### ODBC Databases

Net zo als met printers en scanners, gebruikt Wine `odbc32.dll` om de
gegevens door te geven aan een beschikbare Unix ODBC provider
(bijvoorbeeld `unixODBC`). Als `odbc32.dll` ingesteld staat als
*ingebouwd* dan wordt dus een Unix ODBC pakket gebruikt. Als
`odbc32.dll` wordt ingesteld op *windows* dan kunnen de ODBC32
stuurprogramma's worden gebruikt.

##### Unix ODBC

Zorg eerst dat ODBC (zoals `unixODBC` of een ODBC-ODBC brug) op het
besturingssysteem werkt. Vaak komen deze programma's met het
hulpprogramma `isql` om te kijken of alles werkt.

De volgende stap is om de Unix ODBC bibliotheken te koppelen aan
`odbc32.dll` van Wine. De ingebouwde `odbc32` doorzoekt eerst de
`LIB_ODBC_DRIVER_MANAGER` variabele. Bijvoorbeeld:

`LIB_ODBC_DRIVER_MANAGER=/usr/lib/libodbc.so.1.0.0`

Als deze variabele niet voorkomt, dan wordt gezocht naar `libodbc.so`.
Een symbolische koppeling kan dan gebruikt worden:

```
ln -s libodbc.so.1.0.0 /usr/lib/libodbc.so
/sbin/ldconfig
```

Bij problemen kan `WINEDEBUG=+odbc32` gebruikt worden om te zien wat er
mis gaat. Lees ook *How to use unixODBC with Wine* op de pagina van
unixodbc.org

##### Windows ODBC

Ook kunnen de Windows ODBC stuurprogramma's worden gebruikt. Gebruik
hiervoor eerst `mdac_typ.exe` en configureer daarna met `CLICONFG.EXE`
en `ODBCAD32.EXE` in de verbinding. (`Winetricks` heeft hier handige
hulpmiddelen voor: `mdac27`,`mdac28` en `native_mdac`.) Vergelijk de
uitkomst van deze programma's met die van een Windows installatie.
Mochten er protocollen ontbreken dan kunnen die waarschijnlijk samen met
de register vermeldingen van een Windows installatie naar Wine worden
gekopieerd.

## Problemen oplossen en fouten melden

### Wat te doen als een programma niet werkt

Als er echt al van alles geprobeerd is en het programma werkt nog steeds
niet, geen paniek, hier is hulp!

#### De Wine installatie controleren

- Kijk eerst of de laatste versie van Wine is geïnstalleerd met
  **`wine --version`**.
- Open **`winecfg`** en bekijk of alle opties goed staan.
- Bekijk in de map `~/.wine/dosdevices` of de verwijzing naar `c:`
  klopt.

#### Verschillende Windows versies

In sommige gevallen kan het helpen om een andere [Windows
versie](#wine-configureren) te gebruiken. (Dit kan betekenen
dat er ook een nieuwe wineprefix moet worden gemaakt.)

#### Programma anders starten

Soms kan het helpen als het programma op een andere manier wordt gestart
met Wine. Probeer de volgende manieren:

```
wine c:\\pad\\naar\\het\\programma.exe
wine ~/.wine/drive_c/pad/naar/het/programma.exe
cd ~/.wine/drive_c/pad/naar/het/
wine programma.exe
```

#### DLL aanpassingen

Start het programma met het debug kanaal `+loaddll`.

`WINEDEBUG=+loaddll wine programma.exe`

Op deze manier is zichtbaar welke DLL-bestanden er gebruikt worden.
Probeer de Windows versie van [DLL-bestanden](#bibliotheken)
uit of probeer een andere laadvolgorde.

#### Controleer het systeem

Misschien is er iets mis met één van de onderdelen waar Wine van
afhankelijk is. **Vaak komt het voor dat de 32-bit bibliotheken of
stuurprogramma's niet (goed) zijn geïnstalleerd.** (Zijn ook alle
*aanbevolen* afhankelijkheden van Wine geïnstalleerd?)

#### Andere GUI modes

Gebruik het tabblag *Grafisch* in `winecfg` om de vensterbeheerder uit
te schakelen of een virtueel bureaublad te maken.

#### Controleer het programma

Sommige programma's hebben een kopieerbeveiliging. Helaas werken deze
niet (goed) samen met Wine.

#### Nieuwe wineprefix

Het kan zijn dat de wineprefix kapot is gegaan of dat een update van de
prefix niet goed is gegaan. Probeer dan het programma in een eigen,
nieuwe prefix uit. De oude prefix kan als reservekopie gehouden worden.
Kopieer de benodigde bestanden van de oude naar de nieuwe en als alles
weer goed werkt kan de oude prefix vervangen worden door de nieuwe.

#### Meer informatie

Misschien heeft iemand anders al hetzelfde probleem gehad. Bekijk daarom
ook:

- De [Programma Database](https://appdb.winehq.org). Staat de goede
  versie er niet tussen? Kijk dan ook bij de andere versies.
- Stel de vraag op [IRC](nl/IRC).
- Gebruik [winetricks](nl/Winetricks) om *Visual Basic*, *.Net*,
  *Visual C++* en andere zo genaamde
  *[Redistributable](#missende-dll-bestanden)* pakketten te
  installeren.
- Doorzoek of stel de vraag op het [Wine
  forum](https://forum.winehq.org/) of doorzoek de
  [mailinglijsten](nl/Forums).
- Probeer het programma in Crossover.

#### Fouten opsporen

Gebruik de [ontwikkellaarshandleiding](Wine-Developer's-Guide) om de
oorzaak van de fout te vinden.

### Fouten melden

Fouten kunnen gemeld melden op Bugzilla. Lees [Bugs](Bugs)
voor meer informatie.
