---
title: Debian/Ubuntu
---



<small>
&nbsp;[:flag_gb: English](Debian-Ubuntu)
&nbsp;[:flag_cn: 简体中文](zh_CN/Debian-Ubuntu)
</small>

-----


Hoewel Debian/Ubuntu zijn eigen pakketten voor Wine heeft, zijn deze vaak
verouderd. Om de installatie van de nieuwste Wine versie zo makkelijk
mogelijk te maken heeft WineHQ zijn eigen Debian/Ubuntu
repository. Als een nieuwere versie problemen geeft, dan kan er ook
oudere versie naar keuze geïnstalleerd worden.

## Voorbereiding

- Op 64-bit systemen moeten ook 32-bit pakketten geïnstalleerd kunnen
  worden. Als dit nog niet gedaan is, schakel dit in met:

  ```sh
  sudo dpkg --add-architecture i386
  ```

- Zoek de naam op van je distributie:  
  Achter `UBUNTU_CODENAME` of `VERSION_CODENAME` staat de naam van de distributie.
  Als beide voorkomen, gebruik dan de naam achter `UBUNTU_CODENAME`

  ```sh
  cat /etc/os-release
  ```

## WineHQ archief toevoegen

- Voeg de verificatiesleutel van de WineHQ server toe aan het systeem:

  ```sh
  sudo mkdir -pm755 /etc/apt/keyrings
  wget -O - https://dl.winehq.org/wine-builds/winehq.key | sudo gpg --dearmor -o /etc/apt/keyrings/winehq-archive.key -
  ```

- Voeg de repository toe:
  Als de naam van je distributie niet voorkomt in de lijst, dan kunnen er [oudere pakketten](#mijn-debianubuntu-versie-staat-er-niet-tussen) aanwezig zijn op de server van WineHQ. Voeg **één** repository toe.

  | Distributie&nbsp;naam       | Opdracht                       |
  |:----------------------------|:-------------------------------|
  | **oracular**<br><small>Ubuntu 24.10</small>                  | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/oracular/winehq-oracular.sources`       |
  | **noble**<br><small>Ubuntu 24.04<br>Linux Mint 22</small>    | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/noble/winehq-noble.sources`       |
  | **jammy**<br><small>Ubuntu 22.04<br>Linux Mint 21.x</small>  | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/jammy/winehq-jammy.sources`       |
  | **focal**<br><small>Ubuntu 20.04<br>Linux Mint 20.x</small>  | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/focal/winehq-focal.sources`       |
  | **trixie**<br><small>Debian Testing</small>                  | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/debian/dists/trixie/winehq-trixie.sources`     |
  | **bookworm**<br><small>Debian 12</small>                     | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/debian/dists/bookworm/winehq-bookworm.sources` |
  | **bullseye**<br><small>Debian 11</small>                     | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/debian/dists/bullseye/winehq-bullseye.sources` |

- Werk de pakketbronnen bij met:

  ```sh
  sudo apt update
  ```

## Wine installeren

Installeer **één** van de volgende pakketten:

| Wine versie        | Opdracht                                               |
|--------------------|--------------------------------------------------------|
| Stable versie      | `sudo apt install --install-recommends winehq-stable`  |
| Development versie | `sudo apt install --install-recommends winehq-devel`   |
| Staging versie     | `sudo apt install --install-recommends winehq-staging` |

 
Op de [User's Guide](nl/Wine-User%27s-Guide#wine-van-winehq) staan de
verschillen tussen de verschillende Wine soorten

## Help

Het kan gebeuren dat er problemen zijn bij het installeren van
Wine. Als het probleem niet hier staat, doorzoek dan het
[forum](https://forums.winehq.org/WineHQ). Mocht ook daar geen
antwoord te vinden zijn, stel dan daar je vraag.

### Missende afhankelijkheden

Lees de
[FAQ](nl/FAQ#ik-krijg-een-foutmelding-dat-wine-niet-geïnstalleerd-kan-worden-omdat-sommige-afhankelijkheden-niet-opgelost-kunnen-worden)
om afhankelijkheidsproblemen op te lossen. De meeste voorkomende
problemen zijn:

#### Pakketbronnen van derden

De WineHQ pakketten worden gemaakt en getest voor een schone
installatie. Het gebruik van PPA's of pakketbronnen van derden kunnen de
installatie van Wine verhinderen. Vaak is het probleem dat deze
repositories niet multiarch zijn. De benodigde 32 en 64-bit pakketten
ontbreken of kunnen niet samen geïnstalleerd worden. De *deb.sury.org*
pakketbron is berucht voor het veroorzaken van problemen. De oplossing
is om van de problematische afhankelijkheden de officiële versie te
installeren.

#### KDE Neon

Ubuntu 22.04 KDE Neon gebruikers melden problemen met de
*libpoppler-glib8* afhankelijkheid. De oplossing is om van dit pakket de
Ubuntu versie te installeren.
`sudo apt install libpoppler-glib8:{i386,amd64}=22.02.0-2ubuntu0.1`

#### Backports

Een andere oorzaak kan het gebruik van backports zijn. Een nieuwe versie
van een 64-bit bibliotheek is al geïnstalleerd, maar de 32-bit versie
nog niet. De pakketten uit de backports repository krijgen een lage
prioriteit mee waardoor ze niet automatisch geïnstalleerd worden.
Installeer handmatig het ontbrekende 32-bit pakket uit backports.

#### Faudio

Oudere versies van Wine (tot versie 6.21) hebben Faudio als
afhankelijkheid. Deze pakketten ontbreken voor Ubuntu 18.04. Download de
pakketten van de [Open Build
Service](https://download.opensuse.org/repositories/Emulators:/Wine:/Debian/xUbuntu_18.04/).
Voor Debian 10, staan deze pakketten in backports.

### Problemen met de Winehq key

- **Op 19 december 2018 is de verificatiesleutel van de WineHQ server
  gewijzigd**

  Mocht de sleutel voor die tijd zijn gebruikt, dan moet deze opnieuw
  worden toegevoegd. Gebruik `sudo apt update` om de veranderingen
  door te voeren.

- **Apt-key is verouderd**

  Vroeger werd *apt-key* gebruikt om de Wine sleutel toe te voegen. Mocht
  hierover een melding komen, verwijder de sleutel met:

  ```sh
  sudo apt-key del "D43F 6401 4536 9C51 D786 DDEA 76F1 A20F F987 672F"
  ```

  Verwijder hierna ook de regel van het WineHQ-archief uit
  */etc/apt/sources.list(.d/)*.

### Mirror sync in progress?

Als er een foutmelding komt bij het installeren van een WineHQ pakket
met de regel `Mirror sync in progress?` dan is dat waarschijnlijk ook
het probleem. Er zijn namelijk heel veel pakketten die gesynchroniseerd
moeten worden, wat lang kan duren.

Wacht een paar uur en probeer het nog een keer. Als het probleem meer
dan een dag duurt, maak dan een foutrapport aan.

### Mijn Debian/Ubuntu versie staat er niet tussen

Wanneer een versie van Debian/Ubuntu niet meer ondersteund wordt,
worden er geen nieuwe Wine pakketten meer voor gemaakt. Je kan op de
[WineHQ server](https://dl.winehq.org/wine-builds/) kijken of er
oudere pakketten beschikbaar zijn. Het toevoegen van de WineHQ server
is niet nodig. Er worden namelijk geen nieuwe versies meer
gemaakt. Het downloaden en installeren van [de vier WineHQ](#notities)
deb-pakketten is voldoende.

Let op: deze pakketten worden niet meer bijgewerkt en worden niet meer
ondersteund.

## Notities

- Er worden geen items in het menu gemaakt voor winecfg en andere
  ingebouwde programma's van Wine. Na het bijwerken van Wine van
  Debian/Ubuntu, zullen deze weggehaald worden. Deze kunnen met een
  menu editor zelf gemaakt worden.

- Wine wordt geïnstalleerd in `/opt/wine-<soort>/`

- Er zijn geen pakketten voor *wine-mono* en *wine-gecko*. Deze worden
  automatisch gedownload en geïnstalleerd als Wine voor de eerste keer
  wordt gestart of als er een nieuwe wineprefix wordt gemaakt. Als dit
  niet lukt, lees dan de wiki pagina's van [Gecko](Gecko) of
  [Mono](Wine-Mono).

- Vanaf Wine 5.7 kunnen de WineHQ pakketten ingesteld worden met debconf
  om gebruik te maken van CAP_NET_RAW. Veel programma's hebben dit niet
  nodig en is om veiligheidsredenen uitgeschakeld. Als dit wel nodig is
  (omdat bijvoorbeeld het programma het ping commando nodig heeft) voer
  dan de volgende opdracht uit en beantwoord de drie vragen met "ja":
  `dpkg-reconfigure wine-<soort>-amd64 wine-<soort> wine-<soort>-i386`

- Er wordt geen Binfmt_misc registratie uitgevoerd. Lees de documentatie
  van Debian/Ubuntu om dit handmatig te doen (`man update-binfmts`).

- Een volledige Wine installatie op een 64-bit systeem bestaat uit vier
  pakketten.

  - `winehq-<soort>` Dit pakket zorgt er onder andere voor dat het
    *wine* commando systeembreed beschikbaar is.

  - `wine-<soort>` Dit pakket heeft de volgende twee pakketten als
    afhankelijkheid en zorgt voor een werkende installatie.

  - `wine-<soort>-amd64` Het 64-bit gedeelte van Wine.

  - `wine-<soort>-i386` Het 32-bit gedeelte van Wine.

  Door Wine over verschillende pakketten te verdelen, is het mogelijk om
  verschillende soorten naast elkaar te installeren.

  Bijvoorbeeld: Gebruik Wine-*stable* als standaard versie en installeer
  Wine-*staging* om sommige programma's te testen.

  Installeer Wine-stable met:

  `sudo apt install --install-recommends winehq-stable`

  Installeer Wine-staging met:

  `sudo apt install --install-recommends wine-staging` (Let op: er staat
  geen *hq* na *wine*)

  Voer een programma uit met Wine-*stable*:

  `wine program.exe`

  Voer een ander programma uit met Wine-*staging*:

  `WINEPREFIX=~/wine-staging /opt/wine-staging/bin/wine program.exe`

  (Het wordt aanbevolen om elke Wine-soort zijn eigen wineprefix te
  geven.)

- Er staan meerdere versies van Wine op de repository. Standaard wordt
  de laatste versie geïnstalleerd. Gewoonlijk wordt de nieuwste versie
  aanbevolen, maar het kan voorkomen dat een oudere versie gewenst is.
  Gebruik `apt policy winehq-<soort>` om de beschikbare versies te
  tonen.

  Een oudere versie kan geïnstalleerd worden met:

  ```sh
  sudo apt install winehq-<branch>=<version>
  ```

  Bijvoorbeeld:

  ```sh
  sudo apt install winehq-staging=7.12~bookworm-1
  ```

  Als de Wine pakketten moeten worden afgewaardeerd, dan moeten alle vier
  de pakketten gegeven worden:

  ```sh
  sudo apt install winehq-staging=7.12~bookworm-1 wine-staging=7.12~bookworm-1 wine-staging-amd64=7.12~bookworm-1 wine-staging-i386=7.12~bookworm-1
  ```

## Een installatie zonder internet

Om Wine op een computer zonder internetverbinding te kunnen installeren
is een tweede computer (of VM) met een internetverbinding nodig. Deze
kan de benodigde deb-pakketten downloaden.

Voeg op de computer met de internetverbinding de WineHQ repository toe.
Haal daarna de pakketten binnen zonder deze te installeren:

```sh
sudo apt-get clean
sudo apt-get --download-only install winehq-<branch>
sudo apt-get --download-only dist-upgrade
```

Kopieër alle deb-bestanden van /var/cache/apt/archives naar een USB
stick:

```sh
cp -R /var/cache/apt/archives/ /media/usb-drive/deb-pkgs/
```

Nu kunnen de deb-pakketen geïnstalleerd worden op de computer zonder
internetverbinding

```sh
cd /media/usb-drive/deb-pkgs sudo dpkg -i *.deb
```

## Wine zelf compileren

- Vanaf versie 4.0-rc2, heeft de WineHQ repository ook de .dsc,
  .diff.gz, en .orig.tar.gz bestanden. Deze bronbestanden kunnen hier
  gevonden worden
  `https://dl.winehq.org/wine-builds/<debian|ubuntu>/dists/<versie>/main/source`
- De nieuwste versies van Debian/Ubuntu is multiarch. Het is mogelijk
  om alle benodigde 64 en 32-bit pakketten naast elkaar te
  installeren. Hierdoor kan Wine gebouwd worden door de instructies te
  volgen die staan op [de WineHQ wiki](Building-Wine#shared-wow64).
- Oudere versies van Debian/Ubuntu zijn waarschijnlijk niet
  multiarch. Het lukt niet om de 32 en 64-bit bibliotheken naast
  elkaar te installeren. Op een 64-bit systeem moet dan een aparte
  omgeving gemaakt worden om het 32-bit gedeelte te maken. Op [de
  WineHQ wiki](Building-Wine) staan instructies hoe dit kan in een
  chroot of een container.

## Lees ook

- De officiële WineHQ [download
  website](https://dl.winehq.org/wine-builds/) for Debian/Ubuntu.
- [De scripts en logbestanden van de WineHQ Debian/Ubuntu
  pakketten](https://build.opensuse.org/project/show/Emulators:Wine:Debian)
- De [Debian Wiki's pagina van Wine.](https://wiki.debian.org/Wine)
- [Packaging](Packaging)
