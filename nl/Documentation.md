---
title: Documentatie
---

<small>
&nbsp;[:flag_gb: English](Documentation)
&nbsp;[:flag_fr: Français](fr/Documentation)
&nbsp;[:flag_de: Deutsch](de/Documentation)
&nbsp;[:flag_pl: Polski](pl/Documentation)
&nbsp;[:flag_tr: Türkçe](tr/Documentation)
&nbsp;[:flag_kr: 한국어](ko/Documentation)
</small>

-----

## Handleidingen

- **[Gebruikershandleiding van Wine](nl/Wine-User's-Guide)**

  Wine gebruiken en aanpassen om Windows programma's uit te voeren.

- **[Gebruikershandleiding van Winelib](Winelib-User's-Guide)**

  Wine gebruiken om Windows programma's over te zetten naar Linux.

- **[Ontwikkelaarshandleiding van Wine](Wine-Developer's-Guide)**

  Wine aanpassen, verbeteren en ontwikkelen.

- **[Wine Installeren en Configureren](Wine-Installation-and-Configuration)**

  Een korte handleing voor iedereen die ons wil helpen.

## Command reference

- **[Commands](Commands)**

  Documentation about the commands available with Wine.

- **[Man Pages](Man-Pages)**

  Manual pages for the Wine commands and tools.
