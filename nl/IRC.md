---
title: Live Wine Chat
---

<small>
&nbsp;[:flag_gb: English](IRC)
&nbsp;[:flag_fr: Français](fr/IRC)
&nbsp;[:flag_de: Deutsch](de/IRC)
&nbsp;[:flag_es: Español](es/IRC)
&nbsp;[:flag_pt: Português](pt/IRC)
&nbsp;[:flag_pl: Polski](pl/IRC)
&nbsp;[:flag_tr: Türkçe](tr/IRC)
&nbsp;[:flag_kr: 한국어](ko/IRC)
&nbsp;[:flag_cn: 简体中文](zh_CN/IRC)
</small>

-----


[Libera.​Chat](https://libera.chat/) heeft verschillende IRC kanalen over
Wine. Gebruik uw favorite IRC programma met de volgende instellingen:

>   **Server:**   irc.libera.chat\
>   **Poort:**    6697\
>   **TLS:**      ingeschakeld

Afhankelijk van het onderwerp kies één van de volgende kanalen:

>   **#winehq:**          Ondersteuning en hulp met Wine voor gebruikers\
>   **#crossover:**       Ondersteuning en hulp met CrossOver voor gebruikers\
>   **#winehackers:**     Ontwikkeling en bijdragen aan Wine\
>   **#winehq-social:**   Kletsen met andere Wine gebruikers en ontwikkelaars

Mensen die Firefox gebruiken of een andere webbrowser die IRC
internetadressen ondersteunt, kunnen de chat bezoeken met:

- [#winehq](irc://irc.libera.chat/winehq)
- [#crossover](irc://irc.libera.chat/crossover)
- [#winehackers](irc://irc.libera.chat/winehackers)
- [#winehq-social](irc://irc.libera.chat/winehq-social)

Om de gespekken zo nuttig mogelijk te houden, doorzoek eerst de [Wine
FAQ](nl/FAQ), [AppDB](https://appdb.winehq.org) en de [download
pagina\'s](nl/Download) voordat u een vraag stelt.

## IRC Regels en Straffen

Naast niet onnodig beledigend en brutaal zijn, zijn er een paar regels
waaraan iedereen op IRC zich aan moet houden. In de meeste gevallen
wordt een overtreding van de regels de eerste keer als een vergissing
gezien met een waarschuwing als gevolg. Na meerdere overtredingen wordt
u uit het IRC-kanaal gezet.

Als u de regels blijft overtreden, nadat u uit een kanaal bent gezet,
wordt u de toegang voor twee uur tot het kanaal ontzegt. De volgende
sanctie is complete ontzegging tot het kanaal. Om een ontzegging
ongedaan te maken ga naar **#winehq-social** (of mail naar de
[wine-devel mailinglijst](mailto:wine-devel@winehq.org) als u uit
**#winehq-social** bent gezet) en leg uit waarom u uit het kanaal bent
gezet en waarom de sanctie moet worden opgeheven.

| Regel                                                                                     | Uitleg                                                   | Waarschuwingen | Max. Overtredingen |
|-------------------------------------------------------------------------------------------|----------------------------------------------------------|----------------|--------------------|
| Geen spam.                                                                                |                                                          | 1              | 2                  |
| Gebruik pastebin als u meer dan 1 of 2 regels tekst plakt                                 |                                                          | 0              | 5                  |
| Gebruik het goede kanaal                                                                  | Bij twijfel, vraag het in **#winehq**.                   | 2              | 3                  |
| Alleen Wine en CrossOver worden ondersteund                                               | PlayOnLinux, PlayOnMac enz. worden **niet** ondersteund. | 2              | 1                  |
| Voordat u hulp vraagt in **#winehq**, zorg ervoor dat u de laatste versie van Wine heeft. | U kunt met `wine --version` uw versie vinden             | 3              | 1                  |
| Wacht op uw beurt.                                                                        |                                                          | 3              | 1                  |
| Bespreek **geen** illegale stoftware.                                                     |                                                          | 1              | 1                  |
