---
title: Belangrijke Mailinglijsten / Forums
---

<small>
&nbsp;[:flag_gb: English](Forums)
&nbsp;[:flag_fr: Français](fr/Forums)
&nbsp;[:flag_pt: Português](pt/Forums)
&nbsp;[:flag_pl: Polski](pl/Forums)
&nbsp;[:flag_tr: Türkçe](tr/Forums)
&nbsp;[:flag_kr: 한국어](ko/Forums)
&nbsp;[:flag_cn: 简体中文](zh_CN/Forums)
</small>

-----


## Forums

Wine heeft verschillende mailinglijsten en forums. Voor de gebruiker
staan hieronder de belangrijkste:

- De [WineHQ Forums](https://forum.winehq.org)
- Ubuntu gebruikers kunnen ook het [Wine Ubuntu
  Forum](https://ubuntuforums.org/forumdisplay.php?f=313) bezoeken

Mocht u nog andere actieve Wine forums weten, vooral niet Engelstalige,
laat het ons dan weten. Dan kan die toegevoegd worden aan de lijst.

:information_source: Om op de hoogte gehouden te worden van nieuw
versies kan worden ingeschreven worden op de [Wine-Announce
mailinglijst](https://list.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/).

## Alle WineHQ Mailinglijsten

WineHQ heeft mailinglijsten om patches in te dienen, om Git-commits te
volgen en om te discussiëren over Wine.

:information_source: Voordat u naar een bericht naar maillijst stuurt,
moet u ingeschreven zijn. Anders behandeld de software van de
mailinglijsten uw bericht als spam.

- **<wine-announce@winehq.org>**\
    \[[In-
    uitschrijven](https://list.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/)\]
    \[[Archief](https://list.winehq.org/mailman3/hyperkitty/list/wine-announce@winehq.org/)\]\
    Een lijst (alleen-lezen) met weinig berichten
    (2/maand) voor aankondigingen van Wine versies en belangrijk nieuws
    over Wine of WineHQ.
- **<wine-devel@winehq.org>**\
    \[[In-
    uitschrijven](https://list.winehq.org/mailman3/postorius/lists/wine-devel.winehq.org/)\]
    \[[Archief](https://list.winehq.org/mailman3/hyperkitty/list/wine-devel@winehq.org/)\]\
    Een lijst met ongeveer 100 berichten per dag voor de discussie over
    de ontwikkeling van Wine, WineHQ, patches en alle andere
    interessante dingen voor Wine ontwikkelaars.
- **<wine-commits@winehq.org>**\
    \[[In-
    uitschrijven](https://list.winehq.org/mailman3/postorius/lists/wine-commits.winehq.org/)\]
    \[[Archief](https://list.winehq.org/mailman3/hyperkitty/list/wine-commits@winehq.org/)\]\
    Een alleen-lezen lijst met ongeveer 25 berichten per dag met de
    Git-commits.
- **<wine-releases@winehq.org>**\
    \[[In-
    uitschrijven](https://list.winehq.org/mailman3/postorius/lists/wine-releases.winehq.org/)\]
    \[[Archief](https://list.winehq.org/mailman3/hyperkitty/list/wine-releases@winehq.org/)\]\
    Een lijst (alleen-lezen) met ongeveer 2 berichten per maand met alle
    verschillen bij iedere officiële Wine versie.
- **<wine-bugs@winehq.org>**\
    \[[In-
    uitschrijven](https://list.winehq.org/mailman3/postorius/lists/wine-bugs.winehq.org/)\]
    \[[Archief](https://list.winehq.org/mailman3/hyperkitty/list/wine-bugs@winehq.org/)\]\
    Een drukke lijst (100/dag) die alle activiteit op de [Bug Tracking
    Database](https://bugs.winehq.org/) laat zien.
