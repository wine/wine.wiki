<small>
&nbsp;[:flag_gb: English](Download)
&nbsp;[:flag_cn: 简体中文](zh_CN/Download)
</small>

-----

## Wine met Ondersteuning

<table><tbody><tr>
<td><a href="https://www.codeweavers.com/crossover">

![Crossover logo](/media/Crossover-logo.png)

</a></td>
<td>
[**CrossOver**](https://www.codeweavers.com/products) is
een gepolijste versie van Wine gemaakt door
[**CodeWeavers**](https://www.codeweavers.com).  CrossOver maakt het
makkelijker om Wine te gebruiken én CodeWeavers geeft uitstekende
technische ondersteuning. Alle aankopen van CrossOver worden gebruikt
om de ontwikkelaars en programmeurs van Wine te betalen.  Al met al is
CrossOver de manier om én ondersteuning te krijgen én het Wine project
te helpen.<br><br>

CodeWeavers heeft een volledige [**probeerversie van
CrossOver**](https://www.codeweavers.com/crossover).
<br><br>
<small>
Deze tekst is onze blijk van waardering voor CodeWeavers en in
ruil hosten zij de website van Wine.
</small></td>
</tr>
</tbody>
</table>

## Wine pakketten

[**Nieuws en aankondigingen**](https://www.winehq.org/news/)

[**Wine Installeren en
Configureren**](Wine-Installation-and-Configuration)

### WineHQ Pakketten

*Deze pakketten zijn gemaakt en worden
ondersteund door WineHQ. Problemen met de pakketten kunnen gemeld
worden op [Bugzilla](https://bugs.winehq.org).*

<table><tbody><tr>
<td rowspan="2">![Wine logo](/media/Wine-logo.png){width=60%}</td>
<td>

[**Ubuntu**](Debian-Ubuntu) - WineHQ deb-pakketten voor Ubuntu 20.04, 22.04, 24.04 en 24.10

[**Debian**](Debian-Ubuntu) - WineHQ deb-pakketten voor Debian Bullseye, Bookworm en Trixie

[**Fedora**](Fedora) - WineHQ rpm-pakketten voor Fedora 38 en 39
</td><td>
<small>
<strong>Beheerders:</strong>
[Rosanne DiMesio](mailto:dimesio@earthlink.net),
[Marcus Meissner](mailto:meissner@suse.de)
</small></td></tr>
<tr><td>

[**macOS**](MacOS) - WineHQ pakketten voor MacOS 10.8 tot 10.14
</td><td>
<small>
<strong>Beheerder:</strong>
[Dean Greer](mailto:gcenx83@gmail.com)
</small></td>
</tbody></table>

### Distributie Pakketten

*Deze pakketten zijn gemaakt en worden ondersteund door de
distributie. Problemen en fouten in deze pakketten kunnen gemeld
worden aan de pakketbeheerder.*

<table>
<tbody>
<tr><td>![Suse logo](/media/Suse-logo.png)</td><td>

[**SUSE**](https://en.opensuse.org/Wine#Repositories) – kant-en-klare en broncode pakketten. Dagelijks bijgewerkt met de
laatste Git versie. Voor alle openSUSE versies (Leap en Tumbleweed) en
SUSE Linux Enterprise 12 en 15
</td><td>
<small>
<strong>Beheerder:</strong>
[Marcus Meissner](mailto:meissner@suse.de)
</small></td></tr>
<tr><td>![Slackware logo](/media/Slackware-logo.png)</td><td>

[**Slackware**](https://sourceforge.net/projects/wine/files/Slackware%20Packages) -
– txz pakketten (Slackware 15.0) en tgz pakketten voor oudere versies
</td><td>
<small>
<strong>Beheerder:</strong>
[Simone Giustetti](mailto:studiosg@giustetti.net)
</small></td></tr>
<tr><td>![FreeBSD logo](/media/FreeBSD-logo.png)</td><td>

[**FreeBSD**](https://www.freshports.org/emulators/wine/) – voor
FreeBSD 5.3 en later
</td><td>
<small>
<strong>Beheerder:</strong>
[Gerald Pfeifer](mailto:gerald@pfeifer.com)
</small></td></tr>
</tbody>
</table>

## Programma's van derden

Sommige aangepaste versies van Wine kunnen beter werken voor een
programma. Helaas is het niet altijd mogelijk om deze aanpassingen in
Wine in te bouwen. Daarom bestaan er ook Wine varianten die door anderen
zijn gemaakt. Op deze varianten geeft Wine geen ondersteuning, maar voor
de gebruiker kunnen ze misschien wel handig zijn. Meer informatie staat
op de wiki pagina [Third Party
Applications](Third-Party-Applications).

## De Broncode van Wine

[**WineHQ download server**](https://dl.winehq.org/wine/source/) - De
officiële WineHQ server voor alle uitgaven.

**[Git](Git-Wine-Tutorial)** - De handleiding om Wine in elkaar te
zetten vanaf git.

Lees [Building Wine](Building-Wine) voor hulp en informatie over het
in elkaar zetten van Wine.

[**Wine-staging repository**](https://gitlab.winehq.org/wine/wine-staging)
