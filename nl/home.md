---
title: Wine
---

<small>
&nbsp;[:flag_gb: English](home)
&nbsp;[:flag_fr: Français](fr/home)
&nbsp;[:flag_de: Deutsch](de/home)
&nbsp;[:flag_kr: 한국어](ko/home)
&nbsp;[:flag_cn: 简体中文](zh_CN/home)
</small>

-----

[Wine](https://winehq.org) maakt het mogelijk om Windows programma's uit
te voeren op Linux, MacOS, FreeBSD en Solaris. Het is ook goed mogelijk
dat Wine ook op andere [besturingssystemen](Compatibility)
werkt. Om de Windows programma's uit te voeren is *geen* Windows
installatie nodig. Wine is [Vrije
software](https://www.gnu.org/philosophy/free-sw.html) en is volop in
ontwikkeling.

## Vaak bezochte pagina’s

- [Veel gestelde vragen](nl/FAQ): Voor veel voorkomende problemen,
  lees alstublieft de FAQ.

- [Handleiding](nl/Wine-User's-Guide): De gebruikershandleiding van
  Wine.

- [Wine documentatie](nl/Documentation): Als de FAQ en
  de handleiding niet hebben geholpen, dan is hier nog meer
  documentatie.

- [Download](nl/Download): Kant-en-klare Wine pakketten voor
  verschillende distributies of toch liever de broncode?

- [De Wine Programma Database](https://appdb.winehq.org): Voor hulp bij
  een specifiek programma of spel, doorzoek de AppDB.

- [Het Forum van Wine](https://forum.winehq.org): Antwoord nog niet
  gevonden? Vragen stellen kan hier.

- [Bekende problemen](Known-Issues): Kijk eerst hier voordat
  u een fout meldt.

- [Winetricks](nl/Winetricks): Een handig programma dat
  tekortkomingen in Wine oplost.

- [macOS](MacOS): Wine installeren en gebruiken op macOS

## Over het Wine project

- [Kenmerken](Wine-Features) en doelstellingen van Wine.

- Waarom Wine [belangrijk](Importance-Of-Wine) is.

- Een overzicht van de [organisatie](Project-Organization).

- [Wie is wie](Who's-Who), en belangrijke
  [medewerkers](Acknowledgements).

- De [geschiedenis](Wine-History) en
  [nieuws](News) over Wine.

- [Licentie](Licensing)-informatie van Wine.

## Meedoen

- Gebruikers helpen: Geef hulp aan andere gebruikers op het [Wine
  Forum](https://forum.winehq.org) of via [IRC](nl/IRC) .

- [Ontwikkelaars](Developers): Wine aanpassen en verbeteren
  of vanaf de broncode opbouwen

- [AppDB
  onderhouden](https://gitlab.winehq.org/winehq/appdb/-/wikis/Maintainer-Guidelines):
  Programma’s en spellen in de Wine Programma Database onderhouden

- [Fouten opsporen](Bugs): Problemen en fouten opsporen, of
  fouten die andere gebruikers hebben gemeld beoordelen.

- [Schrijvers](Writers): Handleidingen schrijven, de Wiki
  aan- en bijvullen en Wine vertalen.

- [Ontwerpers](Designers): Teken pictogrammen voor de Wine
  programma’s, ontwerp lettertypes of help mee met de webpagina’s van
  WineHQ.

- [Wine Ontwikkelingsfonds](https://www.winehq.org/donate): Een donatie
  aan het Ontwikkelingsfonds van Wine is altijd welkom!

- [Webpagina’s schrijven](Web-Content-Tasks): De WineHQ Wiki
  aanpassen en verbeteren.

- [Reclame & PR](Public-Relations): Help mee de bekendheid
  van Wine te vergroten.

## Andere handige pagina’s

- Alle [commando’s en opdrachten](Commands) van de Wine programma’s.

- ["Onofficiële" en programma’s van
  derden](Third-Party-Applications) die soms handig zijn.

- [Handige registersleutels](Useful-Registry-Keys) om Wine
  aan te passen.

- [Achteruitgang in de ontwikkeling](Regression-Testing) van
  Wine opsporen.

- [Programma’s die Wine als "besturingssysteem"
  ondersteunen](Apps-That-Support-Wine).

## Meer informatie

- De [broncode](Source-Code) van Wine kan worden gedownload
  of worden bekeken.

- Blader door de [maillijsten](nl/Forums) of meldt u
  aan .

- Informatie over de ontwikkeling en de [staat van
  Wine](Wine-Status).

- Pictogrammen, afbeeldingen, handleidingen, de database van de AppDB,
  Bugzilla en de Wiki kunnen gedownload worden van de [WineHQ
  server](https://dl.winehq.org/wine/).

- Statistieken over de [WineHQ
  pagina](https://www.winehq.org/webalizer/index.html) en de
  [gebruikers](Usage-Statistics).
