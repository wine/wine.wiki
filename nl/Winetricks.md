<small>
&nbsp;[:flag_gb: English](Winetricks)
&nbsp;[:flag_fr: Français](fr/Winetricks)
&nbsp;[:flag_th: ภาษาไทย](th/Winetricks)
&nbsp;[:flag_kr: 한국어](ko/Winetricks)
</small>

-----

## Wat is winetricks?

**Winetricks** is een *shell*-script dat gemaakt is onder leiding van
Austin English. Het script maakt het makkelijk om programma's,
softwareonderdelen, DLL-bestanden en lettertypen te downloaden en te
installeren. Ook kan `winetricks` gebruikt worden om Wine te
configureren en aan te passen.

Veel van deze programma's hebben geen open broncode en hebben een eigen
licentie. WineHQ kan dus ook geen ondersteuning geven op onderdelen die
met `winetricks` geïnstalleerd zijn. Maak dus ook geen foutrapport in
Bugzilla als er DLL-bestanden met winetricks zijn geïnstalleerd.

## Winetricks downloaden en installeren

Veel Linux distributies hebben een `winetricks` pakket. De
makkelijkste manier om `winetricks` te installeren is dan ook gebruik
te maken van de pakketbeheerder. Deze zorgt naast de installatie van
`winetricks` ook voor alle afhankelijkheden. Op
[Debian/Ubuntu](nl/Debian-Ubuntu) kan `winetricks` geïnstalleerd
worden met:

```sh
$ sudo apt install --install-recommends winetricks
```

Helaas is de versie van `winetricks` die aangeboden wordt door de
distributie vaak erg oud. Werk `winetricks` bij tot de laatste versie
met:

```sh
$ sudo winetricks --self-update
```

`--self-update` en `--update-rollback` zijn de enige opdrachten die als
**root** of **sudo** worden uitgevoerd. Gebruikt voor de rest **nooit**
**root** of **sudo** met `winetricks`!

### Handmatig downloaden en installeren

Is er geen kant-en-klaar pakket beschikbaar? Dan kan `winetricks`
gedownload worden vanaf
[GitHub](https://github.com/Winetricks/winetricks). De laatste versie is
via deze
[verwijzing](https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks)
beschikbaar. Gebruik de rechtermuisknop om het bestand op de computer op
te slaan. Of via de opdrachtregel:

```sh
$ cd $HOME/Downloads
$ wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
$ chmod +x winetricks
```

Vergeet niet om de afhankelijkheden zelf te installeren:

```
wine
ar, cabextract, unrar, unzip, en 7z
aria2c, wget, curl, of fetch
fuseiso, archivemount (Linux), of hdiutil (macOS)
perl
pkexec, sudo, of kdesu
sha256sum, sha256, of shasum
torify (Als de optie "--torify" nodig is)
xdg-open of open (op OS X)
xz
zenity
```

Het is niet nodig om `winetricks` te installeren. Het script kan
uitgevoerd worden op de plek waar het staat. Om het gemak te vergroten
is het handig om een plek te kiezen die in de **`$PATH`** variabele
staat. Hierdoor hoeft alleen **`winetricks`** ingetypt te worden om het
programma te starten. Bijvoorbeeld:

```sh
$ cd ~/Downloads`
$ wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
$ chmod +x winetricks`
$ sudo cp winetricks /usr/local/bin`
```

Om het helemaal makkelijk te maken, is er ook een
BASH-automatisch-aanvullen bestand. Hierdoor is het mogelijk op met
`TAB` de opties van winetricks automatisch aan te vullen:

```sh
$ cd ~/Downloads
$ wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks.bash-completion
$ sudo cp winetricks.bash-completion /usr/share/bash-completion/completions/winetricks  # Dit is de standard plek op Arch, Gentoo, OpenSUSE, Fedora, Debian/Ubuntu en Solus
```

## Winetricks gebruiken

Er zijn verschillende manieren om `winetricks` te gebruiken. Via de
grafische interface of met de opdrachtregel. Voor de beginnende
gebruiker zal de eerste optie de aangewezen keuze zijn, maar de tweede
optie werkt net zo makkelijk en is vaak sneller.

### Grafisch

Na het starten van `winetricks` via een snelkoppeling of met de opdracht
**winetricks** wordt het hoofdmenu weergegeven. Hierin kan een keuze
gemaakt worden om een programma of spel te installeren. De standaard
keuze is om door te gaan naar het volgende menu waar DLL-bestanden
geïnstalleerd kunnen worden, hulpprogramma's kunnen worden gestart en
instellingen kunnen worden aangepast.

### Opdrachtregel

Bij het gebruik van de opdrachtregel kan de gebruiker direct een
programma of DLL-bestand om te installeren als optie (in winetricks
wordt dit *verbs* genoemd) meegeven. Op dit moment zijn er ongeveer 600
verbs om uit te kiezen. Veel voorkomende opties kent men op den duur wel
uit het hoofd. Gelukkig heeft `winetricks` ook een handige optie om alle
keuzes (per categorie) te tonen. Om bijvoorbeeld alle DLL-bestanden te
laten zien:

`winetricks dlls list`

Door één of meerdere *verbs* achter het `winetricks` commando te
plaatsen, worden deze geïnstalleerd. Om bijvoorbeeld alle standaard
Windows lettertypen (corefonts) en de Visual Basic 6 runtime (vb6run) te
installeren, gebruik:

`winetricks corefonts vb6run`

Om de installatie van sommige *verbs* sneller te maken kunnen alle
dialoog vensters uitgeschakeld worden met de `-q` optie. Gebruik om
bijvoorbeeld het .NET Framework versie 3.5 (dotnet35) zonder tussenkomst
van de gebruiker te installeren:

`winetricks -q dotnet35`

Een volledig overzicht van alle opties en *verbs* is te vinden met de
`--help` optie:

`winetricks --help`

### Onderdelen verwijderen

Onderdelen die met winetricks zijn geïnstalleerd kunnen vaak niet meer
verwijderd worden. Winetricks haalt namelijk vaak trucjes uit om de
installatie mogelijk te maken. Hierdoor is er geen *uninstaller*
aanwezig. Het is dan ook aan te raden om programma's die een winetrick
nodig hebben in een aparte [wineprefix](nl/FAQ#wineprefix) te
installeren. Mocht er iets niet goed zijn gegaan dan kan de hele prefix
verwijderd worden.

## Tips

De variabelen *[`WINEPREFIX=`](nl/FAQ#wineprefix)* en
*`WINE=`* kunnen ook in combinatie met `winetricks` gebruikt worden. Om
bijvoorbeeld het mfc40.dll bestand (mfc40) in de Wineprefix `~/winetest`
te installeren, gebruik:

`WINEPREFIX=~/winetest winetricks mfc40`

Als er meerdere Wine versies op het systeem staan, gebruik `WINE=` om
naar de goede variant te verwijzen. Bijvoorbeeld

`WINE=/opt/wine-staging/bin/wine winetricks mfc40`

------------------------------------------------------------------------

Winetricks maakt het bestand `winetricks.log` aan in de hoofdmap van de
wineprefix. Hierin is makkelijk terug te vinden welke winetricks er zijn
uitgevoerd in de prefix.

## Fouten in Winetricks

Winetricks is een project dat losstaat van Wine. Fouten in winetricks
kunnen gemeld worden op de
[GitHub](https://github.com/Winetricks/winetricks/issues) pagina.
