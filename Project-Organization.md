## Organization of the Wine Project

Wine is a member project of the [Software Freedom
Conservancy](https://sfconservancy.org/) which provides a non-commercial
home and infrastructure for Wine. Conservancy holds all of the financial
assets of the Wine Project as well as our trademarks. Conservancy also
provides advice and some measure of protection on legal matters.
Formally, there is a Fiscal Sponsorship Agreement between Conservancy
and an initial group of individuals known as the Wine Committee. Those
individuals are Austin English, Alexandre Julliard, Marcus Meissner,
Michael Stefanuic, and Jeremy White.

The Wine Committee is primarily responsible for determining how the
funds raised by donations are allocated. Recently, the spending has
entirely been to help sponsor [the Wine
conference](WineConf). In early 2019, Henri Verbeet proposed
that the committee also take responsibility for determining the location
of the next Wine conference.

The Wine Committee does not have influence over technical aspects of
Wine. Instead, there is one maintainer, Alexandre Julliard. Alexandre
took over development in 1994 and he is the Dictator-in-chief of
applying patches (averaging around 40 a day). There is a system of [sub
maintainers](https://gitlab.winehq.org/wine/wine/-/blob/master/MAINTAINERS)
who provide technical input to Alexandre.

## CodeWeavers

[CodeWeavers](https://www.codeweavers.com/) is the principal corporate
sponsor of Wine, employing a bunch of developers as well as Alexandre.
Approximately 2/3 of patches into Wine are from CodeWeavers staff,
either on or off company time. CodeWeavers also helps sponsor the Wine
conference as well as providing hosting for the WineHQ web site. While
CodeWeavers main commercial interest in Wine is its use in the Crossover
product, they take their responsibility for being a good open source
citizen very seriously and even encourage enhancements to Wine that
compete with Crossover.

## Other Free Software Projects

Wine has a lot of *dependencies* on other pieces of free software. These
include things like sound and graphics libraries that Wine needs to run
applications. In some cases, Wine is the principle reason for certain
technologies or standards being developed. Wine is in turn a component
of other projects, such as Linux distributions that include Wine so that
users can run Windows applications.

Wine is also part of a greater open source community composed of many
projects. Wine needs to work on many *platforms*, including different
desktop environments and operating systems. Consequently, Wine has
historically played a role in helping define or encourage the adoption
of certain standards.

## Other Companies

CodeWeavers isn't the only company with an interest in Wine.

TODO: Summer Of Code, other corporate friends.

## Press Contacts and Spokesperson

If you are writing an article or something similar, you can send an
email to press@winehq.org. This will get forwarded to several active
Wine developers in different time zones, who may be able to respond to
you more immediately if you're on a deadline.
