---
title: Live Community Chat
---

<small>
&nbsp;[:flag_fr: Français](fr/IRC)
&nbsp;[:flag_de: Deutsch](de/IRC)
&nbsp;[:flag_es: Español](es/IRC)
&nbsp;[:flag_pt: Português](pt/IRC)
&nbsp;[:flag_nl: Nederlands](nl/IRC)
&nbsp;[:flag_pl: Polski](pl/IRC)
&nbsp;[:flag_tr: Türkçe](tr/IRC)
&nbsp;[:flag_kr: 한국어](ko/IRC)
&nbsp;[:flag_cn: 简体中文](zh_CN/IRC)
</small>

-----


[Libera.​Chat](https://libera.chat/) hosts multiple IRC channels for
Wine. You can access the chat rooms using an IRC program such as
[HexChat](https://hexchat.github.io/) with these settings:

> **Server:** irc.libera.chat\
> **Port:** 6697\
> **TLS:** ON

Which channel you should pick depends on the issue you want to discuss:

> **#winehq:** User support and help with running Wine\
> **#crossover:** User support and help with running CrossOver\
> **#winehackers:** Development and other ways of contributing to Wine\
> **#winehq-social:** Casual, off-topic chats with other community members

Those using Firefox, or any other browser that supports IRC URLs, can
join a chat by clicking on:

- [#winehq](irc://irc.libera.chat/winehq)
- [#crossover](irc://irc.libera.chat/crossover)
- [#winehackers](irc://irc.libera.chat/winehackers)
- [#winehq-social](irc://irc.libera.chat/winehq-social)

In order to keep discussions focused and as helpful as possible, also
try to research your question some before asking about it on the IRC.
The [Wine FAQ](FAQ), [AppDB](https://appdb.winehq.org), and [download
page](Download) are all good resources to check first.

## IRC Rules and Penalties

Besides not being offensive or blatantly inconsiderate, there are a few
simple rules that everyone is expected to follow on the IRC channels. In
most cases, breaking a rule will be seen as an accident at first and
will result in warnings. After you have used up your warnings though,
you will be kicked from the channel.

If you continue to break a rule after using up your kicks, you will be
banned from the channel for up to 2 hours. Any problems after this will
result in an indefinite ban, which can only be overturned by appeal. If
you want to contest a ban, go to **#winehq-social** (or the [wine-devel
mailing list](mailto:wine-devel@winehq.org) if you've been banned from
**#winehq-social**), and explain why you believe you were banned in the
first place and why the ban should be lifted.

| Rule                                                                                       | Clarification                                                                 | Warnings | Kicks |
|--------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------|----------|-------|
| Do not spam.                                                                               |                                                                               | 1        | 2     |
| Use a pastebin for pasting more than 1 or 2 lines.                                         |                                                                               | 0        | 5     |
| Talk in the appropriate channel.                                                           | If unsure, ask in **#winehq** which channel to join.                          | 2        | 3     |
| Only Wine and CrossOver are supported in their respective channels.                        | Sidenet, WineDoors, Cedega, IEs4Linux, etc. are **not** supported.            | 2        | 1     |
| Before asking for help in **#winehq**, be sure you\'re running the latest version of Wine. | If unsure, run `wine --version` in the command line to determine your version | 3        | 1     |
| Please wait your turn for help.                                                            |                                                                               | 3        | 1     |
| Do **not** discuss pirated software.                                                       |                                                                               | 1        | 1     |
