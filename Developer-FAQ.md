This FAQ covers topics related to software development within the Wine
project. For other topics, see our main [FAQ](FAQ).

## General Questions

### How do I become a Wine developer? What do I need to know?

If you can program C, you are off to a good start. See if there's
anything that you think you think you can fix or work on. You won't
have much trouble finding areas that need work in Wine. For example,
find a program that you want to run that doesn't, or simply grep for
FIXMEs in the source. You may also find the [Developer
documentation](Developers) Wiki page worthwhile. While it is not
essential, having the latest Windows Platform Software Development Kit
(PSDK) available for reference will be a help. Wine is always moving
along with the latest Windows developments and changes should reflect
the latest PSDK.

Once you figure out what to work on, the next step is actually doing
the work and getting to know the other developers. First, download the
sources via [Git](Git-Wine-Tutorial), subscribe to the [mailing
lists](Forums), maybe join **\#winehackers** on
[Libera.​Chat](IRC), and of course, look around
the source. It might not hurt to pay attention to the
comp.emulators.ms-windows.wine newsgroup, although it is not used very
much anymore by Wine developers. Second, you will need to debug a
program, if that is what you wish to fix, or otherwise find something
to patch. Once you have completed some work, create a patch.

### I have written some code that I would like to submit to the Wine project. How do I go about doing this?

For the actual mechanics of contributing code, see the [Submitting
Patches](Submitting-Patches) page, but be sure to also check below and
read the [Clean Room Guidelines](Clean-Room-Guidelines) to make sure
you can contribute code.

## Copyright Issues

### Who can't contribute to Wine?

Some people cannot contribute to Wine because of potential copyright
violation. This would be anyone who has seen Microsoft Windows source
code (stolen, under an NDA, disassembled, or otherwise). There are some
exceptions for the source code of add-on components (ATL, MFC, msvcrt);
see the next question.

### Can I contribute if I've only seen the source to ATL, MFC, and/or msvcrt?

Yes, but **not** on those components. Also please state on the mailing
list that you have seen the source to these and that you will not
contribute to them. You are free to contribute to other areas of the
Wine source code.

## Goals & Coding Standards

### Would documenting Windows code help?

If you're referring to just the Windows API, it would be nice if the
Windows API were fully documented. Then Wine could be a perfect
"clean-room" implementation. Unfortunately, the documentation is often
bad, nonexistent, and even misleading where it exists, so a fair amount
of reverse engineering through black-box testing has been necessary,
particularly in the shell (Explorer) interface. In fact, on a few
occasions, Wine developers have actually submitted corrections for the
official Microsoft public documentation.

However, we are very serious about maintaining strict [Clean Room
Guidelines](Clean-Room-Guidelines), and seeing any source
code makes it harder to guarantee that no copyright violations have
taken place. Besides, beyond the actual API specification itself,
descriptions of Microsoft code would have very little benefit (since our
design is fundamentally different) and be a walking copyright-violation
timebomb.

If you would like to help with this aspect of Wine, first read below to
make sure you can contribute to the project without raising any legal
issues. You can then work on writing test cases for various components
of Wine. Also, the project's documentation can always use a little love
and care, and if your findings affect certain programs, users might want
to hear about it on Bugzilla or AppDB.

### Does Wine allow C++, Objective C, or other code?

Wine is written entirely in C. No C++ code is allowed. Furthermore, C
code must be recognizable by all sane compilers, including notably the
Microsoft Visual C compiler. In practice this means code must conform to
C89, with some C99 features allowed. To be somewhat more specific, we
allow inline functions, variadic macros, and designated initializers,
but not variable-length arrays or mixed declarations and code. We also
do not use C++ comments (i.e. those beginning with `//`).

There are exceptions, but only for very specific scenarios. For example,
the Mac driver uses some Objective-C, because it is the only way to
interface with the Mac platform.

### Does Wine need drivers?

Wine is not an operating system either, so writing device drivers is not
one of our goals. Wine includes support for some HID-related kernel-mode
drivers used by applications, but does not implement any device drivers
itself. However, if you are interested in device drivers, developers
working on the Linux and FreeBSD kernels among others would certainly
appreciate your contribution.

### Why isn't there even a placeholder file for DLL X?

Wine's goal is to make it possible to run Windows applications on Unix.
To this end, it only provides replacements for those DLLs and APIs that
part of the standard Windows installation. Wine will not provide
replacements for DLLs that are not shipped with Windows (many of these,
such as the Visual Basic runtime, are always shipped with applications
that require them).

Another consequence of these goals is that implementing APIs that are
never used is very low-priority. That being said, we always try to keep
our options open and improve our API coverage as much as we can.

### What about a version of solitaire?

Similarly, Wine does not try to be a desktop environment so we have no
plans to implement every program that comes with Windows. Programs such
as games would be much more appropriate as independent projects, but
they also might be redundant with other open-source projects. These
projects (many are associated with desktop environments such as Gnome or
KDE) would certainly appreciate your contributions, and by helping these
projects, you also ensure the whole open-source community will benefit
from your work, not just Wine users.

Programs are typically only accepted into Wine if a 3rd-party program
depends on them. Partial exceptions to this are necessary built-in
programs unique to the Windows system (the registry editor is a good
example). There are also some associated tool programs that Wine uses,
and we welcome anyone willing to help improve these programs.

## The Development Process

### How do I download and build the very latest source code for testing purposes?

Current Wine sources are available on the [WineHQ
Gitlab](https://gitlab.winehq.org). You will need Git 1.4 or above.

First install the packages needed when building Wine per the
instructions at [Building Wine](Building-Wine).

To get an initial copy of the entire Wine source tree, do

```sh
git clone https://gitlab.winehq.org/wine/wine wine-git
```

Then, in the wine-git directory, do

```sh
./configure && make
```

Then, every day, you can get a fresh build of Wine by doing

```sh
git fetch
git rebase origin
./configure && make
```

See the [Git Wine Tutorial](Git-Wine-Tutorial) for more
information.

### I sent a merge request, but it got ignored. Why?

1.  Don't panic! Having your patch ignored is fairly routine,
    particularly for newer contributors.
2.  Check your merge request for comments. If there are any replies at
    all to your patch, they have to be addressed, and then you should
    resubmit your merge request.

Beyond that, a patch is ignored either because it is not obviously
correct (most likely) or because the maintainer did not have the time to
get to it. Developer persistence is seen as a necessary part of the
process.

A patch can be 'not obviously correct' for a variety of reasons. Often
the reason is clear, and a second look on your part will reveal an
obvious mistake. This is why it is valuable for you to double- and
triple-check an ignored patch.

Also, patch approval is based on reputation. Once you have had several
patches approved, and/or are known to submit good code, your patches are
much more likely to be reviewed and approved. Conversely, if you have
had several patches rejected, and/or are known to submit poor code, your
patches are much more likely to be ignored.

Your reputation is informally known as your 'Julliard Rank'. Everyone
starts with a neutral Julliard Rank, and it goes up or down based on the
quality of your patches and how responsive you are to feedback. A high
Julliard Rank and a not obviously broken patch will often be committed
right away. A low Julliard Rank earns you at best dubious scrutiny and
at worst repeated dismissal. The following table gives example Julliard
Ranks:

| Developer           | Rank             | Description                                                                                                                                                                                                                                                                        |
|---------------------|------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Alexandre Julliard  | undefined        | Even dubious patches are often committed without complaint.                                                                                                                                                                                                                        |
| Henri Verbeet       | Highest possible | Normally nothing he sends gets rejected or pending.                                                                                                                                                                                                                                |
| Jane Q. Conformance | High Rank        | Long time developer who has contributed many conformance tests, whose patches rarely break the conformance tests, and who sends small, clean patches that are fully and carefully thought out. Patches that look sound often go into Wine without much delay or critical scrutiny. |
| Joe B. Newbie       | Neutral Rank     | A new developer. His patches are given time, considered carefully, and he is welcomed to Wine and we will actively try to help him get his patches in.                                                                                                                             |
| Betty Q. Sinking    | Low Rank         | A relatively new developer. She fails to follow the guidelines on the web site, doesn't respond to replies to her patches, and never contributes conformance tests.                                                                                                                |
| Jeremy White        | Lowest possible  | Patches automatically directed to /dev/null :)                                                                                                                                                                                                                                     |

The Wine developers first discovered the Julliard Rank at WineConf2008
(see [WWN issue
353](https://www.winehq.org/wwn/353#News:%20WineConf%202008)).

Of course, sometimes patches are 'not obviously correct' because they
are non trivial, or don't look quite right to the maintainer. You may
have to make a case for your patch (best done via conformance tests), or
connect with folks via email or on irc to further explore your patch.

And, finally, patches are often just dropped for no particular reason
other than the maintainer ran out of time that day, or missed it, or was
off skiing. Double check and resend; no one will be upset.

### How can I improve the chances of my changes being approved?

Send a small, very clean patch first.

Make sure your patches are formatted properly and include your real
name.

Make sure your patch includes a conformance test, and verify that the
test passes both on Linux and on various Windows versions (use the
[testbot](https://testbot.winehq.org) for this). Patches that add a
conformance test are much easier to get approved than patches that
change core Wine code.

Make sure the Wine test suite passes after your patch is applied.

If your patch is ignored in a day or two, improve or simplify it and
send a new version. Repeat until it is accepted. Persistence is
important.

Send a conformance test first rather than including both a conformance
test and a bug fix in the same patch.

### Why is becoming a successful Wine contributor so hard?

Two reasons come to mind:
  1. Wine development has a steep learning curve, and
  2. establishing trust takes time.

## Independent Software Vendor Questions

### Can I use Wine to port my Win32 sources to Unix?

Yes. If you just want your application to run on Intel-compatible PCs,
the most practical way is to simply ship your Windows .exe along with an
isolated copy of Wine.

If you need to support non-Intel computers, or if you want to
intimately mix Windows and Unix code, you may want to rebuild your
application in Linux using Winelib. See [Winelib User's
Guide](Winelib-User's-Guide) for info.

### Will MFC work with Wine? What do I need to do?

Wine currently has no built-in MFC implementation. Most applications can
run using native MFC DLLs, so there is not a lot of incentive to work on
one. It is also possible (with a lot of work) to compile the MFC from
source and thus produce an mfc42.dll.so library.

Please refer to the [Winelib User's Guide](Winelib-User's-Guide) for
how to do this.

### Are there any commercial applications which have been ported using Wine?

Here are few examples of applications ported using Wine or Winelib:

- Corel's WordPerfect Office Suite 2000 was ported to Linux using Wine.
- Kylix, the Linux version of Delphi, was ported to Linux using Winelib.
  The IDE actually uses a combination of Qt and Winelib which would not
  have been possible to achieve using only Wine. The generated
  applications however do not depend on Wine in any way.
- Vividas Streaming Video (https://www.vividas.com/support/)
- Ability Office (https://www.ability.com/linux/abilitylinux.php)
- IBM's Websphere
- BricsCad® (BricsCad® V6 for Linux)
- Google Picasa

### Can I build one large Monolithic application?

No. However, if you don't want Wine as a dependency, you can bundle your
private version of Wine into your package (.rpm/.deb). Wine has good
support for such a setup via the WINEPREFIX environment variable. This
is the way Google Picasa works - they actually use the exact same binary
on Windows and Linux, but in the Linux version they bundle a tested
version of Wine.

### How can I detect Wine?

This is a bad idea. The goal of Wine is to be compatible enough that an
application should see no difference in behavior, and therefore should
not apply any workarounds, different paths, etc. So any method to detect
running under Wine is unsupported and may break without warning in the
future.

Wine should not be treated as a "version" of Windows - functionality or
performance-tuning is likely to be different between any two development
versions.

Rather than detecting Wine:

- Detect if functionality exists and use it when available.
- File a bug if something works in Windows that does not work in Wine.
- If you can write a minimal [test](Conformance-Tests) that
  demonstrates the bug, even better!
- Ask for help on the developers' list.

That said: There is nothing wrong with detecting Wine for survey
purposes. Many developers are surprised to learn that a substantial
portion of their userbase has been running their program via Wine;
sometimes developers aren't even aware that this is possible.

If you still really want to detect Wine, check whether ntdll exports the
function wine_get_version. (See
<https://www.winehq.org/pipermail/wine-devel/2008-September/069387.html>)

## See Also

- [Developer Documentation](Developers)
