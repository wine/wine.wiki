## Date

November 12th - 13th, 2016

## Pictures

![Group photo](/media/Wineconf2016-grp.jpg){width=1024px}

[Marcus Flickr
Album](https://www.flickr.com/photos/marcusmeissner/sets/72157675221434252)

## Location

**InterContinental St Paul - Riverfront**

11 East Kellogg Boulevard

St. Paul, Minnesota, 55101

United States

Phone: +1-651-292-1900

<https://www.intercontinental.com/hotels/gb/en/st-paul/mspsp/hoteldetail>

This is a nice hotel in downtown St. Paul, with nice views of the river
and fairly interesting city locations to explore.

Be aware that weather in Minnesota in November is [typically around
freezing](https://www.wunderground.com/history/airport/KSTP/2013/11/1/MonthlyCalendar.html),
especially after dark. Bring warm clothes.

## Schedule

Friday

- 15:00-22:00 - Informal gathering in the hotel bar. Dinner is on your
  own, although there are usually groups that leave for dinner during
  this time.

Saturday

- 07:00-09:00 - Breakfast. This is often a social time and an informal
  gathering as well.
- 09:00-09:30 - Keynote by Alexandre Julliard
- 09:30-09:45 - Review of agenda, fill in schedule based on interest and
  done-ness of presentations
- 09:45-12:00 - Continuing presentations and discussions (see Agenda,
  below)
- 12:00-13:00 - Lunch
- 13:00-17:00 - Continuing presentations and discussions
- 18:30-21:00 - Dinner at CodeWeavers HQ (700 Raymond Ave, St Paul)
- 21:00-??:?? - Informal gatherings, usually at bars near the town
  center. The hotel bar is open until 1:00 am as well.

Sunday:

- 07:00-09:30 - Breakfast
- 09:30-12:00 - Presentations and discussions
- 12:00-13:00 - Lunch
- 13:00-17:00 - Presentations and discussions
- 17:00 - WineConf is done; farewell. Dinner on your own, although there
  are often informal gatherings.

## Agenda

The agenda is usually pretty loosely defined and is often refined
throughout the conference, but the topics will be largely constrained to
those relating to Wine development. If there is a topic you would like
to discuss, send an email to the wineconf mailing list. We usually also
set aside time for ad-hoc working groups; we'll keep scratch pads handy
for such conversations.

![Agenda photo](/media/Wineconf2016-agenda.jpg){width=800px}

There is one traditional item you do not want to miss though:

- Saturday, 9:00 - Keynote presentation by Alexandre
  [slides](/media/Keynote-wineconf2016.pdf)

Other talk/discussion proposals:

- Wine Tests Hackathon - Jeremy White
- Review of recent changes, including the Signed-off-by system, the
  stable branch, and the staging branch.
- HID/PNP discussion - Aric Stewart
  [slides](/media/Hid-wineconf2016.pdf)
- CodeWeavers update (with shiny new Android demos to show off)

## Travel sponsorship

The Wine Party fund will sponsor travel on an as-needed basis. Please
write an email to wine -at- sfconservancy.org to request sponsorship.

## Getting there

**By Plane:**

The nearest airport is Minneapolis/St. Paul International Airport (MSP).

From the Minneapolis airport, you have several choices to get to the
hotel. A taxi cab will cost you about \$30 for the trip. The 54 bus line
will get you there in about 23 minutes (30 minutes if you include the
walk across the airport to the bus stop). Exit the bus at *Wabasha St*
and walk two blocks south. The light rail system is more friendly to
large groups and large amounts of baggage, though it will take about an
hour and include one transfer from the Blue line to the Green line. Exit
the train at *Central Station* and walk two blocks south. Google maps
gives good transit directions for Minneapolis/St. Paul, and should be
reliable.

**By Train:**

Amtrak does run two trains a day into St. Paul Union Depot. Union Depot
is within walking distance to the hotel.

**By Car:**

Driving to and in Minneapolis/St. Paul is simple. Parking at the hotel
may be tricky. If you wish to avoid parking fees at the hotel, email
Jeremy White at CodeWeavers; he may be able to help you work out
strategies to keep your parking costs down.

## Local transportation

Minneapolis/St. Paul has all the standard transit options: our public
transit is Metro Transit (https://metrotransit.org), along with Taxis,
Uber, and so on. The hotel is fairly close to a stop on the Green Line,
which is the train line running between Minneapolis and St. Paul. You
can get to most interesting places in the Twin Cities on the train.
Notably, the CodeWeavers offices are at the Raymond Ave. Green Line
stop.
