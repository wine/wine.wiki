Organising a conference is a lot of work, and it’s easy to forget about
one or two things in the process. This document is intended to be a
reference guide for anyone involved with organising the Wine Projects’
annual conference, WineConf. Some tasks are best done by someone local
to the conference location, but many tasks are also location
independent. A typical structure would be to have one or two local
people, and three or four remote ones. By necessity the local team will
change from year to year depending on the location, while for continuity
it would be best if changes in the remote team are less frequent. As
with anything, it’s important to keep in mind what the goals and
constraints of the conference are:

- Strengthening the Wine community. Making sure contributors feel
  appreciated, and stay engaged with the project. Strengthen
  interpersonal relationships, etc.
- Maintaining relationships with related projects, and creating new
  ones. Transferring knowledge between contributors. Staying in touch
  with the user-base.
- Since Wine is a Free Software project, and a Conservancy member
  project, it’s best to avoid proprietary software and services where
  possible. Typical examples of things to avoid would be Google Maps,
  Forms, and YouTube. Aside from being inconsistent with the goals of
  the project, many potential attendees will have reservations as well.

## Checklist

- Choose a venue and date
- Setup a registration process
- Setup a website/wiki
- Solicit talks
- Publicity/Invitations
- Facilitate travel sponsorships and visas
- Arrange for audio and video equipment for presentations
- Arrange for a venue for the Saturday evening dinner
- Upload video and/or presentation slide to website/wiki

### Choose a venue and date

There are a couple of things to consider here. In the first place, it’s
important to start looking for a venue well in advance—many of the other
tasks depend on having a date, and the date to some extent depends on
venue availability. Knowing the date far enough in advance also helps
attendees make travel arrangements, get a visa if needed, plan vacation,
and so on.

The project has traditionally mainly used hotel conference rooms, but
dedicated conference centres and local educational institutions are
certainly worth considering as venue as well. Educational institutions
may also be able to provide additional attendees, some of which may also
be willing to hold a talk.

In recent years, typical hotel room prices have been around €120 per
room per night, and conference room prices have been around €60 per
person per day. Attendance is never easy to predict in advance, but in
recent years attendance levels have been at around 60 people for the
main conference, and around 10 additional people for social events like
the Saturday evening dinner.

The time of year is of influence on attendance. Although January or
February in Minnesota may seem perfectly reasonable to many long-term
WineConf attendees, that may not seem quite as attractive to first-time
attendees—especially ones living in more agreeable climates themselves.
For a potential attendee having to travel a longer distance, only
visiting the conference for 2 or 3 days may not be worth it, but if
visiting the conference could be combined with some vacation that may be
enough to convince them to attend. On the other hand, a nicer time of
year does tend to be somewhat more expensive, which may deter some
people—a balance needs to be struck.

It’s worthwhile to keep track of the dates and locations of other
relevant conferences like XDC, FOSDEM, LCA, sambaXP, etc. On one hand,
we don’t want to make people choose between going to one conference or
the other. On the other hand, there’s an opportunity to hold a
conference together with another project. That may be especially
attractive if the other project is a Conservancy member project as well.

In additional to the conference room and lodging it will be necessary to
arrange for lunch and coffee breaks. Most hotels will offer various
catering options. It is important to consider attendees food allergies
and restrictions.

### Setup a registration process

It’s of course important to have some sense of the number of people to
expect, and to be able to close registration in the unfortunate event
that much more people would like to attend than can be accommodated at
the venue. We also use the registration information to create conference
badges, and for the public list of attendees. The latter is mostly an
incentive to pull doubters over the line. Registration is currently
free, but it’s not inconceivable that if attendance grows beyond a
certain level we’ll have to ask attendees for a small contribution
towards covering the costs of the venue in particular.

Information to collect:

- Given name
- Family name
- Food preferences and/or restrictions
- Emergency contact information
- Attending the main conference?
- Attending the social events? How many?

A conference should of course have name badges. Names should be readable
from some distance, although not necessarily from across the room.
You’ll need badge holders and lanyards. A7 is a nice format. It’s nice
to have a distinct visual style for the conference, and use that same
style for the badges, slide templates, video titling, etc. Have the
badges printed double-sided by a proper copy shop. While it’s possible
to print badges on a desktop or office printer, it’s just not worth it.
A professional printer will typically give you heavier paper, much
better colour fidelity, cut the badges to the appropriate size, and
isn’t particularly expensive.

### Setup a website/wiki

There needs to be a central place with various information about the
conference. We’ve traditionally used a wiki page for this, but it’s
worth considering using a dedicated website. Information to include:

- A brief introduction to the conference, its goals and audience.
- Contact information for questions and comments.
- Information about the WineConf mailing list and Matrix room.
- The conference date and venue.
- Registration information.
- Travel information. How to get to the venue; Information about
  currency, climate, power, etc.; Emergency numbers.
- Things to do outside the conference; tourism.
- Information about travel sponsorships. The procedure to follow and re-
  quirements to qualify.
- The conference programme.
- The list of attendees.
- A call for proposals for hosting the next conference.
- After the conference, this is where pictures, recordings, and slides
  will be posted.

### Solicit Talks

A call for presentations at WineConf should be done by posting to the
wineconf and wine-devel mailing lists. If there are developers working
on components or changes that are particularly of high interest to the
broader wine community then it would be worthwhile reaching out directly
and asking if they are willing to present on their work.

### Publicity/Invitations

Post the conference announcement to various places. In the first place
the announcement is a general invitation for people to register and
visit the conference. The announcement should include a brief
introduction to the conference, as well as immediately useful
information like the conference date and location, travel sponsorships,
and a reference to the WineConf mailing list. Refer to the website/wiki
for further details. Places to post:

- The wine-devel and WineConf mailing lists.
- The winehq.org front-page.
- The WineHQ forums.
- The \#winehackers and \#winehq IRC topics.
- The wineconf Matrix room topic.

For various reasons, some people may need a little encouragement to
attend the conference. This is also an opportunity to thank people for
their contributions to Wine, and to check in with them to see how
they’re doing, even if they don’t end up attending the conference this
year. Invitations may be extended to:

- New contributors.
- Long-term contributors.
- Members of the Wine user community.
- Members of Wine forks.
- Members of projects Wine depends on. E.g., Gnome, KDE, Mesa/X.org,
  Linux, mingw-w64
- Other Conservancy member projects.
- Companies using Wine in some form. Prominent examples in recent his-
  tory would include Valve, CCP and Square-Enix. Some of these may be
  willing to give a talk as well.
- Previous attendees.
- Local LUGs or similar groups, educational institutions.

### Facilitate travel sponsorships and visas

The project will sponsor travel costs for potential attendees that would
not otherwise be able to attend. These sponsorships are subject to the
SFC’s [Travel and Reimbursable Expense
Policy](https://sfconservancy.org/projects/policies/conservancy-travel-policy.html)
and need to be approved by the PLC.

### Audio and Video

Audio and video equipment for recording talks. Sometimes the venue can
ar- range this, but often the pricing is not particularly competitive.
The absolute minimum for video recording equipment is probably something
that can do 720p@25fps. A more reasonable minimum is 1080p@50fps.
There’s a lot more to video recording equipment than resolution and
refresh rate though, too much to cover here. Professional equipment is
(still) fairly expensive. One point of attention is that many phone and
photo cameras with video recording functionality have limits on the
maximum recording time, either artificial limits or e.g. thermal limits.

In terms of audio equipment, you’ll typically want at least one
microphone for speakers, and at least one for the audience. A second
microphone for the audi- ence can be helpful. Other equipment includes
some kind of mixer, amplifier, loudspeakers and cabling. Sometimes the
venue provides some or all of that equipment. A portable PA system is
another option than can provide much of the required functionality in a
single system.

Relying on the built-in microphone in the video camera is hopeless, and
will not provide acceptable audio quality. Professional video camera
will typically have XLR inputs, allowing the output of the mixer to be
directly recorded. Another option is to use a separate audio recorder.
The Zoom H4n is a popular option.

Storing the raw audio and video takes a significant amount of disk
space. Make sure to bring enough storage. Some equipment like wireless
microphones uses batteries. Make sure to bring enough and replace them
in time; you don’t want equipment to fail in the middle of a talk.
You’ll need a tripod for the video camera, and potentially for the audio
recorder. In any case, make sure to test all the equipment before that
conference, and ensure that it is capable of continuously recording for
the approximately 40 minutes of a talk. Some attendees may also own some
of the required equipment themselves, and may be willing to bring it to
the conference, if asked.

### Saturday Evening Dinner

It is customary for all the attendees to have a celebratory dinner on
the Saturday evening. For such a large group it is necessary to make a
reservation that can accommodate all registered attendees as well as
spouses and friends that may want to participate. Similar to the
conference lunches, a survey of food alergies and restrictions will need
to be considered when choosing a venue and meal options.

### Upload video and/or presentation slide to website/wiki

All presenters are encouraged to submit their slides to be posted on the
conference website/wiki. Video of the presentation should also be
uploaded.
