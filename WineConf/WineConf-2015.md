The conference is over.

## Date

September 19th and 20th, 2015

## Location

InterCityHotel Wien

Mariahilfer Straße 122

1070 Vienna

Austria

<https://en.intercityhotel.com/Vienna/InterCityHotel-Wien>

### Group Photo

![Group photo](/media/Wineconf2015-grp.jpg){width=1024px}

From left top to right bottom:

Sebastian Lackner, Aaryaman Vasishta, Austin English, Rosanne DiMesio,
Ulrich Czekalla, Michael Stefaniuc, Henri Verbeet

Aric Stewart, Piotr Caban, Jacek Caban, Caron Wills, Christian Inci,
André Hentschel, Jeremy White, Alois Schloegl, Andrew Eikum, Huw Davies,
Hans Leidekker, Robert (Focht)

Stefan Doesinger, Dmitry Timoshkov, Alexandre Julliard, Francois Gouget,
Andrei Podoplelov, Nikolay Sivov, Michael Müller

Marcus Meissner, Jactry Zeng, Qian Hong, Josh DuBois, Vincent Povirk,
Józef Kucia, Matteo Bruni

Attendees not in photo: Alex Henrie

## Schedule

Friday

- 15:00-22:00 - Informal gathering in the hotel bar. Dinner is on your
  own, although there are usually groups that leave for dinner during
  this time. There will likely be an early group out for dinner (the
  jetlagged folks from the US).

Saturday

- 07:00-09:00 - Breakfast. This is often a social time and an informal
  gathering as well.
- 09:00-09:30 - Keynote by Alexandre Julliard
- 09:30-09:45 - Review of agenda, fill in schedule based on interest and
  done-ness of presentations
- 09:45-12:00 - Continuing presentations and discussions (see Agenda,
  below)
- 12:00-13:00 - Lunch
- 13:00-17:00 - Continuing presentations and discussions
- 19:00-21:00 - Dinner, at Melker Stiftskeller, Schottengasse 3, -
  <https://www.melkerstiftskeller.at/index-en.html>
- 21:00-??:?? - Informal gatherings, usually at bars near the town
  center. The hotel bar is open until 1:00 am as well.

Sunday:

- 07:00-09:30 - Breakfast
- 09:30-12:00 - Presentations and discussions
- 12:00-13:00 - Lunch
- 13:00-17:00 - Presentations and discussions
- 17:00 - WineConf is done; farewell. Dinner on your own, although there
  are often informal gatherings.

We have the room until 23:00 or so, so we are free to stay and continue
conversations each day as long as we like.

## Agenda

The agenda is usually pretty loosely defined and is often refined
throughout the conference, but the topics will be largely constrained to
those relating to Wine development. If there is a topic you would like
to discuss, send an email to the wineconf mailing list. We usually also
set aside time for ad-hoc working groups; we'll keep scratch pads handy
for such conversations.

There is one traditional item you do not want to miss though:

- Saturday, 9:00 - Keynote presentation by Alexandre
  ([slides](/media/Keynote-wineconf2015.pdf) \|
  ‎[video](https://www.youtube.com/watch?v=GTN5ksxzFnE))

Other talk/discussion proposals:

- Wine Staging Introduction - Michael Stefaniuc
  ([slides](/media/Wine-staging-introduction-wineconf2015.pdf) \|
  [video](https://www.youtube.com/watch?v=QdcFNqWTja4))
- Wine Stable Considered Harmful - Michael Stefaniuc / Open discussion
- Wine USB Overview, Current status, Next steps - Ulrich Czekalla / Open
  discussion
- Regressions - Open discussion
- "Tricking Alexandre into accepting your patches" - Michael Stefaniuc
- Wine Tests Hackathon - Jeremy White
- hid, hidclass and joystick unification - Aric Stewart
  ([slides](/media/Hid-wineconf2015.pdf))
- The other architectures - André Hentschel
  ([slides](/media/Other-architectures-wineconf2015.pdf))
- Full Stack on Wine - Qian Hong
  ([slides](/media/Full-stack-on-wine-wineconf2015.pdf))
- Brief update on CodeWeavers - Jeremy White
  ([video](https://www.youtube.com/watch?v=EzR9RPlWi7c))
- Wine and Valgrind - Austin English
  ([slides](/media/Wine-and-valgrind-wineconf2015.pdf))

Actual flipchart agenda:

![Agenda photo](/media/Wineconf2015-agenda.jpg){width=1024px}

## Travel sponsorship

The Wine Partyfund has sponsored several attendees trip and hotelcosts.
Thanks for all the donations that make this event even better!

## Getting there

By Plane: The nearest airport is Vienna International Airport (VIE):
<https://en.wikipedia.org/wiki/Vienna_International_Airport> . A popular
alternative due to cheap prices is the Bratislava Airport (BTS / LZIB).

From the Vienna airport, you have many choices. You can take a train to
Wien Mitte, and change for the U3 line to Ottakring. Get off at
Westbahnhof and walk to the hotel. But you have two train choices -
first is an 11 EUR CAT train which leaves at 06 and 36 after the hour.
Or you can take the ÖBB S7 for EUR 4,40 at 17 or 47 after the hour, and
ride for an extra 11 minutes. There is also a bus which goes directly to
the Westbahnhof for 8 EUR:
<https://www.viennaairport.com/jart/prj3/va/uploads/data-uploads/Passagier/Parken/VIE_Postbus_1187_de_en.pdf>
.

By Train: The hotel is next to the Wien Westbahnhof railway station:
<https://en.wikipedia.org/wiki/Wien_Westbahnhof_railway_station> . It can
also be reached from the new Vienna Main Station (Wien Hauptbahnhof) via
tram line 18.

By Car: Driving to and in Vienna is simple, but parking is not. The
hotel offers parking space for €€€.

## Local transportation

The Hotel room includes a ticket for the public transportation system in
Vienna. General information about the public transport system can be
found at <https://www.wienerlinien.at/> . Warning: Google Maps does not
have transit data; you will need that link.
