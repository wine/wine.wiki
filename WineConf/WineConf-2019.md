## About

WineConf is the Wine Project's annual conference, where members of the
Wine community, the broader Free Software community and interested users
meet up over food and beverages. We traditionally also have a number of
talks by attendees, as well as some formal and informal discussions
about anything vaguely Wine related.

## Date

October 12 – 13, 2019

## Pictures

![Group photo](/media/Wineconf2019-grp.jpg){width=1024px}
[Group photo with no names](/media/Wineconf2019-grp-nonames.jpg)

## Venue

[Hotel Novotel Toronto
Centre](https://www.accorhotels.com/gb/hotel-0931-novotel-toronto-centre/index.shtml)
45 The Esplanade
Toronto, ON
Canada
+1 416 367-8900

## Registration

Conference registration is now closed. Subscribe to
[wineconf@winehq.org](https://list.winehq.org/mailman3/postorius/lists/wineconf.winehq.org/)
to receive updates. For questions, send an e-mail to the list or join us
in \#winehackers on the Freenode IRC network.

## Travel

**By Plane:**

You will fly into Lester B. Pearson International Airport (YYZ). To get
to the hotel you can take the [UP
Express](https://www.torontopearson.com/en/transportation/up-express)
for \$12.35 CAD each way. Alternatively you can use Uber or Lyft for
about \$45 CAD or a taxi for about \$55 CAD.

### Currency

Canada has their own currency, the Canadian Dollar (CAD). ATMs are
widely available and most stores accept common debit and/or credit
cards.

### Climate

Toronto in October had daily average temperatures ranging from a high of
14˚C (57˚F) to a low of 8˚C (46˚F).

### Power

In Canada the power sockets are of [type
A](https://www.iec.ch/worldplugs/typeA.htm) and [type
B](https://www.iec.ch/worldplugs/typeB.htm). The standard voltage is 120
V and the standard frequency is 60 Hz.

## Travel sponsorship

The [Wine Development Fund](https://www.winehq.org/donate) will sponsor
WineConf attendance for contributors to Wine that would not otherwise be
able to attend. Contact [Jeremy White](mailto:jwhite@codeweavers.com) to
request a sponsorship. If instead you'd like to make a donation to the
[Wine Development Fund](https://www.winehq.org/donate), we'll gladly
accept those as well.

## Programme

[Aric Stewart](@aricstewart) has volunteered to be
responsible for recruiting conference speakers and scheduling talks this
year. If you have a presentation you would like to give at this year's
WineConf, please propose the talk either on the
[wineconf@winehq.org](https://list.winehq.org/mailman3/postorius/lists/wineconf.winehq.org/)
mailing list or by contacting Aric directly.

**General Guidelines**

- Please provide an indication of the length of your talk. We're aiming
  for approximately 40 minute slots, with 15–20 minutes for the talks
  themselves, and an approximately equal amount of time for questions
  and discussion afterwards. Shorter talks are generally not an issue;
  longer talks may be possible depending on the contents of the talk and
  the rest of the schedule.
- Please indicate if a differently timed slot would be desired, we can
  make slots short and fit more talks if people are able.
- Topics should obviously be of interest to the other attendees, i.e.,
  the broader Wine community, but note that that doesn't imply the talk
  has to be about Wine itself.
- We will assign a time slot for you but if you have a need for a
  particular time and date for your talk please indicate that to Aric
  when submitting your talk. We will try to make it work as best we can.

**Saturday October 12**

|               |                                                                                                  |
|---------------|--------------------------------------------------------------------------------------------------|
| 08:00 – 09:00 | Breakfast                                                                                        |
| 09:00 – 09:40 | **Keynote** - Alexandre Julliard                                                                 |
| 09:45 – 10:00 | **WineConf Agenda Review** - Jeremy White                                                        |
| 10:05 – 10:25 | **CodeWeavers Company Update** - Jeremy White                                                    |
| 10:30 – 11:00 | Break                                                                                            |
| 11:00 – 11:40 | **Steam and Wine: Where we are** - Pierre-Loup Griffais                                          |
| 12:00 – 13:00 | Lunch & group picture                                                                            |
| 13:00 – 13:40 | **[AppDB: Goals, Needs, and Rulez](/media/Appdbgoalsneedsrulez-wineconf2019.pdf)** - Jeff Hanson |
| 13:45 – 14:25 | **Wine and Conservancy** - Karen Sandler                                                         |
| 14:30 – 15:00 | Break                                                                                            |
| 15:00 – 15:40 | **[Wine's synchronization primitives](/media/Sync-wineconf2019.pdf)** - Zebediah Figura          |
| 15:45 – 16:25 | **[Remote debugger demo](/media/Remote-debugging-wineconf2019.pdf)** - Jacek Caban               |
| 16:25 – 17:00 | **Overflow time / small group discussion / short presentations**                                 |
| 18:30         | **Pogue Mahone Pub and Kitchen**                                                                 |

### Saturday evening Dinner

**Pogue Mahone Pub and Kitchen**
[Map](https://maps.google.com/maps?ll=43.660819,-79.384492&z=16&t=m&hl=en-CA&gl=US&mapclient=embed&cid=16198007186458961566)
777 Bay Street
Saturday, October 12, 18:30

The easiest way to get there is from Union station, take the Yonge
subway line north to College station. The restaurant is just west on
College St.
**Sunday October 13**

|               |                                                                                                         |
|---------------|---------------------------------------------------------------------------------------------------------|
| 08:00 – 09:00 | Breakfast                                                                                               |
| 09:00 – 09:40 | **Future of Wineconf** - Jeremy White                                                                   |
| 09:45 – 10:25 | **[Update on Wine-Staging](/media/Staging-wineconf2019.pdf)** - Zebediah Figura                         |
| 10:30 – 11:00 | Break                                                                                                   |
| 11:00 – 11:40 | **[Coccinelle Introduction](/media/Coccinelle-wine-introduction-wineconf2019.pdf)** - Michael Stefaniuc |
| 12:00 – 13:00 | Lunch                                                                                                   |
| 13:00 – 13:40 | **Wine 32-bit on Mac State of the Union** - Chip Davis                                                  |
| 13:45 – 14:25 | **Wine D3D** - Stefan Dösinger                                                                          |
| 14:30 – 15:00 | Break                                                                                                   |
| 15:00 – 15:40 | <open>                                                                                                  |
| 15:45 – 16:25 | <open>                                                                                                  |
| 16:25 – 17:00 | **WineConf 2020 discussion**                                                                            |
