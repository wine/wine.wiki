## Date

October 28th - 29th, 2017

## Pictures

![Group photo](/media/Wineconf2017-grp1.jpg){width=1024px}
![Group photo](/media/Wineconf2017-grp2.jpg){width=1024px}

## Location

**Hotel Sofitel Wroclaw Old Town**

ul. Sw. Mikolaja 67

50-127 Wrocław

Poland

Phone: +48 713588300

<https://www.sofitel.com/gb/hotel-5345-sofitel-wroclaw-old-town/index.shtml>

## Schedule

Friday

- 17:00-22:00 - Informal gathering in the hotel bar. Dinner is on your
  own, although there are usually groups that leave for dinner during
  this time.

Saturday

- 09:00-09:30 - Keynote by Alexandre Julliard
- 09:30-12:00 - Presentations and discussions (see below)
- 12:00-13:00 - Lunch
- 13:00-17:00 - Presentations and discussions (see below)
- 18:30 - Dinner at [Jasna restaurant](https://www.restauracjajasna.pl/)

Sunday:

- 09:30-12:00 - Presentations and discussions (see below)
- 12:00-13:00 - Lunch
- 13:00-17:00 - Presentations and discussions (see below)

## Agenda

The agenda is usually pretty loosely defined and is often refined
throughout the conference, but the topics will be largely constrained to
those relating to Wine development. If there is a topic you would like
to discuss, send an email to the wineconf mailing list. We usually also
set aside time for ad-hoc working groups; we'll keep scratch pads handy
for such conversations.

There is one traditional item you do not want to miss though:

- Saturday, 9:00 - Keynote presentation by Alexandre

Other talk/discussion proposals:

- WineTest Review
- CodeWeavers update
- Swag/Branding discussion
- Project Hangover: x86 emulation on ARM with Wine+Qemu (André, Stefan)
- Getting patches from Staging into mainline Wine
- Should we merge the wine-devel and wine-patches mailing lists?

(more TBA, feel free to add more items to the list)

## Hotel reservation

We have a pool of rooms at fixed price 325 PLN/night (around 77
EUR/night) reserved for WineConf attendees. To make a reservation,
please [fill the
form](https://www.winehq.org/pipermail/wineconf/attachments/20170608/04c3884e/attachment-0001.doc)
and e-mail it to the hotel (you can change the arrival/departure dates).

Please also fill [RSVP form](https://www.winehq.org/wineconf/rsvp) (even
if you're not staying in the hotel), so that we know you're coming.

## Getting there

**By Plane:**

The nearest airport is Wrocław Airport (WRO).

From the airport, you have two choices to get to the hotel. A taxi cab
will cost you about 50 PLN (~15 EUR) for the trip (see [air port
page](https://airport.wroclaw.pl/en/passager/getting-here/by-taxi/) for
details). The 106 bus line will get you to the hotel in about 45
minutes, a ticket ("normal" one way for 3 PLN) can be bought at the bus
stop with credit card. Exit the bus at *Orląt Lwowskich* bus stop and
take ~10 minutes walk to the hotel. You can also use Uber.

**By Train:**

The train station nearest to the hotel is *Wrocław Główny* railway
station. It's around 25 minutes away by foot from the hotel. There's
lots of options for getting from there by tram. The easiest is to take
tram no 15 and go to *Rynek* stop.

**By Car:**

The city center is quite crowded. It may be hard to find a parking place
there (it's free to park near town square on weekends and after 6 PM).
Parking at the hotel should be possible but is expensive.If you wish to
avoid parking fees let us know, it should be possible to find a place
free to park nearby.

It's easiest to buy the tickets for public transportation in bus/tram.
There's a machine that sells them in every vehicle. It's only possible
to pay by credit/debit card. If you prefer to pay by cash you should buy
the ticket in e.g. newspaper shop. The best page to find public
transportation routes is
[jakdojade.pl](https://jakdojade.pl/wroclaw/trasa/) (Google Maps also
may be used, but it's known to miss some connections).

It's also possible to pay by card in most taxis (but you have to confirm
it with the driver).

## Tourist

If you want to do some sightseeing in Wroclaw, area around Sofitel Hotel
is a good place to start. It's just next to [Market
Square](https://en.wikipedia.org/wiki/Market_Square,_Wroc%C5%82aw)
(called Rynek), which is the main tourist attraction in Wrocław. All the
area around Rynek is nice for walking.

[Ostrów
Tumski](https://en.wikipedia.org/wiki/Ostr%C3%B3w_Tumski,_Wroc%C5%82aw)
is the oldest part of the town. It's in walking distance from the hotel.

There are [multiple
museums](https://www.tripadvisor.com/Attractions-g274812-Activities-c49-Wroclaw_Lower_Silesia_Province_Southern_Poland.html)
in the city. [Panorama
Racławicka](https://www.panoramaraclawicka.pl/?lang=en), a 15x114m large
paining, is also very popular for tourists (tickets are often sold out,
so it's safer to buy them online in advance).

If you look closely you can see some of many Wroclaw's
[gnomes](https://www.inyourpocket.com/wroclaw/Gnomenclature_70297f) that
are placed next to many monuments and other tourist attractions.

Next to the Sofitel hotel, there is [St. Elizabeth's
Church](https://en.wikipedia.org/wiki/St._Elizabeth%27s_Church,_Wroc%C5%82aw),
which has a high tower with a very nice view of the city (warning: 200
stairs climbing).

# Minutes

## Saturday

### Keynote by Alexandre

See the [keynote](https://www.youtube.com/watch?v=AokFgDSLMWU). Nothing
surprising. Lines of code growing, slightly slower than previously.
First 10 years for 1 million lines, now approaching 4 million LoC.

Wine 3.0 end of this year (we do yearly time based releases).

Main Features:

- Direct3D 11
- Direct3D command stream
- Android driver (not fully upstream)
- Message mode pipes
- Default version bumped from Windows XP to Windows 7 (after 8 years
  being XP;)

We currently have 160 open regressions (down from 182 regressions 1 year
ago)

Google of Summer Code 2017 was not so good this year. 1 project, failed.

Outreach 2017 was successful, longstanding Wine Community admin Rosanne
DiMesio worked on the [AppDB](https://appdb.winehq.org/) (PHP).

### CodeWeavers update

CodeWeavers doing quite well, compared to 15 months ago.

CrossOver commercial Wine sells best on MacOS, but not so much on Linux
(where there are "free" wines). They saw a large increase of their OEM
Business, where they help people port apps to Wine. CodeWeavers hired
quite some people in the last year. The open-source Wine developer
market is pretty much hired up by CodeWeavers now, no free developers
left.

### "Hangover" project by Stefan Dösinger and André Hentschel

The goal is to run x86_64 Windows binaries on Arm64, with minimal
overhead.

Their method is writing a QEMU windows-user-land port, and hooking on
the Windows API level, mapping the calls and forwarding them to arm64
Wine.

```
   win64 x86 app                        win64 x86 app
   QEMU API mapper for kernel32 etc     PE x86 dll that just calls other native dlls
   arm64 kernel32                       QEMU API mapper of kernel32 etc.
```

The mapper code is made out of 1000s auto-generated API thunks, per API
one thunk function on the guest side and one proxy function on the host
side. So uses lots of auto-generated code, some hand adjusted.

It helps being 64 \<-\> 64bit, so pointers do not need to converted.
Exception handling is troublesome as usual.

Demoed on NVIDIA Shield.

Demos:

- notepad++ works nicely and is quite quick.
- A 3D example that reaches 122 FPS, very fluid. (most of the 3D stuff
  happens on the arm side)

A 32 bit x86 \<-\> 64 bit arm was also demoed. This needs more mapping,
especially the 32 bit \<-\> 64 bit pointer need to be mapped. But
"Notepad++" partially works already, is also kind of fast.

### WineTest session

(We usually try to get the failing tests down at the WineConf. See
<https://test.winehq.org/> if you are curious how our tests look.)

### SWAG / Community involvement

Aric told us about the Sticker project, handed out several blocks.

Wine T-Shirt project

Ask Aric about stickers.

(Who was it who should be responsible for Europe?)

How to get more people to contribute ... Lots of back and forth, not
many ideas.

Marcus got a stack of Wine Project Stickers for FOSDEM et.al..

### MacOS pending removal of 32 bit libraries

Apple will remove 32 bit libraries in 12-24 months, which makes 32 bit
Wine much harder.

Solutions: 32 \<-\> 64 bit thunking, might need quite some work. But see
"Hangover".

## Sunday

### Direct3D 12 update

![vkmodelviewer demo](/media/Vkmodelviewer-demo-wineconf2017.jpg){width=800px}

CodeWeavers has been working on a (stand-alone) Direct3D 12
implementation based on Vulkan called "vkd3d". Vkd3d is licensed under
the LGPL, and the intention is that eventually Wine's Direct3D 12
implementation will be based on this library. At the same time, vkd3d is
intended to be useful to non-Wine ("native") ports.

Demoed a Linux port of Microsoft's ModelViewer demo using this library.

We discussed largely where this library should be hosted.

### Wine Staging

The Wine Staging patchset is growing. More stuff should go to regular
wine. Sadly the maintainers of Wine Staging were not there.

We need to focus more on getting patches into mainline. Subsystem
maintainers should perhaps check out staging more frequently. Vincent
Povirk does regularly.

### Merging the wine-patches and wine-devel mailing lists

To ease entry for new submitters and better discussions there was the
proposal to merge the two lists. It looks like we agreed on doing that.

Needs some adjustments to the patch tracking engines, which Alexandre
wants to do.

### AppDB report by Rosanne

Largely a report of improvements over the Outreach work by Rosanne, and
demos e.g. when to assign high ratings.

The appdb admin frontend was demoed.

### Attracting more users to WineConf

Discussions ... some back and forth ...

Aggressively invite more people close to Wine (Staging, PlayOnLinux,
AppDB maintainers, former / inactive Wine Developers like Gerald
Pfeifer)

### Wayland demo

Alexandre has some wayland things working on his Laptop, but it's alpha
and not production ready.

He showed clock.exe and one other app. No keyboard yet, a bit mouse,
window management is not working.

### Git commit logs

Discussion of larger commits logs, and also including bug references.

Concerns are mostly around "how large?", "style?" and "more rejects
based on bad commit messages?".

Bug links should be included (standard format e.g. bug 424242). (Similar
also CIDs, appdb links or similar I think).

It seems we want to try it.

### Reviewer process

Alexandre does not want to frustrate maintainers by rejecting
submissions that were approved by other reviewers. So general question
if this works well.

Review of process of reviewers besides Alexandre? Seems while there is
no big enthusiasm, it seems to work ok-ish right now. Some more
reviewers were found/will be added.

### GSOC/GCodeIn/Outreach

[Google Summer of
Code](https://developers.google.com/open-source/gsoc/): Must be a 3
months coding project, open to 18+ university students

[Google Code-In](https://developers.google.com/open-source/gci/): Can be
other contributions (coding, documentation, outreach, QA, UI, etc.)
taking a few hours, open to 13-17 year old pre-university students

[FSF Outreachy](https://www.outreachy.org/): Can be mixed

André Hentschel admins the Google parts these days. Outreachy is
paid/sponsored by CodeWeavers, mentor list in the wiki. The current plan
is to do it once a year. (Outreachy runs 2 times a year.)

We did a live brainstorming and added topics to the Wiki pages, which
need to be refined further.

[Summer of Code](Summer-Of-Code)

[Code-In](Code-In)

### WineConf 2018

Next year's Wine conference will likely be held in The Hague
('s-Gravenhage), The Netherlands.

### Wine Test review

Another wine test review session.

### Wrap up
