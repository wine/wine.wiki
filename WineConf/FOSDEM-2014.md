## Date

**February 1 - 2, 2014**

We again have a devroom at FOSDEM 2014 (Brussels, Belgium) on Sunday.
For non-local people it is recommended to arrive on Friday/Saturday and
plan to leave either on Sunday evening or on Monday.

Of course you can also plan some extra time in Brussels. Usually there
also informal gatherings in some place where people can eat and talk on
Friday and Sunday night too.

## Location

FOSDEM is at **ULB Campus Solbosch, 1050 Brussels, Belgium**

## Mailing List

Please subscribe to the [wineconf mailing
list](https://list.winehq.org/mailman3/postorius/lists/wineconf.winehq.org/). This is the best
way to keep apprised of the latest news about the conference. This is
also the best place to discuss the agenda, travel planning, and so on.

## Agenda

[Wine devroom at
FOSDEM.org](https://fosdem.org/2014/schedule/track/wine/)

## Presentations

FOSDEM 2014 is over! Slides and Videos from presentations are below:

| Topic                                                                                       | Video                                                                                                                              | Speaker                                              |
|---------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------|
| [State of Wine](/media/State-of-wine-wineconf2014.pdf)                                      | Not recorded (equipment not ready)                                                                                                 | [Alexandre Julliard](@julliard)                      |
| [Pipelight - Netflix and more via Wine](/media/Pipelight-netflix-via-wine-wineconf2014.pdf) | [Video (lacks sound)](https://video.fosdem.org/2014/AW1120/Sunday/Pipelight_Netflix_and_more_via_Wine.webm)                        | Michael Müller, Sebastian Lackner                    |
| The User Experience                                                                         | [Video](https://video.fosdem.org/2014/AW1120/Sunday/The_User_Experience.webm)                                                      | [Rosanne DiMesio](@dimesio)                          |
| The Amazing Wine Test Framework                                                             | [Video](https://video.fosdem.org/2014/AW1120/Sunday/The_Amazing_Wine_Test_Framework.webm)                                          | [Jeremy White](@jwhite), [François Gouget](@fgouget) |
| Wine birds of a feather sessions                                                            | Not recorded                                                                                                                       |                                                      |
| [Wine on Android](/media/Wine-on-android-wineconf2014.pdf)                                  | [Video](https://video.fosdem.org/2014/AW1120/Sunday/Wine_on_Android.webm)                                                          | [Alexandre Julliard](@julliard)                      |
| [Performance of Wine and Common Graphics Drivers](/media/D3d-drivers-wineconf2014.odp)      | [Video](https://video.fosdem.org/2014/AW1120/Sunday/Performance_of_Wine_and_Common_Graphics_Drivers.webm)                          | [Stefan Dösinger](@stefan)                           |
| Direct3D Q&A                                                                                | [Video](https://video.fosdem.org/2014/AW1120/Sunday/Direct3D_QA.webm)                                                              | [Henri Verbeet](@hverbeet)                           |
| Win-builds and Mingw-w64                                                                    | [Video](https://video.fosdem.org/2014/AW1120/Sunday/Winbuilds_and_Mingww64_Package_manager_and_modern_toolchains_for_Windows.webm) | Adrien Nader                                         |
| Wine hacking session                                                                        | Not recorded                                                                                                                       | [Jeremy White](@jwhite)                              |
