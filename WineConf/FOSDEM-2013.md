## Date

**February 2 - 3, 2013**

Instead of having Wineconf 2012 we'll have a devroom at FOSDEM 2013
(Brussels, Belgium) on Sunday. The Keynote will be at 9:00. For
non-local people it is recommended to arrive on Friday/Saturday and plan
to leave either on Sunday evening or on Monday.

Of course you can also plan some extra time in Brussels. Usually there
also informal gatherings in some place where people can eat and talk on
Friday and Sunday night too.

## Location

**ULB Campus Solbosch, 1050 Brussels, Belgium**

FOSDEM is at ULB Campus Solbosch. We choosed a Hotel for the Wine
related people, it's the **Best Western Hotel Royal Centre**. You should
book your room directly from the hotel, the sooner the better.

- For Wine, Jeremy selected the Best Western Hotel Royal Centre
  - Rue Royale 160, 1000 Brussels
  - <https://www.royalcentre.be/>
  - Official Rate is 89€ per night, including breakfast (via hrs as
    example)
  - A Better Price for two nights found at trivago.de (forwarding to
    booking.com)
  - Single Room is 59€ per night, excluding breakfast (Breakfast: 15€)
    and it's still possible to cancel the booking, when needed.

Of course, you can just pick another Hotel, this is just the default for
us.

## Mailing List

Please subscribe to the [wineconf mailing
list](https://list.winehq.org/mailman3/postorius/lists/wineconf.winehq.org/). This is the best
way to keep apprised of the latest news about the conference. This is
also the best place to discuss the agenda, travel planning, and so on.

## Agenda

[Wine Project devroom at
FOSDEM.org](https://fosdem.org/2013/schedule/track/wine_project/)

## Getting there

[Transportation guide at
FOSDEM.org](https://fosdem.org/2013/practical/transportation/)

## Presentations

Wineconf 2013 is over! Slides and Videos from presentations are below:

| Topic                                                                                                                   | Video                                                                             | Speaker                                         |
|-------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------|-------------------------------------------------|
| [Keynote](/media/Keynote-wineconf2013.pdf)                                                                              | [Keynote](https://www.youtube.com/watch?v=ZsG2xhtGIUI)                            | [Alexandre Julliard](@julliard)                 |
| [Wine for Users](/media/Wine-for-Users-wineconf2013.pdf)                                                                | N/A                                                                               | [Rosanne DiMesio](@dimesio)                     |
| [Mono Development for Wine](/media/Mono-Development-for-Wine-wineconf2013.pdf)                                          | [Mono Development for Wine](https://www.youtube.com/watch?v=ZBO1B7E3B64)          | [Vincent Povirk](@madewokherd)                  |
| [Status of 3D Drivers for Modern Gaming](/media/Status-of-3d-drivers-for-modern-gaming-wineconf2013.pdf)                | [Graphics Drivers for Modern Gaming](https://www.youtube.com/watch?v=T4ACXvm2gbc) | [Stefan Dösinger](@stefan)                      |
| [Windows RT and Wine](/media/Windows-RT-and-Wine-wineconf2013.pdf) / [Wine on ARM](/media/Wine-on-ARM-wineconf2013.pdf) | N/A                                                                               | [Vincent Povirk](@madewokherd), André Hentschel |
| [WineTestBot](/media/Winetestbot-wineconf2013.pdf)                                                                      | N/A                                                                               | [François Gouget](@fgouget)                     |
