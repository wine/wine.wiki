## About

WineConf is the Wine Project's annual conference, where members of the
Wine community, the broader Free Software community and interested users
meet up over food and beverages. We traditionally also have a number of
talks by attendees, as well as some formal and informal discussions
about anything vaguely Wine related. **This year, we will also be
celebrating the Wine Project's 25th anniversary.**

## Date

June 29 – July 1, 2018

## Pictures

![Group photo](/media/Wineconf2018-grp1.jpg){width=1024px}
![Group photo](/media/Wineconf2018-grp2.jpg){width=1024px}

Original group photos:
[1](/media/Wineconf2018-grp1-nonames.jpg)
[2](/media/Wineconf2018-grp2-nonames.jpg)

## Videos

[Keynote by Alexandre
Julliard](https://dl.winehq.org/other/wineconf/2018/julliard.mp4)

[More Videos](https://dl.winehq.org/other/wineconf/2018/)

## Venue

[Mercure Hotel Den Haag
Central](https://www.mercure.com/gb/hotel-1317-mercure-hotel-den-haag-central/index.shtml)
Spui 180
2511 BW The Hague
The Netherlands
+31 70 2039002
[Map](https://osm.org/go/0E4DCECmB-?m=)

**Rates**

We have a pool of rooms reserved at the rates below for WineConf
attendees. Send an e-mail to <hverbeet@codeweavers.com> before **May 1
2018**, and we'll get you in contact with the hotel.

Rates are per room per night, including breakfast, and excluding local
city tax of €3.40 per person per night.

|              | Single occupancy | Double occupancy |
|--------------|------------------|------------------|
| 2 night stay | €109             | €124             |
| 3 night stay | €105             | €120             |

## Registration

Conference attendance is free of charge. If you'd like to attend
WineConf 2018, please [RSVP](https://www.winehq.org/wineconf/rsvp)
and/or send an e-mail to <hverbeet@codeweavers.com>. Subscribe to
[wineconf@winehq.org](https://list.winehq.org/mailman3/postorius/lists/wineconf.winehq.org/)
to receive updates. For questions, send an e-mail to either of those, or
join us in \#winehackers on the Freenode IRC network.

## Travel

**By Plane:**

The airport with the best connection to The Hague is Amsterdam Schiphol
(AMS). A possible alternative is Rotterdam The Hague Airport (RTM).
There's an approximately 30 minute direct [train
connection](https://www.ns.nl/en) from "Schiphol Airport" to "Den Haag
Centraal" (The Hague Central Station) for €8,50. It's about a 10 minute
(approximately 1 km) walk from the train station to the hotel, but a
taxicab or [public transport](https://www.htm.nl/english/) are options
as well.

**By Train:**

For attendees travelling from France, Belgium or Germany, the high-speed
[Thalys](https://www.thalys.com/nl/en/) may be an option. Den Haag
Centraal station is 1km away from the hotel and so should be in walking
distance.

**By Road:**

The Hague is well connected by road, but please be aware that paid
parking is in effect in practically the entire city centre.

**Within The Hague:**

The Hague has a decent [public transport](https://www.htm.nl/english/)
system, as well as plenty of taxicab providers.

### Currency

Like large parts of Europe, the currency in use in The Netherlands is
the [Euro](https://en.wikipedia.org/wiki/Euro). There are ATMs [close to
the venue](https://osm.org/go/0E4DAtCkA-?m=), and many stores accept
common debit and/or credit cards.

### Climate

The Hague has a [temperate oceanic
climate](https://en.wikipedia.org/wiki/The_Hague#Climate). Temperatures
are usually fairly comfortable in late June, but it can be windy.
Evenings in June can be slightly chilly.

### Power

The most common (domestic) power socket in the Netherlands is the [CEE
7/3](https://en.wikipedia.org/wiki/AC_power_plugs_and_sockets#CEE_7_standards)
socket. CEE 7/1 is occasionally also still used. Mains operate at 230 V
and 50 Hz.

## Travel sponsorship

The [Wine Development Fund](https://www.winehq.org/donate) will sponsor
WineConf attendance for contributors to Wine that would not otherwise be
able to attend. Contact [Jeremy White](mailto:jwhite@codeweavers.com) to
request a sponsorship. If instead you'd like to make a donation to the
[Wine Development Fund](https://www.winehq.org/donate), we'll gladly
accept those as well.

## Tourism

The Hague has a decent number of opportunities for tourism;
[<https://denhaag.com/en>](https://denhaag.com/en) has some pointers.

## Programme

**Friday June 29**

|               |            |
|---------------|------------|
| 15:00 – 16:30 | **Tour 1** |
| 16:45 – 18:15 | **Tour 2** |
| 18:30         | **Dinner** |

[Aric Stewart](@aricstewart) has volunteered to be
responsible for recruiting conference speakers and scheduling talks this
year. If you have a presentation you would like to give at this years'
WineConf, please propose the talk either on the
[wineconf@winehq.org](https://list.winehq.org/mailman3/postorius/lists/wineconf.winehq.org/)
mailing list or by contacting Aric directly.

**General Guidelines**

- Please provide an indication of the length of your talk. We're aiming
  for approximately 40 minute slots, with 15–20 minutes for the talks
  themselves, and an approximately equal amount of time for questions
  and discussion afterwards. Shorter talks are generally not an issue;
  longer talks may be possible depending on the contents of the talk and
  the rest of the schedule.
- Topics should obviously be of interest to the other attendees, i.e.,
  the broader Wine community, but note that that doesn't imply the talk
  has to be about Wine itself.
- If you have a preferred time and date for your talk please indicate
  that to Aric when submitting your talk. We will try to make it work as
  best we can.

**Saturday June 30**

|               |                                                                                                                                                                              |
|---------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 07:00 – 09:00 | Breakfast                                                                                                                                                                    |
| 09:00 – 09:40 | [**Keynote**](https://dl.winehq.org/other/wineconf/2018/julliard.mp4) - Alexandre Julliard ([slides](/media/Keynote-wineconf2018.pdf))                                       |
| 09:45 – 10:25 | [**CodeWeavers Company Update**](https://dl.winehq.org/other/wineconf/2018/jwhite.mp4) - Jeremy White                                                                        |
| 10:30 – 11:00 | Break                                                                                                                                                                        |
| 11:00 – 11:40 | [**State and future of Wine-Staging**](https://dl.winehq.org/other/wineconf/2018/zeb.mp4) - Zebediah Figura ([slides](/media/Staging-wineconf2018.pdf))                      |
| 12:00 – 13:00 | Lunch & group picture                                                                                                                                                        |
| 13:00 – 13:40 | [**The Electronic Design Automation Market**](https://dl.winehq.org/other/wineconf/2018/jeff.mp4) - Jeff Hanson                                                              |
| 13:45 – 14:25 | [**Having Fun With Emulators**](https://dl.winehq.org/other/wineconf/2018/stefan.mp4) - Stefan Dösinger and André Hentschel ([slides](/media/Stefan-andre-wineconf2018.pdf)) |
| 14:30 – 15:00 | Break                                                                                                                                                                        |
| 15:00 – 15:40 | <open>                                                                                                                                                                       |
| 15:45 – 16:25 | <open>                                                                                                                                                                       |
| 16:25 – 17:00 | Overflow time / small group discussion / short presentations                                                                                                                 |
| 18:00         | **Saturday evening barbecue**                                                                                                                                                |

### Saturday evening barbecue

The Saturday evening barbecue will be provided by

[Poppies & Pastries](https://www.instagram.com/poppiesandpastries/)
Rijswijkseweg 656
2516 HX The Hague
The Netherlands
[Map](https://osm.org/go/0E4CZkaJw-?m=)

If there are any dietary requirements or preferences we should be aware
of, please e-mail <hverbeet@codeweavers.com>, or leave a comment on the
[RSVP form](https://www.winehq.org/wineconf/rsvp), and we'll do our best
to accommodate those.

**Sunday July 1**

|               |                                                                                                                                                                        |
|---------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 07:00 – 09:00 | Breakfast                                                                                                                                                              |
| 09:00 – 09:40 | [**Why We Must Wh?ine for Software Freedom**](https://dl.winehq.org/other/wineconf/2018/bkuhn.mp4) - Bradley Kuhn                                                      |
| 09:45 – 10:25 | [**Wine Vulkan**](https://dl.winehq.org/other/wineconf/2018/roderick.mp4) - Roderick Colenbrander                                                                      |
| 10:30 – 11:00 | Break                                                                                                                                                                  |
| 11:00 – 11:40 | **WineTest infrastructure discussion** - Jeremy White                                                                                                                  |
| 12:00 – 13:00 | Lunch                                                                                                                                                                  |
| 13:00 – 13:40 | [**An introduction to Wine Direct3D architecture**](https://dl.winehq.org/other/wineconf/2018/henri.mp4) - Henri Verbeet ([slides](/media/D3d-intro-wineconf2018.pdf)) |
| 13:45 – 14:25 | [**Wine for EVE Online**](https://dl.winehq.org/other/wineconf/2018/snorri.mp4) - Snorri Sturluson                                                                     |
| 14:30 – 15:00 | Break                                                                                                                                                                  |
| 15:00 – 15:40 | <open>                                                                                                                                                                 |
| 15:45 – 16:25 | <open>                                                                                                                                                                 |
| 16:25 – 17:00 | **WineConf 2019 discussion**                                                                                                                                           |

## WineConf 2019

We traditionally decide where next years' conference will take place
during WineConf. If you'd like to host WineConf 2019, but have any
questions, please send an e-mail to <hverbeet@codeweavers.com>. Since
we'd like to make WineConf interesting to a broader base of developers
and users, we would be especially grateful if WineConf 2019 could be
hosted outside the traditional locations of western Europe and the
United States.

## Attendees

1.  Alexandre Julliard
2.  Henri Verbeet
3.  Stefan Dösinger
4.  Józef Kucia
5.  Andrew Eikum
6.  Michael Stefaniuc
7.  Matteo Bruni
8.  Huw Davies
9.  François Gouget
10. Austin English
11. Josh DuBois (possible, as I am able)
12. Rosanne DiMesio
13. Robert Walker
14. Jeremy White
15. Piotr Caban
16. Jacek Caban
17. Ulrich Czekalla
18. Daniel Lehman
19. André Hentschel
20. Aric Stewart
21. Sergio Andrés Gómez Del Real
22. Hana Pagel
23. Zebediah Figura
24. Owen Rudge
25. Mandi Gagne
26. Hans Leidekker
27. Julius Schwartzenberg
28. Jeremy Newman
29. Fabian Maurer
30. Jactry Zeng
31. Katie Grace
32. Nikolay Sivov
33. Anna Lasky
34. Linards Liepiņš
35. Alex Henrie
36. Akihiro Sagawa
37. Detlef Riekenberg
38. Marcus Meissner
39. Jens Reyer
40. Aaryaman Vasishta
41. Jeff Hanson
42. Gijs Vermeulen
