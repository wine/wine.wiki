---
title: WineConf 2002-2011
---

# WineConf 2011

October 1 - 2, 2011

Minneapolis, Minnesota

- Keynote presentation by Alexandre
  [video](https://www.youtube.com/watch?v=2rdDvMonTnQ) \|
  [slides](/media/Aj-talk-wineconf2011.pdf)

# WineConf 2010

November 20 - 21, 2010

Paris, France

**Talks and slides:**

- Alexandre's [Keynote](/media/Aj-talk-wineconf2010.pdf)
- Introduction round (no slides)
- Shachar Nemesh on [RTL
  weirdness](/media/SetProcessDefaultLayout-wineconf2010.pdf)
- Stefan Doesinger on performance in 3d games
- Vincent Povirk about .NET integration into Wine using Mono
- Detlef Riekenberg on better Crash Error Reporting and submission
- Joerg Hoehle on MCI
- André Hentschel on state of ARM and its naming:
  [Slides](/media/Arm-talk-wineconf2010.odp)
- Dan Kegel standing in for a non-present audio developer
- GPG signing

**Pictures**

Marcus took
[pictures](https://www.flickr.com/photos/marcusmeissner/sets/72157625430516298/).

# WineConf 2009

November 7 - 8, 2009

Enschede, Netherlands

**Presentations**

- Alexandre's traditional keynote:
  [video](https://www.archive.org/download/Wineconf2009/julliardkeynote2009.ogv),
  [low bandwidth
  video](https://www.archive.org/download/Wineconf2009/julliardkeynote2009-small.ogv)
- Ge van Geldorp - [VMware
  ThinApp](https://www.vmware.com/de/products/thinapp/):
  [video](https://www.archive.org/download/Wineconf2009/geldorpthinapp2009.ogv),
  [low bandwidth
  video](https://www.archive.org/download/Wineconf2009/geldorpthinapp2009-small.ogv)
- Austin English - Auto Hotkey:
  [video](https://www.archive.org/download/Wineconf2009/englishautohotkey2009.ogv),
  [low bandwidth
  video](https://www.archive.org/download/Wineconf2009/englishautohotkey2009-small.ogv)
- Michael Stefaniuc presented wineoops - A statistical approach to Wine
  crashes:
  [slides](https://people.redhat.com/mstefani/wineoops/wineoops-wineconf2009.pdf),
  [video](https://www.archive.org/download/Wineconf2009/stefaniucwineoops2009.ogv),
  [low bandwidth
  video](https://www.archive.org/download/Wineconf2009/stefaniucwineoops2009-small.ogv)
- Martin Pilka - Application Testing:
  [video](https://www.archive.org/download/Wineconf2009/pilkaapptest2009.ogv),
  [low bandwidth
  video](https://www.archive.org/download/Wineconf2009/pilkaapptest2009-small.ogv)

**News articles, blog posts, etc**

- Scott Ritchie's blog: ["A brief summary of Wineconf
  2009"](https://yokozar.org/blog/archives/171)

# WineConf 2008

September 27 - 28, 2008

St. Paul, Minnesota, USA

- Alexandre's keynote
  [video](https://www.youtube.com/watch?v=mZQACnvVe3w)

**Group Photo**

![Group photo](/media/Wineconf2008-grp.jpg){width=1024px}
![Annotated group photo](/media/Wineconf2008-grp-annotated.jpg){width=1024px}

# WineConf 2007

October 6 - 7, 2007

Zurich, Switzerland

**Presentations**

- Alexandre gave his yearly updated status report.
  [slides](/media/Aj-talk-wineconf2007.pdf) \|
  [video](https://www.youtube.com/watch?v=GqEp8psjv5s)
- A Wine 1.0 discussion was held.
- Kai Blin gave a report from the CIFS conference that was the week
  before. [slides](/media/Cifs-report-wineconf2007.pdf)‎ \|
  [video](https://www.youtube.com/watch?v=ToKeJbQ87c0)
- Martin Pilka (CodeWeavers) reported about CxTest - what happened since
  WineConf 2006, where are we today with this product and where do we
  want to be tomorrow.
  [slides](/media/Cxtest-wineconf2007.odp) \|
  [Bisect-crossover.jpg](/media/Bisect-crossover-wineconf2007.jpg) \|
  [video](https://www.youtube.com/watch?v=VBZNOTngyeo)
- Detlef Riekenberg gave a presentation about printing.
  [video](https://www.youtube.com/watch?v=1-n6mClDdBQ)
- Maarten Lankhorst gave a talk about directsound.
  [video](https://www.youtube.com/watch?v=qKaYqsm4I_Q)
- Stefan Dösinger gave a short talk about the current Direct3D state.
  [video](https://www.youtube.com/watch?v=eJ-zyKR1N2A)
- Dan Kegel talked about what Google is doing with Wine, and introduced
  the win16test project (see bug #9850).

**Pictures**

[Lei Zhang's pictures](https://picasaweb.google.com/zhanglei/Wineconf)

**Group photo**

![Annotated group photo](/media/Wineconf2007-grp-annotated.jpg){width=1024px}

# WineConf 2006

September 16 - 17, 2006

Reading, UK

Nike Lecture Theatre, University of Reading

**Proposed Presentations**

- Keynote, by Alexandre Julliard
- Presentation by Stefan Dösinger on DirectX and Games support
- Presentation by Dan Kegel on using Wine for an ISV
- Future and past of COM by Rob Shearman
- Presentation on git, by Mike McCormack
- Presentation on cxtest by Martin Pilka

**Group Photo**

![Annotated group photo](/media/Wineconf2006-grp-annotated.jpg){width=1024px}

# WineConf 2005

April 30 - May 1, 2005

Stuttgart, Germany

- [WWN page](https://www.winehq.org/wwn/272)
- [Video downloads](ftp://lisas.de/pub/wine/wineconf/2005/)
  - 2005_04_30_10_12_39.avi Dimi - The road to Wine 1.0
  - 2005_04_30_11_13_44.avi Shachar - Introduction of PGP signing party
    (not Wine related)
  - 2005_04_30_11_22_59.avi Charles Stevenson
  - 2005_04_30_13_31_16.avi Juan
  - 2005_04_30_14_00_31.avi Andrew Tridgell Mostly discussion on Wine
    and Samba cooperation
  - 2005_04_30_16_31_39.avi Andrew Bartlett
  - 2005_05_01_08_52_15.avi ReactOS - Steven & Hyperion - Updates on ROS
    updates, and some demonstrations
  - 2005_05_01_09_52_29.avi Jason E. - Gaming state of the Union
    (Direct3D8/9, ...) plus some demonstrations
  - 2005_05_01_11_12_30.avi Mike M. & Aric - Reimplementing MSI
  - 2005_05_01_13_57_26.avi Marcus - x86 assembly basics
  - 2005_05_01_15_07_37.avi Paul & Jeremy W. - CxTest: automatic
    full-GUI regression tests
  - 2005_05_01_16_48_07.avi Dimi \#2 - Future plans

**Group Photos**

![Annotated group photo](/media/Wineconf2005-grp-annotated.jpg){width=1024px}
![](/media/Wineconf2005-fei.jpg){width=1024px}

# WineConf 2004

January 30 - February 1, 2004

St. Paul, Minnesota, USA

![Annotated group photo](/media/Wineconf2004-grp.jpg){width=1024px}

- [**WWN summary of the event**](https://www.winehq.org/wwn/208)
- [Brian's photo album](https://www.theshell.com/~vinn/wineconf2004/)
- Things to remember:
  - The Icecastle with 30 minutes queue (Marcus)
  - Being there on the coldest day of 2004 and the snowiest day of 2004
    (Marcus)
  - The frozen feet that were numb for hours (Dimi)

# WineConf 2002

March 15 - 16, 2002

San Diego, CA, USA

**Agenda**

**WINE TECHNOLOGY**

- WINE Past, Present, and Future \[Julliard\]
- Supporting Drag and Drop between Wine and non-Wine windows
  \[Weigland\]
- How To Make Fonts Look Great In Wine \[Davies\]
- Package steamlining of WINE \[Meissner\]
- DirectX \[Kaaven\]
- IActiveScript, or how jscript.dll was reimplemented using the Mozilla
  JScript engine, logs of glue, and a little bit of luck \[Hatheway\]
- Beyond Trial And Error, Alternative Ways To Insure Quality Code
  \[Stridvall\]
- A How-to For Regression Testing \[Gouget\]
- Winedoc and Automated Verification \[Paun\]
- Wine/Windows bootup handling and documentation/end user representation
  \[Mohr\]
- Package streamlining of WINE \[Meissner\]

**BUSINESS ISSUES**

- What Code we've been Weaving \[Jeremy White/CodeWeavers\]
- Macadamian - Making It As A Software House \[Boulanger\]
- An Unique Commercial Application for WINE \[Cadlink
  Technologies/Hawkes\]
- Gaming and Beyond \[Transgaming/State\]
- ReactOS - Where it's at, where it's going \[ReactOS/Filby\]
- Corel, A Look Back \[Xandros/Tranter\]
- Lindows.com And WINE \[Robertson\]
