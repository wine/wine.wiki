## About

WineConf is the Wine Project's annual conference, where members of the
Wine community, the broader Free Software community and interested users
meet up over food and beverages. We traditionally also have a number of
talks by attendees, as well as some formal and informal discussions
about anything vaguely Wine related.

This year WineConf was held in Minneapolis together with the [X.Org
developer's conference and FOSS
XR](https://indico.freedesktop.org/event/2/).

## Date

October 5 – 6, 2022

## Pictures

Covid-19 still made it necessary to wear masks, greatly reducing the
point of taking a group picture. So no group picture this year :disappointed:

## Videos

But thanks to Arkadiusz Hiler we do have videos of the talks which you
can now find on the [WineHQ YouTube
channel](https://www.youtube.com/channel/UCWq8EWXLlMyZzaskJAdKHVQ).

[Keynote by Alexandre
Julliard](https://www.youtube.com/watch?v=oatANL4QuQ4)

[All the WineConf 2022
videos](https://www.youtube.com/watch?v=oatANL4QuQ4&list=PLG9u6ypXJGynzRVLZVk7CKHT4XQ2ymEr2)

## Schedule

**Wednesday 5**

|               |                                                                                                                                        |
|---------------|----------------------------------------------------------------------------------------------------------------------------------------|
| 09:00 – 09:40 | [**Keynote**](https://www.youtube.com/watch?v=oatANL4QuQ4) - Alexandre Julliard ([slides](/media/Keynote-wineconf2022.pdf))            |
| 09:45 – 10:15 | [**Proton: What Lies Down the Stream?**](https://www.youtube.com/watch?v=CbVASChdAV8) - Arkadiusz Hiler                                |
| 11:00 – 11:50 | [**The Time Has Come Defending Wine's License**](https://www.youtube.com/watch?v=6dzG38DDS0Y) - Bradley Kuhn                           |
| 13:20 – 14:05 | [**Wine hacking tips**](https://www.youtube.com/watch?v=uBljyk9FpU8) - Huw Davies ([slides](/media/Upstreaming-tips-wineconf2022.pdf)) |
| 14:20 – 15:05 | [**Wine + GitLab**](https://www.youtube.com/watch?v=WPQn8VeizRQ) - Arkadiusz Hiler & Alexandre Julliard                                |
| 15:30 – 16:00 | [**Wine-Staging update**](https://www.youtube.com/watch?v=9bVAw3unmCo) - Zeb Figura ([slides](/media/Staging-wineconf2022.odp))        |
| 16:00 – 16:30 | [**Lutris**](https://www.youtube.com/watch?v=9btMXeSb6ms) - Mathieu Comandon                                                           |
| 16:35 – 16:40 | [**Code of Conduct**](https://www.youtube.com/watch?v=Li13lYeXj0Q) - Austin English                                                    |

**Thursday 6**

|               |                                                                                                                                                                           |
|---------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 09:00 – 09:15 | [**Testing the Unix bits**](https://www.youtube.com/watch?v=XVrMouEab1s) - Esme Povirk                                                                                    |
| 09:30 – 10:15 | [**vkd3d-shader and the HLSL compiler**](https://www.youtube.com/watch?v=_dFzOFhAECM) - Giovanni Mascellani ([slides](/media/Vkd3d-shader-wineconf2022.pdf))              |
| 10:30 – 11:15 | [**What's New in Direct3D**](https://www.youtube.com/watch?v=i58f_LPFEwI) - Zeb Figura ([slides](/media/D3d-wineconf2022.odp))                                            |
| 11:30 – 12:15 | [**Wine on macOS State of the Union**](https://www.youtube.com/watch?v=TlJ9jaNUcUo) - Brendan Shanks ([slides](/media/Wine-on-macos-state-of-the-union-wineconf2022.pdf)) |
| 13:45 – 14:30 | [**MoltenVK and Vulkan Portability**](https://www.youtube.com/watch?v=lGuoDb0I4aA) - Bill Hollings                                                                        |
| 14:50 – 15:20 | [**ProtonDB One Can Make A Difference**](https://www.youtube.com/watch?v=-hU-ECtT_Sk) - Buck DeFore                                                                       |
| 15:25 – 15:45 | [**32 on 64 demo**](https://www.youtube.com/watch?v=NX_ddpNERmQ) - Jacek Caban                                                                                            |
