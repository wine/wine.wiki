---
title: Debian/Ubuntu
---



<small>
&nbsp;[:flag_nl: Nederlands](nl/Debian-Ubuntu)
&nbsp;[:flag_cn: 简体中文](zh_CN/Debian-Ubuntu)
</small>

-----


Although Debian and Ubuntu offer their own Wine packages, they are
often several versions behind. To make installing the latest version
of Wine as easy as possible, WineHQ has its own Debian/Ubuntu
repository. Should a newer version of Wine cause problems, it is also
possible to install an older version of your choice.

The WineHQ repository only offers packages for AMD64 and i386. If you
need the ARM version, you can use the Debian/Ubuntu packages.

## Preparation

- If your system is 64 bit, enable 32 bit architecture:  

  ```sh
  sudo dpkg --add-architecture i386
  ```

- Make a note of your distribution name:  
  Look for the line with either `UBUNTU_CODENAME` or `VERSION_CODENAME`. If both are present, use the name after `UBUNTU_CODENAME`.

  ```sh
  cat /etc/os-release
  ```

## Add the repository

- Download and add the repository key:

  ```sh
  sudo mkdir -pm755 /etc/apt/keyrings
  wget -O - https://dl.winehq.org/wine-builds/winehq.key | sudo gpg --dearmor -o /etc/apt/keyrings/winehq-archive.key -
  ```

- Add the repository:  
  If your distribution name is not on the list, older [packages may be available](#my-debianubuntu-version-is-not-listed) on the download server. Add **one** repository. 

  | Distribution&nbsp;name      | Command                        |
  |:----------------------------|:-------------------------------|
  | **oracular**<br><small>Ubuntu 24.10</small>                  | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/oracular/winehq-oracular.sources`       |
  | **noble**<br><small>Ubuntu 24.04<br>Linux Mint 22</small>    | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/noble/winehq-noble.sources`       |
  | **jammy**<br><small>Ubuntu 22.04<br>Linux Mint 21.x</small>  | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/jammy/winehq-jammy.sources`       |
  | **focal**<br><small>Ubuntu 20.04<br>Linux Mint 20.x</small>  | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/focal/winehq-focal.sources`       |
  | **trixie**<br><small>Debian Testing</small>                  | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/debian/dists/trixie/winehq-trixie.sources`     |
  | **bookworm**<br><small>Debian 12</small>                     | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/debian/dists/bookworm/winehq-bookworm.sources` |
  | **bullseye**<br><small>Debian 11</small>                     | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/debian/dists/bullseye/winehq-bullseye.sources` |

- Update the package information:

  ```sh
  sudo apt update
  ```

## Install Wine

Install **one** of the following packages:

| Wine branch        | Command                                                |
|:-------------------|:-------------------------------------------------------|
| Stable branch      | `sudo apt install --install-recommends winehq-stable`  |
| Development branch | `sudo apt install --install-recommends winehq-devel`   |
| Staging branch     | `sudo apt install --install-recommends winehq-staging` |

The [User's Guide](Wine-User%27s-Guide#wine-from-winehq) explains the
differences between the branches.

## Help

Sometimes there are problems installing Wine. If your problem is not
listed, search the [forum](https://forums.winehq.org/WineHQ) or if you
can't find an answer ask your question.

### Missing dependencies

Read the
[FAQ](FAQ#How_do_I_solve_dependency_errors_when_trying_to_install_Wine.3F)
about dependency errors and tips for troubleshooting dependency issues.
The most common issues are:

#### Third-party repositories

WineHQ packages are created and tested on a clean installation. Using
PPAs or third-party repositories may prevent the installation of Wine.
Often the problem is that these repositories are not multiarch. The
required 32 and 64-bit packages are missing or cannot be installed side
by side. The *deb.sury.org* repository is known for causing problems.

Downgrade the problematic dependency packages to the official version.

#### KDE Neon

Ubuntu 22.04 KDE Neon users report problems with the *libpoppler-glib8*
dependency. The solution is to downgrade this package to the official
Ubuntu version.
`sudo apt install libpoppler-glib8:{i386,amd64}=22.02.0-2ubuntu0.3`

#### Backports

Another cause may be the use of backports. A newer 64-bit version of a
library is already installed, but the 32-bit version isn't. These
packages are given a lower priority so they will not be installed
automatically. The solution is to manually install the missing 32-bit
package from backports.

#### FAudio

Older versions of Wine (prior to version 6.21) have FAudio as a
dependency. These packages are missing on Ubuntu 18.04. These can be
downloaded from the [Open Build
Service](https://download.opensuse.org/repositories/Emulators:/Wine:/Debian/xUbuntu_18.04/).
For Debian 10, these packages are available in backports.

### Winehq key problems

- **The WineHQ repository key was changed on 2018-12-19**

  If you downloaded and added the key before that time, you will need
  to download and add the new key and run `sudo apt update` to accept
  the repository changes.

- **Apt-key is now deprecated**

  Previously, *apt-key* was used to add the Wine key. If you get this
  warning, remove the Wine key with:

  ```sh
  sudo apt-key del "D43F 6401 4536 9C51 D786 DDEA 76F1 A20F F987 672F"
  ```

  And remove the the line about the WineHQ repository from
  */etc/apt/sources.list(.d/\*)*.

### Mirror sync in progress?

If you get an error message when trying to install a package from WineHQ
that includes the line `Mirror sync in progress?` that is most likely
the problem. There are many packages to sync, and it can take a long
time to complete.

Wait a few hours, and try again. If the problem persists for more than a
day, file a bug.

### My Debian/Ubuntu version is not listed

When a Debian/Ubuntu version is no longer supported, no new Wine packages are built.
And the repository will no longer appear in the list above.
Check the [WineHQ server](https://dl.winehq.org/wine-builds/) for older packages.
Since no new versions are built, it is not necessary to add the WineHQ repository.
Just download and install [the four WineHQ deb packages](#notes).

Please note that these packages are no longer maintained and are no
longer supported.

## Notes

- Menu items are not created for Wine's builtin programs (winecfg,
  etc.). If you upgrade the Wine distro packages that had added them,
  they will be removed. You can recreate them yourself using your menu
  editor.

- The Wine files are installed in `/opt/wine-<branch>/`

- WineHQ does not offer *wine-gecko* or *wine-mono* packages. When
  creating a new [wineprefix](FAQ#Wineprefixes), you will be
  asked if you want to download those components. For best
  compatibility, it is recommended to click *Yes* here. If the download
  doesn't work for you, please follow the instructions on the
  [Gecko](Gecko) and [Mono](Wine-Mono) wiki pages to
  install them manually.

- Beginning with Wine 5.7, the WineHQ packages have an optional debconf
  setting to enable CAP_NET_RAW to allow applications that need to send
  and receive raw IP packets to do so. This is disabled by default
  because it carries a potential security risk, and the vast majority of
  applications do not need that capability. Users of applications that
  do need it can enable CAP_NET_RAW after installing Wine by running
  `dpkg-reconfigure wine-<branch>-amd64 wine-<branch> wine-<branch>-i386`
  and answering *yes* to the three questions.

- Binfmt_misc registration is not added. Consult your distro's
  documentation for update-binfmts (`man update-binfmts`) if you wish to
  do this manually.

- A complete Wine installation on a 64-bit system consists of four
  packages.

  - `winehq-<branch>` This package ensures that the *wine* command is
    available system-wide.

  - `wine-<branch>` This package has the following two packages as
    dependencies and provides a working Wine installation.

  - `wine-<branch>-amd64` The 64-bit part of Wine.

  - `wine-<branch>-i386` The 32-bit part of Wine.

  By splitting a Wine over different packages, it is possible to install
  different branches side by side.

  For example: Use Wine *stable* as the default Wine version and install
  Wine *staging* to test other programs.

  Install Wine stable:

  `sudo apt install --install-recommends winehq-stable`

  Install Wine staging:

  `sudo apt install --install-recommends wine-staging` (Note the missing
  *hq* after *wine*)

  Run a program with Wine *stable*:

  `wine program.exe`

  Run a program with Wine *staging*:

  `WINEPREFIX=~/wine-staging /opt/wine-staging/bin/wine program.exe`

  (It is recommended to give each Wine branch its own wineprefix.)

- There are several versions of Wine on the repository. The latest
  version is installed by default. Usually, the latest version is
  recommended. However, it may happen that an older version is desired.
  Use `apt policy winehq-<branch>` to list the different available
  versions.

  Install an older version of your choice with

  ```sh
  sudo apt install winehq-<branch>=<version>
  ```

  For example:

  ```sh
  sudo apt install winehq-staging=7.12~bookworm-1
  ```

  When the Wine packages are downgraded, all four Wine packages must be
  downgraded.

  ```sh
  sudo apt install winehq-staging=7.12~bookworm-1 wine-staging=7.12~bookworm-1 wine-staging-amd64=7.12~bookworm-1 wine-staging-i386=7.12~bookworm-1
  ```

## Installing without Internet

To install Wine on a machine without internet access, you must have
access to a second machine (or VM) with an internet connection to
download the WineHQ .deb package and its dependencies.

On the machine with internet, add the WineHQ repository and run apt
update as described above.

Next, cache just the packages necessary for installing Wine, without
extracting them:

```sh
sudo apt-get clean
sudo apt-get --download-only install winehq-<branch>
sudo apt-get --download-only dist-upgrade
```

Copy all of the .deb files in /var/cache/apt/archives to a USB stick:

```sh
cp -R /var/cache/apt/archives/ /media/usb-drive/deb-pkgs/
```

Finally, on the machine without internet, install all of the packages
from the flash drive:

```sh
cd /media/usb-drive/deb-pkgs sudo dpkg -i *.deb
```

## Building from Source

- Beginning with 4.0-rc2, the WineHQ repository includes the .dsc,
  .diff.gz, and .orig.tar.gz files generated by the Open Build
  Service(OBS). These source packages can be found on
  `https://dl.winehq.org/wine-builds/<debian|ubuntu>/dists/<version>/main/source`
- The latest version of Debian and Ubuntu are multiarch. It is
  possible to install all 64 and 32 bit dependencies side by
  side. This allows Wine to be built using the steps listed under
  [Shared WoW64](Building-Wine#shared-wow64).
- On older versions of Debian/Ubuntu the multiarch implementation
  could be incomplete.  You can't simply install 32-bit and 64-bit
  libraries alongside each other. If you're on a 64-bit system, you'll
  have to create an isolated environment for installing and building
  with 32-bit dependencies. See [Building Wine](Building-Wine) for
  instructions on how to build in a chroot or container.

## See Also

- Official WineHQ [download site](https://dl.winehq.org/wine-builds/)
  for Debian/Ubuntu.
- [WineHQ Debian/Ubuntu package build scripts and
  logs](https://build.opensuse.org/project/show/Emulators:Wine:Debian)
- The [Debian Wiki's page for Wine.](https://wiki.debian.org/Wine)
- [Packaging](Packaging)
