Patches and changes are always being applied to the Wine source code.
There have been some attempts to track the status of Wine as more and
more features are supported and tested.

## Automated Metrics

Some information about current Wine progress can be automatically mined
from the SourceCode, using scripts from the [webtools
repo](https://gitlab.winehq.org/winehq/tools/) and the [tools/
directory](https://gitlab.winehq.org/wine/wine/-/tree/HEAD/tools) of
the wine repo. These statistics are regularly refreshed and should be
up-to-date:

- A report of Wine's translation status can be found
  [here](https://fgouget.free.fr/wine-po/).
- A quick summary of [Wine test-suite
  results](https://test.winehq.org/data/), with links to more detailed
  information.

## Other Status Info

- A quick rundown of the [features Wine currently
  offers](Wine-Features), probably of more use to users or
  third-party developers.
