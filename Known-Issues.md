<small>
&nbsp;[:flag_kr: 한국어](ko/Known-Issues)
</small>

-----

Top level page for the most duplicated bugs in Bugzilla, and the most
frequently reported bugs on wine-devel, wine-users, the user forum,
IRC, and the AppDB.

### Current known issues

- [List of most frequently reported bugs on
  Bugzilla](https://bugs.winehq.org/duplicates.cgi)
- [Bugs with STAGED
  patches](https://bugs.winehq.org/buglist.cgi?bug_status=STAGED&query_format=advanced)

### Not bugs in Wine (but may still cause problems)

|        |                                                                                                                                                |
| ------ | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| #7991  | Rendering is garbled when a program needs to use the maximum number of shader constants of a graphic card                                      |
| #10841 | Restore display resolution when focus is lost or on exit                                                                                       |
| #18118 | Multiple DOS apps/games fail to start in [DOSBox](DOSBox) when started from Wine (need DOSBox \> 0.74)                                         |
| #31882 | Many multithreaded gui apps randomly deadlock in winex11 driver surface section                                                                |
| #32479 | Multiple applications crash or fail to establish secure server connections (Wine package built with 32-bit GnuTLS 2.6 while host provides 2.8) |
| #34166 | Fullscreen flickering on Mac                                                                                                                   |
| #37347 | Black screen or distorted graphics with Intel drivers (Mesa bug [84651](https://bugs.freedesktop.org/show_bug.cgi?id=84651))                   |
| #41637 | Delphi: Wineserver stuck / not responding since linux-4.8.3 (kernel bug fixed in 4.9.7)                                                        |

### Previous issues that have been fixed

- [Bugs fixed in Wine
  3.\*](https://bugs.winehq.org/buglist.cgi?bug_status=CLOSED&chfield=bug_status&chfieldfrom=2018-01-18&chfieldto=now&chfieldvalue=CLOSED&list_id=460065&product=Wine&query_format=advanced&resolution=FIXED)

- [Bugs fixed in Wine
  2.\*](https://bugs.winehq.org/buglist.cgi?bug_status=CLOSED&chfield=bug_status&chfieldfrom=2017-01-24&chfieldto=now&chfieldvalue=CLOSED&list_id=460065&product=Wine&query_format=advanced&resolution=FIXED)

- [Bugs fixed in Wine
  1.9.\*](https://bugs.winehq.org/buglist.cgi?bug_status=CLOSED&chfield=bug_status&chfieldfrom=2015-12-25&chfieldto=2016-12-09&chfieldvalue=CLOSED&product=Wine&query_format=advanced&resolution=FIXED)

- [Bugs fixed in Wine
  1.7.\*](https://bugs.winehq.org/buglist.cgi?bug_status=CLOSED&chfield=bug_status&chfieldfrom=2013-08-02&chfieldto=2015-12-19&chfieldvalue=CLOSED&product=Wine&query_format=advanced&resolution=FIXED)

- [Bugs fixed in Wine
  1.5.\*](https://bugs.winehq.org/buglist.cgi?bug_status=CLOSED&chfield=bug_status&chfieldfrom=2012-03-16&chfieldto=2013-07-18&chfieldvalue=CLOSED&product=Wine&query_format=advanced&resolution=FIXED)

- [Bugs fixed in Wine
  1.3.\*](https://bugs.winehq.org/buglist.cgi?bug_status=CLOSED&chfield=bug_status&chfieldfrom=2010-07-30&chfieldto=2012-03-07&chfieldvalue=CLOSED&product=Wine&query_format=advanced&resolution=FIXED)

- [Bugs fixed in Wine
  1.1.\*](https://bugs.winehq.org/buglist.cgi?bug_status=CLOSED&chfield=bug_status&chfieldfrom=2008-06-27&chfieldto=2010-07-15&chfieldvalue=CLOSED&product=Wine&query_format=advanced&resolution=FIXED)

## See also

- [Bugs](Bugs)
- [Bug Triage](Bug-Triage)
