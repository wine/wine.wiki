---
title: Soporte de chat en directo
---

<small>
&nbsp;[:flag_gb: English](IRC)
&nbsp;[:flag_fr: Français](fr/IRC)
&nbsp;[:flag_de: Deutsch](de/IRC)
&nbsp;[:flag_pt: Português](pt/IRC)
&nbsp;[:flag_nl: Nederlands](nl/IRC)
&nbsp;[:flag_pl: Polski](pl/IRC)
&nbsp;[:flag_tr: Türkçe](tr/IRC)
&nbsp;[:flag_kr: 한국어](ko/IRC)
&nbsp;[:flag_cn: 简体中文](zh_CN/IRC)
</small>

-----


[Libera.​Chat](https://libera.chat/) hospeda un canal IRC para Wine.
Puedes acceder a la sala de chat usando un programa IRC como
[HexChat](https://hexchat.github.io/). Usa las configuraciones listadas
abajo.

> **Servidor:** irc.libera.chat\
> **Puerto:** 6697\
> **Canal:** #winehq

Si utilizas Firefox o otros navegadores que soporte urls IRC, puedes
unirte al chat clickeando en [#winehq](irc://irc.libera.chat/winehq).
