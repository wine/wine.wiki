<small>
&nbsp;[:flag_fr: Français](fr/News)
</small>

-----

## On WineHQ

If you're interested in recent or archived news about the Wine project,
there are a few different sources on the website. You can read or
subscribe to the [WineHQ news RSS feed](https://www.winehq.org/news) to
keep up with new releases and major changes.

The [Wine Weekly News (WWN)](https://www.winehq.org/wwn) newsletter gives
a better picture of Wine's day-to-day progress and what was happening in
the community.

If you want more primary sources (pun intended), you can browse the
[Wine project source code](https://gitlab.winehq.org/wine/wine/) or
download various data such as AppDB and Bugzilla database dumps from
the [download site](https://dl.winehq.org/wine/). A more quantified
overview of Wine's development status can be [found
here](Wine-Status).

## Reviews & Articles

If you come across a current, well-written article on Wine, feel free to
put a link (or citation if it isn't online) right here:

## Other Mentions

As more and more people use Wine for Windows compatibility, you may find
it discussed in other sources such as reference books. You can cite
these mentions here, especially if they give a good explanation of a
feature or how to use Wine.
