## What is WineGUI?

**WineGUI** is a graphical frontend for the Wine software compatibility
layer which allows Linux users to install Windows-based software as well
as games. WineGUI is using the GTK toolkit and is written in the C++
programming language.

**Note:** WineGUI is currently using the Wine version you locally
installed. Look at the [Debian/Ubuntu](Debian-Ubuntu) wiki page for
how to install the latest Wine stable release.

**Note:** Although using WineGUI can be useful for managing your
programs/bottles in Wine, WineHQ does not provide support. See
[Reporting bugs](#reporting-bugs-features-requests) below.

![WineGUI Main Window](/media/Winegui-screenshot0.png){width=25%}<br>
*WineGUI Main Window*

-----
![WineGUI Edit Window](/media/Winegui-screenshot1.png){width=25%}<br>
*WineGUI Edit Window*

-----
![WineGUI Configure Window](/media/Winegui-screenshot2.png){width=25%}<br>
*WineGUI Configure Window*

-----
![WineGUI New Machine Window](/media/Winegui-screenshot3.png){width=25%}<br>
*WineGUI New Machine Window*

-----

### Features

WineGUI features include but not limited to:

- Graphical user-interface on top of [Wine](home)
- Creating a new machine using an easy step-by-step wizard
- Application list per machine (with search feature)
- Editing, removing or cloning Windows machines in a breeze
- Configure window installing additional software with just a single
  click (like installing DirectX)
- One-button click to run a program, open the C: drive, simulate a
  reboot or kill all processes

## Download & Install WineGUI

You can download the latest deb, rpm or tar pre-build package from:
<https://gitlab.melroy.org/melroy/winegui/-/releases>

WineGUI application will inform you when a new release is out.

## Reporting bugs / Features requests

WineGUI has a issue tracking system at:
<https://gitlab.melroy.org/melroy/winegui/-/issues>. There is also a
GitHub mirror as well: <https://github.com/winegui/WineGUI> (mirror)

If you don't have an account, you can [create an
account](https://gitlab.melroy.org/users/sign_up) to file an issue.
