---
title: Wine
---

<small>
&nbsp;[:flag_gb: English](home)
&nbsp;[:flag_fr: Français](fr/home)
&nbsp;[:flag_nl: Nederlands](nl/home)
&nbsp;[:flag_kr: 한국어](ko/home)
&nbsp;[:flag_cn: 简体中文](zh_CN/home)
</small>

-----

[Wine](https://winehq.org) ermöglicht es Linux-, Mac-, FreeBSD- und
Solaris-Nutzern, Windows-Anwendungen ohne eine Kopie von Microsoft
Windows auszuführen. Wine ist [freie
Software](https://www.gnu.org/philosophy/free-sw.html) und wird ständig
weiterentwickelt. [Andere Plattformen](Compatibility)
profitieren ebenfalls.

## Meistbesuchte Links

- [Häufig gestellte Fragen](FAQ): Wenn du ein allgemeines
  Problem mit Wine hast, lies bitte die häufig gestellten Fragen!

- [Wine-Handbuch](de/Documentation): Wenn dir die häufig
  gestellten Fragen nicht weitergeholfen haben, versuche es mit dem
  Handbuch! :smiley:

- [Die Wine Anwendungsdatenbank](https://appdb.winehq.org): Hilfe für
  eine bestimmte Anwendung erhältst du unter ihrem Eintrag in der
  Anwendungsdatenbank.

- [Wine Benutzerforum](https://forum.winehq.org): Hier kannst du Fragen
  stellen, auf die du oben keine Antworten gefunden hast.

- [Bekannte Probleme](Known-Issues): Bitte sieh hier nach,
  bevor du einen Fehler meldest.

- [winetricks](Winetricks): Ein nützliches Werkzeug, mit dem
  man aktuelle Defizite von Wine umgehen kann.

- [MacOS](MacOS): Wine auf einem Apple macOS ausführen.

## Über das Wine-Projekt

- Eine kurzer Überblick über die [Wine Features](Wine-Features) und
  Ziele.

- Ein paar kurze Artikel über die [Wichtigkeit von
  Wine](Importance-Of-Wine).

- Ein Überblick über die [Organisation des
  Wine-Projekts](Project-Organization).

- [Wer macht was](Who's-Who) beim Wine-Projekt,
  und [Danksagungen](Acknowledgements) an bedeutende Mitarbeiter.

- Die [Geschichte des Wine-Projekts](Wine-History) und
  aktuelle [Neuigkeiten](News).

- Informationen zur [Lizensierung](Licensing) von Wine.

## Mitarbeiten

- [Entwickler](Developers): Wenn du Wine mitentwickeln oder
  aus dem Quellcode kompilieren möchtest.

- [Anwendungsdatenbank-Wartung](https://gitlab.winehq.org/winehq/appdb/-/wikis/Maintainer-Guidelines):
  Einige Hinweise, wenn du die Anwendungsdatenbank wartest bzw. warten
  möchtest.

- [Debuggers](Bugs) können uns helfen, indem sie Wine testen,
  Probleme eingrenzen und von anderen Benutzern gemeldete Fehler prüfen.

- [Autoren](Writers) können mithelfen, indem sie die
  Wine-Dokumentation schreiben, das Wiki warten und verschiedene Teile
  des Projekts übersetzen.

- [Designers](Designers) können unter anderem Icons für das
  Programm zeichnen oder Aussehen und Funktion der Website verbessern.

- [Wine Development Fund](https://www.winehq.org/donate): Bitte denke
  darüber nach, das Wine-Projekt durch eine Spende zu unterstützen.

- [Public Relations](Public-Relations): Wege, um den
  Bekanntheitsgrad von Wine zu steigern.

## Andere nützliche Links

- Eine [Liste von Kommandos](Commands) für all die kleinen Werkzeuge,
  die bei Wine dabei sind.

- [Drittanwendungen](Third-Party-Applications) und
  "inoffizielle" Werkzeuge, die von Nutzen sein könnten.

- [Nützliche Registry-Schlüssel](Useful-Registry-Keys) und
  verschiedene Einstellungen für die Konfiguration von Wine.

- Instruktionen hinsichtlich [Regression Testing](Regression-Testing).

- [Anwendungen, die Wine offiziell
  unterstützen](Apps-That-Support-Wine).

## Mehr Informationen

- Der [Quellcode](Source-Code) des Wine-Projekts kann
  heruntergeladen oder online angesehen werden.

- Du kannst archivierte Diskussionen durchstöbern oder dich für die Wine
  [Mailinglisten](Forums) anmelden.

- Informationen zu den Fortschritten bei der Entwicklung und zum [Status
  von Wine](Wine-Status).

- Assorted files such as site icons and images, and database dumps of
  Bugzilla, AppDB, and the wiki can be downloaded from WineHQ's
  [download server](https://dl.winehq.org/wine/).

- View [WineHQ site
  statistics](https://www.winehq.org/webalizer/index.html) and rough
  estimates of Wine's [Usage Statistics](Usage-Statistics).
