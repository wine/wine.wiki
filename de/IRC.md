---
title: Live Community Chat
---

<small>
&nbsp;[:flag_gb: English](IRC)
&nbsp;[:flag_fr: Français](fr/IRC)
&nbsp;[:flag_es: Español](es/IRC)
&nbsp;[:flag_pt: Português](pt/IRC)
&nbsp;[:flag_nl: Nederlands](nl/IRC)
&nbsp;[:flag_pl: Polski](pl/IRC)
&nbsp;[:flag_tr: Türkçe](tr/IRC)
&nbsp;[:flag_kr: 한국어](ko/IRC)
&nbsp;[:flag_cn: 简体中文](zh_CN/IRC)
</small>

-----


Auf [Libera.Chat](https://libera.chat/) gibt es mehrere IRC-Kanäle für
Wine. Den Chatraum kannst du mit einem IRC-Programm betreten, z.B.
[HexChat](https://hexchat.github.io/). Nutze folgende Einstellungen:

> **Server:** irc.libera.chat\
> **Port:** 6697\
> **TLS:** AN

Welchen Kanal du wählen solltest hängt davon ab, welches Thema du
erörtern möchtest:

> **#winehq:** Unterstützung und Hilfe von anderen Benutzern beim Ausführen von Wine\
> **#crossover:** Benutzer-Support und Hilfe beim Ausführen von CrossOver\
> **#winehackers:** Entwicklung und andere Möglichkeiten um einen Beitrag für Wine zu leisten\
> **#winehq-social:** Ungezwungene, andere Themen betreffende Chats, mit verschiedenen Community Mitgliedern

Benutzer eines Browsers (z.B. Firefox), der IRC-URLs unterstützt, können
an einem Chat teilnehmen indem sie ihn anklicken:

- [#winehq](irc://irc.libera.chat/winehq)
- [#crossover](irc://irc.libera.chat/crossover)
- [#winehackers](irc://irc.libera.chat/winehackers)
- [#winehq-social](irc://irc.libera.chat/winehq-social)

Damit Diskussionen so fokussiert und hilfreich wie möglich bleiben,
versuche deine Fragen zu recherchieren, bevor du jemanden im IRC
danach fragst. Das [Wine FAQ](FAQ), [AppDB](https://appdb.winehq.org),
und die [Downloadseite](Download) sind gute Optionen, um deine Frage
vielleicht selbst zu beantworten.

## IRC-Regeln und -Strafen

Abgesehen davon, dass man sich nicht beleidigend oder offensichtlich
Rücksichtslos verhalten soll, gibt es ein Paar einfache Regeln, die
jeder auf IRC einzuhalten hat. In den meisten Fällen wird ein erster
Regelbruch als Versehen aufgefasst und es wird eine Verwarnung geben.
Nachdem du deine Warnungen allerdings aufgebraucht hast, wirst du aus
dem Kanal geworfen.

Wenn du zu oft rausgeworfen wurdest und weiterhin Regeln brichst, wirst
du für zwei Stunden vom Kanal verbannt. Jegliche Probleme danach führen
zu einer endgültigen Verbannung, welche nur durch einen Einspruch
gekippt werden kann. Wenn du einen Rauswurf anfechten möchtest, gehe auf
**#winehq-social** (oder auf die [Wine-Devel
Mailingliste](mailto:wine-devel@winehq.org), wenn du von
**#winehq-social** verbannt wurdest), und erkläre warum du glaubst, dass
dies passiert ist und warum die Verbannung aufgehoben werden sollte.

| Regel                                                                                            | Erläuterung                                                                                              | Verwarnungen | Rauswürfe |
|--------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------|--------------|-----------|
| Bitte nicht spammen.                                                                             |                                                                                                          | 1            | 2         |
| Nutze ein Pastebin, um mehr als eine oder zwei Zeilen zu schicken.                               |                                                                                                          | 0            | 5         |
| Schreibe im passenden Kanal.                                                                     | Wenn du unsicher bist, frage bei **#winehq** welcher Kanal der richtige  ist.                            | 2            | 3         |
| Nur Wine und CrossOver werden in den jeweiligen Kanälen unterstützt.                             | Sidenet, WineDoors, Cedega, IEs4Linux, etc. werden **nicht** unterstützt.                                | 2            | 1         |
| Bevor du auf **#winehq** nach Hilfe fragst, überprüfe, ob du die neueste Version von Wine nutzt. | Wenn du dir unsicher bist, führe `wine --version` in der Befehlszeile aus, um deine Version zu ermitteln | 3            | 1         |
| Bitte warte, bis du an der Reihe bist.                                                           |                                                                                                          | 3            | 1         |
| Diskutiere **nicht** über Raubkopien.                                                            |                                                                                                          | 1            | 1         |
