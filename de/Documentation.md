---
title: Dokumentation
---

<small>
&nbsp;[:flag_gb: English](Documentation)
&nbsp;[:flag_fr: Français](fr/Documentation)
&nbsp;[:flag_nl: Nederlands](nl/Documentation)
&nbsp;[:flag_pl: Polski](pl/Documentation)
&nbsp;[:flag_tr: Türkçe](tr/Documentation)
&nbsp;[:flag_kr: 한국어](ko/Documentation)
</small>

-----

## Handbücher

- **[Wine Benutzerhandbuch (en)](Wine-User's-Guide)**

  Wie man Wine benutzt und konfiguriert um Windows Anwendungen auszuführen.

- **[Winelib Benutzerhandbuch (en)](Winelib-User's-Guide)**

  Wie man Wine benutzt um Windows Anwendungen nach Linux zu portieren.

- **[Wine Entwicklerhandbuch (en)](Wine-Developer's-Guide)**

  Wie man Wine hackt.

- **[Wine Installation and Configuration](Wine-Installation-and-Configuration)**

  Eine kurze Einführung für alle die uns helfen wollen das es funktioniert.

## Command reference

- **[Commands](Commands)**

  Documentation about the commands available with Wine.

- **[Man Pages](de/Man-Pages)**

  Manual pages for the Wine commands and tools.
