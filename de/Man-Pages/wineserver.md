---
title: wineserver - der Wine Server
---

<small>
&nbsp;[:flag_gb: English](Man-Pages/wineserver)
&nbsp;[:flag_fr: Français](fr/Man-Pages/wineserver)
</small>

-----


## ÜBERSICHT

**wineserver&nbsp;**_[options]_

## BESCHREIBUNG

**wineserver**
ist ein Hintergrundprozess, der Wine vergleichbare Dienste bereitstellt,
wie der Windows Kernel unter Windows.

**wineserver**
startet normalerweise automatisch mit [**wine**](./wine), daher sollten Sie sich
darüber keine Gedanken machen müssen. In einigen Fällen kann es jedoch von
Nutzen sein, **wineserver** explizit mit verschiedenen Optionen aufzurufen,
wie im Folgenden beschrieben.

## ARGUMENTE


* **-d**[_n_], **--debug**[**=**_n_]<br>
  Setzt das Debuglevel auf
  _n_.
  0 gibt keine Debuginformationen aus, 1 steht für normale und 2 für extra
  detaillierte Ausgabe. Wenn
  _n_
  nicht angegeben wird, ist 1 der Standardwert. Die Debugausgabe wird
  an stderr geleitet. [**wine**](./wine) wird beim Start von **wineserver**
  automatisch das Debuglevel auf normal setzen, wenn +server in der
  Umgebungsvariable WINEDEBUG angegeben ist.
* **-f**, **--foreground**<br>
  Lässt den Server zur vereinfachten Fehlersuche im Vordergrund laufen,
  zum Beispiel für den Betrieb unter einem Debugger.
* **-h**, **--help**<br>
  Zeigt den Hilfetext an.
* **-k**[_n_], **--kill**[**=**_n_]<br>
  Beendet den momentan laufenden
  **wineserver**,
  optional mit Signal _n_. Wenn kein Signal angegeben wurde, wird
  SIGINT, gefolgt von einem SIGKILL gesendet. Die zu beendende Instanz von
  **wineserver** wird durch die Umgebungsvariable WINEPREFIX bestimmt.
* **-p**[_n_], **--persistent**[**=**_n_]<br>
  Gibt die Dauer an, für die **wineserver** weiterläuft nachdem alle
  Clientprozesse beendet sind. Das erspart den Rechenaufwand und Zeitverlust
  eines Neustarts, wenn Anwendungen in schneller Abfolge gestartet werden.
  Die Verzögerung _n_ ist anzugeben in Sekunden, der Standardwert ist 3.
  Bei fehlender Angabe von _n_ läuft der Server unbegrenzt weiter.
* **-v**, **--version**<br>
  Zeigt Versionsinformationen an und beendet sich wieder.
* **-w**, **--wait**<br>
  Wartet, bis sich der gerade laufende
  **wineserver**
  beendet hat.

## UMGEBUNGSVARIABLEN


* **WINEPREFIX**<br>
  Wenn gesetzt, wird der Inhalt dieser Umgebungsvariable als Pfad zu einem
  Verzeichnis interpretiert, in dem der
  **wineserver**
  seine Daten ablegt (Standardmäßig in _\$HOME/.wine_). Alle
  **wine**
  -Prozesse, die den selben
  **wineserver**
  verwenden (z.B. vom selben Benutzer), teilen sich u.a. die selbe Registry,
  gemeinsamen Speicher und Kernelobjekte.
  Durch Setzen von unterschiedlichen Pfaden als
  **WINEPREFIX**
  für verschiedene Wine-Prozesse ist es möglich, eine beliebige Zahl komplett
  unabhängiger Sitzungen von Wine zu betreiben.

## DATEIEN


* **~/.wine**<br>
  Verzeichnis mit benutzerspezifischen Daten, die von
  **wine**
  verwaltet werden.
* **/tmp/.wine-**_uid_<br>
  Verzeichnis, das den Unix-Socket des Servers und die lock-Datei enthält.
  Diese Dateien werden in einem Unterverzeichnis angelegt, dessen Name sich aus
  den Geräte- und Inode-Nummern des WINEPREFIX-Verzeichnisses zusammensetzt.

## AUTOREN

Der ursprüngliche Autor von
**wineserver**
ist Alexandre Julliard. Viele andere Personen haben neue Funktionen hinzugefügt
und Fehler behoben. Details finden Sie in der Datei Changelog.

## FEHLER

Wenn Sie einen Fehler finden, melden Sie ihn bitte im
[**Wine Bugtracker**](https://bugs.winehq.org).

## VERFÜGBARKEIT

**wineserver**
ist Teil der Wine-Distribution, verfügbar im WineHQ, dem
[**Hauptquartier der Wine-Entwicklung**](https://www.winehq.org/).

## SIEHE AUCH

[**wine**](./wine).
