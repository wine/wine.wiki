---
title: winemaker - Erzeugt eine Build-Infrastruktur, um Windows Programme unter Unix zu kompilieren
---

<small>
&nbsp;[:flag_gb: English](Man-Pages/winemaker)
&nbsp;[:flag_fr: Français](fr/Man-Pages/winemaker)
</small>

-----


## ÜBERSICHT

**winemaker**
[
**--nobanner** ] [ **--backup** | **--nobackup** ] [ **--nosource-fix**
]<br>
  [
**--lower-none** | **--lower-all** | **--lower-uppercase**
]<br>
  [
**--lower-include** | **--nolower-include** ]&nbsp;[ **--mfc** | **--nomfc**
]<br>
  [
**--guiexe** | **--windows** | **--cuiexe** | **--console** | **--dll** | **--lib**
]<br>
  [
**-D**_macro_[=_defn_] ] [_&nbsp;_**-I**_dir_ ]&nbsp;[ **-P**_dir_ ] [ **-i**_dll_ ] [ **-L**_dir_ ] [ **-l**_library_
]<br>
  [
**--nodlls** ] [ **--nomsvcrt** ] [ **--interactive** ] [ **--single-target** _name_
]<br>
  [
**--generated-files** ] [ **--nogenerated-files** ]<br>
  [
**--wine32** ]<br>
  _Arbeitsverzeichnis_ | _Projektdatei_ | _Workspacedatei_


## BESCHREIBUNG


**winemaker**
ist ein Perl-Script um Ihnen das Konvertieren von Windows-Quellcode
zu einem Winelib-Programm zu erleichtern.

Zu diesem Zweck beherrscht **winemaker** folgende Operationen:

- Quellcodedateien und Verzeichnisse in Kleinbuchstaben umbenennen, falls
diese beim Übertragen komplett in Großbuchstaben angekommen sind.

- Konvertierung von DOS- zu Unix-Zeilenenden (CRLF nach LF).

- Include-Anweisungen und Resourcenreferenzen durchsuchen, um Backslashes
durch Slashes zu ersetzen.

- Während des obigen Schrittes wird **winemaker** ebenfalls nach der angegebenen Datei
im Includepfad suchen und die entsprechende Zeile, falls nötig, mit der korrekten
Groß-/Kleinschreibweise austauschen.

- **winemaker** wird ebenso andere, exotischere Probleme wie die Benutzung von
_#pragma pack_, _afxres.h_ in nicht-MFC-Projekten und mehr untersuchen.
Sollte etwas ungwöhnliches vorkommen, wird **winemaker** Sie warnen.

- **winemaker** kann eine ganze Verzeichnisstruktur auf einmal durchsuchen,
schätzen welche ausführbaren Dateien und Bibliotheken Sie zu erstellen
gedenken, diese den passenden Quelldateien zuordnen und entsprechende _Makefile_
generieren.

- letztendlich wird **winemaker** eine globale _Makefile_ für den normalen Gebrauch erzeugen.

- **winemaker** erkennt MFC-basierte Projekte und erstellt angepasste Dateien.

- Existierende Projektdateien können von **winemaker** gelesen werden.
Unterstützt sind dsp, dsw, vcproj und sln-Dateien.


## ARGUMENTE


* **--nobanner**<br>
  Unterdrückt die Anzeige des Banners.
* **--backup**<br>
  Lässt **winemaker** Backups von allen Quellcodedateien anlegen, an denen
  Änderungen vorgenommen werden. Diese Option ist Standard.
* **--nobackup**<br>
  Lässt **winemaker** keine Backups anlegen.
* **--nosource-fix**<br>
  Weist **winemaker** an, keine Quellcodedateien zu ändern (z.B. DOS zu Unix
  Konvertierung). Verhindert Fehlermeldungen bei schreibgeschützten Dateien.
* **--lower-all**<br>
  Alle Dateien und Verzeichnisse werden in Kleinschreibung umbenannt.
* **--lower-uppercase**<br>
  Nur Dateien und Verzeichnisse, die komplett groß geschrieben sind, werden
  in Kleinschreibung umbenannt.
  _HALLO.C_ würde beispielsweise umbenannt werden, _Welt.c_ jedoch nicht.
* **--lower-none**<br>
  Keine Dateien und Verzeichnisse werden in Kleinschreibung umbenannt.
  Beachten Sie, dass dies nicht die Umbenennung von Dateien verhindert, deren
  Erweiterungen nicht unverändert verarbeitet werden können, z.B. ".Cxx".
  Diese Option ist Standard.
* **--lower-include** <br>
  Wenn die Datei zu einer Include-Anweisung (oder einer anderen Form von
  Dateireferenz für Resourcen) nicht auffindbar ist, wird der Dateiname in
  Kleinschreibung umbenannt. Diese Option ist Standard.
* **--nolower-include** <br>
  Es werden keine Änderungen an Include-Anweisungen oder Referenzen vorgenommen,
  wenn die entsprechende Datei nicht auffindbar ist.
* **--guiexe** | **--windows**<br>
  Legt fest, dass für jedes gefundene, ausführbare Target, oder Target unbekannten
  Typs angenommen wird, dass es sich um eine grafische Anwendung handelt.
  Diese Option ist Standard.
* **--cuiexe** | **--console**<br>
  Legt fest, dass für jedes gefundene, ausführbare Target, oder Target unbekannten
  Typs angenommen wird, dass es sich um eine Konsolenanwendung handelt.
* **--dll**<br>
  **winemaker** wird im Zweifelsfall annehmen, dass es sich bei einem unbekannten
  Target um eine DLL handelt.
* **--lib**<br>
  **winemaker** wird im Zweifelsfall annehmen, dass es sich bei einem unbekannten
  Target um eine statische Bibliothek handelt.
* **--mfc**<br>
  Teilt **winemaker** mit, dass es sich um MFC-basierte Ziele handelt. In solch einem
  Fall passt **winemaker** Pfade für Header und Bibliotheken entsprechend an und
  verlinkt die Ziele mit der MFC-Bibliothek.
* **--nomfc**<br>
  Teilt **winemaker** mit, dass es sich nicht um MFC-basierte Ziele handelt. Diese
  Option verhindert die Benutzung von MFC-Bibliotheken, selbst wenn **winemaker**
  Dateien wie _stdafx.cpp_ oder _stdafx.h_ begegnet, was normalerweise automatisch
  MFC aktivieren würde, wenn weder **--nomfc** noch **--mfc** angegeben wurden.
* **-D**_macro_[=_defn_]<br>
  Fügt diese Makrodefinition zur globalen Makroliste hinzu.
* **-I**_dir_<br>
  Hängt das angegebene Verzeichnis dem globalen Include-Pfad an.
* **-P**_dir_<br>
  Hängt das angegebene Verzeichnis dem globalen DLL-Pfad an.
* **-i**_dll_<br>
  Fügt die angegebene Winelib-Bibliothek zur globalen Liste der zu importierenden
  Winelib-Bibliotheken hinzu.
* **-L**_dir_<br>
  Hängt das angegebene Verzeichnis dem globalen Bibliotheks-Pfad an.
* **-l**_library_<br>
  Fügt die angegebene Bibliothek zur globalen Liste der zu verlinkenden
  Bibliotheken hinzu.
* **--nodlls**<br>
  Diese Option teilt **winemaker** mit, nicht den Standardsatz an Winelib-Bibliotheken
  zu importieren. Dies bedeutet, dass jede DLL, die Ihr Quellcode nutzt, explizit
  mit **-i** an **winemaker** übergeben werden muss.
  Die Standard-Bibliotheken sind: _odbc32.dll_, _odbccp32.dll_, _ole32.dll_,
  _oleaut32.dll_ und _winspool.drv_.
* **--nomsvcrt**<br>
  Setzt einige Optionen, die **winegcc** daran hindern, gegen msvcrt zu kompilieren.
  Nutzen Sie diese Option bei cpp-Dateien, die _\<string\>_ einbinden.
* **--interactive**<br>
  Versetzt **winemaker** in einen interaktiven Modus. In diesem Modus wird **winemaker**
  Sie für die Targetliste jedes Verzeichnisses nach Bestätigung und jeweils
  target- und verzeichnisspezifischen Optionen fragen.
* **--single-target** _name_<br>
  Gibt an, dass es nur ein einziges Target gibt, namens _name_.
* **--generated-files**<br>
  Weist **winemaker** an, eine _Makefile_ zu erzeugen. Diese Option ist Standard.
* **--nogenerated-files**<br>
  Weist **winemaker** an, keine _Makefile_ zu erzeugen.
* **--wine32**<br>
  Weist **winemaker** an, ein 32-Bit Target zu erstellen. Dies ist nützlich bei
  wow64-Systemen. Ohne diese Option wird die Standardarchitektur benutzt.


## BEISPIELE


Ein typischer **winemaker** Aufruf:

\$ winemaker --lower-uppercase -DSTRICT .

Damit scannt **winemaker** das aktuelle Verzeichnis und die Unterverzeichnisse nach
Quellcodedateien. Jede Datei und jedes Verzeichnis, das ganz in Großbuchstaben
geschrieben ist, wird in Kleinbuchstaben umbenannt. Danach werden alle Quellcodedateien
an die Kompilierung mit Winelib angepasst und _Makefile_s erzeugt. **-DSTRICT** gibt
an, dass das **STRICT**-Makro gesetzt sein muss, um diesen Quellcode zu kompilieren.
Letztendlich wird **winemaker** die globale _Makefile_ erzeugen.

Der nächste Schritt wäre dann:

\$ make

Wenn Sie an diesem Punkt Compilerfehler erhalten (was recht wahrscheinlich ist,
ab einer gewissen Projektgröße), sollten Sie den Winelib User Guide zu Rate
ziehen, um Problemlösungen und Tipps zu finden.

Bei einem MFC-basierten Projekt sollten Sie stattdessen folgenden Befehl ausführen:

\$ winemaker --lower-uppercase --mfc .<br>
\$ make

Mit einer existierenden Projektdatei lautet der passende Befehl:

\$ winemaker meinprojekt.dsp<br>
\$ make



## TODO / FEHLER


In einigen Fällen werden Sie die _Makefile_ oder den Quellcode von Hand
nachbearbeiten müssen.

Angenommen, die fertigen Windows-Bibliotheken oder Binärdateien sind vorhanden,
könnte mit **winedump** ermittelt werden, um welche Art von ausführbarer Datei es
sich handelt (grafisch oder Konsole), gegen welche Bibliotheken sie gelinkt
sind und welche Funktionen exportiert werden (bei Bibliotheken). All diese
Informationen könnten dann für das Winelib-Projekt verwendet werden.

Weiterhin ist **winemaker** nicht sehr gut darin, die Bibliothek zu finden, die
die Anwendung enthält: Sie muss entweder im aktuellen Verzeichnis oder im
_LD_LIBRARY_PATH_liegen.

**winemaker** unterstützt noch keine Messagedateien und deren Compiler.

Fehler können im
[**Wine Bugtracker**](https://bugs.winehq.org)
gemeldet werden.

## AUTOREN

François Gouget für CodeWeavers<br>
Dimitrie O. Paun<br>
André Hentschel

## VERFÜGBARKEIT

**winemaker**
ist Teil der Wine-Distribution, verfügbar im WineHQ, dem
[**Hauptquartier der Wine-Entwicklung**](https://www.winehq.org/).

## SIEHE AUCH

[**wine**](./wine),<br>
[**Wine-Dokumentation und Support**](https://www.winehq.org/help).
