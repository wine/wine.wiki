---
title: wine - Windows-Programme auf Unix-Systemen ausführen
---

<small>
&nbsp;[:flag_gb: English](Man-Pages/wine)
&nbsp;[:flag_fr: Français](fr/Man-Pages/wine)
&nbsp;[:flag_pl: Polski](pl/Man-Pages/wine)
</small>

-----


## ÜBERSICHT

**wine**
_Programm_ [_Argumente_]<br>
**wine --help**<br>
**wine --version**

Für das Übergeben von Kommandos an Windows-Programme siehe den
Abschnitt
**PROGRAMM/ARGUMENTE**
in dieser Manpage.

## BESCHREIBUNG

**wine**
lädt und führt ein Windows-Programm aus. Dieses Programm kann ein
beliebiges DOS/Win3.1/Win32-Programm sein; es wird nur die
x86-Architektur unterstützt.

Um wine zu debuggen, nutzen Sie anstelledessen das Kommando
**winedbg**.

Um rein kommandozeilenbasierte Programme auszuführen, nutzen Sie
**wineconsole**
anstelle von
**wine**.
Wenn Sie nicht
**wineconsole**
für CLI-Programme nutzen, kann dies dazu führen, dass das Programm
nicht korrekt ausgeführt werden kann.

Wenn wine nur mit
**--help**
oder
**--version**
als Argument aufgerufen wird, wird
**wine**
nur einen kleinen Hilfetext oder die Version ausgeben und sich beenden.

## PROGRAMM/ARGUMENTE

Der Programmname kann auf DOS-Art
(_C:\\\\WINDOWS\\\\SOL.EXE_)
oder auf UNIX-Art angegeben werden
(_/msdos/windows/sol.exe_).
Sie können Argumente an die Windows-Anwendung übergeben, indem Sie
sie einfach an den wine-Befehl anhängen (z. B.:
_wine notepad C:\\\\TEMP\\\\README.TXT_).
Sie müssen unter Umständen Sonderzeichen und/oder Leerzeichen
mit '\\' maskieren, wenn Sie wine über die Kommandokonsole aufrufen,
z.B.

wine C:\\\\Program\\ Files\\\\MyPrg\\\\test.exe


## UMGEBUNGSVARIABLEN

**wine**
leitet die Umgebungsvariablen der Shell, in der es gestartet wurde, an
die Windows-Applikation weiter. Um eine für Ihre Applikation nötige
Variable zugänglich zu machen, folgen Sie der Anleitung Ihrer Shell zu
Umgebungsvariablen.

* **WINEPREFIX**<br>
  Wenn gesetzt, wird dieser Ordner als Speicherort für die
  Konfigurationsdateien von
  **wine**
  genutzt. Die Standardeinstellung ist
  _\$HOME/.wine_.
  Dieser Ordner wird auch für den UNIX-Socket zur Kommunikation mit
  _wineserver_.
  genutzt. Alle
  **wine**
  -Prozesse, die den gleichen
  **wineserver**
  nutzen (z.B. Prozesse desselben Nutzers) teilen sich bestimmte Objekte
  wie die Registry, Arbeitsspeicher und die Konfigurationsdateien.  Mit
  dem Setzen von
  _WINEPREFIX_
  beim Starten verschiedener
  **wine**
  -Prozesse ist es möglich, eine Anzahl vollkommen unabhängiger
  **wine**
  -Prozesse zu starten.
* **WINESERVER**<br>
  Gibt den Ort der
  **wineserver**
  -Anwendung an. Wenn diese Variable nicht gesetzt ist, sucht
  **wine**
  in \$PATH und anderen Orten nach wineserver.
* **WINEDEBUG**<br>
  Wählt die Stufe der Debug-Meldungen aus. Die Variable hat das Format
  [_Klasse_][+/-]_Kanal_[,[_Klasse2_][+/-]_Kanal2_].

  _Klasse_
  ist optional und kann z.B. folgende Werte annehmen:
  **err**,
  **warn**,
  **fixme**,
  oder
  **trace**.
  Wenn
  _class_
  nicht angegeben ist, werden alle Debugmeldungen dieses Kanals
  ausgegeben. Jeder Kanal gibt Meldungen einer bestimmten
  **wine**.
  -Komponente aus. Das folgende Zeichen kann entweder + oder - sein, je
  nachdem ob der Kanal ein- oder ausgeschaltet werden soll.  Wenn keine
  _Klasse_
  angegeben ist, kann das führende + weggelassen werden. In WINEDEBUG
  sind keine Leerzeichen erlaubt.

  Beispiele:
    * WINEDEBUG=warn+all<br>
  zeigt alle Nachrichten der Kategorie "warning" an (empfohlen zum
  Debuggen).<br>
    * WINEDEBUG=warn+dll,+heap<br>
  schaltet alle DLL-Meldungen der Kategorie "warning" sowie jegliche
  Heap-Meldungen an.<br>
    * WINEDEBUG=fixme-all,warn+cursor,+relay<br>
  schaltet alle FIXME-Nachrichten ab, Zeigernachrichten der Kategorie
  "warning" an und schaltet alle Relay-Meldungen (API-Aufrufe) an.<br>
    * WINEDEBUG=relay<br>
  schaltet alle Relay-Nachrichten an. Für mehr Kontrolle über die im
  Relaytrace angezeigten DLLs und Funktionen siehe den [Debug]-Abschnitt
  der Wine-Konfigurationsdatei.

  Für mehr Informationen zu den Debug-Meldungen siehe den Abschnitt
  _Running Wine_
  im Wine-Benutzerhandbuch.

* **WINEDLLPATH**<br>
  Gibt den Pfad/die Pfade an, in denen wine nach eigenen DLLs und
  Winelib-Anwendungen sucht. Die Einträge der Liste werden mit einem ":"
  getrennt.
* **WINEDLLOVERRIDES**<br>
  Definiert die Bibliotheksüberschreibung und Ladereihenfolge der DLLs,
  die beim Laden jeder DLL berücksichtigt wird. Derzeit gibt es zwei Typen von
  DLLs, die in den Speicher eines Prozesses geladen werden können:
  Native Windows-DLLs
  (_native_),
  und
  **wine**
  -interne DLLs
  (_builtin_).
  Der Typ kann mit dem ersten Buchstaben abgekürzt werden
  (_n_, _b_).
  Jede Anweisungssequenz muss mit einem Komma abgeschlossen werden.

  Jede DLL kann ihre eigene Ladereihenfolge besitzen. Die
  Ladereihenfolge bestimmt, welche DLL-Version als erste geladen werden
  soll. Wenn die erste versagt, ist die nächste an der Reihe und so
  weiter. Viele DLLs mit derselben Reihenfolge können durch Kommata
  getrennt werden. Es ist auch möglich, mit dem Semikolon verschiedene
  Reihenfolgen für verschiedene DLLs festzulegen.

  Die Ladereihenfolge für eine 16bit-DLL wird immer durch die
  Reihenfolge der 32bit-DLL bestimmt, die sie enthält. Diese 32bit-DLL
  kann durch Ansehen des symbolischen Links der 16bit .dll.so-Datei
  gefunden werden. Wenn zum Beispiel ole32.dll als "builtin" eingestellt
  ist, wird storage.dll ebenfalls als "builtin" geladen, da die
  32bit-DLL ole32.dll die 16bit-DLL storage.dll enthält.

  Beispiele:
    * WINEDLLOVERRIDES="comdlg32,shell32=n,b"<br><br>
  Versuche, die DLLs comdlg32 und shell32 als native Windows-DLLs zu
  laden; wenn dies fehlschlägt, soll Wine die mitgebrachte Version
  benutzen.
    * WINEDLLOVERRIDES="comdlg32,shell32=n;c:\\\\foo\\\\bar\\\\baz=b"<br><br>
  Versuche, die DLLs comdlg32 und shell32 als native Windows-DLLs zu
  laden. Weiterhin, wenn eine Anwendung versucht, die DLL
  c:\\foo\\bar\\baz.dll zu laden, soll wine die eingebaute DLL baz
  verwenden.
    * WINEDLLOVERRIDES="comdlg32=b,n;shell32=b;comctl32=n"<br><br>
  Versuche, die mitgebrachte comdlg32-Bibliothek zu laden; wenn dies
  fehlschlägt soll Wine die native comdlg32-DLL nutzen. Bei shell32 soll
  immer die mitgebrachte Version verwendet werden; bei comctl32 immer
  die native.

* **DISPLAY**<br>
  Gibt das zu nutzende X11-Display an.
* OSS sound driver configuration variables<br>
* **AUDIODEV**<br>
  Gerät für Audio-Ein-/Ausgabe festlegen. Standard:
  **/dev/dsp**.
* **MIXERDEV**<br>
  Audiomixer-Gerät festlegen. Standard:
  **/dev/mixer**.
* **MIDIDEV**<br>
  MIDI-Sequencergerät festlegen. Standard:
  **/dev/sequencer**.

## DATEIEN


* _wine_<br>
  Der
  **wine**
  -Programmstarter
* _wineconsole_<br>
  Der
  **wine**
  -Programmstarter für Konsolenapplikationen (CLI)
* _wineserver_<br>
  Der
  **wine**
  -Server
* _winedbg_<br>
  Der
  **wine**
  -Debugger
* _\$WINEPREFIX/dosdevices_<br>
  Dieser Ordner enthält die DOS-Gerätezuweisungen. Jede Datei in diesem
  Ordner ist ein Symlink auf die Unix-Gerätedatei, die dieses Gerät
  bereitstellt.  Wenn zum Beispiel COM1 /dev/ttyS0 repräsentieren soll,
  wird der Symlink \$WINEPREFIX/dosdevices/com1 -\> /dev/ttyS0 benötigt.<br>
  DOS-Laufwerke werden auch mit Symlinks angegeben. Wenn z.B. das
  Laufwerk D: dem CD-ROM-Laufwerk entsprechen soll, das auf /mnt/cdrom
  eingebunden ist, wird der Link \$WINEPREFIX/dosdevices/d: -\> /mnt/cdrom
  benötigt. Es kann auch die Unix-Gerätedatei angegeben werden; der
  einzige Unterschied ist der "::" anstelle dem einfachen ":" im Namen:
  \$WINEPREFIX/dosdevices/d:: -\> /dev/hdc.

## AUTOREN

**wine**
ist dank der Arbeit vieler Entwickler verfügbar. Für eine Liste siehe
die Datei
**AUTHORS**
im obersten Ordner der Quellcodedistribution.

## COPYRIGHT

**wine**
kann unter den Bedingungen der LGPL genutzt werden; für eine Kopie der
Lizenz siehe die Datei
**COPYING.LIB**
im obersten Ordner der Quellcodedistribution.

## FEHLER


Statusberichte für viele Anwendungen sind unter
_https://appdb.winehq.org_
 verfügbar. Bitte fügen Sie Anwendungen, die Sie mit Wine nutzen, der
 Liste hinzu, sofern noch kein Eintrag existiert.

Fehler können unter
_https://bugs.winehq.org_
gemeldet werden.

## VERFÜGBARKEIT

Die aktuellste öffentliche Wine-Version kann auf WineHQ,
[die Hauptseite der wine-Entwicklung](https://www.winehq.org/),
bezogen werden.

## SIEHE AUCH

[**wineserver**](./wineserver),&nbsp;[**winedbg**](./winedbg)
