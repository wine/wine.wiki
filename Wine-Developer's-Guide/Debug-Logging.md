To better manage the large volume of debugging
messages that Wine can generate, we divide the messages on a component
basis, and classify them based on the severity of the reported problem.
Therefore a message belongs to a *channel* and a *class* respectively.

This section will describe the debugging classes, how you can create a
new debugging channel, what the debugging API is, and how you can
control the debugging output. A picture is worth a thousand words, so
here are a few examples of the debugging API in action:

``` c
ERR("lock_count == 0 ... please report\n");
FIXME("Unsupported RTL style!\n");
WARN(": file seems to be truncated!\n");
TRACE("[%p]: new horz extent = %d\n", hwnd, extent );
MESSAGE( "Could not create graphics driver '%s'\n", buffer );
```

## Debugging classes

A debugging class categorizes a message based on the severity of the
reported problem. There is a fixed set of classes, and you must
carefully choose the appropriate one for your messages. There are five
classes of messages:

`FIXME`
Messages in this class are meant to signal unimplemented features, known
bugs, etc. They serve as a constant and active reminder of what needs to
be done.

`ERR`
Messages in this class indicate serious errors in Wine, such as as
conditions that should never happen by design.

`WARN`
These are warning messages. You should report a warning when something
unwanted happens, and the function cannot deal with the condition. This
is seldom used since proper functions can usually report failures back
to the caller. Think twice before making the message a warning.

`TRACE`
These are detailed debugging messages that are mainly useful to debug a
component. These are turned off unless explicitly enabled.

`MESSAGE`
There messages are intended for the end user. They do not belong to any
channel. As with warnings, you will seldom need to output such messages.

## Debugging channels

Each component is assigned a debugging channel. The identifier of the
channel must be a valid C identifier (reserved words like `int` or
`static` are permitted). To use a new channel, simply use it in your
code. It will be picked up automatically by the build process.

Typically, a file contains code pertaining to only one component, and as
such, there is only one channel to output to. You can declare a default
channel for the file using the WINE_DEFAULT_DEBUG_CHANNEL() macro:

``` c
#include "wine/debug.h"

WINE_DEFAULT_DEBUG_CHANNEL(xxx);
...

    FIXME("some unimplemented feature", ...);
...
    if (zero != 0)
        ERR("This should never be non-null: %d", zero);
...
```

In rare situations there is a need to output to more than one debug
channel per file. In such cases, you need to declare all the additional
channels at the top of the file, and use the _-version of the debugging
macros:

``` c
#include "wine/debug.h"

WINE_DEFAULT_DEBUG_CHANNEL(xxx);
WINE_DECLARE_DEBUG_CHANNEL(yyy);
WINE_DECLARE_DEBUG_CHANNEL(zzz);
...

    FIXME("this one goes to xxx channel");
...
    FIXME_(yyy)("Some other msg for the yyy channel");
...
    WARN_(zzz)("And yet another msg on another channel!");
...
```

## Are we debugging?

To test whether the debugging channel `xxx` is enabled, use the
TRACE_ON, WARN_ON, FIXME_ON, or ERR_ON macros. For example:

``` c
if (TRACE_ON(atom))
{
    ...blah...
}
```

You should normally need to test only if `TRACE_ON`, all the others are
very seldom used. With careful coding, you can avoid the use of these
macros, which is generally desired.

## Helper functions

Resource identifiers can be either strings or numbers. To make life a
bit easier for outputting these beasts (and to help you avoid the need
to build the message in memory), I introduced a new function called
`debugres`.

The function is defined in `wine/debug.h` and has the following
prototype:

``` c
LPSTR debugres(const void *id);
```

It takes a pointer to the resource id and returns a nicely formatted
string of the identifier (which can be a string or a number, depending
on the value of the high word). Numbers are formatted as such:

    #xxxx

while strings as:

    'some-string'

Simply use it in your code like this:

``` c
#include "wine/debug.h"

...

TRACE("resource is %s", debugres(myresource));
```

Many times strings need to be massaged before output: they may be
`NULL`, contain control characters, or they may be too long. Similarly,
Unicode strings need to be converted to ASCII for usage with the
debugging API. For all this, you can use the `debugstr_[aw]n?` family of
functions:

``` c
HANDLE32 WINAPI YourFunc(LPCSTR s)
{
    FIXME("(%s): stub\n", debugstr_a(s));
}
```

## Controlling the debugging output

It is possible to turn on and off debugging output from within the
debugger using the set command. Please see the [WineDbg Command
Reference
section](Wine-Developer's-Guide/Debugging-Wine#debug-channels).

You can do the same using the task manager (**taskmgr**) and
selecting your application in the **Processes** list. Right clicking on
the process, and selecting the **Edit Debug Channels** option in the
popup menu, will let you select the modifications you want on the debug
channels.

Another way to conditionally log debug output (e.g. in case of very
large installers which may create gigabytes of log output) is to create
a pipe:

```sh
$ mknod /tmp/debug_pipe p
```

and then to run wine like that:

```sh
$ WINEDEBUG=+relay,+snoop wine setup.exe &>/tmp/debug_pipe
```

Since the pipe is initially blocking (and thus wine as a whole), you
have to activate it by doing:

```sh
$ cat /tmp/debug_pipe
```

(press Ctrl-C to stop pasting the pipe content)

Once you are about to approach the problematic part of the program, you
just do:

```sh
$ cat /tmp/debug_pipe >/tmp/wine.log
```

to capture specifically the part that interests you from the pipe
without wasting excessive amounts of HDD space and slowing down
installation considerably.

The `WINEDEBUG` environment variable controls the output of the debug
messages. It has the following syntax:
`WINEDEBUG=[yyy]#xxx[,[yyy1]#xxx1]*`

- where \# is either `+` or `-`
- when the optional class argument (*yyy*) is not present, then the
  statement will enable(`+`)/disable(`-`) all messages for the given
  channel (xxx) on all classes. For example:
      WINEDEBUG=+reg,-file

  enables all messages on the `reg` channel and disables all messages on
  the `file` channel.
- when the optional class argument (`yyy`) is present, then the
  statement will enable (`+`)/disable(`-`) messages for the given
  channel (`xxx`) only on the given class. For example:
      WINEDEBUG=trace+reg,warn-file

  enables trace messages on the `reg` channel and disables warning
  messages on the `file` channel.
- also, the pseudo-channel all is also supported and it has the
  intuitive semantics:

    WINEDEBUG=+all      -- enables all debug messages
    WINEDEBUG=-all      -- disables all debug messages
    WINEDEBUG=yyy+all   -- enables debug messages for class yyy on all channels
    WINEDEBUG=yyy-all   -- disables debug messages for class yyy on all channels

  So, for example:

      WINEDEBUG=warn-all  -- disables all warning messages

Also, note that at the moment:

- the `FIXME` and `ERR` classes are enabled by default
- the `TRACE` and `WARN` classes are disabled by default

## A Few Notes on Style

This new scheme makes certain things more consistent but there is still
room for improvement by using a common style of debug messages. Before I
continue, let me note that the output format is the following:

*`yyy:xxx:fff message`*

where:

*`yyy`* = the class (`fixme`, `err`, `warn`, `trace`)

`xxx` = the channel (`atom`, `win`, `font`, etc)

`fff` = the function name

These fields are output automatically. All you have to provide is the
*message* part.

So here are some ideas:

- do not include the name of the function: it is included automatically
- if you want to output the parameters of the function, do it as the
  first thing and include them in parentheses, like this:
  ``` c
  TRACE("(%d, %p, ...)\n", par1, par2, ...);
  ```
- if you want to name a parameter, use `=` :
  ``` c
  TRACE("(fd=%d, file=%s): stub\n", fd, name);
  ```
- for stubs, you should output a `FIXME` message. I suggest this style:
  ``` c
  FIXME("(%x, %d, ...): stub\n", par1, par2, ...);
  ```
- try to output one line per message. That is, the format string should
  contain only one '\n' and it should always appear at the end of the
  string.
- if the output string needs to be dynamically constructed, render it in
  memory before outputting it:
  ``` c
  char buffer[128] = "";

  if (flags & FLAG_A) strcat(buffer, "FLAG_A ");
  if (flags & FLAG_B) strcat(buffer, "FLAG_B ");
  if (flags & FLAG_C) strcat(buffer, "FLAG_C ");
  TRACE("flags = %s\n", buffer);
  ```

  Most of the time however, it is better to create a helper function
  that renders to a temporary buffer:

  ``` c
  static const char *dbgstr_flags(int flags)
  {
      char buffer[128] = "";

      if (flags & FLAG_A) strcat(buffer, "FLAG_A ");
      if (flags & FLAG_B) strcat(buffer, "FLAG_B ");
      if (flags & FLAG_C) strcat(buffer, "FLAG_C ");
      return wine_dbg_sprintf("flags = %s\n\n", buffer);
  }

  ...

  TRACE("flags = %s\n", dbgstr_flags(flags));
  ```

