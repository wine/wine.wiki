![WineConf logo](media/Wineconf-logo-text.png)

The Wine Conference (**WineConf**) is held regularly around the world.

We discuss all things wineconf on the [wineconf mailing
list](https://list.winehq.org/mailman3/hyperkitty/list/wineconf@winehq.org/).
Please feel free to follow along or join in on the conversation.

Potential hosts can find a checklist and guide for hosting
[here](WineConf/Organising-WineConf).

## Past Conferences

- [WineConf 2022](WineConf/WineConf-2022)
- [WineConf 2019](WineConf/WineConf-2019)
- [WineConf 2018](WineConf/WineConf-2018)
- [WineConf 2017](WineConf/WineConf-2017)
- [WineConf 2016](WineConf/WineConf-2016)
- [WineConf 2015](WineConf/WineConf-2015)
- [FOSDEM 2014](WineConf/FOSDEM-2014)
- [FOSDEM 2013](WineConf/FOSDEM-2013)
- [WineConf 2011](WineConf/WineConf-2002-2011#wineconf-2011)
- [WineConf 2010](WineConf/WineConf-2002-2011#wineconf-2010)
- [WineConf 2009](WineConf/WineConf-2002-2011#wineconf-2009)
- [WineConf 2008](WineConf/WineConf-2002-2011#wineconf-2008)
- [WineConf 2007](WineConf/WineConf-2002-2011#wineconf-2007)
- [WineConf 2006](WineConf/WineConf-2002-2011#wineconf-2006)
- [WineConf 2005](WineConf/WineConf-2002-2011#wineconf-2005)
- [WineConf 2004](WineConf/WineConf-2002-2011#wineconf-2004)
- [WineConf 2002](WineConf/WineConf-2002-2011#wineconf-2002)
