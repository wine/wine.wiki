---
title: Wsparcie na Żywo
---

<small>
&nbsp;[:flag_gb: English](IRC)
&nbsp;[:flag_fr: Français](fr/IRC)
&nbsp;[:flag_de: Deutsch](de/IRC)
&nbsp;[:flag_es: Español](es/IRC)
&nbsp;[:flag_pt: Português](pt/IRC)
&nbsp;[:flag_nl: Nederlands](nl/IRC)
&nbsp;[:flag_tr: Türkçe](tr/IRC)
&nbsp;[:flag_kr: 한국어](ko/IRC)
&nbsp;[:flag_cn: 简体中文](zh_CN/IRC)
</small>

-----


[Libera.​Chat](https://libera.chat/) hostuje kanał IRC dla Wine. Możesz
wejść do pokoju rozmów używając programu IRC takiego jak np.
[HexChat](https://hexchat.github.io/). Użyj ustawień wymienionych
poniżej.

> **Serwer:** irc.libera.chat\
> **Port:** 6697\
> **Kanał:** #winehq

Jeżeli używasz Firefox lub innej przeglądarki która wspiera
**u**e**r**e**l**e IRC, to możesz dołączyć do rozmowy klikając na
[#winehq](irc://irc.libera.chat/winehq).
