---
title: Dokumentacja
---

<small>
&nbsp;[:flag_gb: English](Documentation)
&nbsp;[:flag_fr: Français](fr/Documentation)
&nbsp;[:flag_de: Deutsch](de/Documentation)
&nbsp;[:flag_nl: Nederlands](nl/Documentation)
&nbsp;[:flag_tr: Türkçe](tr/Documentation)
&nbsp;[:flag_kr: 한국어](ko/Documentation)
</small>

-----

## Przewodniki

- **[Przewodnik użytkownika Wine](Wine-User's-Guide)**

  Jak skonfigurować i używać Wine do uruchamiania aplikacji Windowsowych.

- **[Przewodnik użytkownika Winelib](Winelib-User's-Guide)**

  Jak używać Wine do portowania aplikacji Windowsowych na Linuksa.

- **[Przewodnik programisty Wine](Wine-Developer's-Guide)**

  Jak hakować Wine.

- **[Wine Installation and Configuration](Wine-Installation-and-Configuration)**

  Krótki przewodnik dla tych, którzy chcą nam pomóc w umożliwianiu uruchamiania aplikacji.

## Command reference

- **[Commands](Commands)**

  Documentation about the commands available with Wine.

- **[Man Pages](pl/Man-Pages)**

  Manual pages for the Wine commands and tools.
