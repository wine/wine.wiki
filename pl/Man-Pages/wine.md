---
title: wine - uruchamiaj programy Windowsowe na Uniksie
---

<small>
&nbsp;[:flag_gb: English](Man-Pages/wine)
&nbsp;[:flag_fr: Français](fr/Man-Pages/wine)
&nbsp;[:flag_de: Deutsch](de/Man-Pages/wine)
</small>

-----


## SKŁADNIA

**wine**
_program_ [_opcje_]<br>
**wine --help**<br>
**wine --version**

Informacji na temat przekazywania opcji programom Windowsowym szukaj w rozdziale
**OPCJE**
tej instrukcji.

## OPIS

**wine**
ładuje i wykonuje dany program, gdzie program to plik wykonywalny DOSa, Windowsa
3.x lub Win32 (tylko binarne x86).

Do debugowania wine lepiej jednak użyć
**winedbg**

Do uruchamiania plików wykonywalnych CUI (programy konsolowe Windowsa) używaj
**wineconsole**
zamiast
**wine**.
Spowoduje to, że całe wyjście zostanie pokazane w osobnym oknie (wymaganiem jest uruchomiony X11).
Przez nie użycie
**wineconsole**
do programów CUI otrzymasz ograniczone wsparcie dla konsoli
,a twój program może nie zadziałać prawidłowo.

Gdy wywołasz wine z
**--help**
lub
**--version**
jako jedyną opcją,
**wine**
pokaże małą informację pomocy lub odpowiednio wyświetli swoją wersję i zakończy działanie.

## OPCJE

Nazwa programu może być określona w formacie DOS
(_C:\\\\WINDOWS\\\\SOL.EXE_)
lub w formacie Unix
(_/msdos/windows/sol.exe_).
Możesz dodać opcje do wykonywanego programu przez dodanie ich do
końca wywołania linii wiersza poleceń
**wine**
(tak jak np.: wine notepad C:\\\\TEMP\\\\README.TXT).
Zauważ, że musisz '\\' poradzić sobie ze znakami specjalnymi (i spacjami) podczas wywoływania Wine przez
powłokę, np.

wine C:\\\\Program\\ Files\\\\MyPrg\\\\test.exe


## ZMIENNE ŚRODOWISKOWE

**wine**
udostępnia zmienne środowiskowe z powłoki, z której wystartował
**wine**
programom windowsowym/dosowym, więc używaj odpowiedniej składni
twojej powłoki, aby wpisać zmienne środowiskowe, których potrzebujesz.

* **WINEPREFIX**<br>
  Jeżeli ta zmienna jest ustawiona, to jej zawartość jest brana jako nazwa katalogu gdzie
  **wine**
  przechowuje swoje dane (domyślny katalog to
  _\$HOME/.wine_).
  Katalog ten jest także wykorzystywany do identyfikacji gniazda używanego do
  porozumiewania się z
  _wineserver_.
  Wszystkie procesy
  **wine**
  używające tego samego
  **wineserver**
  (np.: ten sam użytkownik) dzielą pewne elementy takie jak rejrestr, współdzielona pamięć,
  i plik konfiguracyjny.
  Poprzez ustawianie
  _WINEPREFIX_
  na inne wartości dla różnych procesów
  **wine**
  jest możliwe uruchamianie kilka prawdziwie niezależnych procesów
  **wine**.
* **WINESERVER**<br>
  Określa ścieżkę i nazwę programu binarnego
  **wineserver**
  Jeżeli nie ustawione, Wine będzie szukał pliku o nazwie "wineserver" w
  podanej ścieżce i kilku innych miejscach prawdopodobnego występowania.
* **WINEDEBUG**<br>
  Włącza lub wyłącza wiadomości debuggera. Składnia zmiennej
  wygląda następująco
  [_klasa_][+/-]_kanał_[,[_klasa2_][+/-]_kanał2_].

  _klasa_
  jest opcjonalna i może być jedną z następujących:
  **err**,
  **warn**,
  **fixme**,
  lub
  **trace**.
  Jeżeli
  _klasa_
  nie jest określona, to wszystkie wiadomości debuggera dla określonego
  kanału są wyłączone.  Każdy kanał będzie wyświetlał wiadomości o poszczególnym
  komponencie
  **wine**.
  Następny znak może być albo + albo - i służy odpowiednio do włączenia albo wyłączenia
  określonego kanału.  Jeżeli nie ma części
  _klasa_
  przed nim, to znak znaczący + może być pominięty. Zauważ, że spacje są niedozwolone
  w żadnym miejscu łańcucha znaków.

  Przykłady:
    * WINEDEBUG=warn+all<br>
  włączy wszystkie ostrzeżenia (zalecane przy debugowaniu).<br>
    * WINEDEBUG=warn+dll,+heap<br>
  włączy wszystkie ostrzeżenia bibliotek DLL i wszystkie wiadomości stosu.<br>
    * WINEDEBUG=fixme-all,warn+cursor,+relay<br>
  wyłączy wszystkie wiadomości FIXME, włączy ostrzeżenia kursora i
  wszystkie wiadomości relay (wywołania API).<br>
    * WINEDEBUG=relay<br>
  włączy wszystkie wiadomości relay. Aby mieć większą kontrolę przy uwzględnianiu i wykluczaniu
  funkcji i bibliotek dll ze śladu relay, zapoznaj się z kluczem rejestru
  **HKEY_CURRENT_USER\\\\Software\\\\Wine\\\\Debug**

  Informacji na temat wiadomości debugera szukaj w rozdziale
  _Running Wine_
  z Przewodnika użytkownika Wine.

* **WINEDLLPATH**<br>
  Określa ścieżkę/ki, w których należy szukać wbudowanych bibliotek dll i programów
  Winelib. To lista katalogów oddzielonych znakiem ":". W dodatku do
  każdego katalogu określonego w
  _WINEDLLPATH_,
  Wine będzie także szukał w katalogu instalacyjnym.
* **WINEDLLOVERRIDES**<br>
  Definiuje typ nadpisania i kolejność ładowania bibliotek dll użytych do ładowania
  procesu dla jakiejkolwiek biblioteki dll. Obecnie istnieją dwa typy bibliotek, które można załadować
  do przestrzeni adresowej procesu: natywne biblioteki Windowsa
  (_native_),
  **wine**
  wewnętrzne biblioteki dll
  (_builtin_).
  Typ może być skrócony przez pierwszą literkę typu
  (_n_, _b_).
  Biblioteka może być także całkowicie wyłączona (''). Każda sekwencja rozkazów musi być oddzielona przecinkami.

  Każda biblioteka dll może mieć swoją własną kolejność ładowania. Kolejność ładowania
  określa, którą wersję biblioteki dll będzie się próbowało załadować do
  przestrzeni adresowej. Jeżeli pierwsza zawiedzie, to próbowana jest następna i tak dalej.
  Wiele bibliotek z tą samą kolejnością ładowania może być oddzielona przecinkami.
  Istnieje także możliwość określenia różnych kolejności ładowania dla różnych bibliotek
  przez oddzielanie wpisów znakiem ";".

  Kolejność ładowania dla 16-bitowej biblioteki dll jest zawsze określona przez kolejność ładowania
  32-bitowej biblioteki dll, która ją zawiera (co może być rozpoznane przez podgląd
  symbolicznych dowiązań 16-bitowego pliku .dll.so). Dla przykładu jeżeli biblioteka
  ole32.dll jest skonfigurowana jako wbudowana, to biblioteka storage.dll będzie również zładowana jako
  wbudowana, ponieważ 32-bitowa biblioteka ole32.dll zawiera 16-bitową bibliotekę
  storage.dll.

  Przykłady:
    * WINEDLLOVERRIDES="comdlg32,shell32=n,b"<br><br>
  Spróbuj załadować comdlg32 i shell32 jako natywne biblioteki windowsowe i powróć
  do wersji wbudowanych jeżeli natywne zawiodą.
    * WINEDLLOVERRIDES="comdlg32,shell32=n;c:\\\\foo\\\\bar\\\\baz=b"<br><br>
  Spróbuj załadować comdlg32 i shell32 jako natywne biblioteki windowsowe. Dodatkowo, jeżeli
  program zażąda załadowania c:\\foo\\bar\\baz.dll to załaduj wbudowaną bibliotekę rsbaz.
    * WINEDLLOVERRIDES="comdlg32=b,n;shell32=b;comctl32=n;oleaut32="<br><br>
  Najpierw spróbuj załadować comdlg32 jako wbudowaną i skorzystaj z wersji natywnej jeżeli
  wbudowane zawiodą; zawsze ładuj shell32 jako wbudowaną i comctl32
  jako natywną. Oleaut32 pozostaw wyłączone.

* **WINEARCH**<br>
  Określa jaką architekturę Windowsa wspierać. Może to być zarówno
  **win32**
  (wsparcie tylko 32-bitowych programów), lub
  **win64**
  (wsparcie dla programów 64-bitowych jak i 32-bitowych w trybie WoW64).<br>
  Architektura wspierana przez dany prefiks Wine jest ustawiana już w momencie tworzenia prefiksa
  i nie może być później zmieniona. Gdy opcja zostanie uruchomiona z istniejącym
  prefiksem, Wine odmówi uruchomienie jeżeli
  _WINEARCH_
  nie zgadza się z architekturą prefiksu.
* **DISPLAY**<br>
  Określa, którego wyświetlacza X11 użyć.
* Zmienne konfiguracyjne sterownika dźwięku OSS<br>
* **AUDIODEV**<br>
  Ustaw urządzenie dla wejścia / wyjścia dźwięku. Domyślnie
  **/dev/dsp**.
* **MIXERDEV**<br>
  Ustaw urządzenie dla suwaków miksera. Domyślnie
  **/dev/mixer**.
* **MIDIDEV**<br>
  Ustaw urządzanie MIDI (sekwencer). Domyślnie
  **/dev/sequencer**.

## FILES


* _wine_<br>
  Ładowarka programów
  **wine**
* _wineconsole_<br>
  Ładowarka programów
  **wine**
  dla aplikacji CUI (konsolowych).
* _wineserver_<br>
  Serwer
  **wine**
* _winedbg_<br>
  Debugger
  **wine**
* _\$WINEPREFIX/dosdevices_<br>
  Katalog zawierający mapowania urządzeń DOS. Każdy plik w tym
  katalogu jest dowiązaniem symbolicznym do pliku urządzenia Uniksowego implementującego
  dane urządzenie. Dla przykładu, jeżeli COM1 byłoby zmapowane do /dev/ttyS0 to miałbyś
  symboliczene dowiązanie w formie \$WINEPREFIX/dosdevices/com1 -\> /dev/ttyS0.<br>
  Napędy DOS również są określone przez dowiązania symboliczne; Dla przykładu jeżeli napęd D:
  odpowiadałby napędowi CDROM zamontowanemu w /mnt/cdrom, miałbyś dowiązanie symboliczne
  \$WINEPREFIX/dosdevices/d: -\> /mnt/cdrom. Urządzenia Uniksowe odpowiadające
  napędom DOS mogą być określone w ten sam sposób, z użyciem '::' zamiast ':'.
  Tak więc dla poprzedniego przykładu, jeżeli urządzenie CDROM byłoby zamontowane
  z /dev/hdc, to odpowiadające dowiązanie symboliczne wyglądałoby następująco
  \$WINEPREFIX/dosdevices/d:: -\> /dev/hdc.

## AUTORZY

**wine**
jest dostępne dzięki pracy wielu programistów. Lista autorów
jest dostępna w pliku
**AUTOHORS**
w głównym katalogu dystrybucyjnym źródła.

## PRAWA AUTORSKIE

**wine**
może być rozpowszechniane pod warunkami licencji LGPL. Kopia
licencji jest dostępna w pliku
**COPYING.LIB**
w głównym katalogu dystrybucyjnym źródła.

## BŁĘDY


Raporty stanu działania programów są dostępne na stronie
_https://appdb.winehq.org_.
Jeżeli brakuje na liście aplikacji, której używasz, to nie wahaj się
dodać jej samodzielnie.

Raporty błędów mogą być wysyłane do Wine Bugzilla
_https://bugs.winehq.org_

## DOSTĘPNOŚĆ

Najaktualniejszą publiczną wersję
**wine**
można pobrać ze strony
_https://www.winehq.org_

## ZOBACZ TAKŻE

[**wineserver**](./wineserver),
[**winedbg**](./winedbg)
