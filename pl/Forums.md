---
title: Kluczowe listy mailingowe / fora
---

<small>
&nbsp;[:flag_gb: English](Forums)
&nbsp;[:flag_fr: Français](fr/Forums)
&nbsp;[:flag_pt: Português](pt/Forums)
&nbsp;[:flag_nl: Nederlands](nl/Forums)
&nbsp;[:flag_tr: Türkçe](tr/Forums)
&nbsp;[:flag_kr: 한국어](ko/Forums)
&nbsp;[:flag_cn: 简体中文](zh_CN/Forums)
</small>

-----


## Fora

Wine posiada wiele list mailingowych i for. Tutaj wymieniono najbardziej
użyteczne dla użytkowników:

- The [Fora WineHQ](https://forum.winehq.org) - dla tych z was, którzy
  wolą interfejs forum od listy mailingowej. Lista mailingowa i forum
  są ze sobą połączone. Wiadomości wysyłane do jednego automatycznie
  są przesyłane do drugiego.
- Użytkownicy Ubuntu mogą takżę odwiedzić [Dział Wine na forum
  Ubuntu](https://ubuntuforums.org/forumdisplay.php?f=313)

Jeżeli znasz jakiekolwiek inne aktywne forum Wine - szczególnie takie
nie-angielskie - daj nam znać, tak abyśmy mogli je dodać do listy.

:information_source: Jeżeli chcesz być powiadamiany o nowych
wydaniach, zapisz się do [Zawiadamiająca lista mailingowa
Wine](https://list.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/).

## Wszystkie listy mailingowe WineHQ

WineHQ dysponuje listami mailingowymi do przesyłania łatek, śledzenia
zatwierdzeń git i dyskutowania nt. Wine.

:information_source: Powinieneś być zapisany do listy zanim napiszesz
do niej, inaczej twój post zostanie potraktowany jako potencjalny spam
przez oprogramowanie listy mailingowej.

- **<wine-announce@winehq.org>**\
    \[[(Wy-/Za-)pisz](https://list.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/)\]
    \[[Archiwum](https://list.winehq.org/mailman3/hyperkitty/list/wine-announce@winehq.org/)\]\
    Niski ruch (2/miesięcznie) lista tylko-do-odczytu do ogłaszania
    wydań i innych istotnych aktualności o Wine lub WineHQ.
- **<wine-devel@winehq.org>**\
    \[[(Wy-/Za-)pisz](https://list.winehq.org/mailman3/postorius/lists/wine-devel.winehq.org/)\]
    \[[Archiwum](https://list.winehq.org/mailman3/hyperkitty/list/wine-devel@winehq.org/)\]\
    Średni ruch (50/dziennie) otwarta lista dla dyskusji nt. tworzenia
    Wine, WineHQ, łatek lub czegokolwiek innego leżącego w zakresie
    interesu programistów Wine.
- **<wine-commits@winehq.org>**\
    \[[(Wy-/Za-)pisz](https://list.winehq.org/mailman3/postorius/lists/wine-commits.winehq.org/)\]
    \[[Archiwum](https://list.winehq.org/mailman3/hyperkitty/list/wine-commits@winehq.org/)\]\
    Średni ruch (25/dziennie) lista tylko-do-odczytu, która śledzi
    zatwierdzenia do drzewa Git.
- **<wine-releases@winehq.org>**\
    \[[(Wy-/Za-)pisz](https://list.winehq.org/mailman3/postorius/lists/wine-releases.winehq.org/)\]
    \[[Archiwum](https://list.winehq.org/mailman3/hyperkitty/list/wine-releases@winehq.org/)\]\
    Niski ruch (2/miesięcznie) lista tylko-do-odczytu do otrzymywania
    dużych plików diff przy każdym oficjalnym wydaniu Wine.
- **<wine-bugs@winehq.org>**\
    \[[(Wy-/Za-)pisz](https://list.winehq.org/mailman3/postorius/lists/wine-bugs.winehq.org/)\]
    \[[Archiwum](https://list.winehq.org/mailman3/hyperkitty/list/wine-bugs@winehq.org/)\]\
    Wysoki ruch (100/dziennie) lista tylko-do-odczytu do monitorowania
    aktywności [bazy danych śledzącej błędy](https://bugs.winehq.org/).
