[Outreachy](https://wiki.gnome.org/Outreachy) is run by the Software
Freedom Conservancy, and sponsored by several Free and Open Source
(FOSS) projects and FOSS friendly companies, to encourage women and
people traditionally underrepresented in tech to participate in FOSS.
The project is similar to [Google Summer of Code](Summer-Of-Code),
however, an important difference between the two is that Outreachy
encourages new participants to FOSS to get involved. Repeat
internships are not allowed, and previous GSOC participants are not
eligible.

## Coordinator:

Austin English
IRC Nick: austin987
Contact: austinenglish -at - gmail dot com

## Potential Mentors:

Stefan Dösinger:
IRC Nick: stefand
Contact: stefandoesinger -at- gmx dot at

Aric Stewart
IRC Nick: aricstewart
Contact: aric -at- codeweavers dot com

Michael Stefaniuc
IRC Nick: puk
Contact: mstefani -at- winehq dot org

## Applying:

Please see
[here](https://wiki.gnome.org/Outreachy#Submit_an_Application) for
application info. Interested people should introduce themselves by
sending an email to wine-devel@winehq.org, with information about
themselves and their project of interest. Introducing yourself and
idling on \#winehackers on Libera.chat is also highly encouraged. For
help with using IRC, see [here](https://wiki.gnome.org/Outreachy/IRC)
As with Google Summer of Code, applicants must have successfully a
patch into wine to be considered. A patch in the area of code that
your project would be in are best. For simple starter bugs, consider
sending a stub for an unimplemented function that currently breaks an
application. These are useful, but easy enough for a beginner to
accomplish. Example bugs can be found
[here](https://bugs.winehq.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&component=-unknown&component=advapi32&component=advpack&component=amstream&component=api-ms-win-*&component=apphelp&component=atl&component=bcrypt&component=build-env&component=cabinet&component=clusapi&component=cmd&component=comctl32&component=comdlg32&component=crypt32&component=cryptui&component=d2d&component=dbghelp&component=devenum&component=difxapi&component=dnsapi&component=documentation&component=dos&component=dwmapi&component=dwrite&component=dxva2&component=fltmgr&component=fusion&component=gameux&component=gdi32&component=gdiplus&component=glu32&component=hal&component=hhctrl.ocx&component=hid&component=hnetcfg&component=httpapi&component=iccvid&component=ieframe&component=imagehlp&component=imm32&component=inetcpl.cpl&component=inkobj&component=inseng&component=iphlpapi&component=joy.cpl&component=jscript&component=kernel32&component=ktmw32&component=loader&component=loadperf&component=mfplat&component=mlang&component=mmdevapi&component=mountmgr.sys&component=msacm32&component=msadp32&component=msasn1&component=mscms&component=msctf&component=mshtml&component=msi&component=mspatcha&component=mssign32&component=mstask&component=msvfw32&component=msvidc32&component=msxml3&component=msxml4&component=ndis.sys&component=netapi32&component=netcfgx&component=netprofm&component=npptools&component=ntdll&component=ntdsapi&component=ntoskrnl&component=nvcuda&component=odbc&component=opengl&component=pdh&component=photometadatahandler&component=pnpmgr&component=programs&component=propsys&component=psapi&component=qmgr&component=quartz&component=rasapi32&component=registry&component=richedit&component=rpc&component=rsaenh&component=samlib&component=scrrun&component=secur32&component=setupapi&component=shdocvw&component=shell32&component=shlwapi&component=slc&component=spooler&component=svchost&component=sxs&component=t2embed&component=tapi32&component=taskschd&component=testcases&component=tools&component=url&component=urlmon&component=user16&component=user32&component=userenv&component=usp10&component=uxtheme&component=vbscript&component=vcomp&component=version&component=vssapi&component=wer&component=wevtapi&component=wia&component=wimgapi&component=windowscodecs&component=winealsa.drv&component=winecrt0&component=winedbg&component=winegstreamer&component=winelib&component=winemac.drv&component=winemp3.acm&component=wineoss.drv&component=wineps.drv&component=winepulse.drv&component=wineserver&component=winex11.drv&component=wing32&component=winhttp&component=wininet&component=winmm%26mci&component=winnls32&component=winscard&component=winsta&component=wintab32&component=wintrust&component=wldap32&component=wmi%26wbemprox&component=wmp%26wmvcore&component=wpcap&component=wscript&component=wshom.ocx&component=wsnmp32&component=wtsapi32&component=wuapi&component=xmllite&component=xolehlp&f1=short_desc&keywords=patch%2C%20&keywords_type=nowords&known_name=needs%2Fwants%2Funimplemented%20%28potential%20easy%20bugs%29%20-%20no%20patch&o1=notsubstring&order=Importance&product=Wine&query_based_on=needs%2Fwants%2Funimplemented%20%28potential%20easy%20bugs%29%20-%20no%20patch&query_format=advanced&short_desc=needs%20wants%20unimplemented&short_desc_type=anywordssubstr&v1=implementation)

For a list of projects, please see the [Google Summer of
Code](Summer-Of-Code) page
