---
title: 알려진 문제
---

<small>
&nbsp;[:flag_gb: English](Known-Issues)
</small>

-----

버그질라에 가장 많이 중복 보고된 문제와 와인 개발자, 와인
사용자, 사용자 토론장, IRC, 응용 무른모 정보기지에서 가장 자주 보고된
오류를 안내합니다.

### 현재 알려진 문제

- [버그질라에 가장 많이 보고된 오류
  목록](https://bugs.winehq.org/duplicates.cgi)
- [등록된 깁기에서 보고된
  문제](https://bugs.winehq.org/buglist.cgi?bug_status=STAGED&query_format=advanced)

### 와인 벌레가 아닌 목록 (문제 발생 가능성 있음)

|        |                                                                                                                                                |
| ------ | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| #7991  | Rendering is garbled when a program needs to use the maximum number of shader constants of a graphic card                                      |
| #10841 | Restore display resolution when focus is lost or on exit                                                                                       |
| #18118 | Multiple DOS apps/games fail to start in [DOSBox](DOSBox) when started from Wine (need DOSBox \> 0.74)                                         |
| #31882 | Many multithreaded gui apps randomly deadlock in winex11 driver surface section                                                                |
| #32479 | Multiple applications crash or fail to establish secure server connections (Wine package built with 32-bit GnuTLS 2.6 while host provides 2.8) |
| #34166 | Fullscreen flickering on Mac                                                                                                                   |
| #37347 | Black screen or distorted graphics with Intel drivers (Mesa bug [84651](https://bugs.freedesktop.org/show_bug.cgi?id=84651))                   |
| #41637 | Delphi: Wineserver stuck / not responding since linux-4.8.3 (kernel bug fixed in 4.9.7)                                                        |
| #41639 | Wine with freetype 2.7 causes font rendering issues                                                                                            |

### 고친 문제

- [와인 3.\*판에서
  고침](https://bugs.winehq.org/buglist.cgi?bug_status=CLOSED&chfield=bug_status&chfieldfrom=2018-01-18&chfieldto=now&chfieldvalue=CLOSED&list_id=460065&product=Wine&query_format=advanced&resolution=FIXED)

- [와인 2.\*판에서
  고침](https://bugs.winehq.org/buglist.cgi?bug_status=CLOSED&chfield=bug_status&chfieldfrom=2017-01-24&chfieldto=now&chfieldvalue=CLOSED&list_id=460065&product=Wine&query_format=advanced&resolution=FIXED)

- [와인 1.9.\*판에서
  고침](https://bugs.winehq.org/buglist.cgi?bug_status=CLOSED&chfield=bug_status&chfieldfrom=2015-12-25&chfieldto=2016-12-09&chfieldvalue=CLOSED&product=Wine&query_format=advanced&resolution=FIXED)

- [와인 1.7.\*판에서
  고침](https://bugs.winehq.org/buglist.cgi?bug_status=CLOSED&chfield=bug_status&chfieldfrom=2013-08-02&chfieldto=2015-12-19&chfieldvalue=CLOSED&product=Wine&query_format=advanced&resolution=FIXED)

- [와인 1.5.\*판에서
  고침](https://bugs.winehq.org/buglist.cgi?bug_status=CLOSED&chfield=bug_status&chfieldfrom=2012-03-16&chfieldto=2013-07-18&chfieldvalue=CLOSED&product=Wine&query_format=advanced&resolution=FIXED)

- [와인 1.3.\*판에서
  고침](https://bugs.winehq.org/buglist.cgi?bug_status=CLOSED&chfield=bug_status&chfieldfrom=2010-07-30&chfieldto=2012-03-07&chfieldvalue=CLOSED&product=Wine&query_format=advanced&resolution=FIXED)

- [와인 1.1.\*판에서
  고침](https://bugs.winehq.org/buglist.cgi?bug_status=CLOSED&chfield=bug_status&chfieldfrom=2008-06-27&chfieldto=2010-07-15&chfieldvalue=CLOSED&product=Wine&query_format=advanced&resolution=FIXED)

## 같이 보기

- [Bugs](Bugs)
- [Bug Triage](Bug-Triage)
