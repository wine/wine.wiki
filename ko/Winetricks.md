---
title: 와인트릭
---

<small>
&nbsp;[:flag_gb: English](Winetricks)
&nbsp;[:flag_fr: Français](fr/Winetricks)
&nbsp;[:flag_nl: Nederlands](nl/Winetricks)
&nbsp;[:flag_th: ภาษาไทย](th/Winetricks)
</small>

-----

## 와인트릭이란?

**와인트릭**은 와인에서 일부 풀그림을 실행하는데 필요한 재배포 가능 실행
자료관을 내려받고 설치하는데 도움을 주는 명령줄 모음입니다. 와인트릭은
기본 와인 구성 요소가 아닌 비공개 원시 부호 자료관을 사용할 가능성이
있습니다.

**주의:** 그러나 와인트릭 사용은 와인에서의 일부 풀그림 실행에 *매우*
유용하지만 본 누리집에서 제대로된 지원을 받지 못합니다. 특히, 와인트릭을
통해 와인 구성 요소를 교체하였다면 벌레 버고가 불가능할 수 있습니다.
[와인트릭 사용 후 벌레 보고](#winetricks-and-bugs)를
참조하십시오.

**주의:** 목록에 있는 일부 꾸러미는 구형 와인에서 제대로 작동하지
않습니다. 때문에 [와인 최신판](Download) 사용을 권장드립니다.

## 와인트릭 얻기

본 명령줄 모음은 <https://github.com/Winetricks/winetricks>에서 오스틴
잉글리쉬가 관리합니다. 최신판은
<https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks>에
있습니다. 주소를 우딸깍하고 '다른 이름으로 저장'을 선택해 빠르게 복사할
수 있습니다.
명령줄로 와인트릭을 얻으려면 다음 명령줄을 입력하세요:

```sh
$ cd "${HOME}/Downloads"`
$ wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
$ chmod +x winetricks`
```

또한 일부 와인트릭 "꾸러미"는 다음 외부 도구를 필요로 합니다.
**cabextract**, **unzip**, **p7zip**, **wget** (또는 **curl**). 그림
사용자 사이틀로는 **zenity** 또는 **kdialog**가 있습니다.
리눅스 사용자는 보통 배포판 내 꾸러미 관리자를 통해 이상의 도구를 설치할
수 있습니다.

## 와인트릭 사용

와인트릭을 받으면 단말기에 `sh winetricks` 명령으로 손쉽게 실행할 수
있습니다. `chmod +x winetricks`를 먼저 사용했다면 `./winetricks`도
가능합니다. 매개변수 없이 실행하면, 그림 사용자 사이틀에 가능한 꾸러미
목록이 표시됩니다. 설치할 꾸러미 이름을 알고 있다면, 와인트릭 명령어에
덧붙여 사용해 곧바로 설치 과정을 진행할 수 있습니다. 예를 들어

`sh winetricks corefonts vcrun6`

는 corefonts와 vcrun6 꾸러미를 설치합니다.

## 변수

20190912판 도움말:

```
문법: /usr/bin/winetricks [options] [command|verb|path-to-verb] ...
Executes given verbs.  Each verb installs an application or changes a setting.

변수:
    --country=CC      국가번호를 CC로 설정하고 국가 설정에 IP를 사용하지 않습니다.
    --force           꾸러미 설치 여부를 검사하지 않습니다.
    --gui             명령줄로 실행 중일 때 그림 사용자 사이틀 대화창을 띄웁니다.
    --isolate         무른모 또는 전자오락을 각각의 저장소에 설치합니다. (WINEPREFIX)
    --self-update     해당 무른모의 판을 올립니다.
    --update-rollback 마지막 --self-update를 취소합니다.
-k, --keep_isos       .iso 기록철을 인식합니다. (저장판 없이 설치했을 때 사용)
    --no-clean        임시 폴더를 삭제하지 않습니다. (벌레잡을 때 사용)
-q, --unattended      묻지 않고, 자동으로 설치합니다.
-r, --ddrescue        저장판이 손상되었을 때 모두 재설치합니다.
-t  --torify          가능하면 토어로 내려받고 실행합니다.
    --verify          Run (automated) GUI tests for verbs, if available
-v, --verbose         실행한 명령줄을 출력합니다.
-h, --help            이 도움말을 띄웁니다.
-V, --version         판 정보를 띄웁니다.

명령:
list                  목록 띄우기
list-all              list all categories and their verbs
apps list             list verbs in category 'applications'
benchmarks list       list verbs in category 'benchmarks'
dlls list             list verbs in category 'dlls'
games list            list verbs in category 'games'
settings list         list verbs in category 'settings'
list-cached           list cached-and-ready-to-install verbs
list-download         list verbs which download automatically
list-manual-download  list verbs which download with some help from the user
list-installed        list already-installed verbs
arch=32|64            create wineprefix with 32 or 64 bit, this option must be
                      given before prefix=foobar and will not work in case of
                      the default wineprefix.
prefix=foobar         select WINEPREFIX=/home/$USER/.local/share/wineprefixes/foobar
annihilate            Delete ALL DATA AND APPLICATIONS INSIDE THIS WINEPREFIX
```

**Tip:** As with all Wine commands, winetricks knows about the
`WINEPREFIX` environment variable. This is useful for using winetricks
with different Wine prefix locations. For example,

`env WINEPREFIX=~/.winetest sh winetricks mfc40 `

installs the mfc40 package in the `~/.winetest` prefix.

**Tip:** Users with more than one version of Wine on their system (for
example, an installed package and an uninstalled Wine built from git)
can specify which version winetricks should use. For example,

`env WINE=~/wine-git/wine sh winetricks mfc40 `

installs the mfc40 package using the Wine in the ~/wine-git directory.

## Reporting bugs after you have used Winetricks

Please do not report bugs if you have used winetricks to install native
(ie non Wine) files, as we cannot support Microsoft dlls.

Using winetricks to install gecko, mono, and fakeie6 options is
acceptable for bug reports - just be sure to mention that's what you've
done.

Additionally if you found it necessary to use winetricks for an
application please mention it when submitting to the AppDB, mailing
lists, and other Wine resources.

## Reporting bugs **in** Winetricks

Winetricks has a bug tracking system at
<https://github.com/Winetricks/winetricks/issues>, please use it. If you
don't want to get an account there to file a bug, posting on the wine
user forum may also eventually get noticed.

## How to remove things installed by Winetricks

It's easy to install an entire wineprefix, so by default, winetricks
installs each app into its own Wine prefix, and offers an easy way to
remove wineprefixes and the menu items they created.

Winetricks does not provide a way to uninstall individual apps or DLLs
inside a Wine prefix. This is for several reasons, but mainly because
the preferred way to uninstall anything in Wine is to simply install
into a fresh Wine prefix. (Yes, it would be nice to have uninstallers
for everything, but I don't need it myself. Patches welcome.)

If for some reason, you still don't want to fiddle at all with your Wine
prefixes, Wine does offer a built-in
[Uninstaller](Commands/uninstaller) program. Like the Windows
"Add/Remove Programs" applet though, it only recognizes programs
installed by well-behaved Windows installers that respect the registry,
like InstallShield or WISE. There are no guarantees it will work with a
program installed by Winetricks or other installers like *.msi*
packages.

## Installing winetricks

It's not necessary to install winetricks to use it. You may choose to
install winetricks in a global location so you can just type
`winetricks` on the command line. Some Linux distributions include
winetricks in their Wine packages, so you don't have to download it
separately. You probably do want to follow these steps, if the
distributions packaged winetricks version lags behind the current
winetricks release (e.g. Debian/Ubuntu users).

To download and install your own copy of winetricks, you can install it
manually like this:

```sh
$ cd $HOME/Downloads
$ wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
$ chmod +x winetricks
$ sudo cp winetricks /usr/local/bin
```

To download and install the (separate) BASH completion script for
winetricks:

```sh
$ cd $HOME/Downloads
$ wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks.bash-completion
$ sudo cp winetricks.bash-completion /usr/share/bash-completion/completions/winetricks  # Standard location for BASH completion scripts (Arch, Gentoo, OpenSUSE, Fedora, Debian/Ubuntu, Solus)
```

## See Also

- <https://www.cabextract.org.uk/> -- cabextract is a tool for extracting
  MS cabinet files under Unix environments.
