---
title: 와인
---

<small>
&nbsp;[:flag_gb: English](home)
&nbsp;[:flag_fr: Français](fr/home)
&nbsp;[:flag_de: Deutsch](de/home)
&nbsp;[:flag_nl: Nederlands](nl/home)
&nbsp;[:flag_cn: 简体中文](zh_CN/home)
</small>

-----

## 와인이란?

[와인](https://winehq.org) 은 리눅스, 맥, FreeBSD, 솔라리스 사용자가
마이크로소프트용 응용 프로그램을 윈도우 설치 없이 사용 가능하게 하는
프로그램입니다. [무료](https://www.gnu.org/philosophy/free-sw.html)이며
지속적으로 개발됩니다. [다른 운영체제](Compatibility)에서도 사용
가능합니다.

## 가장 많이 사용되는 게시물

- [자주 묻는 질문](ko/FAQ) : 와인을 사용하는데 있어
  일반적인 문제가 발생한다면 FAQ를 읽어주세요!

- [와인 문서](ko/Documentation) : 자주 묻는 질문이
  도움이 되지 않는다면 사용 설명서를 읽어주세요! 😃

- [와인 응용 무른모 정보기지](https://appdb.winehq.org) : 개별
  응용프로그램에 관한 정보는 응용 무른모 정보기지에서 볼 수 있습니다.

- [와인 사용자 토론장](https://forum.winehq.org) : 아무리 해도 확답을
  얻지 못한다면 토론장에 물어보세요.

- [알려진_문제](Known-Issues) : 오류 보고를 하기 전에
  확인하세요.

- [와인트릭](ko/Winetricks) : 현 와인의 부족한 점을 메꿔 주는
  유용한 도구입니다.

- [맥](MacOS) : 맥에서 와인을 돌리세요.

## 와인 프로젝트에 대해

- [와인의 기능](Wine-Features)과 목표에 대한 개요

- [와인의 중요성](Importance-Of-Wine)에 관한 몇몇 기사

- [와인 사업 조직](Project-Organization)에 관한 개요

- [와인 참여자](Who's-Who) , 주요 기여자들에 대한 [감사
  인사](Acknowledgements)

- [와인 프로젝트의 역사](Wine-History)와
  [소식](News)

- [와인의 저작권](Licensing).

## 기여하기

- 사용자 지원 : [User's Forum 토론장](https://forum.winehq.org)이나
  [IRC](ko/IRC)에서 여러 질문에 답변을 해 다른 사용자를 도와줍니다.

- [개발](Developers) : 와인을 돕거나 소스로 빌드하려 할 때
  도움이 됩니다.

- [정보기지 관리자](https://gitlab.winehq.org/winehq/appdb/-/wikis/Maintainer-Guidelines) : 응용
  무른모 정보기지 관리자시거나 관리자가 되고 싶으신 분은 들어가 주세요.

- [벌레잡이](Bugs)는 벌레잡기를 통해 와인을 시험하고, 오류를
  줄이며, 다른 사용자가 보고한 오류를 분류하는데 도움을 줄 수 있습니다.

- [글쓴이](Writers)는 와인에 관해 문서를 작성하거나, 위키를
  보수하거나, 프로젝트의 여러 부분을 번역하여 기여 할 수 있습니다.

- [설계자](Designers)는 틈틈히 프로그램 아이콘이나 누리집의
  맵시와 기능을 개선할 수 있습니다.

- [와인 개발 자금](https://www.winehq.org/donate) : 와인 프로젝트에
  기부하는 것을 고려해 주세요.

- [대중 홍보](Public-Relations) : 와인을 전세계에 널리
  알리는데 도움을 줍니다.

## 유용한 누리집

- 와인에 딸린 모든 자잘한 도구들의 [명령어
  목록](Commands)

- 아마 편리할 수도 있는 [제3자 응용
  무른모](Third-Party-Applications) 또는 비공식 도구

- 와인을 구성하는데 [유용한 레지스트리
  값](Useful-Registry-Keys)과 여러 설정

- [회기 시험](Regression-Testing) 명령

- [와인을 공식적으로 지원하는 응용
  무른모](Apps-That-Support-Wine)

## 더 많은 정보

- 와인 프로젝트의 [원시 부호](Source-Code)는 온라인 상에서
  보거나 내려받을 수 있습니다.

- 보존된 토론을 둘러보거나, [전자우편
  알리미](ko/Forums)에 가입할 수 있습니다.

- 와인의 [개발 진행 상황](Wine-Status)을 볼 수 있습니다.

- Assorted files such as site icons and images, and database dumps of
  Bugzilla, AppDB, and the wiki can be downloaded from WineHQ's
  [download server](https://dl.winehq.org/wine/).

- View [WineHQ site
  statistics](https://www.winehq.org/webalizer/index.html) and rough
  estimates of Wine's [Usage Statistics](Usage-Statistics).
