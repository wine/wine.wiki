---
title: 문서
---

<small>
&nbsp;[:flag_gb: English](Documentation)
&nbsp;[:flag_fr: Français](fr/Documentation)
&nbsp;[:flag_de: Deutsch](de/Documentation)
&nbsp;[:flag_nl: Nederlands](nl/Documentation)
&nbsp;[:flag_pl: Polski](pl/Documentation)
&nbsp;[:flag_tr: Türkçe](tr/Documentation)
</small>

-----

## 설명서

- **[Wine 사용 설명서](Wine-User's-Guide)**

  Wine으로 Windows 프로그램을 실행하는 사용 및 설정 방법입니다.

- **[Winelib 사용 설명서](Winelib-User's-Guide)**

  Wine을 사용하여 Windows 프로그램을 리눅스로 이식하는 방법입니다.

- **[Wine 개발자 설명서](Wine-Developer's-Guide)**

  Wine 해킹 가이드입니다.

- **[Wine 설치와 설정](Wine-Installation-and-Configuration)**

  Wine으로 소프트웨어를 작동하게 만들기 전에 읽어 볼 가이드입니다.

## Command reference

- **[Commands](Commands)**

  Documentation about the commands available with Wine.

- **[Man Pages](Man-Pages)**

  Manual pages for the Wine commands and tools.
