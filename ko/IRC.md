---
title: 실시간 커뮤니티 채팅
---

<small>
&nbsp;[:flag_gb: English](IRC)
&nbsp;[:flag_fr: Français](fr/IRC)
&nbsp;[:flag_de: Deutsch](de/IRC)
&nbsp;[:flag_es: Español](es/IRC)
&nbsp;[:flag_pt: Português](pt/IRC)
&nbsp;[:flag_nl: Nederlands](nl/IRC)
&nbsp;[:flag_pl: Polski](pl/IRC)
&nbsp;[:flag_tr: Türkçe](tr/IRC)
&nbsp;[:flag_cn: 简体中文](zh_CN/IRC)
</small>

-----


[Libera.​Chat](https://libera.chat/)에서 여러 Wine IRC 채널을 호스트하고
있습니다. [HexChat](https://hexchat.github.io/)과 같은 IRC 프로그램으로
채팅에 참여할 수 있습니다. 다음 설정을 사용하십시오:

> **서버:** irc.libera.chat\
> **포트:** 6697

대화하고 싶은 주제에 따라서 채널을 선택하십시오:

> **#winehq:** Wine 실행에 대한 사용자 지원과 도움\
> **#crossover:** CrossOver 실행에 대한 사용자 지원과 도움\
> **#winehackers:** Wine 개발과 기타 기여\
> **#winehq-social:** 다른 커뮤니티 구성원과의 일반적인 대화

Firefox 및 IRC URL을 지원하는 브라우저를 사용 중이라면 다음 링크를
클릭하여 접속할 수 있습니다:

- [#winehq](irc://irc.libera.chat/winehq)
- [#crossover](irc://irc.libera.chat/crossover)
- [#winehackers](irc://irc.libera.chat/winehackers)
- [#winehq-social](irc://irc.libera.chat/winehq-social)

대화가 주제를 벗어나지 않고 최대한 건설적으로 진행될 수 있도록 IRC에
물어보기 전에 질문할 것에 대해서 찾아보는 것을 추천합니다. [Wine
FAQ](ko/FAQ), [AppDB](https://appdb.winehq.org), [다운로드
페이지](Download)에 있는 내용부터 검색을 시작해 보십시오.

## IRC 규칙과 제재

다른 사람에게 공격적으로 나서거나 무시하지 않는 것 이외에도 IRC 채널에
참여하는 모든 사람이 지켜야 할 규칙이 있습니다. 규칙을 첫 번째로 지키지
않았다면 대개의 경우 실수로 보고 경고만 받게 됩니다. 여러 번 경고를 받게
된다면 채널에서 추방당할 것입니다.

여러 번 추방당한 이후에도 계속 규칙을 어긴다면 최대 2시간까지 채널
접속이 차단될 수 있습니다. 이 이후에도 규칙을 어긴다면 무기한 차단되며,
차단 해제 요청을 하지 않는 한 계속 유지됩니다. 차단에 이의를 제기하고
싶으면 **#winehq-social** 채널(만약 **#winehq-social** 채널에서
차단당했으면 [wine-devel 메일링 리스트](mailto:wine-devel@winehq.org))에
자신이 왜 차단당했으며 왜 차단이 해제되어야 하는지에 대한 메시지를 남겨
주십시오.

| 규칙                                                   | 설명                                                                                                            | 경고 | 추방 |
|--------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|------|------|
| 스팸 전송 금지                                         |                                                                                                                 | 1    | 2    |
| 1, 2줄 이상의 내용을 붙여넣을 때에는 Pastebin 사용     | [winehq.pastebin.com](http://winehq.pastebin.com) 및 [pastebin.ca](http://pastebin.ca) 등을 사용할 수 있습니다. | 0    | 5    |
| 채널 주제에 맞는 이야기 하기                           | 확실하지 않은 경우 **#winehq** 채널에 참여할 채널을 물어 보십시오.                                              | 2    | 3    |
| Wine과 CrossOver만 해당하는 채널에서 지원함            | Sidenet, WineDoors, Cedega, IEs4Linux 등은 지원되지 **않습니다**.                                               | 2    | 1    |
| **#winehq**에 질문하기 전에 최신 버전의 Wine 사용 확인 | 확실하지 않은 경우 명령행에 `wine --version` 명령을 실행하여 사용 중인 버전을 알아 보십시오.                    | 3    | 1    |
| 도움을 받고 싶다면 차례 기다리기                       |                                                                                                                 | 3    | 1    |
| 불법 복제 소프트웨어에 관한 언급 **금지**              |                                                                                                                 | 1    | 1    |
