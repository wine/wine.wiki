---
title: 주요 메일링 리스트 및 포럼
---

<small>
&nbsp;[:flag_gb: English](Forums)
&nbsp;[:flag_fr: Français](fr/Forums)
&nbsp;[:flag_pt: Português](pt/Forums)
&nbsp;[:flag_nl: Nederlands](nl/Forums)
&nbsp;[:flag_pl: Polski](pl/Forums)
&nbsp;[:flag_tr: Türkçe](tr/Forums)
&nbsp;[:flag_cn: 简体中文](zh_CN/Forums)
</small>

-----


## WineHQ 포럼

Wine에는 여러 메일링 리스트와 포럼이 있습니다. 다음은 사용자에게 유용한
메일링 리스트와 포럼 목록입니다.

- [WineHQ 포럼](https://forum.winehq.org)
- 우분투 사용자라면 [우분투 포럼의 Wine 게시판](https://ubuntuforums.org/forumdisplay.php?f=313)을 확인해
  보는 것도 추천합니다.

(특히 비영어권의) 다른 활성화된 Wine 포럼을 알고 있다면 저희에게 연락해
주시면 목록에 추가하겠습니다.

:information_source: 새 릴리스 알림을 받으려면 [Wine-Announce 메일링
    리스트](https://list.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/)를
    구독하십시오.

## 모든 WineHQ 메일링 리스트

WineHQ에는 패치 제출, Git 커밋 추적, Wine 토론에 대한 메일링 리스트가
있습니다. 

:information_source: 메일링 리스트에 글을 올리기 전에 구독하고 있어야 합니다.
구독하지 않은 상태에서 메일을 보내면 메일링 리스트 소프트웨어에서 스팸
메시지로 취급할 수도 있습니다.

- **<wine-announce@winehq.org>**\
    \[[구독/해지](https://list.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/)\]
    \[[메시지
    기록](https://list.winehq.org/mailman3/hyperkitty/list/wine-announce@winehq.org/)\]\
    Wine 릴리스나 주요 Wine 및 WineHQ 뉴스를 전송하는 읽기 전용 메일링
    리스트입니다. 메시지 전송 빈도: 1개월에 2여통(낮음)
- **<wine-zh@freelists.org>**\
    \[[구독/해지](https://www.freelists.org/list/wine-zh)\]
    \[[메시지
    기록](https://www.freelists.org/archive/wine-zh/)\]\
    Wine 중국어 사용자 토론을 위한 공개 메일링 리스트입니다. 메시지 전송
    빈도: 1일에 10여통(낮음)
- **<wine-devel@winehq.org>**\
    \[[구독/해지](https://list.winehq.org/mailman3/postorius/lists/wine-devel.winehq.org/)\]
    \[[메시지
    기록](https://list.winehq.org/mailman3/hyperkitty/list/wine-devel@winehq.org/)\]\
    Wine 개발, WineHQ, 패치, 기타 Wine 개발자들의 관심사에 대해서
    토론하는 공개 메일링 리스트입니다. 메시지 전송 빈도: 1일에
    50여통(중간)
- **<wine-commits@winehq.org>**\
    \[[구독/해지](https://list.winehq.org/mailman3/postorius/lists/wine-commits.winehq.org/)\]
    \[[메시지
    기록](https://list.winehq.org/mailman3/hyperkitty/list/wine-commits@winehq.org/)\]\
    Git 트리의 커밋 정보를 게시하는 읽기 전용 리스트입니다. 메시지 전송
    빈도: 1일에 25여통(중간)
- **<wine-releases@winehq.org>**\
    \[[구독/해지](https://list.winehq.org/mailman3/postorius/lists/wine-releases.winehq.org/)\]
    \[[메시지
    기록](https://list.winehq.org/mailman3/hyperkitty/list/wine-releases@winehq.org/)\]\
    매 Wine 공식 릴리스마다 큰 diff 파일을 전송하는 읽기 전용 메일링
    리스트입니다. 메시지 전송 빈도: 1개월에 2여통(낮음)
- **<wine-bugs@winehq.org>**\
    \[[구독/해지](https://list.winehq.org/mailman3/postorius/lists/wine-bugs.winehq.org/)\]
    \[[메시지
    기록](https://list.winehq.org/mailman3/hyperkitty/list/wine-bugs@winehq.org/)\]\
    [버그 추적 데이터베이스](https://bugs.winehq.org/)의 활동을 알려 주는 읽기
    전용 메일링 리스트입니다. 메시지 전송 빈도: 1일에 100여통(높음)
