---
title: 이식
---

<small>
&nbsp;[:flag_gb: English](Compatibility)
</small>

-----

## 휴대용 와인 만들기

무수한 운영체제와 구성에서 와인을 빌드하고 작동시키기 위해 휴대용 와인을
만드는 것을 목표 했습니다. 와인은 새 구조나 운영체제에 이식하려면 언제나
지원할 의향이 있는 색다른 개발 도구 집합을 이용하거나 각 운영체제의
독특한 특성을 해결해야 합니다. 이러한 개선은 와인을 더 완전하게 해주고
어느 운영체제에서나 안정적이게 하는 부차적인 이점을 줍니다.

이식에 관심이 있다면 [와인 빌드](와인-빌드)에서 각 구성을
보거나 등록할 수 있고, 실험 결과에 관한 사항을 공유할 수도 있습니다.
다른 [개발진](개발진)에게 여려분의 목표와 경과를 알리는데
주저하지 마세요. 최소한 1명의 개발자라도 같은 구성을 좋은 시험할 기회가
있습니다. 그러면 유리한 시작점을 갖고, 시험판의 가능한 방향이나 오류를
보내줄 수 있습니다.

오류를 줄이려면 기본 32두값 와인, 리눅스, FreeBSD 커널, 32두값과 AMD64
구조, GCC/GNU 빌드 도구, 와인 헤더 파일에서(저희가 통제 가능한 환경)
가능한 적은 적은 매개변수를 사용하십시오.

클랭 번역기가 많이 개선되었습니다. 오류없이 32두값 와인을 빌드할 수
있으며, 근시일 내에 공식 지원될 것으로 보입니다. 단, 일부 동적 연결
자료관과 64두값 지원이 부실합니다. 아래에 더 자세한 내용이 있습니다.

## 다른 선택지

### 운영체제

| 운영체제         | 번역여부           | 작동여부           | 64두값 와인 지원여부 | 그외                                                                                                                                         |
|------------------|:------------------:|:------------------:|:--------------------:|----------------------------------------------------------------------------------------------------------------------------------------------|
| FreeBSD          | :white_check_mark: | :white_check_mark: | :white_check_mark:   | [FreeBSD 위키 내 와인 문서](https://wiki.freebsd.org/Wine)                                                                                   |
| macOS            | :white_check_mark: | :white_check_mark: | :white_check_mark:   | [MacOS Building](MacOS-Building)에서 특이점을 확인할 수 있습니다.                                                                            |
| DragonFlyBSD     | :white_check_mark: | :white_check_mark: | :x:                  | 일반적으로 FreeBSD 계열에서 잘 작동합니다.                                                                                                   |
| Android          | Cross-compiled     | Partially          | ?                    | 아직 작업 중입니다. [Alexandre Julliard's slides](/media/Wine-on-android-wineconf2014.pdf) from [FOSDEM2014](WineConf/FOSDEM-2014)를 보세요. |
| NetBSD           | :white_check_mark: | :white_check_mark: | :x:                  | Packaged as recently as Dec 2015                                                                                                             |
| OpenBSD          | :x:                | :x:                | :x:                  | 이전에는 지원했습니다. 와인과 OpenBSD 부호가 호환되지 않습니다. (irreconcilably for the foreseeable future)                                  |
| Cygwin           | Partially          | ?                  | ?                    | 아직 시험되지 않았습니다.                                                                                                                    |
| GNU [Hurd](Hurd) | :white_check_mark: | :white_check_mark: | :x:                  | See page for details                                                                                                                         |
| [Haiku](Haiku)   | :x:                | :x:                | :x:                  | See page for details                                                                                                                         |
| OpenIndiana      | :x:                | :x:                | :x:                  | 2013년 12월에 마지막으로 꾸러미가 만들어졌습니다.                                                                                            |

### 구조

| 구조           | 번역?              | 작동?              | 비고                  |
|----------------|:------------------:|:------------------:|-----------------------|
| [ARM](ARM)     | :white_check_mark: | :white_check_mark: |                       |
| [ARM64](ARM64) | :white_check_mark: | :white_check_mark: |                       |
| PowerPC        | :x:                | :x:                | 한동안 시험하지 않음. |

### 번역기

| 이름                                                | 빌드 완료 가능?    | 완벽하게 작동?     | 비고                                                                                                                                                                                        |
|-----------------------------------------------------|:------------------:|:------------------:|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Clang](Clang)                                      | :white_check_mark: | :white_check_mark: | 64두값 와인만 불가 (~~[Bug #8851](https://llvm.org/bugs/show_bug.cgi?id=8851)~~, fixed in Clang v3.7.1) or expose certain APIs ([Bug #10212]("https://llvm.org/bugs/show_bug.cgi?id=10212)) |
| [icc](https://software.intel.com/en-us/c-compilers) | :x:                | :x:                | 한동안 시험되지 않음. 작업 환경 구축 필요.                                                                                                                                                  |

## 문제있는 구조

이론상 32두값 무른모는 반드시 기본 32두값 와인이나 64두값 호환판
와인이나 동일하게 작동해야 합니다. 안타깝게도 이 두 판본의 동일한 작동을
아직까지 보장할 수 없습니다. 만약 두 개 판본이 다르게 작동한다면
망설이지 말고 작동 차이를 자세하게 언급해 주십시오.

[64두값](64두값)은 이 차이의 실험에 대한 많은 정보가
있습니다. 그 정보는 와인을 64두값 굳은모 상의 운영체제에 이식하는 데에
많은 도움이 됩니다.

## 단종 운영체제

와인 프로젝트는 근 20년 동안 외길을 걸어왔습니다. 그동안 무수한
운영체제가 흥망했습니다. 그러나 일부 운영체제는 전용되거나 전문가에 의해
아직까지 사용됩니다. 아래 목록은 과거 와인이 최소한도로 시험된 운영체제
입니다.

### 구형 번역기

- Open64 (본판과 파생작들의 개발이 중단되었습니다.
  대학교[OpenUH](https://web.cs.uh.edu/~openuh/)에 한두 개의 사본이
  있습니다.)
- MinGW/MSYS (계속 개발되고 있으나 판올림은 요원합니다.
  [mingw-w64](https://mingw-w64.org/doku.php) 사용을 고려하는 것이
  좋습니다.)

### 구형 전산기

- [SCO OpenServer](https://en.wikipedia.org/wiki/SCO_OpenServer) 5 (AT&T
  SysV Release 3에 기반합니다.)
- [AT&T SysV Release 4](https://en.wikipedia.org/wiki/UNIX_System_V)
- SCO OpenServer 6 (AT&T SysV Release 5에 기반합니다.)
- Sun Solaris & OpenSolaris (FOSS OpenIndiana & commercial Oracle
  Solaris에서 지원합니다.)

### 구형 구조

- [IA-64](https://en.wikipedia.org/wiki/IA-64) (아이테니엄) 지원은
  전무합니다.
- [Alpha](https://en.wikipedia.org/wiki/DEC_Alpha) 지원은 1.3.19판에서
  중단되었습니다.
- [SPARC](https://en.wikipedia.org/wiki/SPARC) 지원은 1.5.26판에서
  중단되었습니다.
- 과거에 [IBM s/390](https://en.wikipedia.org/wiki/IBM_ESA/390)에서
  [실행한 적도
  있습니다.](https://www.mail-archive.com/wine-devel@winehq.com/msg04305.html)
