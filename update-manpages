#!/bin/sh

# Update the manual pages from the Wine tree
#
# Usage:
#
#  $ cd wiki_top_dir
#  $ ./update-manpages [commit] [wine_git_dir]

set -e
umask 022

commit=${1-master}
winedir=${2-../wine/.git}

index="Man-Pages"
pages=""
files="$index.md"

output_index()
{
    file=$1$index.md
    (sed '/^- \[/,$d' $file
     for p in $pages
     do
         f=$1$index/$p
         test -f "$f.md" || f=$index/$p  # fallback to English page
         sed "2!d
s!^title: \\(.*\\) - \\(.*\\)!- [\\1]($f) - \\2!" $f.md
     done) >$file.new && mv $file.new $file
}

for f in $(git --git-dir="$winedir" ls-tree -r --name-only "$commit" | grep \\.man\\.in$)
do
    name=$(basename $f .man.in)
    lang=$(echo $name | cut -d . -f 2)
    if test "$lang" != "$name"
    then
        mkdir -p $lang/$index
        output=$lang/$index/$(echo $name | cut -d . -f 1).md
    else
        output=$index/$name.md
        pages="$pages $name"
        files="$files $output"
    fi
    git --git-dir="$winedir" show "$commit:$f" | ./man-to-markdown >$output
done

for dir in $index */$index
do
    output_index $(expr $dir : "\\(.*\\)$index$")
done

./update-translations $files
git add $files
