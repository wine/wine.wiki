---
title: 主要的邮件列表/论坛
---

<small>
&nbsp;[:flag_gb: English](Forums)
&nbsp;[:flag_fr: Français](fr/Forums)
&nbsp;[:flag_pt: Português](pt/Forums)
&nbsp;[:flag_nl: Nederlands](nl/Forums)
&nbsp;[:flag_pl: Polski](pl/Forums)
&nbsp;[:flag_tr: Türkçe](tr/Forums)
&nbsp;[:flag_kr: 한국어](ko/Forums)
</small>

-----


## 论坛

Wine 有很多邮件列表和论坛，这里列举了对用户最有用的一些：

- [WineHQ 论坛](https://forum.winehq.org)
- Ubuntu 用户也许还想去 [Ubuntu 论坛 Wine
  板块](https://ubuntuforums.org/forumdisplay.php?f=313)

如果你知道除此之外活跃的 Wine
论坛，特别是非英文论坛，请告诉我们以便我们将它添加到这个列表来。

:information_source: 如果你想被告知最新的发布，请订阅 [Wine-Announce
    邮件列表](https://list.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/)

## 所有 WineHQ 邮件列表

WineHQ 分别有用于提交 patches、追踪 Git 提交、讨论 Wine
的邮件列表。

:information_source: 你必须订阅后才能发送邮件到列表，否则你的邮件将被
邮件列表软件判定为垃圾邮件。

- **<wine-announce@winehq.org>**\
    \[[（取消）订阅](https://list.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/)\]
    \[[存档](https://list.winehq.org/mailman3/hyperkitty/list/wine-announce@winehq.org/)\]\
    一个低流量（2封/月）的只读列表，用于宣布发布和其他主要的 Wine 或者
    WineHQ 消息。
- **<wine-zh@freelists.org>**\
    \[[（取消）订阅](https://www.freelists.org/list/wine-zh)\]
    \[[存档](https://www.freelists.org/archive/wine-zh/)\]\
    一个低流量（10封/天）的开放性列表，用于中文终端用户讨论。
- **<wine-devel@winehq.org>**\
    \[[（取消）订阅](https://list.winehq.org/mailman3/postorius/lists/wine-devel.winehq.org/)\]
    \[[存档](https://list.winehq.org/mailman3/hyperkitty/list/wine-devel@winehq.org/)\]\
    一个中等流量（50封/天）的开放性列表，用于讨论 Wine
    开发、WineHQ、patches 或者其他 Wine 开发者感兴趣的事情。
- **<wine-commits@winehq.org>**\
    \[[（取消）订阅](https://list.winehq.org/mailman3/postorius/lists/wine-commits.winehq.org/)\]
    \[[存档](https://list.winehq.org/mailman3/hyperkitty/list/wine-commits@winehq.org/)\]\
    一个中等流量（25封/天）的只读列表，用于追踪 Git tree 上的 commits。
- **<wine-releases@winehq.org>**\
    \[[（取消）订阅](https://list.winehq.org/mailman3/postorius/lists/wine-releases.winehq.org/)\]
    \[[存档](https://list.winehq.org/mailman3/hyperkitty/list/wine-releases@winehq.org/)\]\
    一个低流量（2封/月）的只读列表，用于接收每个 Wine official release
    的大体积 diff 文件。
- **<wine-bugs@winehq.org>**\
    \[[（取消）订阅](https://list.winehq.org/mailman3/postorius/lists/wine-bugs.winehq.org/)\]
    \[[存档](https://list.winehq.org/mailman3/hyperkitty/list/wine-bugs@winehq.org/)\]\
    一个大流量（100封/天）的只读列表，用于监听 [Bug Tracking
    Database](https://bugs.winehq.org/) 的活动。
