<small>
&nbsp;[:flag_gb: English](Fedora)
</small>

-----

## 安装 WineHQ 安装包

添加软件源：

*Fedora 39:*

```
dnf config-manager --add-repo https://dl.winehq.org/wine-builds/fedora/39/winehq.repo
```

*Fedora 40:*

```
dnf config-manager --add-repo https://dl.winehq.org/wine-builds/fedora/40/winehq.repo
```

安装 **以下任一安装包**：

|              |                              |
|--------------|------------------------------|
| 稳定分支     | `dnf install winehq-stable`  |
| 开发分支     | `dnf install winehq-devel`   |
| Staging 分支 | `dnf install winehq-staging` |

## 构建依赖

### Fedora 22 或更新版本

*（注意：该段尚不完整。）*

尝试运行以下命令（由 Zhenbo 提供，在 Fedora 23 测试过）

`sudo dnf install alsa-plugins-pulseaudio.i686 glibc-devel.i686 glibc-devel libgcc.i686 libX11-devel.i686 freetype-devel.i686 libXcursor-devel.i686 libXi-devel.i686 libNX_Xext-devel.i686 libXext-devel.i686 libXxf86vm-devel.i686 libXrandr-devel.i686 libXinerama-devel.i686 mesa-libGLU-devel.i686 mesa-libOSMesa-devel.i686 libXrender-devel.i686 libpcap-devel.i686 ncurses-devel.i686 libzip-devel.i686 lcms2-devel.i686 zlib-devel.i686 libv4l-devel.i686 libgphoto2-devel.i686 libcapifax-devel.i686  cups-devel.i686 libxml2-devel.i686 openldap-devel.i686 libxslt-devel.i686 gnutls-devel.i686 libpng-devel.i686 flac-libs.i686 json-c.i686 libICE.i686 libSM.i686 libXtst.i686 libasyncns.i686 libedit.i686 liberation-narrow-fonts.noarch libieee1284.i686 libogg.i686 libsndfile.i686 libuuid.i686 libva.i686 libvorbis.i686 libwayland-client.i686 libwayland-server.i686 llvm-libs.i686 mesa-dri-drivers.i686 mesa-filesystem.i686 mesa-libEGL.i686 mesa-libgbm.i686 nss-mdns.i686 ocl-icd.i686 pulseaudio-libs.i686 python-talloc.x86_64 sane-backends-libs.i686 tcp_wrappers-libs.i686 unixODBC.i686 samba-common-tools.x86_64 samba-libs.x86_64 samba-winbind.x86_64 samba-winbind-clients.x86_64 samba-winbind-modules.x86_64 mesa-libGL-devel.i686 fontconfig-devel.i686 libXcomposite-devel.i686 libtiff-devel.i686 openal-soft-devel.i686 mesa-libOpenCL-devel.i686 opencl-utils-devel.i686 alsa-lib-devel.i686 gsm-devel.i686 libjpeg-turbo-devel.i686 pulseaudio-libs-devel.i686 pulseaudio-libs-devel gtk3-devel.i686 libattr-devel.i686 libva-devel.i686 libexif-devel.i686 libexif.i686 glib2-devel.i686 mpg123-devel.i686 mpg123-devel.x86_64 libcom_err-devel.i686 libcom_err-devel.x86_64 libFAudio-devel.i686 libFAudio-devel.x86_64`

`sudo dnf groupinstall "C Development Tools and Libraries"`
`sudo dnf groupinstall "Development Tools"`

您还需要一些来自 [rpmfusion](https://www.rpmfusion.org) 的安装包

`gstreamer-plugins-base-devel gstreamer-devel.i686 gstreamer.i686 gstreamer-plugins-base.i686 gstreamer-devel gstreamer1.i686 gstreamer1-devel gstreamer1-plugins-base-devel.i686 gstreamer-plugins-base.x86_64 gstreamer.x86_64 gstreamer1-devel.i686 gstreamer1-plugins-base-devel gstreamer-plugins-base-devel.i686 gstreamer-ffmpeg.i686 gstreamer1-plugins-bad-free-devel.i686 gstreamer1-plugins-bad-free-extras.i686 gstreamer1-plugins-good-extras.i686 gstreamer1-libav.i686 gstreamer1-plugins-bad-freeworld.i686`

## 编译

得益于 Fedora 的 multilib 功能，一旦你安装了编译依赖，无论是 32 bit 还是
WoW64 的 Wine 您都可以十分简单的编译获得。详细步骤可以在 [Building
Wine](Building-Wine) 中的对应段落找到。

### 64 位系统上的纯 32 位 Wine

```sh
$ PKG_CONFIG_PATH=/usr/lib/pkgconfig CC="ccache gcc -m32" ./configure
$ make
```

## A Bit of History

Fedora has a unique history among GNU/Linux distributions that partly
explains its goals and relationship to other distros. It initially began
as a minor distro, Fedora Linux, that would test and package extra
software on top of the venerable RedHat distro (now technically Red Hat
Enterprise Linux). Around the same time though, Red Hat Inc. decided to
focus on very stable releases with long-term support for enterprises.

At first, the company tried directly managing a more fluid branch geared
towards desktops, but many non-subscribing PC users began switching to
Fedora Linux. At that point, Red Hat made the farsighted decision to
cooperate with the Fedora Linux community on experimental work, and the
Fedora project was born. The resulting distribution was originally
called "Fedora Core" before being renamed simply "Fedora" after a few
years.

Today, Fedora is the work of a worldwide community of volunteers, but
Red Hat Inc. still contributes a great deal to the project through both
collaboration and financial support. In a bit of a role-reversal, Fedora
is also now effectively upstream of its parent distro; RedHat (and its
child distros) will periodically branch off a new Fedora release, then
after much more testing and bug-fixing, provide a new release to their
more stability-minded users.

## 相关内容

- WineHQ 官方 [Fedora
  安装包下载](https://dl.winehq.org/wine-builds/fedora/)
- Fedora 关于 [Wine](https://fedoraproject.org/wiki/Wine) 的 Wiki 页面
- [构建 Wine](Building-Wine)
- [打包](Packaging)
