---
title: Wine
---

<small>
&nbsp;[:flag_gb: English](home)
&nbsp;[:flag_fr: Français](fr/home)
&nbsp;[:flag_de: Deutsch](de/home)
&nbsp;[:flag_nl: Nederlands](nl/home)
&nbsp;[:flag_kr: 한국어](ko/home)
</small>

-----

[Wine](https://winehq.org) 使 Linux、Mac、FreeBSD、Solaris 用户可以在没有
Microsoft Windows 的情况下运行 Windows 应用程序。Wine
是正在持续开发中的[自由软件](https://www.gnu.org/philosophy/free-sw.html)。除了上述的几种平台之外，[其他平台](Compatibility)也可能从本项目中受益。

## 热门链接

- [常见问题](FAQ)：关于 Wine 的常见疑问, 在这里都有解答！

- [Wine文档](Documentation)：如果《常见问题》帮不上忙，试试这个！😃

- [Wine
  应用数据库(AppDB)](https://appdb.winehq.org)：想知道某个应用程序目前是否好使？可以在此数据库中搜索关于它的条目。

- [Wine
  用户论坛](https://forum.winehq.org)：在以上渠道都找不到答案，那就来这儿问问吧。

- [已知的问题](Known-Issues)：在报告 Bug 之前，请先查看这个。

- [winetricks](Winetricks)：一个帮助你解决使用 Wine
  时遇到的问题的脚本工具。

- [macOS](MacOS)：在 macOS 上运行 Wine。

## 关于 Wine 项目

- Wine 的[功能](Wine-Features)简介。

- Wine 的[重要性](Importance-Of-Wine)。

- Wine 的[项目组织](Project-Organization)概况.

- Wine
  的[温暖大家庭](Who's-Who)，对重大贡献的[致谢](Acknowledgements)。

- Wine 的[历史](Wine-History)和[新闻](News).

- Wine 的[许可证](Licensing).

## 做一点微小的贡献 😎

- 用户支持：你可以在[用户论坛](https://forum.winehq.org)或
  [IRC](zh_CN/IRC) 上回答问题，帮助其他用户。

- [参与开发](Developers)：如果你想从源码构建 Wine，看看这个。

- [维护应用数据库](https://gitlab.winehq.org/winehq/appdb/-/wikis/Maintainer-Guidelines)：一起来维护应用数据库吧，告诉大家哪些应用好使！

- [找找 Bug](Bugs)：测试 Wine
  并找出问题，或者诊断其他用户报告的问题。

- [激扬文字](Writers)：为这个项目写文档，维护这个
  wiki，帮助翻译。

- [指点江山](Designers)：为 Wine
  设计图标，把这个网站打扮得漂亮一点。

- [老板大气](https://www.winehq.org/donate)：通过向 Wine 开发基金（Wine
  Development Fund）捐款来表达你对 Wine 的肯定。

- [公共关系](Public-Relations)：让更多的人知道 Wine。

## 一些有用的链接

- [命令大全](Commands)：Wine 附带的所有小工具。

- [第三方应用](Third-Party-Applications)：可能会派上用场的“非官方”工具。

- [有用的注册表项](Useful-Registry-Keys)：有时候，你不得不自己处理注册表......

- [回归测试](Regression-Testing)指南。

- [一些明确表示支持在 Wine
  上运行的应用程序](Apps-That-Support-Wine)。

## 更多信息

- Wine 项目的[源代码](Source-Code)可在线阅读和下载。

- 你可以浏览已归档的讨论，或订阅 Wine
  的[邮件列表](zh_CN/Forums)。

- Wine 的[开发进度](Wine-Status)。

- 杂七杂八的文件，例如网站图标和图片，Bugzilla 和 AppDB
  的数据库转储文件，还有整个 wiki，都可以从 WineHQ
  的[下载服务器](https://dl.winehq.org/wine/)下载。

- [WineHQ
  网站统计数据](https://www.winehq.org/webalizer/index.html)，还有 Wine
  大致的 [用途统计](Usage-Statistics)。
