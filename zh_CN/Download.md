<small>
&nbsp;[:flag_gb: English](Download)
&nbsp;[:flag_nl: Nederlands](nl/Download)
</small>

-----

## 提供技术支持的 Wine

<table><tbody><tr>
<td><a href="https://www.codeweavers.com/crossover">

![Crossover logo](/media/Crossover-logo.png)

</a></td>
<td>
[**CrossOver**](https://www.codeweavers.com/products) 是由
[**CodeWeavers**](https://www.codeweavers.com) 提供的一个经过打磨的
Wine。CrossOver 使得 Wine 更易于使用，同时CodeWeavers 为用户提供了极好
的技术支持。所有 CrossOver的销售收入都将被直接用于支持 Wine 开发者。所
以使用 CrossOver可谓一举两得，您在得到使用 Wine 的帮助的同时也可以帮助
Wine 项目的发展。<br><br>

CodeWeavers 提供全功能的 [**CrossOver 试用版**](https://www.codeweavers.com/crossover)
<br><br>
<small>
This endorsement is the primary recognition that CodeWeavers
has requested in exchange for hosting the Wine web site.
</small></td>
</tr>
</tbody>
</table>

## Wine 安装包

[**发布公告**](https://www.winehq.org/news/)

[**安装和配置指南**](Wine-Installation-and-Configuration)

### WineHQ 安装包

这些安装包由 WineHQ 构建并提供支持。如果您在使用过程发现问题，请提交到 [WineHQ 的 bugzilla](https://bugs.winehq.org)

<table><tbody><tr>
<td rowspan="2">![Wine logo](/media/Wine-logo.png){width=60%}</td>
<td>

[**Ubuntu**](Debian-Ubuntu) - 适用于 Ubuntu 20.04、22.04、23.04 和
23.10 的 WineHQ 安装包

[**Debian**](Debian-Ubuntu) - 适用于 Debian Bullseye 和 Bookworm 的
WineHQ 安装包

[**Fedora**](Fedora) - 适用于 Fedora 38 及 39 的 WineHQ 安装包
</td><td>
<small>
<strong>维护者:</strong>
[Rosanne DiMesio](mailto:dimesio@earthlink.net),
[Marcus Meissner](mailto:meissner@suse.de)
</small></td></tr>
<tr><td>

[**macOS**](MacOS) - 适用于 macOS 10.8 至 10.14 的 WineHQ 安装包
</td><td>
<small>
<strong>维护者:</strong>
[Dean Greer](mailto:gcenx83@gmail.com)
</small></td>
</tbody></table>

### 由发行版提供的安装包

这些安装包由各发行版构建并提供支持。如果您在使用过程发现问题，请提交给
打包的维护者。

<table>
<tbody>
<tr><td>![Suse logo](/media/Suse-logo.png)</td><td>

[**SUSE**](https://en.opensuse.org/Wine#Repositories) - 适用于有版本的
openSUSE（Leap 及 Tumbleweed）和 SUSE Linux Enterprise 12 及 15 的安装
包、.rmps 源码包及每日构建 RPM 安装包。
</td><td>
<small>
<strong>维护者:</strong>
[Marcus Meissner](mailto:meissner@suse.de)
</small></td></tr>
<tr><td>![Slackware logo](/media/Slackware-logo.png)</td><td>

[**Slackware**](https://sourceforge.net/projects/wine/files/Slackware%20Packages) -
txz 安装包（Slackware 15.0）和 tgz（适用于旧版本 Slackware）
</td><td>
<small>
<strong>维护者:</strong>
[Simone Giustetti](mailto:studiosg@giustetti.net)
</small></td></tr>
<tr><td>![FreeBSD logo](/media/FreeBSD-logo.png)</td><td>

[**FreeBSD**](https://www.freshports.org/emulators/wine/) - 适用于
FreeBSD 5.3 或更新版本的软件源
</td><td>
<small>
<strong>维护者:</strong>
[Gerald Pfeifer](mailto:gerald@pfeifer.com)
</small></td></tr>
</tbody>
</table>

## 第三方工具

有时候，一个经过修改的 Wine
可以让一个应用程序工作，但由于种种原因这些修改并没有被合并到 Wine
中。同时为了克服 Wine
的不足，这些年来出现了许多面向终端用户的第三方工具。然而这
些第三方应用都是不被 Wine
开发者提供支持的，虽然有时候他们对您很有用。更多相关内容，请查看我们
[关于第三方应用](Third-Party-Applications) 的 wiki 页面。

## Wine 源代码下载

[**WineHQ 下载服务器**](https://dl.winehq.org/wine/source/) -
官方源码发布网站。

**[Git](Git-Wine-Tutorial)** - 从 git 构建 Wine 的说明。

查看 [Building Wine](Building-Wine) 获取配置和构建 Wine
的帮助。

[**Wine-staging repository**](https://gitlab.winehq.org/wine/wine-staging)
