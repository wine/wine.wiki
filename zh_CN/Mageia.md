<small>
&nbsp;[:flag_gb: English](Mageia)
</small>

-----

## 安装 WineHQ 安装包

**Packages for Mageia have been discontinued as of 2018-12-19. Old
packages are still available in the repository.**

(64 位 Mageia) 开启 multiarch 支持：

```
sudo dnf config-manager --set-enabled mageia-i586 updates-i586
```

添加软件源：

```
sudo dnf config-manager --add-repo https://dl.winehq.org/wine-builds/mageia/6/winehq.repo
```

安装 **以下任一安装包**：

|              |                                   |
|--------------|-----------------------------------|
| 开发分支     | `sudo dnf install winehq-devel`   |
| 稳定分支     | `sudo dnf install winehq-stable`  |
| Staging 分支 | `sudo dnf install winehq-staging` |
