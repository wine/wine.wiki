---
title: 在线聊天社区
---

<small>
&nbsp;[:flag_gb: English](IRC)
&nbsp;[:flag_fr: Français](fr/IRC)
&nbsp;[:flag_de: Deutsch](de/IRC)
&nbsp;[:flag_es: Español](es/IRC)
&nbsp;[:flag_pt: Português](pt/IRC)
&nbsp;[:flag_nl: Nederlands](nl/IRC)
&nbsp;[:flag_pl: Polski](pl/IRC)
&nbsp;[:flag_tr: Türkçe](tr/IRC)
&nbsp;[:flag_kr: 한국어](ko/IRC)
</small>

-----


[Libera.​Chat](https://libera.chat/) 上有各种 Wine 的频道。 你可以使用像
[HexChat](https://hexchat.github.io/) 这样的 IRC
程序来进入聊天室，按照如下配置：

> **Server:** irc.libera.chat\
> **Port:** 6697

你可以根据你想讨论的话题来选择频道：

> **#winehq:** Wine 用户频道\
> **#crossover:** CrossOver 用户支持频道\
> **#winehackers:** Wine 开发者与其它贡献者频道\
> **#winehq-social:** 灌水闲聊频道

使用 Firefox 或者其他支持 IRC 地址的浏览器，可以通过点击加入聊天：

- [#winehq](irc://irc.libera.chat/winehq)
- [#crossover](irc://irc.libera.chat/crossover)
- [#winehackers](irc://irc.libera.chat/winehackers)
- [#winehq-social](irc://irc.libera.chat/winehq-social)

为了使讨论更加专注和尽可能有效，在 IRC 提问之前可以先尝试搜索你的问题。
[Wine FAQ](FAQ)、[AppDB](https://appdb.winehq.org)、 [下载页
面](zh_CN/Download)都是值得首先查阅的资源。

## IRC 规定和惩罚

除了不能带有攻击性和不顾他人地吵闹外，这里还有一些简单的规定希望每个人在
IRC 频道里都能遵守。
大多数情况下，初次违法规定会被视为意外并予以警告。但当你用完了警告次数后，你将会被从频道中踢出。

如果你继续违反规定并用完"踢出"次数，你将会被禁止进入频道两个小时。
在这之后如果还造成其他任何问题，将会被永久禁止，并且只能通过申诉来解除禁止。
如果你想为禁令申辩，请到 **#winehq-social** （或者 [wine-devel
邮件列表](mailto:wine-devel@winehq.org)，当你被禁止进入
**#winehq-social**
时），并且首先解释你为什么会被禁止和为什么你认为禁令应该解除。

| 规定                                                       | 说明                                                                                                    | 警告 | 踢出 |
|------------------------------------------------------------|---------------------------------------------------------------------------------------------------------|------|------|
| 不要发送垃圾邮件。                                         |                                                                                                         | 1    | 2    |
| 当粘贴内容超过一或两行时者使用 pastebin。                  | [winehq.pastebin.com](http://winehq.pastebin.com) 和 [pastebin.ca](http://pastebin.ca) 都是可以使用的。 | 0    | 5    |
| 在合适的频道中讨论合适的话题。                             | 如果不确定，可以在 **#winehq** 询问应该加入什么频道。                                                   | 2    | 3    |
| 只有 Wine 和 CrossOver 才会在他们对应的频道获得支持。      | Sidenet、WineDoors、Cedega、IEs4Linux 等等是**不会**被支持的。                                          | 2    | 1    |
| 在 **#winehq** 提问前，确保你正在使用的是最新版本的 Wine。 | 如果不确定，在命令行中使用 `wine --version` 来确定你的版本。                                            | 3    | 1    |
| 请等候直到轮到你获取帮助。                                 |                                                                                                         | 3    | 1    |
| **不要**讨论有关盗版软件的。                               |                                                                                                         | 1    | 1    |
