---
title: Debian/Ubuntu
---


<small>
&nbsp;[:flag_gb: English](Debian-Ubuntu)
&nbsp;[:flag_nl: Nederlands](nl/Debian-Ubuntu)
</small>

-----

## 安装 WineHQ 安装包

**WineHQ 源仓库的密钥于 2018-12-19
改变过。如果您在此之前下载添加过该密钥，您需要重新下载和添加新的密钥并运行
sudo apt update 以获得更新。**

**WineHQ 安装包安装于 Debian 10 或更新的版本时，会要求安装 libfaudio0
依赖。由于发行版尚未提供该包，您可以从 OBS 下载 libfaudio0
安装包。详情请看：https://forum.winehq.org/viewtopic.php?f=8&t=32192
。**

**Apt-key 已被弃用。** 如果您看到关于这个的警告，请使用命令
`sudo apt-key del "D43F 6401 4536 9C51 D786 DDEA 76F1 A20F F987 672F"`
来删除密钥。 然后从 */etc/apt/sources.list* 删除有关 WineHQ
仓库的那一行。

***Raspbian 用户:** WineHQ 提供的安装包只能用于 x86
架构；它们不能被安装于 ARM 架构系统。*

- 首先，开启 32 位支持：

  ```sh
  sudo dpkg --add-architecture i386
  ```

- 然后安装被用于签发安装包的密钥：

  ```sh
  sudo mkdir -pm755 /etc/apt/keyrings
  wget -O - https://dl.winehq.org/wine-builds/winehq.key | sudo gpg --dearmor -o /etc/apt/keyrings/winehq-archive.key -
  ```

- 下载和安装 WineHQ 源文件：

  |                             |                                |
  |:----------------------------|:-------------------------------|
  | **noble**<br><small>Ubuntu 24.04<br>Linux Mint 22</small>    | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/noble/winehq-noble.sources`       |
  | **jammy**<br><small>Ubuntu 22.04<br>Linux Mint 21.x</small>  | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/jammy/winehq-jammy.sources`       |
  | **focal**<br><small>Ubuntu 20.04<br>Linux Mint 20.x</small>  | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/focal/winehq-focal.sources`       |
  | **trixie**<br><small>Debian Testing</small>                  | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/debian/dists/trixie/winehq-trixie.sources`     |
  | **bookworm**<br><small>Debian 12</small>                     | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/debian/dists/bookworm/winehq-bookworm.sources` |
  | **bullseye**<br><small>Debian 11</small>                     | `sudo wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/debian/dists/bullseye/winehq-bullseye.sources` |

- 更新软件仓库：

  ```sh
  sudo apt update
  ```

然后安装 **以下其中一个安装包**：

|              |                                                        |
|--------------|--------------------------------------------------------|
| 稳定分支     | `sudo apt install --install-recommends winehq-stable`  |
| 开发分支     | `sudo apt install --install-recommends winehq-devel`   |
| Staging 分支 | `sudo apt install --install-recommends winehq-staging` |

如果 apt 提示缺少依赖，请先安装缺少的依赖，然后重复以上两步（update 和
install）。

------------------------------------------------------------------------

**如果您之前使用过来自发行版自己打包的安装包，您会发现它们和 WineHQ
提供的有以下不同：**

- 文件被安装在 /opt/wine-devel 或 /opt/wine-staging。

- 没有为 Wine 的内置程序（winecfg
  等等）创建菜单项，并且如果您是从发行版自己打包的安装包升级上来的，原来的菜单项也会被删除。您可以使用菜单编辑器自己再次创建。

- 没有添加 Binfmt_misc
  注册项。如果您想手动添加，请查看您使用的发行版关于
  [update-binfmts](https://manpages.ubuntu.com/manpages/jaunty/man8/update-binfmts.8.html)
  的文档。

- WineHQ 当前没有提供 wine-gecko 和 wine-moon 的安装包。所以当创建新的
  wine配置目录时，您将会被询问是否下载这些组建。为了得到更好的兼容性，
  我们建议您选择“安装”。如果下载过程发生出错，请查看[Gecko](Gecko) 和
  [Mono](Wine-Mono) 的 wiki 页面来进行手动安装。

- 从 Wine 5.7 开始，WineHQ 的 Debian 安装包有一个 debconf 选项用于开启
  CAP_NET_RAW 以兼容需要发送和接收 raw IP
  包的应用程序。由于具有潜在的安全风险，并且大多数应用程序不需要该功能，该功能默认被关闭。需要该功能运行应用程序的用户可以在安装
  Wine 之后运行

`dpkg-reconfigure wine-<branch>-amd64 wine-<branch> wine-<branch>-i386`

并且对接着的三个问题回答 yes 来开启 CAP_NET_RAW。（`<branch>`
请对应上文使用 devel，staging 或 stable 替换。）

------------------------------------------------------------------------

## 构建源码包

从 4.0-rc2 开始，WineHQ 的源仓库已经包含了由 OBS 生成的 .dsc、.diff.gz
和 .orig.tar.gz 等文件。这些源码包可以在 /main/source
下以您的发行版版本（ 例如： jessie、stretch 或 buster
）命名的子目录找到。

## 编译 WoW64

Debian 的 [Multiarch](Multiarch)
支持目前尚不完整，所以目前您无法简单地同时安装 32 位和 64
位库。如果您使用的不是 64
位系统，您将会需要创建一个独立的环境来安装和构建 32 位依赖。 请查看
[构建 Wine](Building-Wine) 来了解如何在 chroot 或容器中构建
Wine。

## 相关文档

- [WineHQ
  官方安装包](https://dl.winehq.org/wine-builds/).
- The [Debian Wiki's page for Wine.](https://wiki.debian.org/Wine)
- [构建 Wine](Building-Wine)
- [打包](Packaging)
