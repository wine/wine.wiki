<small>
&nbsp;[:flag_gb: English](Winetricks)
&nbsp;[:flag_fr: Français](fr/Winetricks)
&nbsp;[:flag_nl: Nederlands](nl/Winetricks)
&nbsp;[:flag_kr: 한국어](ko/Winetricks)
</small>

-----

## อะไรคือ winetricks?

**Winetricks**
คือสคริปต์ช่วยสำหรับดาวน์โหลดเเละติดตั้งรันไทม์ไลบารี่ที่จำเป็นสำหรับการรันโปรเเกรมใน
Wine. นี่อาจรวมถึงการเปลี่ยนเเปลงองค์ประกอบของ Wine ที่ใช้ไลบารี่เเบบ closed source

**โปรดจำไว้:** การใช้งาน winetricks
จะเป็นประโยชน์อย่างมากสำหรับการทำให้บางโปรเเกรมของ wine ใช้งานได้
เเต่การกระทำนั้นเป็นการจำกัดการสนับสนุนผ่าน WineHQ
โดยทั่วไปการรายงานบัคอาจเป็นไปไม่ได้ถ้าคุณเปลี่ยนเเปลงบางส่วนขอว Wine. โปรดดู
[รายงานบัคหลังจากใช้งาน Winetricks](#winetricks-and-bugs) ข้างล่าง

**โปรดจำไว้:** บางเเพ็กเกจที่ลิสไว้ข้างล่างอาจใช้ไม่ได้กับ Wine รุ่นเก่า เราเเนะนำให้ใช้
[เวอร์ชั่นใหม่สุดของ Wine](Download)

## รับ winetricks

สคริปต์นี้รักษาโดย Austin English ที่
<https://github.com/Winetricks/winetricks>. เวอร์ชั่นล่าสุดที่
<https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks>.
กดคลิกขวาที่ลิ้งเเละเลือก 'Save As' เพื่อทำการบันทึกก็อปปี้.
หรือคุณจะรับ winetricks ด้วย commandline โดยใช้คำสั่งนี้ก็ได้:

```sh
$ cd "${HOME}/Downloads"
$ wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
$ chmod +x winetricks
```

นอกจากนี้บางเเพ็กเกจของ winetricks จำเป็นต้องมีเครื่องมือภายนอกติดตั้งด้วย เช่น:
**cabextract**, **unzip**, **p7zip**, **wget** (หรือ **curl**).
หสับการรองรับ GUI ก็มี **zenity** or **kdialog**.
ผู้ใช้งาน Linux สามารถรับเครื่องมือเหล่านี้ได้ผ่านทาง distribution's package
management system.

## การใช้งาน winetricks

หลังจากที่คุณได้รับ winetricks เเล้ว คุณสามารถรันได้เพียงเเค่พิมพ์ `sh winetricks` ใน
console. เเละคุณก็สามารถใช้ `./winetricks` ได้ถ้าคุณ `chmod +x winetricks`
ก่อน. ถ้ารันโดยไม่ใช้พารามิเตอร์ winetricks จะเเสดง GUI ด้วยลิสของเเพ็กเกจที่มีอยู่
ถ้าคุณรู้ชื่อของเเพ็กเกจที่คุณอยากจะติดตั้ง คุณก็สามารถรวมมันเข้ากับ winetricks command ได้
เเละมันก็จะเริ่มต้นกระบวนการติดตั้ง สำหรับตัวอย่าง,

sh winetricks corefonts vcrun6 will install both the corefonts and
vcrun6 packages.

## การปรับเเต่ง

Version 20190912 help text:

```
การนำไปใช้: /usr/bin/winetricks [options] [command|verb|path-to-verb] ...
Executes given verbs.  เเต่ละ verb ติดตั้งเเอพพลิเคชั่นหรือเเก้ไขการตั้งค่า

การปรับเเต่ง:

    --country=CC      ตั้ง country code เป็ร CC เเละไม่ตรวจพบ IP address ของคุณ
    --force           ไม่ตรวจสอบว่าเเพ็กเกจติดตั้งเเล้ว
    --gui             เเสดง gui diagnostics เเม้กระทั่งตอนขับเคลื่อนด้วย commandline
    --isolate         ติดตั้งเเอพหรือเกมใน bottle นั่นๆ (WINEPREFIX)
    --self-update     อัพเดทเเอพพลิเคชั่นเป็นรุ่นล่าสุด
    --update-rollback ย้อนกลับอัพเดทล่าสุด
-k, --keep_isos       Cache isos (อนุญาติให้ติดตั้งภายหลังโดยไม่ใช้ disc)
    --no-clean        อย่าลบ temp directories (มีประโยชน์สำหรับการเเก้บัค)
-q, --unattended      ห้ามถามคำถาม เเค่ติดตั้งอัตโนมัติ
-r, --ddrescue        ลองอีกครั้งเมื่อ caching scratched discs
-t  --torify          รันดาวโหลดน์ภายใต้ torify ถ้ามี
    --verify          รัน (อัตโนมัติ) ทดสอบ GUI สำหรับ verbs ถ้ามี
-v, --verbose         เอคโคทุกคำสั่งที่พึ่งรันไป
-h, --help            เเสดงข้อความเเละออก
-V, --version         เเสดงเวอร์ชั่นเเละออก

คำสั่ง:
list                  ลิส categories
list-all              ลิส categories ทั้งหมดเเละ verbs ของมัน
apps list             ลิส verbs ใน category 'applications'
benchmarks list       ลิส verbs ใน category 'benchmarks'
dlls list             ลิส verbs ใน category 'dlls'
games list            ลิส verbs ใน category 'games'
settings list         ลิส verbs ใน category 'settings'
list-cached           ลิส cached-and-ready-to-install verbs
list-download         ลิส verbs ที่ดาวน์โหลดอัตโนมัติ
list-manual-download  list verbs ที่ดาวน์โหลดด้วยตัวช่วยจากผู้ใช้
list-installed        ลิส verbs ที่ติดตั้งเเล้ว
arch=32|64            สร้าง wineprefix ด้วยสถาปัตยกรรมเเบบ 32 หรือ 64 บิต การตั้งค่านี้ต้อง
                      มาก่อน prefix=foobar เเละมันจะใช้งานไม่ได้ในกรณีที่เป็น default wineprefix
prefix=foobar         เลือก WINEPREFIX=/home/$USER/.local/share/wineprefixes/foobar
annihilate            ลบ *ข้อมูลเเละเเอพพลิเคชั่นทั้งหมดภายใน* WINEPREFIX
```

**คำเเนะนำ:** ด้วยคำสั่งของ Wine ทั้งหมดนี้ winetricks
รู้เกี่ยวกับตัวเเปรสภาพเเวดล้อมของ \`WINEPREFIX\` นี่จึงเป็นประโยชน์ของการใช้
winetricks ด้วยตำเเหน่งของ Wine prefix ที่ต่างกัน ตัวอย่าง,

`env WINEPREFIX=~/.winetest sh winetricks mfc40 `

installs the mfc40 package in the \`~/.winetest\` prefix.

**คำเเนะนำ:** ผู้ใช้ที่มีเวอร์ชั่นของ Wine มากกว่าหนึ่งบนระบบ (อย่างเช่น,
การติดตั้งเเพ็กเกจเเละถอนการติดตั้ง Wine ที่สร้างจาก git) สามารถระบุได้ว่าควรใช้
winetricks เวอร์ชั่นไหน ยกตัวอย่าง,

`env WINE=~/wine-git/wine sh winetricks mfc40 `

installs the mfc40 package using the Wine in the ~/wine-git directory.

## รายงานบัคหลังจากใช้งาน Winetricks

โปรดอย่ารายงานบัคถ้าคุณใช้ winetricks ในการติดตั้ง native (เช่น อะไรที่ไม่ใช่ Wine)
ไฟล์ เพราะเราไม่สามารถสนับสนุน Microsoft dlls.

การใช้ winetricks เพื่อติดตั้ง gecko, mono, เเละ fakeie6
เป็นทางเลือกที่ยอมรับได้สำหรับการรายงานบัค

เพิ่มเติมจากนี้ ถ้าคุณพบว่ามันจำเป็นที่จะใช้ winetricks สำหรับเเอพพลิเคชั่น
โปรดระบุด้วยต้องรายงานไปยัง AppDB, mailing lists, เเละทรัพยากรอื่นๆ ของ Wine

## รายงานบัค \*ใน\* Winetricks

Winetricks มีระบบติดตามบัคที่
<https://github.com/Winetricks/winetricks/issues>, โปรดใช้มัน.
ถ้าคุณไม่ต้องการสมัครบัญชีที่นั่นเพื่อตามหบัค ก็ให้โพสต์ที่ wine forum ของผู้ใช้

## ลบสิ่งที่ติดตั้งโดย Winetricks

มันง่ายที่จะติดตั้ง wineprefix ทั้งหมด โดยปกติ winetricks ติดตั้งเเต่ละเเอพไว้ใน Wine
prefix ของมัน เเละเสนอวิธีที่จะลบ wineprefixes เเละ menu items ที่มันสร้าง.

Winetricks ไม่ได้เสนอทางสำหรับถอนการติดตั้งเเอพหรือ DLLs บางอย่างภายใน Wine
prefix.

ถ้าเกิดหตุบางอย่าง คุณคงไม่อยากเล่นกับ Wine prefixes ทั้งหมด Wine มาพร้อมกับโปรเเกรม
[ถอนการติดตั้ง](Commands/uninstaller) ภายใน. เหมือนกับที่ Windows "Add/Remove
Programs" มี มันจะตรวจพบสำหรับโปรเเกรมที่ติดตั้งอย่างในตัวติดตั้ง Windows ที่ขึ้นตรงกับ
registry เช่น InstallShield หรือ WISE.
ทั้งนี้ไม่มีการการันตีได้ว่ามันจะทำงานได้กับโปรเเกรมที่ติดตั้งโดย Winetricks
หรือตัวติดตั้งอื่นอย่างเเพ็กเกจ *.msi*

## ติดตั้ง winetricks

มันไม่จำเป็นต้องติดตั้ง winetricks เพื่อที่จะใช้มัน. คุณสามารถเลือกที่จะติดตั้ง winetricks
ในพื้นที่โกโบล์ได้เพียงเเค่พิมพ์ `winetricks` บน command line. บาง Linux
distributions มาพร้อมกับ winetricks ใน Wine เเพ็กเกจอยู่เเล้ว
ดังนั้นคุณไม่ต้องดาวน์โหลดเเยกต่างหาก คุณอาจจะอยากทำตามขั้นตอนถ้า distributions
เเพ็กเกจมี winetricks ที่ล่าสมัย (เช่น ผู้ใช้ Debian/Ubuntu).

สำหรับดาวโหลดน์เเละติดตั้งก็อปปี้ winetricks คุณสามารถติดตั้งได้ด้วยตัวเอง:

```sh
$ cd "${HOME}/Downloads"
$ wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
$ chmod +x winetricks
$ sudo cp winetricks /usr/local/bin
```

สำหรับดาวโหลดน์เเละติดตั้ง(เเยก) BASH completion script สำหรับ winetricks:

```sh
$ cd "${HOME}/Downloads"
$ wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks.bash-completion
$ sudo cp winetricks.bash-completion /usr/share/bash-completion/completions/winetricks  # โลเคชั่นมาตรฐานของ BASH completion scripts (Arch, Gentoo, OpenSUSE, Fedora, Debian/Ubuntu, Solus)
```

## ดูเพิ่มเติม

- <https://www.cabextract.org.uk/> -- cabextract คือเครื่องมือสำหรับเเยก MS
  cabinet ไฟล์ ภายใต้สภาพเเวดล้อมของ Unix
