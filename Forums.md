---
title: Forums and Mailing Lists
---

<small>
&nbsp;[:flag_fr: Français](fr/Forums)
&nbsp;[:flag_pt: Português](pt/Forums)
&nbsp;[:flag_nl: Nederlands](nl/Forums)
&nbsp;[:flag_pl: Polski](pl/Forums)
&nbsp;[:flag_tr: Türkçe](tr/Forums)
&nbsp;[:flag_kr: 한국어](ko/Forums)
&nbsp;[:flag_cn: 简体中文](zh_CN/Forums)
</small>

-----


## WineHQ Forums

Wine has many mailing lists and forums. Here are the most useful ones
for users:

- [WineHQ Forums](https://forum.winehq.org)
- [Wine area at Ubuntu Forums](https://ubuntuforums.org/forumdisplay.php?f=313)

If you know of another active Wine forum - especially a non-English
one - please [let us know](mailto:web-admin@winehq.org) so we can add it
to the list.

:information_source: If you only want to be notified about new releases, subscribe to the
[wine-announce](https://list.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/)
mailing list.

## WineHQ Mailing Lists

WineHQ has mailing lists for submitting patches, tracking Git commits,
and discussing Wine development.

:information_source: You need to subscribe to the lists before you can
post to them, otherwise your posting will be discarded. You can set
your subscription to not send you email, or send as a digest.

### User Mailing Lists

- **<wine-announce@winehq.org>**\
    \[[(Un-)Subscribe](https://list.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/)\]
    \[[Archive](https://list.winehq.org/mailman3/hyperkitty/list/wine-announce@winehq.org/)\]\
    A low traffic (2/month) read-only list for announcing releases and
    other major Wine or WineHQ news.
- **<wine-zh@freelists.org>**\
    \[[(Un-)Subscribe](https://www.freelists.org/list/wine-zh)\]
    \[[Archive](https://www.freelists.org/archive/wine-zh/)\]\
    A low traffic (10/day) open list for end-user discussions in
    Chinese.

### Developer Mailing Lists

- **<wine-devel@winehq.org>**\
    \[[(Un-)Subscribe](https://list.winehq.org/mailman3/postorius/lists/wine-devel.winehq.org/)\]
    \[[Archive](https://list.winehq.org/mailman3/hyperkitty/list/wine-devel@winehq.org/)\]\
    A medium traffic (50/day) open list for discussing Wine development,
    WineHQ, patches or anything else of interest to Wine developers.
- **<wine-gitlab@winehq.org>**\
    \[[(Un-)Subscribe](https://list.winehq.org/mailman3/postorius/lists/wine-gitlab.winehq.org/)\]
    \[[Archive](https://list.winehq.org/mailman3/hyperkitty/list/wine-gitlab@winehq.org/)\]\
    A medium traffic (50/day) read-only list for patches and merge
    requests from [Wine Gitlab](https://gitlab.winehq.org/).
- **<wine-commits@winehq.org>**\
    \[[(Un-)Subscribe](https://list.winehq.org/mailman3/postorius/lists/wine-commits.winehq.org/)\]
    \[[Archive](https://list.winehq.org/mailman3/hyperkitty/list/wine-commits@winehq.org/)\]\
    A medium traffic (25/day) read-only list which tracks commits to the
    Git tree.
- **<wine-releases@winehq.org>**\
    \[[(Un-)Subscribe](https://list.winehq.org/mailman3/postorius/lists/wine-releases.winehq.org/)\]
    \[[Archive](https://list.winehq.org/mailman3/hyperkitty/list/wine-releases@winehq.org/)\]\
    A low traffic (2/month) read-only list for receiving a large diff
    file at every Wine official release.
- **<wine-bugs@winehq.org>**\
    \[[(Un-)Subscribe](https://list.winehq.org/mailman3/postorius/lists/wine-bugs.winehq.org/)\]
    \[[Archive](https://list.winehq.org/mailman3/hyperkitty/list/wine-bugs@winehq.org/)\]\
    A high traffic (100/day) read-only list for monitoring activity on
    the [Bug Tracking Database](https://bugs.winehq.org/).
