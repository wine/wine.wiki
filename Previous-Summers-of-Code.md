## Summer of Code 2020

Sadly no projects in 2020

## Summer of Code 2019

Sadly no projects in 2019

## Summer of Code 2018

The following projects was accepted by GSoC 2018:

- Dimitris Gounaridis mentored by Aaryaman Vasishta and Stefan Dösinger
  [Direct3D - Automated game
  benchmarks](https://summerofcode.withgoogle.com/archive/2018/projects/6255317272756224/)
- Meng Hua mentored by Alex Henrie and Piotr Caban [Implementing a
  subset of Concurrency
  namespace](https://summerofcode.withgoogle.com/archive/2018/projects/6520878254784512/)

## Summer of Code 2017

The following project was accepted by GSoC 2017:

- Artur Swigon mentored by Nikolay Sivov: [Unicode string
  normalisation](https://summerofcode.withgoogle.com/dashboard/organization/5682280509997056/proposal/6408324076535808/)

## Summer of Code 2016

The following projects were accepted by GSoC 2016:

- Aaryaman Vasishta mentored by Stefan Dösinger and André Hentschel:
  [Implement basic rendering for Direct3D Retained
  mode](https://summerofcode.withgoogle.com/dashboard/project/6367022048870400/overview/)
- Iván Matellanes mentored by Piotr Caban: [Further work on the Visual
  C++ iostream
  library](https://summerofcode.withgoogle.com/dashboard/project/6054761128263680/overview/)
- Sergei Bolotov mentored by Michael Müller: [Winebuilder - enhance MIME
  type
  handling](https://summerofcode.withgoogle.com/dashboard/project/6517339138818048/overview/)

## Summer of Code 2015

The following projects were accepted by GSoC 2015:

- Aaryaman Vasishta mentored by Stefan Dösinger and André Hentschel:
  [Direct3DRM - Implement rendering backend for
  D3DRM](https://www.google-melange.com/archive/gsoc/2015/orgs/wine/projects/jam.html)
- Iván Matellanes mentored by Piotr Caban: [Implement the Visual C++
  iostream
  library](https://www.google-melange.com/archive/gsoc/2015/orgs/wine/projects/sulley.html)
- YongHaoHu mentored by Sebastian Lackner: [Implementing functions from
  tr2
  namespace](https://www.google-melange.com/archive/gsoc/2015/orgs/wine/projects/yonghaohu.html)
- Zhenbo Li mentored by Jacek Caban: [Improve
  mshtml.dll](https://www.google-melange.com/archive/gsoc/2015/orgs/wine/projects/zhenbo.html)

## Summer of Code 2014

The following projects were accepted by GSoC 2014:

- Jactry Zeng mentored by Huw Davies and André Hentschel: [Implement
  some features for Text Object
  Model](https://www.google-melange.com/gsoc/project/details/google/gsoc2014/jactry/5759800252039168)
- Shuai Meng mentored by Piotr Caban and Detlef Riekenberg: [Implement
  VBScript built-in
  functions](https://www.google-melange.com/gsoc/project/details/google/gsoc2014/mscool/5750085036015616)
- 李臻博 mentored by Jacek Caban and Ulrich Czekalla: [Implemention
  for
  mshtml.dll](https://www.google-melange.com/gsoc/project/details/google/gsoc2014/zhenbo/5676830073815040)

## Summer of Code 2013

The following projects were accepted by GSoC 2013:

- Jactry Zeng mentored by Huw Davies: [Implement ITextDocument in
  Richedit](https://www.google-melange.com/gsoc/project/details/google/gsoc2013/jactry/5704420943724544)
- John Chadwick mentored by Nikolay Sivov: [MSXML - Implement MSXML
  without
  libxml2](https://www.google-melange.com/gsoc/project/details/google/gsoc2013/jchadwick/5818821692620800)
- George Stephanos mentored by Detlef Riekenberg and André Hentschel:
  [Registry - implement merging between HKCR and
  HKCU\Software\Classes](https://www.google-melange.com/gsoc/project/details/google/gsoc2013/jacktheripper/5698390809640960)

## Summer of Code 2012

The following projects were accepted by GSoC 2012:

- Józef Kucia mentored by Stefan Dösinger: [Implement missing
  functions in
  D3DX9](https://www.google-melange.com/gsoc/project/details/google/gsoc2012/jos/5676830073815040)
- Lucas Fialho Zawacki mentored by Marcus Meissner: [Joystick
  configuration and testing
  applet](https://www.google-melange.com/gsoc/project/details/google/gsoc2012/lfzawacki/5685265389584384)
- Magdalena Nowak mentored by Owen Rudge: [Control
  Panel](https://www.google-melange.com/gsoc/project/details/google/gsoc2012/magdalena/5668600916475904)
- Marek K Chmiel mentored by Juan Lang: [Implementing a DSS
  provider](https://www.google-melange.com/gsoc/project/details/google/gsoc2012/mkchmiel/5668600916475904)
- Qian Hong mentored by Aric Stewart: [Improve CJK font
  support](https://www.google-melange.com/gsoc/project/details/google/gsoc2012/fracting/5668600916475904)

## Summer of Code 2011

The following projects were accepted by GSoC 2011:

- Jay Yang mentored by Owen Rudge: [Implement the
  Explorer](https://www.google-melange.com/gsoc/project/details/google/gsoc2011/yangjay/5649050225344512).
- Lucas Fialho Zawacki mentored by Marcus Meissner: [Implementation of
  DirectInput8 Action Mapping
  feature](https://www.google-melange.com/gsoc/project/details/google/gsoc2011/lfzawacki/5724160613416960)
  and Lucas [status
  page](https://lfzawacki.heroku.com/wine/published/HomePage).
- Michael Mc Donnell mentored by Stefan Dösinger: [Implement Missing
  Mesh Functions in Wine’s
  D3DX9](https://www.google-melange.com/gsoc/project/details/google/gsoc2011/mmd/5668600916475904)
- Mariusz Pluciński mentored by Vincent Povirk: [Extending gameux.dll
  by Games Explorer Shell
  Extension](https://www.google-melange.com/gsoc/project/details/google/gsoc2011/vshader/5717271485874176)

## Summer of Code 2010

The following four projects were accepted by GSoC 2010:

- Alexander Soernes, mentored by Maarten Lankhorst: [Improving Wine's
  Internet Explorer
  GUI](https://www.google-melange.com/gsoc/project/details/google/gsoc2010/emptydoor/5668600916475904)
- David Hedberg, mentored by Eric Durbin: [Implementing
  IExplorerBrowser](https://www.google-melange.com/gsoc/project/details/google/gsoc2010/davidh/5741031244955648)
- Mariusz Plucinski, mentored by Vincent Povirk: [Implementation of
  Windows Game Explorer
  equivalent](https://www.google-melange.com/gsoc/project/details/google/gsoc2010/vshader/5639274879778816)
- Thomas Mullaly, mentored by Jacek Caban: [Implementing the IUri
  interface](https://www.google-melange.com/gsoc/project/details/google/gsoc2010/thomas_mullaly/5668600916475904)

## Summer of Code 2009

The following projects were accepted for GSoC 2009:

- [Wine Application Test
  Suite](https://www.google-melange.com/gsoc/project/details/google/gsoc2009/austinenglish/5741031244955648)
  - by Austin English
  - mentored by James Hawkins
  - Wiki page: [Appinstall](Appinstall)
  - code done for SoC:
    [1](https://google-summer-of-code-2009-wine.googlecode.com/files/AustinMatthew_English.tar.gz)
  - current code:
    [2](https://code.google.com/p/winezeug/source/browse/trunk/appinstall)

- [Improving JScript
  implementation](https://www.google-melange.com/gsoc/project/details/google/gsoc2009/piotr_caban/5649050225344512)
  - by Piotr Caban
  - mentored by Marcus Meissner
  - code done for SoC:
    [3](https://google-summer-of-code-2009-wine.googlecode.com/files/Piotr_Caban.tar.gz)
  - [List of patches in Wine by Piotr
    Caban](https://gitlab.winehq.org/wine/wine/-/commits/master?author=Piotr%20Caban)

- [Direct3D - Implement D3DXAssembleShader
  function](https://www.google-melange.com/gsoc/project/details/google/gsoc2009/mystral/5724160613416960)
  - by Matteo Bruni
  - mentored by Stefan Dösinger
  - code done for SoC:
    [4](https://google-summer-of-code-2009-wine.googlecode.com/files/Matteo_Bruni.tar.gz)

- [DirectShow/GStreamer
  Bridge](https://www.google-melange.com/gsoc/project/details/google/gsoc2009/tdaven/5668600916475904)
  - by Trevor Davenport
  - mentored by Edward Hervey
  - Wiki page: GstreamerSummerOfCode2009
  - code done for SoC:
    [5](https://google-summer-of-code-2009-wine.googlecode.com/files/Trevor_Davenport.tar.gz)
  - [status at end of
    summer](https://www.winehq.org/pipermail/wine-devel/2009-August/078105.html)
  - current code: <https://github.com/tdaven>

- [Implement Texture, Mesh and Font handling in D3DX and get some
  DirectX SDK samples running in
  Wine](https://www.google-melange.com/gsoc/project/details/google/gsoc2009/bigbrain/5668600916475904)
  - by Tony Wasserka
  - mentored by Roderick Colenbrander
  - code done for SoC:
    [6](https://google-summer-of-code-2009-wine.googlecode.com/files/Tony_Wasserka.tar.gz)
  - [List of patches in Wine by Tony
    Wasserka](https://gitlab.winehq.org/wine/wine/-/commits/master?author=Tony%20Wasserka)

## Summer of Code 2008

For 2008, six Wine proposals were accepted:

- "Improving Wine MSXML implementation" by Piotr Caban, mentored by
  James Hawkins
- "Print Dialog" by Gal Topper, mentored by Detlef Riekenberg
- "Implementing proper Control Panel support for Wine" by Owen Rudge,
  mentored by Juan Lang
- "DirectPlay implementation" by Ismael Barros Barros, mentored by Kai
  Blin
- "Wine - Richedit with Tables" by Dylan Andrew Harper Smith, mentored
  by Huw D M Davies
- "Improve Gdi+ Implementation" by Adam Joel Petaccia, mentored by Lei
  Zhang

## Summer of Code 2007

Google accepted ten Wine proposals in 2007:

- "Improve sound in wine" by Maarten Lankhorst, mentored by Marcus
  Meissner
- "Improve Wine's rich edit implementation" by Matthew Finnicum,
  mentored by Ulrich Czekalla
- "The DIB Engine" by Jessie Laine Allen, mentored by Huw D M Davies
- "Implementing mscoree.dll and Mono-Wine bridge" by
  Bryan DeGrendel, mentored by James Hawkins
- "Tablet PC support in Wine" by Carl John Klehm, mentored by Daniel
  Kegel
- "Beginning of Direct3D10 implementation" by András Kovács, mentored
  by Stefan Dösinger
- "Improve Wine's built-in text editors" by Alexander Nicolaysen Sørnes,
  mentored by Eric Pouech
- "Windows Printing subsystem bridge (i.e. use WIN32 drivers to print
  from wine)" by Marcel Partap, mentored by Detlef Riekenberg
- "CHM compiler" by Miikka Viljanen, mentored by Jacek Caban

[Successful 2007 final project
submissions](https://code.google.com/p/google-summer-of-code-2007-wine/downloads/list)

## Summer of Code 2006

Google funded seven Wine proposals in 2006:

- Add OleView, by Piotr Caban, mentored by James Hawkins
- Improve RichEd20, by Matthew Finnicum, mentored by Mike Hearn
- Add ClamAntiVirusIntegration, by Christoph Probst, mentored by Marcus Meissner
- Improve MsWsock, by Łukas Chróst, mentored by Eric Pouech
- Add ShellIntegration, by Mikołai Zalewski, mentored by Ulrich
  Czekalla
- Add NtlmSigningAndSealing using GENSEC, by Kai Blin, mentored by
  Juan Lang
- Add Mp3SupportInDirectShow, by Sagar Mittal, mentored by Rob Sherman

## Summer of Code 2005

In 2005, four Summer of Code proposals related to Wine were successful:

- Mozilla Integration: Jacek Caban
- Theming Support: Frank Richter
- Single Sign-on: Kai Blin
- Force Feedback: Daniel Remenak

Jacek's project was so successful he now has a job enhancing Wine.
