---
title: Nouvelles
---

<small>
&nbsp;[:flag_gb: English](News)
</small>

-----

## Sur WineHQ

Si vous êtes intéressé par des nouvelles récentes ou archivées à propos
du projet Wine, il y a différentes sources sur le site internet. Vous
pouvez lire ou souscrire au [flux RSS des nouvelles de
WineHQ](https://www.winehq.org/news) pour vous tenir informer des
nouvelles versions et changements majeurs.

La newsletter [Wine Weekly News (WWN)](https://www.winehq.org/wwn) vous
donne une meilleure image de l'avancement jour par jour de Wine et de ce
qui se passe dans la communauté.

Si vous voulez des sources encore plus primaires (jeux de mots
intentionnel), vous pouvez naviguer dans le [code source du projet
Wine](https://gitlab.winehq.org/wine/wine//) ou télécharger des dump
des base de donnés de l'AppBD ou du Bugzilla depuis le [site
publique](https://dl.winehq.org/wine/). Un aperçu plus quantifié du
statut du développement de Wine peut éventuellement être déplacer dans
le wiki, mais peut actuellement être [trouvé ici](Wine-Status).

## Revues & articles

Si vous tombez sur un article récent, bien écrit sur Wine, vous êtes
libre de placer un lien (ou une citation si ce n'est en ligne) ici :

## Autres mentions

De plus en plus de personnes utilisent Wine pour sa compatibilité avec
Windows, vous pouvez trouver des discussions dans d'autres sources comme
des livres. Vous pouvez les mentionner ici, surtout si elles ont une
bonne explication sur l'une des fonctionnalités ou comment utiliser Wine.
