---
title: Installation et configuration de Wine
---

<small>
&nbsp;[:flag_gb: English](Wine-Installation-and-Configuration)
</small>

-----

## Installer Wine

Avant d'installer Wine, assurez-vous qu'il n'y a pas d'ancienne version
de Wine dans votre système, que ce soit depuis un paquet ou les sources.
Si vous n'avez pas encore installé Wine, vous devriez être tranquille.
Certaines distributions Linux distribuent des paquets Wine, mais à cause
du développement rapide de Wine ils sont généralement vieux et souvent
cassés. Le mieux est de désinstaller les paquets de votre distribution
et d'utiliser les dernières versions de Wine disponibles ici.

Des liens vers les paquets binaires pour la plupart des distributions
majeures peuvent être trouvés à la [page de téléchargement
WineHQ](Download). Le code source complet pour la version en cours de
développement et chaque version de Wine est également disponible
[ici](Download#wine-source-downloads). Pour obtenir de l'aide lors de
l'installation depuis un paquet ou les sources, consultez le chapitre
[Obtenir Wine](Wine-User's-Guide#getting-wine) du Guide de
l'Utilisateur.

## Comment aider à faire fonctionner des applications dans Wine

Si vous voulez aider à faire fonctionner des applications dans Wine, la
première chose que vous devriez faire est de vous enregistrez dans
l'[AppDB](https://appdb.winehq.org/) et de remplir un rapport de test,
comme ça les autres utilisateurs savent ce qui marche et ne marche pas.
Aussi, pensez à voter pour votre application préférée, ainsi les
développeurs sauront où concentrer leurs efforts.

Si l'application que vous souhaiteriez faire fonctionner n'est pas listé
dans l'[AppDB](https://appdb.winehq.org/), vous avez un formulaire simple
pour l'ajouter. Si votre application est dans l'AppDB, mais n'a pas de
mainteneur, vous devriez réfléchir à devenir volontaire. Si vous êtes
familier avec Wine, avez une copie légale de l'application et souhaitez
la tester, la mettre et la maintenir en fonctionnement et aider les
autres utilisateurs, faites le en cliquant sur le lien dans la page de
l'application. Chaque application devrait avoir un super mainteneur, et
si chaque version d'une application sont substantiellement différentes
(comme Adobe Creative Suite), chaque sous-version devrait avoir un
mainteneur.

Si vous êtes le développeur ou l'éditeur de l'application, vous avez
bien sûr un énorme intérêt d'avoir votre application qui fonctionne dans
Wine. Heureusement, il y a plusieurs options possibles autres que
rapporter des bugs et espérer que quelqu'un les résolvent. La méthode la
plus facile est de loin d'enregistrer un bug dans le
[Bugzilla](https://bugs.winehq.org/), avec un petit test à ajouter dans
la [suite de test](Conformance-Tests) de Wine. Une autre
option est d'envoyer des copies de votre logiciel aux développeurs de
Wine et espérer qu'ils prennent un intérêt à le faire fonctionner. Une
autre option, peut-être plus efficace, bien que payante, est de payer
les développeurs Wine pour qu'ils fassent travaillent sur votre
application, soit directement en négociant un contrat ou indirectement
en offrant une prime. CodeWeavers, un développeur majeur de Wine, offre
une section spéciale pour les offres dans leur [compatibility
center](https://www.codeweavers.com/compatibility/). Toutefois la méthode
la plus directe, est d'aider le développement de Wine lui-même en
contribuant directement au code, c'est exactement ce qu'a fait Corel
pour WordPerfect il y a quelque années. Dans tout les cas, faire un
post dans la [liste de mailing](fr/Forums) des
développeurs Wine peut allez loin.

## Si votre application ne fonctionne pas

Si votre application rencontre des problèmes dans un endroit
particulier, ou n'arrive pas du tout à démarrer, il y a des choses que
vous pouvez faire nous aider. La chose la plus importante est de trouver
où exactement l'application plante. Pour diagnostiquer le problème, la
première chose à faire est de lancer le programme depuis une console
avec Wine, plutôt que d'utiliser le raccourci d'une GUI. Cela permet à
Wine d'afficher les messages d'erreur dans la console, leur
compréhension est la clé pour résoudre le problème et faire fonctionner
l'application. Pour aller plus loin dans la configuration.

Une application peut ne pas fonctionner parce que Wine n'implémente
pas l'une des DLL que l'application essaie d'utiliser. Si vous
rencontrer une erreur "DLL not found", ou voyez beaucoup de messages
"FIXME:" lorsque vous lancer l'application dans Wine, c'est
certainement le cas.  Quand ça arrive, vous pouvez essayer d'utiliser
des DLL natives (non Wine) au lieu de ceux inclus dans Wine. Vérifier
la page AppDB du programme. Il peut y avoir une configuration ou des
instructions spéciales pour installer une DLL native afin de faire
fonctionner l'application. Pour allez plus loin, lisez la section
[Lancer Wine](Wine-User's-Guide#running-wine) du Guide de
l'Utilisateur.

Si l'application ne fonctionne toujours pas, c'est probablement due à un
bug ou une déficience de Wine et nous aimerions le savoir. Lisez la page
[rapporter un bug](Bugs) pour connaître la meilleure méthode
pour rapporter un bug dans les applications. Autrement, si vous êtes un
programmeur, nous aimerions vraiment que vous essayez de nous aider
directement ; veuillez prendre connaissance de la page
[Développeurs](Developers) et l'[Aide du
développeur](Developer-Hints) si vous êtes intéressé.

## Si votre application fonctionne, mais avec difficulté

Parfois, les applications tournent sous Wine mais pas avec la fluidité
qu'ils ont sous Windows. Elles peuvent aussi afficher des erreurs, une
fonctionnalité peut ne pas fonctionner ou est inhabituellement lente.
Ces applications devrait recevoir une faible note de part des
mainteneurs ("bronze" ou "garbage") dans l'AppDB, selon le degrés de
difficulté rencontrer.

Si vous avez trouvé un moyen de faire fonctionner une application autre
que juste l'installer, merci de partager cette information en la postant
sur la page de l'application dans l'AppDB. Si vous êtes le mainteneur de
cette application, mettez les instructions dans un "tutoriel" qui va
apparaître en haut de la page de l'application.

## Si votre application a fonctionné, mais ne fonctionne plus dans une nouvelle version de Wine

Wine est un projet large et complexe, composé de plusieurs fichiers
écrit par des auteurs différents. Parfois, une tentative de changement
d'un fichier pour étendre le support à une autre application va
involontairement empêcher une autre application de fonctionner. Ces
changement sont connus sous le nom de régression, et ils sont
malheureusement parfois introduits dans le code source de Wine parce que
les auteurs d'un patch ne sont pas au courant de la régression qu'elle
cause. Les développeurs Wine ne peuvent pas tester toutes les
applications avec chaque patch, nous comptons sur la communauté pour
nous informer de ces régressions ainsi ce problème peut facilement être
identifié et ensuite corrigé. Sans la participation de la communauté,
des régressions peuvent rester comme tel pendant une très longue
période.

Si votre application subit une régression, faites des tests,
transmettez-nous le plus d'informations possibles sur quand et comment
votre application est affecté. Cela nous permet d'isoler exactement le
morceau de code qui bug et de fournir un correctif. Indiquer également
ce que vous savez sur les versions de Wine qui ont fonctionné et quelle
version ne fonctionne pas, indiquer le numéro de version et comment vous
avez installé Wine (depuis les sources, paquets binaires, etc). Pour
finir, postez ces élément dans un bug.

Si possible, vous devriez également essayer d'isoler le patch exact qui
brise votre application. Cela prend un peu de temps, mais avec un
minimum d'efforts et de compétences informatiques, c'est le meilleure
moyen de voir votre application fonctionner de nouveau. Quand il est
temps de corriger la régression, la seule chose plus utile aux
développeurs Wine que de connaître le patch qui a causé la régression,
est un correctif pour le patch lui-même. Pour plus d'aide pour isoler le
problème des patchs, lisez la documentation sur les [tests de
régressions](Regression-Testing).
