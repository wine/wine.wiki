<small>
&nbsp;[:flag_gb: English](Documentation)
&nbsp;[:flag_de: Deutsch](de/Documentation)
&nbsp;[:flag_nl: Nederlands](nl/Documentation)
&nbsp;[:flag_pl: Polski](pl/Documentation)
&nbsp;[:flag_tr: Türkçe](tr/Documentation)
&nbsp;[:flag_kr: 한국어](ko/Documentation)
</small>

-----

## Guides

- **[Guide de l'utilisateur de Wine](Wine-User's-Guide)**

  Comment utiliser et configurer Wine pour exécuter des applications Windows.

- **[Guide de l'utilisateur de Winelib](Winelib-User's-Guide)**

  Comment utiliser Wine pour porter des applications Windows sous Linux.

- **[Guide du développeur Wine](Wine-Developer's-Guide)**

  Comment devenir un développeur Wine.

- **[Installation et configuration de Wine](fr/Wine-Installation-and-Configuration)**

  Un petit guide pour ceux qui veulent donner un coup de main.

## Command reference

- **[Commands](Commands)**

  Documentation about the commands available with Wine.

- **[Pages de manuel](fr/Man-Pages)**

  Manual pages for the Wine commands and tools.
