---
title: Listes de diffusion principales / Forums
---

<small>
&nbsp;[:flag_gb: English](Forums)
&nbsp;[:flag_pt: Português](pt/Forums)
&nbsp;[:flag_nl: Nederlands](nl/Forums)
&nbsp;[:flag_pl: Polski](pl/Forums)
&nbsp;[:flag_tr: Türkçe](tr/Forums)
&nbsp;[:flag_kr: 한국어](ko/Forums)
&nbsp;[:flag_cn: 简体中文](zh_CN/Forums)
</small>

-----


## Forums

Wine propose divers listes de diffusion et forums. Voici les plus utiles
pour les utilisateurs :

- Les [forums WineHQ](https://forum.winehq.org)
- Les utilisateurs Ubuntu peuvent également visiter l\'[espace Wine
  des forums Ubuntu](https://ubuntuforums.org/forumdisplay.php?f=313)

Si vous connaissez un autre forum Wine actif (en particulier un
non-anglophone), merci de [nous prévenir](mailto:web-admin@winehq.org)
afin de pouvoir l'ajouter à la liste.

:information_source: Si vous voulez être prévenu(e) des nouvelles
versions, inscrivez-vous à la [liste de diffusion
Wine-Announce](https://list.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/).

## Toutes les listes WineHQ

WineHQ dispose de listes pour soumettre des patchs, suivre des commits
Git, et discuter sur Wine.

:information_source: Il est nécessaire d'être inscrit pour pouvoir
poster, ou votre message sera considéré comme un pourriel potentiel
par le logiciel de gestion de la liste.

- **<wine-announce@winehq.org>**\
    \[[(Dés-)inscription](https://list.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/)\]
    \[[Archive](https://list.winehq.org/mailman3/hyperkitty/list/wine-announce@winehq.org/)\]\
    Une liste bas trafic (2 messages par mois) en lecture seule
    annonçant les nouvelles versions et d\'autres nouvelles majeures sur
    Wine ou WineHQ.
- **<wine-devel@winehq.org>**\
    \[[(Dés-)inscription](https://list.winehq.org/mailman3/postorius/lists/wine-devel.winehq.org/)\]
    \[[Archive](https://list.winehq.org/mailman3/hyperkitty/list/wine-devel@winehq.org/)\]\
    Une liste moyen trafic (50 messages par jour) pour la discussion sur
    le développement de Wine, les patchs ou tout autre élément
    intéressant pour les développeurs Wine.
- **<wine-commits@winehq.org>**\
    \[[(Dés-)inscription](https://list.winehq.org/mailman3/postorius/lists/wine-commits.winehq.org/)\]
    \[[Archive](https://list.winehq.org/mailman3/hyperkitty/list/wine-commits@winehq.org/)\]\
    Une liste moyen trafic (25 messages par jour) en lecture seule pour
    le suivi des commits dans Git.
- **<wine-releases@winehq.org>**\
    \[[(Dés-)inscription](https://list.winehq.org/mailman3/postorius/lists/wine-releases.winehq.org/)\]
    \[[Archive](https://list.winehq.org/mailman3/hyperkitty/list/wine-releases@winehq.org/)\]\
    Une liste bas trafic (2 messages par mois) en lecture seule pour la
    réception de fichiers diff conséquents lors de la sortie d\'une
    version officielle de Wine.
- **<wine-bugs@winehq.org>**\
    \[[(Dés-)inscription](https://list.winehq.org/mailman3/postorius/lists/wine-bugs.winehq.org/)\]
    \[[Archive](https://list.winehq.org/mailman3/hyperkitty/list/wine-bugs@winehq.org/)\]\
    Une liste haut trafic (100 messages par jour) en lecture seule pour
    la surveillances des activités sur la [base de données de suivi de
    bogues](https://bugs.winehq.org/).
