---
title: Wine
---

<small>
&nbsp;[:flag_gb: English](home)
&nbsp;[:flag_de: Deutsch](de/home)
&nbsp;[:flag_nl: Nederlands](nl/home)
&nbsp;[:flag_kr: 한국어](ko/home)
&nbsp;[:flag_cn: 简体中文](zh_CN/home)
</small>

-----

[Wine](https://winehq.org) permet aux utilisateurs Linux, Mac, FreeBSD
et Solaris d'exécuter des applications Windows sans nécessiter de
copie de Microsoft Windows. Wine est un [logiciel
libre](https://www.gnu.org/philosophy/free-sw.fr.html) en
développement permanent. [D'autres plates-formes](Compatibility)
peuvent également en bénéficier.

## Liens les plus populaires

- [Foire Aux Questions](fr/FAQ) : si vous avez un
  problème général avec Wine, veuillez lire la FAQ !

- [Documentation de Wine](fr/Documentation) : si la FAQ
  n'a pas aidé, lisez le manuel ! :smiley:

- [Base de données d'applications de Wine](https://appdb.winehq.org) :
  pour de l'aide sur une application particulière, recherchez son entrée
  dans la base de données d'applications (AppDB).

- [Forum de support des utilisateurs de
  Wine](https://forum.winehq.org/) : pour les questions auxquelles vous
  ne trouvez pas de réponse plus haut.

- [Problèmes connus](Known-Issues) : à consulter avant de
  [rapporter un bug](Bugs).

- [winetricks](fr/Winetricks) : un outil utile pour utiliser
  divers contournements de défectuosités actuelles dans Wine.

- [macOS](fr/MacOS) : exécuter Wine sous Apple macOS.

## À propos du projet Wine

- Un bref aperçu des buts et [fonctionnalités de
  Wine](fr/Wine-Features).

- Quelques courts articles sur l'[importance de
  Wine](Importance-Of-Wine).

- Une vue d'ensemble de l'[organisation du
  projet](Project-Organization) Wine.

- [Qui est qui](Who's-Who) dans le projet Wine, et des
  [remerciements](Acknowledgements) pour les contributeurs majeurs.

- L'[historique](Wine-History) du projet Wine et les
  [nouvelles](fr/News) récentes.

- Informations sur la [licence](Licensing) de Wine.

## Contribuer

- [Développement](Developers) : si vous voulez améliorer Wine
  ou le construire à partir des sources.

- [Mainteneurs de l'AppDB](https://gitlab.winehq.org/winehq/appdb/-/wikis/Maintainer-Guidelines) : quelques
  conseils si vous êtes (ou souhaitez devenir) un mainteneur de la base
  de données d'applications.

- Les [débogueurs](Bugs) peuvent aider en testant Wine et en
  précisant les problèmes, ou en participant au tri des bugs signalés
  par d'autres utilisateurs.

- Les [auteurs](Writers) peuvent contribuer en documentant le
  programme, maintenant le wiki ou traduisant diverses parties du
  projet.

- Les [designers](Designers) peuvent, entres autres, dessiner
  des icônes pour des programmes ou améliorer le style et les
  fonctionnalités du site web.

- [Fonds de développement de Wine](https://www.winehq.org/donate) : merci
  de penser à effectuer un don pour supporter le projet Wine.

## Autres liens utiles

- [Liste des commandes](Commands) pour tous les petits outils venant
  avec Wine.

- [Applications tierces](Third-Party-Applications) et outils « non
  officiels » pouvant être pratiques.

- [Clés de registre utiles](Useful-Registry-Keys) et divers paramètres
  de configuration de Wine.

- Instructions pour les [tests de régression](Regression-Testing).

- [Applications qui testent officiellement Wine comme
  plate-forme](Apps-That-Support-Wine).

## Informations additionnelles

- Le [code source](Source-Code) du projet Wine peut être
  téléchargé ou parcouru en ligne.

- Vous pouvez consulter les discussions archivées ou vous abonner aux
  [listes de diffusion](fr/Forums) de Wine.

- Informations sur les progrès du développement et l'[état de
  Wine](Wine-Status).

- Assorted files such as site icons and images, and database dumps of
  Bugzilla, AppDB, and the wiki can be downloaded from WineHQ's
  [download server](https://dl.winehq.org/wine/).

- View [WineHQ site
  statistics](https://www.winehq.org/webalizer/index.html) and rough
  estimates of Wine's [Usage Statistics](Usage-Statistics).
