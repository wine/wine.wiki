<small>
&nbsp;[:flag_gb: English](FAQ)
&nbsp;[:flag_nl: Nederlands](nl/FAQ)
&nbsp;[:flag_kr: 한국어](ko/FAQ)
</small>

-----

Cette FAQ, ou **F**oire **A**ux **Q**uestions, recouvre différents
sujets relatifs à Wine. Pour les questions liées au développement de
Wine, voyez la [FAQ de développement](Developer-FAQ).

Liens rapides :
- [Exécuter un programme avec
Wine](#comment-exécuter-une-application-une-fois-installée)
- [Exécuter Wine dans un
terminal](#comment-démarrer-des-programmes-windows-à-partir-de-la-ligne-de-commande)
- [Exécuter un programme comme
root](#faut-il-exécuter-wine-comme-root)
- [Désinstaller une
application](#comment-désinstaller-des-applications-windows)
- [Obtenir la sortie du
terminal](#comment-obtenir-la-sortie-du-terminal)
- [Utiliser un préfixe
Wine](#est-il-possible-de-stocker-linstallation-virtuelle-de-windows-ailleurs-que-dans-wine)
- [Créer un préfixe Wine
32 bits](#comment-créer-un-préfixe-wine-32-bits-sur-un-système-64-bits)

## Questions générales

### Qui est responsable de Wine ?

Wine est disponible grâce au travail de nombreuses personnes à travers
le monde. Certaines sociétés actuellement ou antérieurement impliquées
dans le développement de Wine sont CodeWeavers,
[Bordeaux](https://bordeauxgroup.com/), TransGaming, Corel, Macadamian et
Google. Voyez les pages [Remerciements](Acknowledgements) et
[Historique de Wine](Wine-History).

### Wine nuit-il à Linux ou à d'autres systèmes d'exploitation libres ?

Wine accroît l'utilité de Linux, facilite le passage des utilisateurs à
des systèmes d'exploitation libres, ainsi que la création d'applications
Windows qui y tournent. Cela accroît considérablement la part de marché
de Linux, attirant plus de développeurs commerciaux et de la communauté
vers Linux.

### Wine est-il un émulateur ? Il semble y avoir discussion

Il y a énormément de confusion à ce propos, en particulier due aux
personnes interprétant erronément le nom « Wine » en l'appelant
« WINdows Emulator » (émulateur Windows).

Quand les utilisateurs pensent à un émulateur, ils imaginent des choses
comme des émulateurs de console de jeux ou des logiciels de
virtualisation. Néanmoins, Wine est une [couche de
compatibilité](https://en.wikipedia.org/wiki/Compatibility_layer) : elle
exécute des applications Windows d'une manière proche de Windows. Il n'y
a aucune perte de vitesse inhérente à l'« émulation » lors de
l'utilisation de Wine, ni un besoin de lancer Wine avant d'exécuter une
application.

Cela dit, on peut concevoir Wine comme un émulateur de même manière que
Windows Vista peut être compris comme un émulateur Windows XP : les deux
vous permettent d'exécuter les mêmes applications en traduisant les
appels système d'une façon comparable. Configurer Wine pour qu'il imite
Windows XP n'est pas très différent de configurer Vista pour exécuter
une application en mode de compatibilité XP.

Plusieurs propriétés font de Wine plus qu'un simple émulateur :

- Des sections de Wine peuvent être utilisées sous Windows. Certaines
  machines virtuelles utilisent l'implémentation OpenGL de Direct3D de
  Wine plutôt que d'émuler véritablement un matériel 3D.
- Winelib peut être utilisée pour porter du code source d'applications
  Windows vers d'autres systèmes d'exploitation pris en charge par Wine
  afin qu'elles tournent sur n'importe quel processeur, même ceux que
  Windows lui-même ne prend pas en charge.

« Wine n'est pas qu'un émulateur » est plus précis. Penser que Wine
n'est qu'un émulateur revient à oublier toutes les autres choses qu'il
représente. L'« émulateur » de Wine n'est véritablement qu'un chargeur
de binaire qui permet à des applications Windows de s'interfacer avec
les API de remplacement de Wine.

### Quelle est la différence entre Wine, CrossOver et Cedega ?

Wine est la base du projet, là où la plus grosse partie du développement
est effectuée. Wine n'est pas parfait, mais des [dizaines de
milliers](Usage-Statistics) de personnes utilisent néanmoins
la version « standard » de Wine pour exécuter un grand nombre de
programmes Windows.

CrossOver XI (anciennement CrossOver Office) est un produit réalisé par
une société appelée CodeWeavers qui est basé directement sur Wine, avec
quelques ajustements et extensions propriétaires. À la différence des
versions bimensuelles de Wine, les versions de CrossOver sont
rigoureusement testées pour la compatibilité avec les applications
prises en charge par CodeWeavers afin d'éviter les « régressions ».
CodeWeavers emploie une bonne partie des développeurs de Wine et assure
principalement la direction du projet. Toutes les améliorations
apportées à Wine atterrissent tôt ou tard dans CrossOver.

Cedega (anciennement WineX) est un produit d'une société appelée
TransGaming. TransGaming a basé son produit sur une version de Wine de
2002 lorsque Wine avait une licence différente, fermé le code source et
repositionné sa version comme spécialisée pour les joueurs. Dans les
années suivant la création originale de Cedega à partir de Wine, les
développements de Wine et de Cedega ont continué de façon presque
entièrement indépendante. TransGaming contribue actuellement très peu de
code à Wine. Cedega n'est **pas** « Wine avec plus de prise en charge de
jeux » car Wine a accumulé des années de développement depuis la
création de Cedega, et beaucoup de jeux tournent actuellement mieux sous
Wine que sous Cedega. Wine dispose d'une prise en charge plus avancée de
Direct3D que Cedega, mais Cedega propose toujours une prise en charge
plus avancée des systèmes de protection contre la copie du fait de la
concession de licence à TransGaming de code (fermé) de plusieurs
sociétés proposant des systèmes de protection contre la copie. À la
différence de CrossOver, la plupart des améliorations de Wine
n'atterrissent *pas* dans Cedega à cause de différences de licences
entre Cedega et Wine.

Pour plus d'informations, voyez l'[historique de
Wine](Wine-History).

### Dois-je utiliser la ligne de commande ?

Il n'est pas nécessaire d'utiliser la ligne de commande pour utiliser
Wine. Vous pouvez utiliser une interface graphique pour presque tout,
comme sur Windows. Dans la plupart des cas, vous pouvez cliquer avec le
bouton droit de la souris sur un programme d'installation et
sélectionner « Ouvrir avec Wine » ou simplement double-cliquer dessus.
Vous pouvez démarrer des programmes installés en utilisant une icône du
bureau ou une entrée de menu.

Même si vous avez une version récente, il y a différentes situations où
vous pourriez devoir utiliser la ligne de commande. La raison la plus
commune est pour obtenir une sortie de débogage quand votre programme ne
fonctionne pas correctement. Vous pourriez également vouloir utiliser
des utilitaires comme `regedit` qui n'ont pas d'entrée dans le menu
système.

Ceci n'est pas vrai sous Mac OS X, où seule la ligne de commande est
actuellement disponible, à moins que vous n'utilisiez une [application
tierce](Third-Party-Applications).

### Wine fonctionne-t-il avec mon application ?

Probablement ! Si votre application est ne serait-ce que légèrement
connue, vous trouverez certainement d'autres rapports d'utilisateurs
dans la [base de données d'applications](https://appdb.winehq.org/). S'il
n'y a néanmoins aucun rapport utilisant une version récente de Wine, il
suffit simplement d'essayer et de voir ce que cela donne. Si ça ne
marche pas, ce n'est probablement pas de votre faute. Wine n'est pas
encore achevé. Demandez de l'aide sur le forum si vous êtes bloqué(e).

### Quelles applications fonctionnent correctement avec Wine ?

Des milliers d'applications fonctionnent correctement. En règle
générale, les applications simples ou anciennes ont tendance à
fonctionner correctement, à la différence des versions les plus récentes
d'applications complexes ou de jeux. Voyez la [base de données
d'applications](https://appdb.winehq.org/) pour obtenir des détails sur
des applications individuelles. Si votre application est évaluée
*Silver* (argent), *Gold* (or) ou *Platinum* (platine), vous n'avez
généralement pas de souci à vous faire ; si elle est par contre évaluée
*Bronze* ou *Garbage* (rebut), ce n'est généralement pas le cas.

### Comment exécuter Wine ?

Wine n'est pas une application qu'on exécute directement. Wine permet à
votre ordinateur d'exécuter des applications Windows. Voyez [Comment
exécuter un programme d'installation avec
Wine](Wine-User's-Guide#how-to-install-and-run-windows-programs).

### Où est mon lecteur C: ?

Wine utilise un lecteur C: virtuel plutôt que votre véritable lecteur
C:. Le répertoire où il est situé est appelé un « préfixe Wine ».

Par défaut, il s'agit du sous-répertoire `.wine/drive_c` de votre
répertoire personnel (sous MacOSX, voyez la [FAQ Wine
macOS](MacOS-FAQ) pour le trouver).

Voyez également la variable d'environnement `WINEPREFIX` : si est est
configurée, Wine l'utilise pour trouver le préfixe Wine.

### Comment contribuer au projet Wine ?

Vous pouvez contribuer en programmant, en écrivant de la documentation
ou en effectuant des dons monétaires ou en équipement, afin d'aider les
développeurs de Wine à atteindre leurs objectifs.

Un domaine où chacun peut contribuer à Wine est la création de rapports
de bugs de haute qualité dans notre [Bugzilla](https://bugs.winehq.org/)
et l'assistance aux développeurs en répondant à toute éventuelle
question de suivi requise. Il est impossible et peu pratique pour un
développeur d'avoir une copie de chaque programme sur le marché, et
votre aide est par conséquent nécessaire même après votre rapport de bug
initial. Si un développeur a une bonne idée sur ce qui peut être à
l'origine du bug, il/elle peut vous demander d'essayer un correctif et
de vérifier s'il corrige le problème. Si le correctif fonctionne et
atterrit dans notre arbre de développement principal, le rapport de bug
sera fermé, votre aide sera appréciée par tout un chacun et votre
problème sera corrigé.

Pour une liste d'idées sur la façon d'apporter votre aide, consultez la
page [Contribuer](fr/home#contribuer) de la page principale.

## Installer Wine

### Quelle version de Wine utiliser ?

**Réponse courte :** utilisez la version qui fonctionne le mieux pour
les applications particulières que vous souhaitez exécuter. Dans la
plupart des cas, ce sera la version de développement la plus récente.
Néanmoins, à l'occasion, vous devrez expérimenter pour la déterminer.

**Réponse longue :** le développement de Wine est rapide, avec des
nouvelles versions généralement bimensuelles dans la branche de
développement. La fonctionnalité sera souvent la meilleure dans la
version de développement la plus récente, mais il y a cependant des
cas où des changements dans du code existant dans Wine ont pour effet
que des applications fonctionnant bien dans des versions plus
anciennes ne tournent plus dans la nouvelle version (cela s'appelle
des régressions), ainsi que des problèmes causés par l'introduction de
nouvelles fonctionnalités incomplètes ou non entièrement testées.

La règle empirique est de commencer avec la version de Wine installée
par votre distribution et de vérifier si elle fonctionne avec les
applications que vous voulez utiliser. Si c'est le cas, bien ! Sinon,
mettez à jour votre version. Dans la plupart des cas, la mise à jour
devrait se faire vers la dernière version de développement, mais c'est
tout de même une bonne idée de vérifier dans Bugzilla et la [base de
données d'applications](https://appdb.winehq.org/) s'il n'y a pas de
régressions et/ou nouveaux bugs. S'il y en a, et en l'absence de
solutions de contournement faciles, mettez à jour vers la version la
plus récente connue pour faire tourner correctement votre application.

Bien que Wine possède une branche « stable », ce terme fait référence à
la branche en elle-même, qui est rarement mise à jour, et uniquement
avec des correctifs ne devant pas causer de perte de fonctionnalité. Les
utilisateurs d'une version de développement peuvent obtenir le même
degré de stabilité en n'effectuant simplement pas de mise à jour. Notez
que le support utilisateur pour la branche stable est limité à la
faculté de soumission de rapports de tests dans l'AppDB. Les
utilisateurs demandant de l'aide sur le forum/IRC ou soumettant des
rapports de bugs relatifs à la version stable se verront demander de
retester dans la version de développement actuelle.

Les versions stable et de développement actuelles sont listées sur la
[page d'accueil de WineHQ](https://www.winehq.org).

**Utilisateurs des paquets Ubuntu :** Le nom donné par Ubuntu à ses
paquets Wine ne correspond pas toujours à la version actuelle de Wine.
Avant de chercher de l'aide sur le forum, la liste de discussion, IRC ou
soumettre des bugs, veuillez vérifier la version de Wine installée en
tapant `wine --version` dans un terminal, et si ce n'est pas la version
de développement la plus récente, mettez-la à jour.

### Comment installer Wine ?

- Utilisez un paquet binaire précompilé pour votre système
  d'exploitation/distribution (voyez la [page de téléchargement de
  Wine](Download) pour des liens et des informations additionnelles).
- [Compilez Wine à partir du code source](Building-Wine) si
  vous ne trouvez pas de paquet à jour pour votre système
  d'exploitation/distribution.

### Je n'arrive pas à installer le paquet Wine de ma distribution et j'ai besoin d'aide

WineHQ ne fournit du support que pour les paquets binaires que nous
construisons ([disponibles ici](Download), uniquement
ceux listés sous « Paquets binaires WineHQ »). Consultez les canaux de
support de votre distribution pour obtenir de l'aide sur l'utilisation
de votre gestionnaire de paquets ou pour interpréter des messages
d'erreur que vous pourriez recevoir en cas de problème avec les paquets
de votre distribution. Si vous être un utilisateur expérimenté et pensez
qu'il y a un problème avec le paquet en lui-même et/ou le dépôt,
contactez le mainteneur du paquet Wine de votre distribution.

### Peut-on installer plusieurs versions de Wine sur un système ?

Oui, mais vous devrez compiler Wine vous-même (voyez [Comment compiler
Wine à partir du code source](#compiler-wine)), car il est
impossible d'installer plusieurs paquets Wine dans une même
distribution. La manière la plus simple pour cela est d'exécuter Wine
depuis son répertoire de compilation (ne faites pas de `make install`).
Si vous voulez véritablement installer plusieurs versions, utilisez
`--prefix` lors de la compilation pour spécifier un répertoire
d'installation spécifique à chaque version, p.ex.

```sh
./configure --prefix=chemin_du_répertoire_d'installation
make
```

et installez-le ensuite avec

```sh
sudo make install
```

Sous Linux, cette étape n'est pas suffisante : vous devez également
ajuster des variables d'environnement pour que l'exécutable `wine` dans
votre `$PATH` trouve les bibliothèques partagées appropriées. En
supposant que le chemin du répertoire d'installation est `$W`, alors,
pour Wine version 1.4, les réglages suivants semblent être suffisants :

`export WINEVERPATH=$W`
`export PATH=$W/bin:$PATH`
`export WINESERVER=$W/bin/wineserver`
`export WINELOADER=$W/bin/wine`
`export WINEDLLPATH=$W/lib/wine/fakedlls`
`export LD_LIBRARY_PATH="$W/lib:$LD_LIBRARY_PATH"`

Le `LD_LIBRARY_PATH` est assurément requis.

Notez que vous vouliez ou non installer plusieurs versions ou simplement
les exécuter depuis le répertoire de compilation, vous devrez de toute
façon spécifier quelle version de Wine utiliser lors de l'exécution
d'applications. Il est également recommandé que les applications
exécutées avec des versions différentes des Wine soient installées dans
des préfixes Wine différents.

### Y a-t-il un Wine 64 bits ?

Oui. Wine 64 bits est disponible sous Linux depuis la version 1.2, et la
plupart des distributions majeures le proposent à présent. Normalement,
son installation revient simplement à installer le paquet Wine de votre
distribution via le gestionnaire de paquets. Consultez la page de
[téléchargements](Download).

Quelques points importants :

- Wine 32 bits tourne sur les installations Linux/Unix tant 32 bits que
  64 bits. Les applications Windows 16 et 32 bits tourneront dessus.
- Wine 64 bits ne tourne que sur les installations 64 bits, et pour le
  moment uniquement sur Linux. Il requiert l'installation de
  bibliothèques 32 bits pour exécuter des applications Windows 32 bits.
  Les applications 32 et 64 bits fonctionnent dessus (ou le devraient) ;
  néanmoins il y a encore beaucoup de
  [bugs](https://bugs.winehq.org/buglist.cgi?keywords=win64&resolution=---).

Si vous désirez construire Wine à partir des sources, voyez [Construire
Wine](Building-Wine) pour des instructions sur la façon de
construire un Wine 32 bits sur un système 64 bits, et des instructions
sur la façon de construire un Wine 64 bits dans une configuration
partagée WoW64.

### Wine tourne-t-il sur tous les systèmes de fichiers Unix ?

La plupart du temps. Wine est conçu de manière à être indépendant du
système de fichiers et les applications MS Windows devraient donc
fonctionner sur quasiment tous les systèmes de fichiers UNIX complets.
La principale exception est que tous les systèmes de fichiers/pilotes ne
prennent pas en charge toutes les fonctionnalités de fat32 ou NTFS. Par
exemple, les pilotes ntfsv3 ne prennent pas en charge `mmap` en mode
écriture partagée, une fonctionnalité qui ne peut être émulée et est
utilisée par des applications comme Steam.

De plus, Wine est une application particulière et certains programmes
fonctionnent *mieux* sur des systèmes de fichiers insensibles à la
casse.

### Wine ne tournera-t-il que sur X ?

Jusqu'à récemment avec des projets comme Wayland, aucune alternative
sérieuse à x11drv ne pointait à l'horizon, et le développement s'est
donc focalisé sur X. Néanmoins, l'interface entre Wine et le pilote
graphique est conçue de sorte à être suffisamment abstraite pour
permettre avec un peu de chance une prise en charge aisée d'autres
systèmes graphiques.

### Comment télécharger des anciennes versions de Wine pour Ubuntu ?

Certaines anciennes versions sont archivées sur
<https://wine.budgetdedicated.com/archive/binary/> .

### Comment installer Wine sur un miniportable (eeePC, Acer Aspire One, etc.) ?

Si vous avez remplacé la distribution personnalisée préinstallée sur
votre miniportable (Xandros, Linpus, etc.) par l'une des distributions
principales fournissant des paquets à jour de Wine, vous devriez être en
mesure d'installer Wine comme de mise pour cette distribution.

Si vous utilisez toujours Xandros ([eeePC](https://forum.eeeuser.com/)),
Linpus ([Acer Aspire One](https://www.aspireoneuser.com/forum/)) ou une
autre distribution personnalisée, vous devrez consulter le forum d'aide
de votre miniportable. Seuls d'autres utilisateurs de ces distributions
peuvent vous indiquer quels paquets binaires fonctionneront sur votre
système (s'il y en a), où les trouver et comment les installer.

Vous pouvez également essayer de construire Wine à partir des sources
en suivant les instructions du [Guide de l'utilisateur de
Wine](Wine-User's-Guide#installing-wine-from-source), mais vous devrez
tout de même consulter le forum d'aide de votre miniportable pour ce
qui concerne la satisfaction des dépendances sur votre système
particulier.

### Installer sur Apple

#### Comment installer Wine sur un Mac ?

- Les **[paquets WineHQ](MacOS)** sont disponibles pour MacOSX 10.8 et
  ultérieur.
- Construisez et installez Wine en utilisant [Homebrew](https://brew.sh),
  [MacPorts](https://www.macports.org) ou
  [Fink](https://www.finkproject.org) pour installer Wine. Ils prennent
  tous en charge les versions actuelles de OSX. L'installation
  [MacPorts](https://www.macports.org) de Wine installera automatiquement
  toutes les dépendances nécessaires pour une installation de Wine.
- Si c'est trop compliqué pour vous, il existe plusieurs [applications
  tierces](Third-Party-Applications) utilisables comme
  [CrossOver Mac de
  Codeweavers](https://www.codeweavers.com/crossover/).
- **Linux** : si vous utilisez Linux sur votre Mac, l'installation de
  Wine est aussi simple que sous Linux sur un PC. Visitez simplement
  la [page de téléchargements](Download).

#### Peut-on utiliser Wine sur un Mac plus ancien sans puce Intel ?

Non, pas même sous Linux. Les anciens Macs utilisaient des processeurs
PowerPC incompatibles avec du code compilé pour des processeur x86
(Intel et AMD), à moins que le code tourne sous émulation de CPU. Wine
n'est pas un émulateur de CPU, ni n'en inclut. Le projet Darwine avait
précisément ce but, mais n'est plus maintenu depuis plusieurs années.

## Compiler Wine

### Comment compiler Wine à partir des sources ?

Voyez [Construire Wine](Building-Wine).

### Comment appliquer un patch ?

Vous devez construire Wine à partir des sources ; voyez ci-dessus.

## Désinstallation

### Comment désinstaller des applications Windows ?

Vous pouvez exécuter la commande `uninstaller` de Wine ; c'est similaire
à la fonction « Ajout/Suppression de programmes » de Windows. Pour
désinstaller des applications 64 bits, y compris wine-mono, vous devez
l'exécuter avec `wine64`. Le programme de désinstallation devrait
supprimer les entrées de menus et du bureau... mais ce n'est pas très
bien testé : cela peut ne pas fonctionner avec toutes les applications.
Voyez plus bas pour une façon fiable de supprimer \*toutes\* les
applications Windows.

### Comment désinstaller \*toutes\* les applications Windows ?

Pour supprimer **tous les programmes installés avec Wine**, voyez
[Comment effacer l'installation virtuelle de
Windows ?](#comment-effacer-linstallation-virtuelle-de-windows)

### Comment nettoyer la liste Ouvrir avec ?

Pour nettoyer la liste **Ouvrir avec**, reproduisez
**précautionneusement** les commandes suivantes dans un terminal :

`rm -f ~/.local/share/mime/packages/x-wine*`
`rm -f ~/.local/share/applications/wine-extension*`
`rm -f ~/.local/share/icons/hicolor/*/*/application-x-wine-extension*`
`rm -f ~/.local/share/mime/application/x-wine-extension*`

### Comment désinstaller Wine ?

**Désinstaller Wine ne modifiera pas vos réglages Wine ni ne
désinstallera vos applications Windows, qui sont conservés de façon
permanente dans votre répertoire personnel**. Ne désinstallez pas Wine
si vous voulez uniquement supprimer tous vos réglages et applications.
Pour cela, voyez [Comment effacer l'installation virtuelle de
Windows ?](#comment-effacer-linstallation-virtuelle-de-windows)

Si vous avez installé Wine en utilisant le gestionnaire de paquets de
votre distribution, utilisez-le également pour désinstaller Wine (si
vous avez installé Wine depuis ses sources, utilisez `make uninstall`
dans le répertoire des sources pour le supprimer).

### Comment effacer l'installation virtuelle de Windows ?

Vous pouvez supprimer votre installation virtuelle de Windows et
redémarrer à partir de zéro en effaçant le répertoire caché `.wine`
situé dans votre répertoire personnel. Cela supprimera tous vos réglages
et applications Windows. La manière la plus simple et sure pour faire
cela est d'utiliser votre gestionnaire de fichiers. Configurez-le pour
qu'il affiche les fichiers cachés, naviguez jusqu'à votre répertoire
personnel et supprimez `.wine` comme vous le feriez pour tout autre
répertoire. Alternativement, si vous désirez le garder comme sauvegarde,
vous pouvez également le renommer ou le déplacer. Pour le système hôte,
un préfixe Wine n'est qu'un répertoire ordinaire qui peut être effacé,
déplacé, renommé, etc. comme tout autre répertoire.

Si vous préférez le faire depuis la ligne de commande, reproduisez
**précautionneusement** les commandes suivantes dans un terminal :

```sh
cd
rm -rf .wine
```

Pour le renommer depuis la ligne de commande plutôt que l'effacer :

```sh
mv ~/.wine ~/.wine-ancien
```

Vos applications Windows, bien qu'effacées, auront toujours des entrées
dans votre menu système (les fichiers de lancement et icônes sont situés
dans `~/.local/share`).

Pour nettoyer les menus, reproduisez **précautionneusement** les
commandes suivantes dans un terminal :

```sh
rm -f ~/.config/menus/applications-merged/wine*
rm -rf ~/.local/share/applications/wine
rm -f ~/.local/share/desktop-directories/wine*
rm -f ~/.local/share/icons/????_*.{xpm,png}
rm -f ~/.local/share/icons/*-x-wine-*.{xpm,png}
```

Alternativement, vous pouvez [empêcher Wine de créer des entrées de menu
en premier lieu](#comment-empêcher-que-wine-modifie-les-associations-de-types-de-fichiers-sur-mon-système-ou-ajoute-des-entrées-de-menuliens-sur-le-bureau-lors-de-linstallation-dun-programme-windows).

## Installer des applications Windows

### Beaucoup d'applications sont déjà installées sous Windows. Comment les exécuter sous Wine ?

Réponse courte : vous devez **les installer sous Wine comme vous l'avez
fait sous Windows**. Les applications ont souvent un programme
d'installation.

Réponse longue : certaines applications peuvent être copiées depuis
Windows vers Wine et encore fonctionner, mais n'essayez pas cela à moins
que vous n'appréciiez bricoler le moteur de votre voiture alors qu'il
est en marche.

**Wine n'est pas conçu pour interagir avec une installation Windows
existante. Si vous avez besoin de données stockées dans une installation
Windows, copiez-les à un autre endroit.**

**ATTENTION** : ne configurez pas Wine pour qu'il pointe vers le lecteur
`C:\` de votre véritable installation Windows. **Cela détruira Windows
et requerra une réinstallation de Windows.** Nous avons essayé de rendre
cela difficile à faire, et ce n'est donc probablement pas faisable par
accident. Si vous le faites quand même, Wine pourrait continuer à
fonctionner, mais votre installation Windows sera 100 % morte, du fait
de l'écrasement de ses parties critiques. La seule façon de réparer
Windows après cela est de le réinstaller.

### Comment exécuter un programme d'installation sous Wine ?

**Double-cliquez sur le programme d'installation, comme sous Windows !**

Cela peut parfois ouvrir le fichier dans le mauvais programme ; si cela
se produit, vérifiez les associations de fichiers.

Vous pouvez également effectuer un clic droit et sélectionner « Exécuter
avec » puis « Wine ».

Si rien ne marche, [ouvrez une fenêtre de
terminal](https://psychocats.net/ubuntu/terminal), déplacez-vous dans le
dossier contenant le fichier `.exe` du programme d'installation et
exécutez-le en tapant `wine` suivi du nom de fichier. Par exemple :

```sh
cd ~/Bureau
wine MonProgrammeInstalleur.exe
```

Si le nom ne se termine pas par `.exe`, utilisez « wine start » suivi du
nom de fichier :

```sh
cd ~/Bureau
wine start MonProgrammeInstalleur.msi
```

Plutôt que de taper les chemin et nom de fichier entiers, vous pouvez
habituellement taper les premières lettres, appuyer ensuite sur Tab et
l'ordinateur complétera ensuite le nom de fichier pour vous. Si
plusieurs fichiers débutent par les mêmes lettres, appuyez à nouveau sur
Tab pour en obtenir la liste.

Si le programme est livré sur plusieurs CD-ROM, il ne faut pas démarrer
Wine depuis le répertoire du CD-ROM ou sinon vous ne pourrez pas éjecter
les disques sans quitter le programme d'installation. Tout d'abord,
vérifiez qu'une lettre de lecteur est affectée à votre CD-ROM dans
l'onglet « Lecteurs » de `winecfg` (p.ex. D: → `/media/cdrom`). Exécutez
ensuite le programme d'installation de cette façon :

```sh
wine start 'D:\setup.exe'
```

ou

```sh
wine start /unix /media/cdrom/setup.exe
```

### Comment installer des applications partagées entre plusieurs utilisateurs ?

Wine ne permet actuellement pas de partager sa configuration
(« préfixes ») entre des utilisateurs, à cause du risque de corruption
du registre lors de l'exécution simultanée de plusieurs processus
`wineserver` (bug #11112). À l'heure actuelle, les applications
doivent être installées séparément pour chaque utilisateur.

Néanmoins, vous pouvez copier les préfixes Wine ; vous pouvez tout
installer dans un seul préfixe, ensuite en faire une copie dans le
répertoire personnel de chaque utilisateur. Ainsi, vous évitez
d'exécuter de façon répétée les programmes d'installation.

### Comment empêcher que Wine modifie les associations de types de fichiers sur mon système ou ajoute des entrées de menu/liens sur le bureau lors de l'installation d'un programme Windows ?

Les utilisateurs ne souhaitant pas qu'un programme d'installation d'une
application Windows modifie des associations de types de fichiers ou
crée des liens sur le bureau, peuvent désactiver `winemenubuilder.exe`.
Il y a plusieurs façons de faire :

- *Dans winecfg :* avant d'exécuter le programme d'installation, lancez
  `winecfg`. Rendez-vous sur l'onglet Bibliothèques et tapez
  `winemenubuilder.exe` dans la boîte « Nouveau remplacement » (pas dans
  la liste déroulante). Cliquez Ajouter et sélectionnez-le dans la boîte
  « Remplacements existants ». Cliquez « Modifier » et sélectionnez
  « Désactivée » dans la liste, puis cliquez sur « OK ».
- *Registre :* si vous devez appliquer à plusieurs reprises ce réglage
  (c.-à-d. à chaque fois que vous recréez le préfixe Wine), cette
  approche peut être plus pratique. Créez un fichier avec une extension
  `.reg` (p.ex. `désactiver-winemenubuilder.reg`) contenant :

  ```
  [HKEY_CURRENT_USER\Software\Wine\DllOverrides]
  "winemenubuilder.exe"=""
  ```

  Pour appliquer ce réglage, exécutez :

  ```sh
  regedit désactiver-winemenubuilder.reg
  ```

- *Variable d'environnement :* définissez la variable d'environnement
  `WINEDLLOVERRIDES` quand vous exécutez le programme d'installation,
  p.ex.

  ```sh
  WINEDLLOVERRIDES=winemenubuilder.exe=d wine setup.exe
  ```

  Les utilisateurs qui créent fréquemment de nouveaux préfixes Wine
  peuvent indiquer `WINEDLLOVERRIDES=winemenubuilder.exe=d` dans leur
  `.bashrc` pour éviter de devoir le spécifier pour chaque préfixe Wine.

Désactiver `winemenubuilder.exe` a pour effet que Wine affiche parfois
des messages d'erreurs dans la console. Ces messages sont anodins, mais
les utilisateurs que ça dérange peuvent les éliminer en remplaçant
`winemenubuilder.exe` par un programme n'ayant aucun effet. Compilez ce
qui suit avec le gcc de MinGW sous wine et copiez l'exécutable dans
`C:\Windows\System32\winemenubuilder.exe` :

```c
int main() { /* Rien à faire */ return 0; }
```

Suivez ensuite la même procédure que ci-dessus, mais fixez
`winemenubuilder.exe` à « native » plutôt que « désactivée » dans
`winecfg` ou utilisez le fichier de registre :

```
[HKEY_CURRENT_USER\Software\Wine\DllOverrides]
"winemenubuilder.exe"="native"
```

ou la variable d'environnement :

```sh
WINEDLLOVERRIDES=winemenubuilder.exe=n wine setup.exe
```

### Comment installer/exécuter un fichier MSI ?

Les fichiers MSI ne peuvent être exécutés directement : vous devez
utiliser le programme `msiexec` comme suit dans un terminal :

```sh
wine msiexec /i monfichier.msi
```

Alternativement ,

```sh
wine start monfichier.msi
```

Cela exécutera le programme MSI comme si vous aviez double-cliqué dessus
sous Windows.

### Comment installer/exécuter un fichier ClickOnce (`.application`) ?

Utilisez winetricks pour installer la version de .NET requise par le
programme, rendez-vous dans le répertoire contenant le fichier
.application et exécutez-le avec

```sh
wine start monfichier.application
```

### Puis-je utiliser Wine pour installer des pilotes pour mon matériel ?

Non. Wine requiert que votre matériel fonctionne déjà dans votre système
d'exploitation. La raison technique pour cela est que Wine, comme la
plupart des applications, tourne en mode utilisateur, et pas en [mode
noyau](https://fr.wikipedia.org/wiki/Anneau_de_protection).

### Mon programme d'installation me dit que l'espace disque libre est insuffisant

Souvent, c'est réellement le cas. Le lecteur C: de Wine est situé dans
votre répertoire personnel. La partition contenant `/home` doit disposer
de suffisamment d'espace libre pour votre programme. Vous pouvez
vérifier en exécutant :

```sh
df -h ~
```

Si l'espace requis est inférieur à 1 Go et que `df` rapporte que plus
d'un Go est disponible, essayez de fixer la version Windows à
Windows 98. Cela contournera des bugs dans certains anciens programmes
d'installation (de l'époque Windows 98) qui ne géraient pas les disques
de grande taille.

Si vous voulez utiliser une partition différente qui a plus d'espace
libre, utilisez un préfixe Wine situé sur l'autre partition. Notez que
votre autre partition doit être un système de fichiers Unix : les
partitions FAT et NTFS ne fonctionneront pas. Voyez [Comment exécuter
des programmes comme s'ils tournaient sur des ordinateurs
différents](#comment-exécuter-des-programmes-comme-sils-tournaient-sur-des-ordinateurs-différents) pour les
instructions.

### Le CD ou DVD de l'application semble vide, ou quelques fichiers sont manquants !

Certains disques sont mal manufacturés d'une façon qui n'affecte que les
systèmes d'exploitation de type Unix.

Il se peut que vous deviez utiliser les options de montage « unhide » ou
« norock » pour ces disques.

Exécutez tout d'abord `mount` pour voir quel disque est monté, et
montez-le ensuite à nouveau au même endroit avec l'option requise (et
l'option « remount »).

Exemples :

```sh
sudo mount -o remount,unhide /dev/sr0 /mnt/cdrom
```

ou

```sh
sudo mount -t iso9660 -o ro,unhide /dev/cdrom /media/cdrom0
```

Consultez également la documentation de votre système d'exploitation,
p.ex. <https://help.ubuntu.com/community/Mount> ou
<https://manpages.ubuntu.com/manpages/natty/man8/mount.8.html>

## Exécuter des applications

### Comment exécuter une application une fois installée ?

Après avoir installé une application avec Wine, il y aura probablement
une entrée de menu dans Applications → Wine → Programmes, et/ou une
icône sur le bureau, comme sous Windows. Vous devriez pouvoir l'utiliser
comme sous Windows.

Alternativement, allez dans le répertoire `.wine/drive_c/Program Files`
de votre répertoire personnel (il est caché, donc vous pourriez devoir
utiliser Voir → Montrer les fichier cachés dans votre gestionnaire de
fichiers), trouvez le fichier `.exe` principal de l'application (vous
pourriez devoir deviner) et double-cliquez dessus.

Ou, si cela échoue, ouvrez une fenêtre de terminal et naviguez dans le
répertoire de l'application. Par exemple :

```sh
$ cd ~/.wine/drive_c/Program\ Files
$ ls
Adobe     Microsoft   ChatonMignon
$ cd ChatonMignon
```

Recherchez ensuite le fichier .exe principal de l'application et
exécutez-le en utilisant la commande `wine` :

```sh
$ ls *.exe
chaton.exe  uninstall.exe  ereg.exe
$ wine chaton.exe
```

### J'ai double-cliqué sur un fichier .exe, mais le système dit « Le fichier foo.exe n'est pas marqué exécutable... »

Si la boîte de dialogue dit « En savoir plus sur le bit exécutable »,
avec un hyperlien, cliquez sur l'hyperlien et lisez cette page.

Si le fichier est sur votre disque dur, faites un clic droit dessus,
choisissez Propriétés et cochez « Permettre l'exécution de ce fichier en
tant que programme ».

Si le fichier est sur un CD-ROM, vous pouvez l'exécutez à partir de la
ligne de commande comme décrit plus haut. Ou, si vous savez utiliser
`mount`, remontez le CD-ROM pour marquer tous les fichiers comme
exécutables avec une commande du type
`mount -o remount,mode=0777,exec /media/cdrom` mais en utilisant le
point de montage réel s'il diffère de `/media/cdrom`.

### Comment passer des arguments en ligne de commande à un programme ?

Si vous utilisez un programme avec des options sous Windows, p.ex.

```
quake.exe -map e1m1
```

vous pouvez faire l'équivalent sous Wine avec

```sh
wine quake.exe -map e1m1
```

c.-à-d. que la ligne de commande est identique, à part le `wine`
préfixé. Notez néanmoins que vous pourriez devoir protéger certains
caractères spéciaux avec des backslashs du fait de la façon dont ils
sont gérés par le shell Linux. Par exemple,

```
quake.exe -map C:\Quake\e1m1.bsp
```

devient

```sh
wine quake.exe -map C:\\Quake\\e1m1.bsp
```

Pour plus d'informations sur l'utilisation des backslashs, voyez
<https://www.tuxfiles.org/linuxhelp/weirdchars.html>

### Comment démarrer des programmes Windows à partir de la ligne de commande ?

Cela vous permet de voir les messages de Wine, ce qui peut aider au
dépannage de problèmes éventuels.

Étant donné que les programmes Windows recherchent souvent des fichiers
là où ils ont été démarrés, vous devriez les démarrer d'une manière très
spécifique lorsque vous utilisez la ligne de commande : déplacez-vous
dans le répertoire où le programme est situé et lancez le fichier `.exe`
en utilisant **uniquement** son nom de fichier. Par exemple :

```sh
cd '.wine/drive_c/Games/Tron'
wine tron.exe
```

Dans certains cas, il est nécessaire de spécifier le chemin complet vers
le fichier .exe du programme. Par exemple, si vous devez installer un
programme proposé sur plusieurs CD, la méthode précédente ne fonctionne
pas (entrer dans le répertoire dans le terminal vous empêchera d'éjecter
le CD). Vous pouvez spécifier un chemin de style DOS ou Windows à Wine
en utilisant des apostrophes comme suit :

```sh
wine start 'C:\Games\Tron\tron.exe'
```

Vous devez utiliser `wine start` si vous spécifiez un chemin complet,
car cela permet à Wine de spécifier le répertoire courant pour le
programme s'il le requiert. Vous pouvez également utiliser des
guillemets, mais vous devrez utiliser deux backslashs au lieu d'un :

```sh
wine start "C:\\Games\\Tron\\tron.exe"
```

Si vous voulez utiliser un nom de chemin de style Unix, utilisez
l'option /Unix, p.ex.

```sh
wine start /Unix "$HOME/installers/TronSetup.exe"
```

Dans le Wine actuel, une fois un programme installé, vous pouvez
utiliser sans crainte tout raccourci créé par le programme
d'installation.

### Quand je double-clique sur un fichier .exe dans mon gestionnaire de fichiers, rien ne se produit

Note : si possible, démarrez les applications en cliquant sur l'icône de
l'application dans le menu Applications/Wine ou sur le bureau.
Double-cliquer sur les fichiers `.exe` n'est généralement nécessaire que
pour les applications qui ne sont pas encore installées, p.ex. pour
exécuter le fichier `.exe` d'un jeu sur CD-ROM ou d'un programme
d'installation téléchargé.

Si le double-clic ne fonctionne pas, effectuez un clic droit sur le
fichier et sélectionnez « Exécuter avec Wine » (le libellé effectif
dépend de votre gestionnaire de fichiers). Si ça ne marche pas non plus,
contactez la personne qui a construit vos paquets Wine pour la prévenir
qu'il y a un problème.

Vous pouvez contourner ce problème en utilisant la ligne de commande
plutôt que votre gestionnaire de fichiers (voyez la question
précédente). Si vous voyez une ligne comme

`err:module:import_dll Library MFC42.DLL (which is needed by L"C:\\Program Files\\Yoyodyne\\Overthruster.DLL") not found`

cela signifie que vous devez [installer une bibliothèque d'exécution
manquante](#mon-application-dit-quune-dll-ou-police-est-manquante-que-faire).

## Utiliser Wine

### Est-il possible de stocker l'installation virtuelle de Windows ailleurs que dans ~/.wine ?

Oui : `~/.wine` n'est que le « préfixe » par défaut de Wine (alias
« répertoire de configuration » ou « bouteille »).

Vous pouvez changer le préfixe utilisé par Wine via la variable
d'environnement `WINEPREFIX` (en dehors de Wine), en exécutant ce qui
suit dans un terminal :

```sh
export WINEPREFIX=~/.wine-nouveau
wine winecfg
```

Wine créera alors un nouveau préfixe dans `~/.wine-nouveau`.

Pour utiliser le préfixe par défaut, utilisez la commande
`unset WINEPREFIX`. Ou fixez `WINEPREFIX` à `~/.wine`.

Alternativement, vous pouvez spécifier le préfixe Wine pour chaque
commande, p.ex.

```sh
WINEPREFIX=chemin_vers_le_préfixe_wine wine winecfg
```

Vous pouvez renommer, déplacer, copier et effacer des préfixes sans
affecter les autres, et chaque préfixe disposera de sa propre instance
de `wineserver`.

À chaque fois que vous voyez `~/.wine` ou `$HOME/.wine` dans ce wiki,
vous pouvez généralement le remplacer par `$WINEPREFIX`.

### Comment créer un préfixe Wine 32 bits sur un système 64 bits ?

Pour le moment, il y a quelques
[bugs](https://bugs.winehq.org/buglist.cgi?keywords=win64&resolution=---)
importants qui empêchent beaucoup d'applications 32 bits de fonctionner
dans un préfixe Wine 64 bits. Pour contourner ce problème, vous pouvez
créer un nouveau préfixe Wine 32 bits en utilisant la variable
d'environnement `WINEARCH`. Dans un terminal, tapez :

```sh
WINEARCH=win32 WINEPREFIX=chemin_vers_le_préfixe_wine winecfg
```

puis installez votre ou vos applications 32 bits dans ce préfixe Wine.

Ne créez pas manuellement le répertoire avant d'exécuter cette
commande ; Wine doit le créer. `WINEARCH` ne doit être spécifiée que
lors de la création du préfixe Wine, et l'architecture d'un préfixe Wine
existant ne peut être modifiée.

### Comment exécuter des programmes comme s'ils tournaient sur des ordinateurs différents ?

Exemple : vous avez des programmes serveur et client, mais l'un ne
fonctionnera pas en la présence de l'autre.

Utiliser des préfixes Wine différents vous aidera ici, car ils simulent
essentiellement deux ordinateurs Windows.

Exécutez le premier programme normalement :

```sh
wine premier-programme.exe
```

Le second doit tourner dans un préfixe différent, et nous devons donc
modifier la variable d'environnement `WINEPREFIX` :

```sh
WINEPREFIX="$HOME/.wine-second-prog" wine second-programme.exe
```

Les *premier-programme.exe* et *second-programme.exe* peuvent être deux
copies du même programme.

### Comment lancer des applications natives depuis une application Windows ?

Vous pouvez démarrer des applications natives directement depuis Wine
uniquement si vous spécifiez le chemin complet ou utilisez le shell :

```
/usr/bin/glxgears
```

ou

```
/bin/sh -c glxgears
```

Vous pouvez également utiliser `winepath` pour traduire le nom de
fichier du format Windows au format Linux (voyez la question suivante).

### Comment associer un programme natif à un type de fichiers dans Wine ?

Il y a deux façons d'associer un programme natif à un type de fichiers.
La première méthode est d'utiliser `winebrowser` et une alternative est
d'écrire un script shell.

L'exemple suivant utilise `winebrowser` pour lancer le gestionnaire par
défaut des PDF sur votre système (`xdg-open` dans un bureau Unix).
Sauvez les lignes suivantes dans un fichier `pdf.reg`.

```
[HKEY_CLASSES_ROOT\.pdf]
@="PDFfile"
"Content Type"="application/pdf"
[HKEY_CLASSES_ROOT\PDFfile\Shell\Open\command]
@="winebrowser \"%1\""
```

Importez le fichier `.reg` dans le registre en utilisant la commande

```sh
wine regedit $HOME/pdf.reg
```

Une autre possibilité est d'utiliser un script shell pour appeler une
application native. Sauvez-le sous le nom `lancer_programme_linux` sous
`$HOME/bin` :

```sh
#!/bin/sh $1
wine winepath -u "$2"
```

N'oubliez pas d'exécuter `chmod a+x $HOME/bin/lancer_programme_linux`
pour le rendre exécutable. Assurez-vous également que le répertoire
`$HOME/bin` est présent dans votre `$PATH`. Sinon, cela ne fonctionnera
pas.

Pour associer (par exemple) les fichiers `.pdf` avec le programme linux
`acroread`, sauvez ce qui suit dans `$HOME/pdf.reg` et importez-le
ensuite avec la commande `regedit $HOME/pdf.reg` :

```
[HKEY_CLASSES_ROOT\.pdf]
@="PDFfile"
"Content Type"="application/pdf"
[HKEY_CLASSES_ROOT\PDFfile\Shell\Open\command]
@="/bin/sh lancer_programme_linux acroread \"%1\""
```

Vous pouvez réutiliser ce script et simplement éditer le fichier. Par
exemple, pour associer les documents `.doc` avec OpenOffice
(`ooffice`) :

```
[HKEY_CLASSES_ROOT\.doc]
@="DOCfile"
"Content Type"="application/msword"
[HKEY_CLASSES_ROOT\DOCfile\Shell\Open\command]
@="/bin/sh lancer_programme_linux ooffice \"%1\""`
```

### Qu'est-ce que « winetricks » ? Où l'obtenir ?

Winetricks est un script shell qui télécharge les DLL et polices
manquantes pour vous depuis des sources réputées. Il a été écrit pour
aider les développeurs de Wine, mais il s'est également avéré utile pour
les utilisateurs. Voyez la [page wiki sur
winetricks](Winetricks) pour plus d'informations.

### Comment installer Internet Explorer sous Wine ?

Si vous voulez simplement qu'une application pense que IE est installé,
voyez [Mon application ne tourne pas, et dit qu'elle a besoin d'Internet
Explorer](#mon-application-ne-tourne-pas-et-dit-quelle-a-besoin-dinternet-explorer).

Le projet Wine ne prend pas en charge l'installation de l'Internet
Explorer véritable, car celui-ci requiert un grand nombre de DLL
natives, ce qui est difficile à configurer.

Si vous avez réellement besoin de l'IE réel, utilisez
[winetricks](winetricks). Fixez votre fausse version de
Windows à win2k et sélectionnez ensuite « ie6 » ou « ie7 ». IE installé
via winetricks est loin d'être complètement fonctionnel, mais marche
suffisamment pour p.ex. tester le rendu de pages web. Merci de ne *pas*
demander de l'aide au projet Wine si vous rencontrez des problèmes.

Vous devriez placer IE dans un [préfixe Wine
séparé](#est-il-possible-de-stocker-linstallation-virtuelle-de-windows-ailleurs-que-dans-wine), avec l'application qui en a réellement
besoin.

Vous pouvez également essayer des solutions commerciales, comme
CrossOver, mais si vous rencontrez des problèmes, ne cherchez pas d'aide
sur le forum/liste de diffusion ou IRC, car les applications tierces n'y
sont pas prises en charge.

### Comment faire en sorte que Wine lance une application dans un bureau virtuel ?

C'est possible en utilisant [winecfg](Commands/winecfg). Ajoutez
l'application dans l'onglet Applications, puis activez « Émuler un
bureau virtuel » dans l'onglet Affichage.

Vous pouvez également utiliser la commande suivante :

```sh
wine explorer /desktop=nom,1024x768 programme.exe
```

Remplacez *programme.exe* par le nom de votre programme et changez la
résolution par la taille du bureau virtuel désiré. Changer *nom* vous
permet d'ouvrir plusieurs bureaux simultanément.

### Wine tourne-t-il en mode texte ?

L'effort de développement sur Wine se concentre principalement sur les
programmes écrits pour l'interface graphique de Windows, mais une prise
en charge limitée du mode texte est disponible avec le pilote « null ».
Wine active automatiquement ce mode quand le pilote x11 n'est pas
chargé, mais dépend même dans ce cas des bibliothèques xorg.

De plus, le pilote « null » ne fonctionne que pour les applications
console *pures* n'utilisant \*aucune\* fonction de fenêtrage (par
exemple, certaines parties de OLE créent des fenêtres purement
internes).

### Comment connaître la version de Wine utilisée ?

Ouvrez un terminal et exécutez `wine --version`. Cela produira quelque
chose du genre « wine-1.9.2 » ; si vous utilisez
[git](Git-Wine-Tutorial), alors la version ressemblera à
« wine-1.9.2-231-gb922b55 ».

**Astuce** : vous pouvez connaître la version la plus récente de Wine
en visitant la [page d'accueil de WineHQ](https://www.winehq.org).
Actuellement, les versions de développement de Wine sortent toutes les
deux semaines. Votre système d'exploitation peut livrer une version
non à jour (obsolète) de Wine. En fonction du système d'exploitation
utilisé, il peut être possible d'ajouter une source de mises à jour à
votre système de gestion de paquets pour le maintenir à jour. Vérifiez
la page de [téléchargements](Download) pour les détails.

### Comment fonctionne le système de numérotation de versions de Wine ?

Chaque version de Wine possède un label de révision, dans le format
suivant :

`wine-x.y.z`

Cela représente Wine version *x*.*y*.*z*. (pour une liste des labels,
voyez [ici](https://gitlab.winehq.org/wine/wine/-/tags).)

- *x* est le numéro de version majeure. Il a changé exactement une fois
  depuis que le système de numérotation a été introduit, et il est
  improbable qu'il change avant un bon moment.
- *y* est le numéro de version mineure. Il change après quelques années.
  Si *y* est pair, il s'agit d'une version « stable » : des versions
  ultérieures sont peu susceptibles de casser quoi que ce soit. S'il est
  impair, il s'agit d'une version de « développement » : des versions
  ultérieures peuvent causer des régressions.
- *z* est le numéro de révision. Il est incrémenté toutes les deux
  semaines pour les versions de développement, tous les quelques mois
  pour les versions stables.

Si vous utilisez [git](Git-Wine-Tutorial), le label sera
généré en utilisant la commande `git-describe`, et ressemble à :

`wine-x.y.z-n-gccccccc`

où *n* est le nombre de patchs/commits appliqués depuis que *x*.*y*.*z*
a été livré, et *ccccccc* représente les premiers chiffres hexadécimaux
de l'identifiant du commit le plus récent. Exemples :
wine-1.1.19-228-g1e256e4, wine-1.1.25-311-g3d6bb38,
wine-1.1.32-584-g10b0b86.

Juste avant une version stable de Wine, il y a des versions dites
candidates, qui ont des labels de la forme :

`wine-x.y-rcn`

qui représente la *n*<sup>e</sup> version candidate pour Wine *x*.*y*.

Exemples :

| Label de version | Date        | Note                        |
|------------------|-------------|-----------------------------|
| wine-1.0         | 17 jun 2008 | Première version « stable » |
| wine-1.1.1       | 11 jun 2008 |                             |
| wine-1.1.2       | 25 jul 2008 |                             |
| wine-1.2         | 16 jul 2010 | Deuxième version « stable » |

### Faut-il exécuter Wine comme root ?

**N'exécutez JAMAIS Wine comme superutilisateur !**

Faire cela donne aux programmes (et virus) Windows une accès complet à
votre ordinateur et à tout média qui y est attaché. L'exécuter avec
`sudo` comporte les mêmes risques, mais en sus altère les permissions
dans votre répertoire `~/.wine`. Si vous avez exécuté Wine avec `sudo`,
vous devrez corriger les erreurs de permissions comme décrit dans la
question suivante, et lancer ensuite [winecfg](Commands/winecfg) pour
reconfigurer Wine. Vous devriez toujours exécuter Wine avec le compte
utilisateur habituel utilisé pour ouvrir une session.

Pour les systèmes Linux, toute conception que Wine nécessite root peut
être désavouée grâce à l'utilisation des [capacités
Posix](https://www.linuxjournal.com/article/5737)) ou des [capacités de
fichiers
Posix](https://www.ibm.com/developerworks/linux/library/l-posixcap/index.html)
ou en corrigeant d'autres réglages de sécurité.

Pour ce qui concerne les programmes Windows, vous possédez les
privilèges administrateur. Si une application se plaint d'un manque de
privilèges administrateur, signalez un bug ; exécuter Wine comme root
n'aidera probablement pas.

### J'ai exécuté wine avec sudo ou comme root. Comment corriger les erreurs de permission ?

Vous devez corriger les permissions dans votre répertoire `~/.wine` :
c'est là où tout l'état de Wine, sa configuration et toutes données
importantes telles que les programmes installés, les données sauvées
avec des programmes Wine, etc. sont stockées. Une fois les permissions
effacées ou corrigées dans ce répertoire, réexécutez toujours Wine en
tant qu'utilisateur normal ! Exécutez ce qui suit pour corriger les
permissions dans votre répertoire `~/.wine` s'il a les permissions de
root.

```sh
cd $HOME
sudo chown -R $USER:$USER .wine
```

### Comment utiliser le lissage de polices dans Wine ?

La prise en charge du rendu de sous-pixels a été ajoutée à Wine dans la
version 1.1.12, mais elle peut ne pas être activée. Utilisez
[winetricks](Winetricks) et sélectionnez l'une des options
fontsmooth-gray, fontsmooth-rgb ou fontsmooth-bgr.

### Comment modifier les "points par pouce" (taille de police) ?

D'abord, vous devriez essayer avec `winecfg`. Dans l'onglet Affichage,
ajustez le curseur de défilement « Résolution de l'écran ». Les
modifications n'auront d'effet dans `winecfg` qu'une fois redémarré.

Si les fenêtres et polices sont si larges que vous n'arrivez pas à
atteindre les contrôles dans winecfg, voyez [Les fenêtres et polices de
Wine sont extrêmement larges](#les-fenêtres-et-polices-de-wine-sont-extrêmement-larges).

### Comment éditer le registre de Wine ?

Le registre de Wine est stocké dans les fichiers `.reg` dans `~/.wine`,
mais vous ne devriez néanmoins pas éditer ces fichiers à la main du fait
du codage utilisé. Utilisez toujours le programme
[regedit](regedit) livré avec Wine, en tapant `wine regedit`
dans un terminal. Le `regedit` de Wine est virtuellement identique à la
version Windows et prend également en charge l'import/export de fichiers
de registre. N'essayez JAMAIS d'importer votre registre Windows entier :
cela ne ferait qu'endommager Wine.

Voyez également les [entrées de registre
utiles](Useful-Registry-Keys).

### Comment configurer un proxy ?

Si vous voulez utiliser un serveur mandataire pour toutes les connexions
HTTP, fixez simplement la variable d'environnement `http_proxy`. Sur la
plupart des distributions Linux, la configuration d'un serveur
mandataire réseau, p.ex. avec l'outil Serveur Mandataire, automatise
cela pour vous.

Alternativement, vous pouvez effectuer la configuration dans le
registre. Il y a des emplacements séparés pour wininet.dll et
winhttp.dll.

Pour wininet, utilisez `regedit` pour ajouter les valeurs suivantes dans
la clé
`[HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings]` :

```
"ProxyEnable"=dword:00000001
"ProxyServer"="adresse-serveur-mandataire:port"
```

Pour le moment, le `wininet.dll` de Wine ne prend pas en charge les
scripts d'autoconfiguration de serveur mandataire (PAC).

Pour winhttp, vous devrez utiliser l'utilitaire `proxycfg.exe` pour
configurer le registre. Cet utilitaire est disponible dans le répertoire
system32 d'une installation Windows, et MSDN décrit son utilisation.

### Quels applications/services entrent en conflit avec Wine ?

Beaucoup de distributions tendent à activer Compiz/Beryl/autres
fioritures par défaut. Le problème est que ces programmes entrent en
conflit avec Wine quant à l'utilisation du pilote d'affichage.
Désactiver ces programmes préalablement à l'utilisation de toute
application avec Wine (en particulier les jeux, ou lors de la survenue
de bizarres problèmes de fenêtres). Par ailleurs, désactiver l'extension
Composite dans `/etc/X11/xorg.conf` empêchera très probablement la
composition d'affecter Wine.

Les outils de commutation de claviers comme IBUS, `xneur` (`gxneur`,
`kxneur`) et SCIM ont été rapportés entrer en conflit avec l'entrée
clavier dans Wine.

## Applications

### Wine supporte-t-il DirectX ? Est-il possible d'installer Microsoft DirectX sous Wine ?

Wine fournit lui-même une implémentation de DirectX qui, même s'il y
subsiste quelques bugs, devrait fonctionner correctement. Wine prend
actuellement en charge DirectX 9.0c. Les plans pour DirectX 10 sont en
cours.

**Si vous essayez d'installer Microsoft DirectX, vous aurez des
problèmes.** Il n'est ni recommandé ni pris en charge par Wine HQ
d'essayer ceci. Vous pouvez installer le moteur d'exécution, mais il ne
fonctionnera pas. Il nécessite l'accès aux pilotes Windows, et Wine ne
peut y accéder pour des raisons évidentes. Les seules DLL Microsoft
natives pouvant être utiles sont celles du type `d4dx9_xx.dll`, et elles
requièrent que vous acceptiez la licence de Microsoft. De plus, des
versions de ces DLL font à présent partie de Wine. Ainsi, au fil des
améliorations dans Wine, ces DLL seront de moins en moins pertinentes.

Cela dit, il existe certains guides décrivant comment installer
Microsoft DirectX. Nous répétons : il n'est ni recommandé ni pris en
charge par Wine HQ d'essayer ceci. De plus, c'est considéré hors sujet
dans les médias de support de Wine HQ (comme par exemple les forums).
Veuillez utiliser un répertoire de configuration de wine vierge avant
de demander de l'aide (effacez votre `~/.wine` ou utilisez un autre
[préfixe
Wine](#est-il-possible-de-stocker-linstallation-virtuelle-de-windows-ailleurs-que-dans-wine)
et réinstallez vos applications Windows).

### Pourquoi Wine ne fonctionne-t-il pas correctement avec les systèmes de protection contre la copie ?

Les systèmes de protection contre la copie utilisent différentes
méthodes déplaisantes pour détecter si des disques sont authentiques ou
pas. Certaines de ces méthodes fonctionnent sous Wine, mais la plupart
pas, tel le système extrêmement invasif StarForce. Wine devrait être
modifié pour permettre des fonctionnalités de type quasi-rootkit afin
que certains de ces systèmes de protection fonctionnent, et la prise en
charge est probablement très éloignée, si elle a jamais lieu (certaines
personnes utilisent des jeux modifiés ou piratés illégalement où la
protection contre la copie est supprimée, mais le projet Wine ne peut
préconiser cela).

Voyez également notre page sur la [protection contre la
copie](Copy-Protection).

### J'ai suivi un guide pratique sur un site web, et mon application ne fonctionne toujours pas

Il y a de nombreux guides pratiques non officiels pour différentes
applications postés sur des blogs et forums. Ils sont rarement maintenus
par leurs auteurs, et du fait du rythme de développement rapide de Wine,
même ceux qui étaient corrects au moment où ils ont été postés peuvent
vite devenir obsolètes. L'application de bidouilles qui ne sont plus
nécessaires dans le Wine actuel peut en fait empêcher une application
fonctionnant actuellement de fonctionner du tout (Office 2007 est un
exemple parfait). Les seuls guides pratiques pris en charge ici sont
ceux présents sur ce site. Si vous persistez à vouloir suivre un guide
extérieur, demandez de l'aide à son auteur.

En cas de doute, réinstallez l'application dans un nouveau préfixe Wine
et demandez de l'aide sur le forum utilisateurs.

### J'ai utilisé une application tierce (PlayOnLinux, WineBottler, etc.) pour installer ou gérer des applications dans Wine et j'ai besoin d'aide

Consultez les canaux de support appropriés pour toute application tierce
utilisée. Des liens vers certaines de ces applications externes sont
fournis par commodité sur la page [Applications
tierces](Third-Party-Applications), mais elles ne sont pas
supportées ici. Avant de demander de l'aide sur les forum/liste de
diffusion/IRC ou de rapporter des bugs, réinstallez votre application
Windows en utilisant un Wine standard.

### Mon application fonctionnait avec une version plus ancienne de Wine, mais ne marche plus !

Cela s'appelle une régression. Veuillez effectuer un [test de
régression](Regression-Testing) pour identifier quel patch
l'a causée, puis [soumettez un rapport de bug](https://bugs.winehq.org)
et ajoutez-y le mot-clé « regression » : nous y porterons
particulièrement attention, car les régressions sont beaucoup plus
faciles à corriger quand elles sont constatées rapidement.

### J'ai absolument besoin que cette application particulière fonctionne. Comment y parvenir ?

Brièvement, vous pouvez améliorer Wine vous-même, convaincre quelqu'un
de le faire pour vous ou essayer quelques bidouilles compliquées
impliquant des DLL Windows natives plutôt que celles (incomplètes) de
Wine.

### J'écris une application Windows. Comment détecter si elle tourne sous Wine ?

C'est une mauvaise idée. Voyez la [FAQ
développeurs](Developer-FAQ).

## Performance

### Quelles sont les exigences système de Wine ?

La règle empirique est que si votre application tourne correctement dans
Windows, elle devrait tourner correctement sur le même système avec
Wine. Wine, avec le système d'exploitation utilisé pour le faire
tourner, requiert généralement moins d'espace disque et de mémoire que
Windows lui-même. Si vous n'exécutez pas d'application Windows, Wine ne
consommera aucune ressource à part ± 20 Mo d'espace disque.

### Mon application/jeu 3D est très lent(e) (images par seconde)

Un problème de performances 3D indique généralement que quelque chose
cloche dans vos pilotes OpenGL 3D. Voyez [Problèmes de pilote
3D](3D-Driver-Issues) pour plus d'informations.

### Wine est-il plus lent que de simplement utiliser Windows ?

En fait, Wine est parfois plus rapide. La vitesse d'une application
dépend d'un tas de facteurs : le matériel disponible et les pilotes
associés, la qualité du code des API utilisées par l'application et la
qualité du code dans le système d'exploitation sous-jacent.

Le code des pilotes importe énormément. Si vous exécutez une application
intense en graphiques en utilisant une carte vidéo avec des pilotes très
médiocres comme ceux des cartes ATI sous Linux, la performance sera
substantiellement dégradée. D'un autre côté, Linux gère mieux la
mémoire, et devance Windows dans beaucoup de tâches intensives en CPU .

Parfois, des bugs dans Wine peuvent ralentir excessivement des
applications . Cela dit, Wine vise d'abord la correction avant la
performance.

Voyez [Performance](Performance) pour plus d'informations.

### J'ai un tas de messages « fixme: » dans le terminal et Wine tourne un peu lentement

Ignorez-les. Généralement, un message `fixme` (littéralement,
« corrigez-moi ») n'a que peu de sens pour quelqu'un non familier avec
le développement de Wine. Ils sont destinés aux [développeurs de
Wine](Developers). De plus, il est important de comprendre
que les messages `fixme` n'indiquent généralement *pas* un problème
sérieux. Beaucoup d'applications fonctionnent correctement même si Wine
affiche quelques messages `fixme`. Cela dit, ils peuvent parfois fournir
des indications sur le fonctionnement d'une application ou la raison
pour laquelle elle ne fonctionne pas.

Si ces messages apparaissent à maintes reprises en très grand nombre,
vous pouvez parfois accélérer un peu Wine en les désactivant totalement,
en fixant la variable d'environnement `WINEDEBUG` à `-all`. Par exemple,
votre ligne de commande ressemblerait à :

```sh
WINEDEBUG=-all wine programme.exe
```

Des utilisateurs plus avancés ou des programmeurs intéressés à déboguer
Wine devraient consulter les pages wiki [Canaux de
débogage](Debug-Channels) et [Documentation
développeurs](Developers). Voici un exemple pour désactiver
sélectivement les messages `fixme` de dsound et d'une partie de D3D :

```sh
WINEDEBUG=fixme-dsound,fixme-d3d_surface wine programme.exe
```

## Dépannage

### Comment obtenir la sortie du terminal ?

Lancez votre application à partir de la ligne de commande (voyez
[Comment démarrer des programmes Windows à partir de la ligne de
commande ?](#comment-démarrer-des-programmes-windows-à-partir-de-la-ligne-de-commande)).

Vous pouvez ensuite copier la sortie à l'écran et la coller dans un
fichier si elle est réduite, ou la rediriger vers un fichier en
utilisant une [redirection de shell
Unix](https://en.wikipedia.org/wiki/Redirection_(computing)), p.ex.

```sh
cd ~/.wine/drive_c/Games/Tron
wine tron.exe &> log.txt
```

**Important** : ne fournissez une [trace de
débogage](#comment-obtenir-une-trace-de-débogage) **que** si on vous la demande
**explicitement**. Suivez simplement les instructions ci-dessus.

**Important** : si vous obtenez une boîte de dialogue de plantage en
faisant cela, cliquez sur « Fermer ». Sinon, les données enregistrées
seront incomplètes.

**Important** : si le fichier texte résultant n'inclut pas de nom de
fichiers sources C, votre copie de Wine ne contient probablement pas de
symboles de débogage. Veuillez soit compiler Wine à partir des sources,
soit installer le paquet renfermant les symboles de débogage (dans
Ubuntu, il s'agit du paquet wine1.4-dbg pour le paquet wine1.4).

### Comment obtenir une trace de débogage ?

**Note** : veuillez n'utiliser cette procédure que si cela vous est
demandé. Dans la plupart des cas, la sortie normale du terminal est
suffisante (voyez ci-dessus). Lors de la soumission de bugs, il est
souvent nécessaire de fournir une trace de débogage additionnelle
(généralement `+relay,+seh`, mais on pourrait vous demander d'utiliser
des [canaux de débogage](Debug-Channels) spécifiques. Pour
obtenir une trace, exécutez :

```sh
WINEDEBUG=+relay,+seh,+tid wine votre_programme.exe >> /tmp/sortie.txt 2>&1
```

**Attachez** ensuite `/tmp/sortie.txt` au rapport de bug. Si le fichier
résultant est plus gros que 1 Mo, compactez-le avec `bzip2` ou `rzip` au
préalable. Dans certains cas, le problème semble disparaître quand
`WINEDEBUG` est utilisé avec un canal précis. Merci de l'indiquer dans
le rapport de bug si c'est le cas. Pour une liste des canaux de débogage
disponibles dans Wine, voyez la page [Canaux de
débogage](Debug-Channels).

### Comment désactiver la boîte de dialogue de plantage ?

Depuis la version 1.1.20, Wine inclut une boîte de dialogue de plantage
qui est activée par défaut. Les utilisateurs d'applications qui
fonctionnent malgré un plantage en arrière-plan peuvent trouver cela
ennuyant, et cela peut même dans certains cas empêcher une application
de fonctionner.

La façon la plus facile de désactiver la boîte de dialogue de plantage
est d'utiliser [winetricks](Winetricks) :

```sh
sh winetricks nocrashdialog
```

Si vous préférez la manière manuelle, copiez la clé suivante dans un
éditeur de texte :

```
[HKEY_CURRENT_USER\Software\Wine\WineDbg]
"ShowCrashDialog"=dword:00000000
```

Enregistrez le contenu dans un fichier avec une extension `.reg` (p.ex.
`boîte_plantage.reg`) et appliquez-le avec `regedit` :

```sh
wine regedit boîte_plantage.reg
```

Vous pourriez devoir spécifier le chemin complet du fichier, en fonction
de l'endroit où vous l'avez enregistré.

Pour réactiver la boîte de dialogue, remplacez 00000000 par 00000001 et
réappliquez.

Ces changements peuvent également être effectués en exécutant simplement
`regedit` et en y ajoutant/modifiant la clé appropriée via une interface
graphique.

### Mon programme s'est bloqué, comment le fermer ?

Si vous avez lancé le programme à partir d'une fenêtre de terminal en
tapant `wine mon_programme.exe`*, vous pouvez généralement y
retourner et presser Ctrl+C. Si vous avez exécuté l'application d'une
autre façon (p.ex. à partir d'un raccourci), vous pouvez ouvrir un
terminal et tuer violemment le processus :

```sh
killall -9 Application.exe
```

Si vous voulez tuer tous les programmes Wine en même temps, exécutez :

```sh
wineserver -k
```

Vous pouvez également ouvrir une version Wine du gestionnaire de tâches
Windows en exécutant `wine taskmgr` dans un terminal. Cela vous
permettra de tuer des processus Wine individuels.

### Mon ordinateur complet se bloque, redémarre ou s'arrête quand j'exécute un jeu dans Wine !

Si vous obtenez un blocage complet et ne pouvez même plus utiliser votre
souris après avoir exécuté Wine, ce n'est probablement pas dû à un
problème spécifique dans le logiciel Wine. Wine est un processus non
privilégié, et ne devrait jamais pouvoir bloquer complètement le système
d'exploitation. Wine expose plutôt probablement un problème plus profond
sur votre système, comme un pilote matériel défectueux, une barrette
mémoire endommagée ou un comportement saugrenu dû à un surcadencement
(overclocking).

C'est souvent un problème de pilote graphique, auquel cas les
applications non-Wine peuvent également être affectées. Si l'exécution
de `glxgears` plante aussi, c'est assurément un problème de pilote
graphique. La cause la plus probable est la mise à jour vers un nouveau
noyau sans avoir mis à jour en parallèle le pilote graphique pour qu'il
corresponde. Essayez de réinstaller vos pilotes graphiques.

Si l'ordinateur est un portable et s'arrête entièrement, une cause
probable est une surchauffe. Certains portables ont à la base des
problèmes de refroidissement, et le code ACPI de Linux contrôlant les
ventilateurs est réputé être bogué.

Si cela n'a pas d'effet, demandez de l'aide sur le forum wine-users.
Assurez-vous de mentionner le nom de l'application, la version de Wine,
les sorties de `cat /etc/issue`, `lspci | grep -i vga` et, si vous
utilisez les pilotes propriétaires NVidia, de
`cat /proc/driver/nvidia/version`. Peut-être quelqu'un pourra-t-il vous
aider.

### Toutes les applications que j'essaie de lancer plantent

Votre répertoire `.wine` (le préfixe Wine) est corrompu. Par exemple, si
vous installez une application qui démarre un service lors du démarrage
du système, et que ce service plante, alors un plantage aura lieu à
chaque démarrage de Wine.

Pour contourner ce problème, essayez de [supprimer votre répertoire
.wine](#comment-effacer-linstallation-virtuelle-de-windows) et de
réinstaller vos applications, en laissant tomber l'application
fautive.

### Mon application dit qu'une DLL ou police est manquante. Que faire ?

Les applications *devraient* être accompagnées de toutes les DLL
requises (à part les DLL basiques de Windows). Elles ne le sont pas
toujours, et présupposent que la DLL ou police est déjà installée. Vous
pouvez installer la DLL ou police manquante de plusieurs façons :

- téléchargez-la à partir du site des créateurs originaux du moteur
  d'exécution (p.ex. Microsoft). La manière la plus simple est
  d'utiliser [winetricks](#quest-ce-que-winetricks-où-lobtenir).
- installez d'autres applications qui l'incluent.
- copiez-la depuis une version sous licence de Windows installée sur la
  même machine.

*Ne téléchargez pas de DLL ou de script depuis des sites web inconnus ou
auxquels vous ne faites pas confiance !* Des DLL fausses ou infectées
peuvent causer des gros ennuis, même sous Wine.

Voyez la [page wiki sur winetricks](Winetricks) pour plus
d'informations sur winetricks.

### Mon application ne démarre pas, et dit qu'elle a besoin de .NET

Les versions récentes de Wine vous demanderont de télécharger
[wine-mono](Wine-Mono) lors de la création du préfixe Wine. Pour
beaucoup d'applications .NET, en particulier les plus anciennes, cela
est suffisant.

Si wine-mono ne fonctionne pas pour votre application/jeu, rapportez un
bug (si cela n'a pas déjà été fait) et essayez d'utiliser .NET natif.
Vous pouvez l'installer en exécutant [winetricks](Winetricks)
et en sélectionnant la version .NET appropriée. Notez que le .NET natif
ne peut actuellement être installé que dans un préfixe Wine 32 bits, et
vous en êtes donc pour vos frais si votre application/jeu est en
64 bits.

### Mon application ne tourne pas, et dit qu'elle a besoin d'Internet Explorer

Wine utilise le cœur de Firefox pour implémenter son propre Internet
Explorer (appelé [wine-gecko](Gecko)). Les versions récentes
de Wine devraient vous demander de l'installer lors de la création du
préfixe Wine.

Si wine-gecko ne fonctionne pas pour votre application/jeu, vous pouvez
essayer d'installer le véritable Internet Explorer en utilisant
winetricks : voyez [Comment installer Internet Explorer sous
Wine ?](#comment-installer-internet-explorer-sous-wine).

### Mon application tourne, mais certaines zones de texte se comportent bizarrement (p.ex. la coupure de lignes ne fonctionne pas, le double-clic ne sélectionne pas de mots)

Vous pouvez avoir rencontré un bug dans le RICHED20.DLL de Wine. Vous
pouvez tenter d'utiliser le RICHED20.DLL de Microsoft en exécutant
[winetricks](Winetricks) et en sélectionnant `riched20`. Cela
peut permettre de contourner le problème en attendant que les
développeurs de Wine corrigent le bug.

### Mon application ne tourne pas bien, et la sortie dit :

#### `Trop de fichiers ouverts, ulimit -n doit probablement être augmenté`

Votre système d'exploitation vit probablement dans le passé, et a une
limite stricte trop basse du nombre de descripteurs de fichiers ouverts
(voyez <https://bugs.launchpad.net/ubuntu/+bug/663090> pour savoir
pourquoi augmenter la limite stricte est la bonne chose à faire, mais
augmenter la limite souple par défaut est dangereux).

Pour Ubuntu et la plupart des versions modernes de Linux, vous pouvez
éditer `/etc/security/limits.conf` comme root et remplacer la ligne

```
* hard nofile 2048   (la limite peut être différente)
```

par

```
 * hard nofile 8192
```

(l'astérisque signifie « pour tous les utilisateurs »).

Clôturez et réouvrez une session, puis tapez `ulimit -H -n`. Cela
devrait indiquer 8192 à présent, et Wine devrait avoir accès à plus de
descripteurs de fichiers.

Voici une autre méthode plus portable (pouvant même fonctionner sous Mac
OS X), mais qui ne fonctionne que temporairement, et n'augmente la
limite que pour les applications démarrées depuis la fenêtre de terminal
courante :

```
$ sudo bash
# ulimit -n 8192
# su votre_nom_d_utilisateur
$ wine votre_programme.exe
```

#### `preloader: Warning: failed to reserve range 00000000-60000000` ou `winevdm: unable to exec 'nom de l'application': DOS memory range unavailable`

Ce problème est suivi dans le bug #12516.

La cause est un réglage du noyau Linux. Exécutez
`cat /proc/sys/vm/mmap_min_addr` comme root : si ce n'est pas égal à 0,
alors utilisez `sysctl -w vm.mmap_min_addr=0` comme root pour corriger
temporairement le problème ; pour le corriger de façon permanente,
ajoutez la ligne `vm.mmap_min_addr=0` à `/etc/sysctl.conf`. Consignez
que vous utilisez cette altération, car la zone que Wine nécessite peut
changer.

Voyez la page [Problème de la page zéro du
préchargeur](Preloader-Page-Zero-Problem) pour plus
d'informations.

#### `Failed to use ICMP (network ping), this requires special permissions`

Sur les systèmes \*NIX, le ping ICMP requiert l'utilisation de sockets
en mode brut, qui sont réservées au superutilisateur (root). Et exécuter
Wine comme root est une [mauvaise idée](#faut-il-exécuter-wine-comme-root).
Heureusement, les versions plus récentes de Linux permettent un contrôle
des permissions granulaire pour n'accorder les permissions requises qu'à
des fichiers spécifiques.

Pour autoriser Wine à ouvrir des sockets en mode brut, exécutez cette
commande :

```sh
sudo setcap cap_net_raw+epi /usr/bin/wine-preloader
```

**Note** : cela ne fonctionne avec le binaire Wine par défaut que sur la
plupart des distributions. Un Wine compilé à la main sera situé dans
/usr/local/bin. Le nom 64 bits est wine64-preloader. Les programmes
enveloppants tiers (comme PlayOnLinux) peuvent placer ailleurs le
binaire Wine. Vous devrez réexécuter la commande après chaque mise à
jour de Wine.

Pour supprimer les permissions, utilisez :

```sh
sudo setcap -r /usr/bin/wine-preloader
```

#### `err:virtual:map_image failed to set 60000020 protection on section .text, noexec filesystem?`

Cela peut être causé par des systèmes de fichiers montés avec l'option
`user` ou `noexec`, ou par SELinux. Assurez-vous que l'application en
question ne réside pas sur un systèmes de fichiers louche ou essayez de
désactiver temporairement SELinux.

### Graphiques

#### Mon application se plaint de ne pouvoir modifier la résolution ou la profondeur des couleurs

Vous devez généralement éditer la section « Screen » de votre fichier
`/etc/X11/xorg.conf` pour prendre en charge des profondeurs de couleurs
et résolutions additionnelles. Cela peut également être dû à un problème
dans Xrandr.

#### Mon application se plaint qu'elle a besoin de 256 couleurs mais j'en ai des millions !

L'impossibilité de passer d'un mode 24 bits par pixel à 8 bits par pixel
est une limitation de X, pas un bug dans Wine. Voyez la page [Mode
256 couleurs](256-Color-Mode) pour quelques contournements
possibles.

#### Mon écran X ne revient pas à sa résolution normale après avoir exécuté un jeu en plein écran

Vous pouvez souvent contourner ce problème en modifiant la résolution de
l'écran et en la rechangeant à nouveau dans les préférences système.

Alternativement, vous pouvez exécuter cette commande dans un terminal
pour restaurer vos réglages X :

```sh
xrandr -s 0
```

#### J'utilise les effets de bureau avec Compiz, Fusion ou XGL et j'obtiens de mauvaises performances/des messages bizarres/des applications défaillantes

L'utilisation de gestionnaires de composition dans X11 tend à estropier
la performance de X ou à casser complètement OpenGL (cela ne s'applique
pas au compositeur Mac OS X, qui ne peut être désactivé). Nous
recommandons de les désactiver entièrement avant d'utiliser Wine. Si
vous en utilisez un et souffrez de mauvaises performances, ne rapportez
pas de bugs dans Wine, car ce sont des bugs dans votre gestionnaire de
fenêtres ou vos pilotes vidéo. Par ailleurs, désactiver l'extension
Composite extension dans `/etc/X11/xorg.conf` empêchera très
certainement toute composition d'affecter Wine.

#### Les graphiques dans des jeux avec de bonnes évaluations dans l'AppDB sont brouillés

- Réessayez avec les derniers pilotes graphiques.
- La plupart des entrées de l'\[//appdb.winehq.org/ AppDB\] sont basées
  sur du matériel **NVIDIA**/**GeForce** utilisant le pilote
  propriétaire.
- Les cartes **ATI**/**AMD**/**Radeon** utilisant le pilote
  propriétaire fglrx ont des problèmes dans Wine. En règle générale, à
  tout le moins les jeux utilisant des shaders sont affectés. Voyez
  [cet
  article](https://article.gmane.org/gmane.comp.emulators.wine.user/36669)
  et le bug #7411 de Wine pour plus de détails.
- **Les autres matériels** (Intel/S3/Matrox/etc.) ne feront probablement
  tourner que les anciens jeux (sans shaders). La compatibilité n'est
  pas bien testée.
- Idem pour les pilotes **open source** vu leur prise en charge
  généralement basique de la 3D.

#### Wine affiche du texte corrompu/n'affiche pas de texte

Cela peut être le bug #16146, causé par le pilote historique
`nvidia-96xx` ou le bug #18120, qui affecte les applications Qt
4.5.0. Une autre cause pourrait être des polices manquantes, des
conflits de polices ou l'ajout de nouvelles polices dans Wine.

Essayez d'utiliser un nouveau préfixe Wine (en renommant ou supprimant
`~/.wine` ou en changeant la variable d'environnement `$WINEPREFIX`). Si
le problème persiste, essayez de modifier le registre Wine en important

```
[HKEY_CURRENT_USER\Software\Wine\X11 Driver]
"ClientSideWithRender"="N"
```

préalablement sauvé dans un fichier `norender.txt` grâce à la commande
`regedit norender.txt`. Ne l'appliquez que si nécessaire (on a rapporté
que c'était requis sous OS X au 1<sup>er</sup> décembre 2007 ou plus
récemment sous d'autre plates-formes, comme Ubuntu.)

#### Les fenêtres et polices de Wine sont extrêmement larges

Vous pouvez parfois utiliser la touche Alt et la souris pour déplacer la
fenêtre `winecfg` afin d'atteindre le curseur de défilement « Résolution
de l'écran » dans l'onglet Affichage, et réduire ainsi la taille de
police. Les changements n'auront d'effet dans `winecfg` qu'une fois
redémarré.

Si ça ne fonctionne pas, utilisez ce changement du registre (sur une
seule ligne) :

```sh
echo -e "[HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Hardware Profiles\\Current\\Software\\Fonts]\n\"LogPixels\"=dword:60\n" | wine regedit -
```

Si cela ne marche toujours pas, supprimez votre répertoire `~/.wine` et
réinstallez vos applications Windows.

#### « Broken NVIDIA RandR detected, falling back to RandR 1.0. » est affiché sur la console

[RandR](https://en.wikipedia.org/wiki/RandR) est un protocole utilisé
par les applications pour indiquer au serveur X de changer la résolution
de l'écran, entre autres choses. Les pilotes propriétaires de nVidia
pour GNU/Linux n'implémentent délibérément pas les versions plus
récentes de RandR, sur lesquelles Wine se base normalement. Cela peut
poser des problèmes, en particulier dans des logiciels qui tentent de
modifier la résolution ou de s'afficher sur plusieurs moniteurs.

Jusqu'à ce que soit nVidia corrige le comportement de ses pilotes ou
que les développeurs de Wine contournent les limitations imposées par
le pilote (bug #34348), les utilisateurs de matériel nVidia peuvent
contourner les problèmes associés en utilisant les pilotes logiciels
libres [Nouveau](https://en.wikipedia.org/wiki/Nouveau_%28software%29)
ou en passant à une carte graphique d'un autre vendeur offrant un
meilleur support.

Notez également que de nombreuses applications essaieront de démarrer
dans des résolutions plus basses si (et seulement si) elles sont
disponibles, mais utiliseront volontiers une résolution plus haute si
c'est la seule possibilité. Dans ce cas, vous pourriez éviter un crash
des pilotes nVidia en forçant votre serveur X à ne prendre en charge que
la résolution native de votre moniteur, et rien d'autre. Cela a marché
pour une série de jeux.

Pour effectuer ce changement dans Xorg, éditez (comme `root`) le fichier
`/etc/X11/xorg.conf` et ajoutez-y la ligne

```
Option "metamodes" "1920x1080 +0+0"
```

dans la section `Screen` (en remplaçant 1920x1080 par la résolution
souhaitée). Après le redémarrage de Xorg, vous pouvez tester vos
modifications en utilisant la commande xrandr. `xrandr` devrait lister
la résolution que vous avez choisie, et `xrandr --q1` devrait faire de
même. Aucune autre résolution ne devrait être listée. Vous pouvez dès
lors retester votre application Windows, espérons-le avec plus de
chance. Souvenez-vous de commenter la ligne et de redémarrer Xorg quand
vous avez terminé si vous requerrez d'autres résolutions pour d'autres
logiciels.

### Son

#### Les MP3 ne sont pas joués dans Windows Media Player ou les applications qui en dépendent

Pour que le son MP3 soit joué directement dans les applications
utilisant les moteur et codecs WMP, la bibliothèque `libmpg123` 32 bits
doit être installée sur votre système et Wine doit avoir été compilé
avec la prise en charge du MP3. Tous les paquets de distributions ne
l'intègrent pas ; les paquets openSUSE en particulier sont réputés être
compilés sans prise en charge MP3. Si le paquet Wine de votre
distribution ne prend pas en charge le MP3, consultez le forum
utilisateur de votre distribution pour voir si un paquet le prend en
charge (pour openSUSE, le paquet `wine-mp3` disponible sur
[Packman](https://packman.links3linux.org/) ajoute la prise en charge mp3
à Wine en utilisant `libmpg123`).

La solution de rechange à l'absence de libmpg123 et/ou winemp3.acm
consiste en l'utilisation du codec installé par WMP9, l3codeca.acm.
Copiez l3codeca.acm dans le répertoire /windows/system32 du préfixe Wine
(ou utilisez winetricks pour installer WMP9), créez ensuite un lien
symbolique nommé winemp3.acm pointant vers ce dernier dans le même
répertoire. Wine utilisera par la suite le codec natif pour jouer les
MP3.

Cela n'affecte que WMP et les applications qui en dépendent pour la
lecture MP3 (Powerpoint inclus). Les applications installant leur propre
codec MP3, comme Winamp ou VLC Player, devraient pouvoir jouer les MP3
sans mesure particulière.

#### Son absent ou corrompu sur les systèmes utilisant PulseAudio

L'absence de son ou un son corrompu était un problème sur certains
systèmes avec Pulseaudio dans les anciennes versions de Wine utilisant
le pilote winealsa. Si vous utilisez une version de Wine antérieure à
1.8, veuillez la mettre à niveau, car la plupart des problèmes devraient
être résolus par le pilote winepulse. Si le problème persiste, essayez
de supprimer la définition de la variable d'environnement
PULSE_LATENCY_MSEC. Certains paquets de distributions (notamment Debian)
définissaient auparavant cette variable pour contourner les problèmes de
son avec le pilote winealsa, mais cela n'est plus nécessaire avec le
pilote winepulse et pourrait empêcher le son de fonctionner.

### Je ne parviens pas à éjecter un CD/DVD

Essayez `wine eject`. Cela permet de libérer, déverrouiller et éjecter
le disque. Assurez-vous que le lecteur est associé à un CD-ROM/DVD dans
`winecfg` et spécifiez la lettre du lecteur sur la ligne de commande,
p.ex.

```sh
wine eject d:
```

### Réseau

#### Mon programme n'a pas accès au réseau, à la différence d'autres applications

**Note** : ces instructions ne sont valables que pour les plus anciennes
installations Wine. Si vous utilisez Wine 1.x et que votre application
n'a toujours pas accès au réseau, ceci pourrait également être utile. Si
vous exécutez Wine 1.x et que les instructions ci-dessous fonctionnent
pour vous, **rapportez un bug** afin que nous puissions améliorer
l'expérience Wine des autres utilisateurs.

Assurez-vous que votre nom d'hôte est résolu en l'adresse IP de votre
interface réseau. Pour vérifier si ce problème vous affecte, exécutez
`hostname -i`. Si cela retourne une adresse IP commençant par « 127. »,
continuez à lire.

Pour effectuer une configuration correcte, tapez ce qui suit dans un
terminal :

```sh
hostname
```

Cela renverra votre nom d'hôte comme votre ordinateur se le représente.
Éditez ensuite le fichier `/etc/hosts` avec les privilèges
d'administrateur système et vérifiez s'il contient une entrée pour votre
nom d'hôte. En supposant que votre nom d'hôte est « votrehôte » et que
votre adresse IP est 192.168.0.23, l'entrée pourrait ressembler à :

```
127.0.0.1      votrehôte.votredomaine.com      votrehôte
```

Remplacez cette ligne par (ou ajoutez-la, s'il n'y en a pas) :

```
192.168.0.23   votrehôte.votredomaine.com      votrehôte
```

Pour la plupart des jeux Windows ayant des problèmes d'accès réseau,
c'est tout ce qu'il y a à faire.

#### Pourquoi le DNS ne résout-il pas dans les systèmes d'exploitation 64 bits ?

Beaucoup de distributions ne fournissent pas toutes les bibliothèques de
compatibilité 32 bits requises par wine. Dans ce cas précis, wine
nécessite des bibliothèques DNS 32 bits. Sous Ubuntu/Debian, il s'agit
du paquet `lib32nss-mdns`. Pour l'installer sur ces systèmes, utilisez :

```sh
sudo apt-get install lib32nss-mdns
```

Pour d'autres systèmes d'exploitation, le nom du paquet et la méthode
d'installation peuvent différer. Consultez les canaux de support de
votre distribution.

### J'ai effacé mon menu Wine, et je ne parviens pas à le récupérer

Plutôt que de réellement effacer quoi que ce soit, les éditeurs de menus
dans les bureaux Unix marquent simplement les entrées de menu comme
étant cachées afin qu'elles n'apparaissent pas dans le menu. Ainsi,
elles demeurent dissimulées même après la réinstallation de
l'application. Tout d'abord, vérifiez si ces entrées de menu peuvent
être trouvées dans votre éditeur de menus et réactivées.

Ces informations sont stockées dans `~/.config/menus/applications.menu`.
Éditez `~/.config/menus/applications.menu` et vous devriez trouver une
section à la fin ressemblant à :

```xml
<Menu>
    <Name>wine-wine</Name>
    <Deleted/>
</Menu>
```

ou peut-être ceci :

```xml
<Menu>
    <Name>wine-wine</Name>
    <Menu>
        <Name>wine-Programs</Name>
        <Menu>
            <Name>wine-Programs-AutoHotkey</Name>
            <DirectoryDir>/home/user/.local/share/desktop-directories</DirectoryDir>
        </Menu>
    </Menu>
    <Deleted/>
</Menu>
```

Supprimez la ligne <Deleted/> et votre menu Wine réapparaîtra.

### Certaines combinaisons de touches ne fonctionnent pas dans mon application

Même en mode plein écran, les gestionnaires de fenêtres capturent
typiquement certaines touches. Par exemple, dans KDE et GNOME, Alt+Clic
gauche est utilisé par défaut pour déplacer la fenêtre de l'application
entière. Par conséquent, cette combinaison de touches n'est pas
disponible pour les applications dans Wine. Vous devez désactiver les
combinaisons entrant en collision dans votre gestionnaire de fenêtres.
Pour KDE, voyez Centre de contrôle/Comportement des fenêtres ou (mieux)
les raccourcis globaux Réglages spécifiques aux
fenêtres/Contournements/Bloquer. Pour GNOME, voyez
Système/Préférences/Fenêtres et modifiez le réglage « Touche de
mouvement ». Vérifiez également Système/Préférences/Raccourcis clavier
pour des combinaisons de touches spécifiques.

(Mots-clés : clavier, raccourci, modificateur, Alt, Ctrl, Contrôle.)

### Où obtenir de l'aide additionnelle ?

En plus de [ce wiki](fr/home), consultez la [documentation
sur Wine HQ](fr/Documentation) et les [forums
utilisateurs](https://forums.winehq.org/). Si vous êtes un éditeur de
logiciels portant une application avec Winelib, vous pouvez également
fréquenter [wine-devel](fr/Forums).

Pour de l'aide sur une application spécifique, parcourez la [base de
données d'applications](https://appdb.winehq.org), où les utilisateurs
partagent leur expérience en soumettant des données de test, des trucs
et astuces et en posant des questions.

Le canal IRC [\#WineHQ](irc://irc.libera.chat:6697/#winehq) sur
irc.libera.chat. Des utilisateurs de Wine bien informés y traînent,
ainsi que bien souvent des développeurs. Voyez la page
[IRC](fr/IRC) pour plus d'informations.

### Je pense avoir trouvé un bug. Comment le signaler à l'équipe de programmation de Wine ?

Les bugs devraient être rapportés dans notre système Bugzilla en ligne
(https://bugs.winehq.org/). Pour accroître la productivité des
développeurs et faciliter la résolution des bugs soumis, **lisez
l'article Wiki sur les [bugs](Bugs)**. Un rapport de bug mal
rédigé peut être marqué invalide et fermé, ne vous amenant pas plus près
d'une résolution de votre problème. Des rapports de bugs de haute
qualité sont une composante essentielle du processus d'amélioration de
Wine.

Notez que vous devez *généralement* éviter des soumettre des rapports de
bugs si vous avez utilisé des [applications
tierces](Third-Party-Applications) ou des remplacements par
des DLL natives.

### Ma souris sautille/change souvent de position dans les jeux

Voyez bug #27156 et <https://bugs.freedesktop.org/show_bug.cgi?id=30068>.

### Les applications 16 bits ne démarrent pas

Voyez le bug #36664. Pour les noyaux Linux 64 bits 3.15+, 3.14.6+,
3.10.42+, 3.13.22+, 3.4.92+ et 3.2.60+, vous devrez exécuter :

```sh
echo 1 > /proc/sys/abi/ldt16
```

comme `root`, mais soyez conscient que cela a des implications de
sécurité.

Si votre noyau est plus ancien que les versions précitées,
`/proc/sys/abi/ldt16` sera probablement absent.

Le correctif de sécurité est espfix64 et n'est présent à l'heure
actuelle que dans la branche de développement du noyau Linux 3.16.
J'ajouterai ultérieurement une liste des noyaux contenant le correctif
de sécurité vu son probable rétroportage une fois testé. La commande sur
`/proc/sys/abi/ldt16` sera requise tant sur les noyaux contenant le
correctif, que sur les noyaux actuels n'en disposant pas.

## Risques

### Wine est compatible avec les logiciels malveillants

Le fait simple d'exécuter Wine sur un OS non Windows ne signifie pas que
vous être protégé(e) des virus, chevaux de Troie et autres logiciels
malveillants.

Plusieurs possibilités existent pour vous protéger :

- N'exécutez jamais d'exécutable provenant de sites suspects. [Des
  infections ont déjà eu
  lieu.](https://www.winehq.org/pipermail/wine-devel/2007-January/053719.html)
- Dans les navigateurs web et clients de courriel, méfiez-vous des liens
  vers des URL suspectes ou incompréhensibles.
- N'exécutez jamais d'application (y compris d'application Wine) comme
  root (voyez [plus haut](#faut-il-exécuter-wine-comme-root)).
- Utilisez un antivirus. P.ex. [ClamAV](https://www.clamav.org) est un
  antivirus libre à considérer si vous craignez une infection ; voyez
  également les [notes Ubuntu sur la façon d'utiliser
  ClamAV](https://help.ubuntu.com/community/ClamAV). Aucun antivirus
  n'est néanmoins efficace à 100 %.
- Supprimer le lecteur Z: par défaut de Wine, qui est associé au
  répertoire racine Unix, n'est qu'une faible défense. Cela n'empêchera
  pas des applications Windows de lire votre système de fichiers tout
  entier, et **vous ne pourrez exécuter d'applications Windows non
  accessibles à partir d'un lecteur Wine (comme C: ou D:)**. Une parade
  consiste à copier/déplacer les programmes d'installation téléchargés
  dans `~/.wine/drive_c` (ou utiliser des liens symboliques) avant de
  les exécuter.
- Si vous exécutez des applications que vous suspectez être infectées,
  utilisez un compte utilisateur Linux dédié ou une machine virtuelle
  (l'analyseur de logiciels malveillants ZeroWine fonctionne de cette
  manière).

### Quelle est la capacité de Wine à isoler les applications Windows ?

**Wine ne fournit aucune isolation.** Lorsqu'elle tourne sous Wine, une
application Windows a exactement les mêmes droits que l'utilisateur qui
l'a exécutée. Wine n'empêche pas (ni ne le peut) une application Windows
d'effectuer directement des appels système natifs, d'endommager vos
fichiers, d'altérer vos scripts de démarrage ou tout autre calamité.

Vous devez utiliser AppArmor, SELinux ou un autre type de machine
virtuelle si vous voulez isoler correctement des applications Windows.

Notez que le verbe `sandbox` de [winetricks](Winetricks)
supprime simplement l'intégration au bureau et le lien symbolique Z:
mais n'est pas réellement une sandbox. Il protège contre les erreurs
plutôt que contre la malveillance. C'est utile p.ex. pour empêcher à des
jeux d'enregistrer leurs réglages dans des sous-répertoires arbitraires
de votre répertoire personnel.
