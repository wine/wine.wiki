---
title: wine - exécuter des programmes Windows sur Unix
---

<small>
&nbsp;[:flag_gb: English](Man-Pages/wine)
&nbsp;[:flag_de: Deutsch](de/Man-Pages/wine)
&nbsp;[:flag_pl: Polski](pl/Man-Pages/wine)
</small>

-----


## SYNOPSIS

**wine**
_programme_ [_arguments_]<br>
**wine --help**<br>
**wine --version**

Pour des instructions sur le passage d'arguments aux programmes Windows, veuillez lire la section
**PROGRAMME/ARGUMENTS**
de la page de manuel.

## DESCRIPTION

**wine**
charge et exécute le programme indiqué, qui peut être un exécutable DOS, Windows
3.x, Win32 ou Win64 (sur les systèmes 64 bits).

Pour déboguer wine, utilisez plutôt
**winedbg**.

Pour exécuter des applications en ligne de commande (programmes Windows
console), préférez
**wineconsole**.
Cela permet d'afficher la sortie dans une fenêtre séparée.
Si vous n'utilisez pas
**wineconsole**
pour les programmes en ligne de commande, le support console sera très limité et votre
programme pourrait ne pas fonctionner correctement.

Lorsque wine est invoqué avec
**--help**
ou
**--version**
pour seul argument, il
affichera seulement un petit message d'aide ou sa version respectivement, puis se terminera.

## PROGRAMME/ARGUMENTS

Le nom du programme peut être spécifié au format DOS
(_C:\\\\WINDOWS\\\\SOL.EXE_)
ou au format Unix
(_/msdos/windows/sol.exe_).
Vous pouvez passer des arguments au programme exécuté en les ajoutant
à la fin de la ligne de commande invoquant
**wine**
(par exemple : _wine notepad C:\\\\TEMP\\\\LISEZMOI.TXT_).
Notez que vous devrez protéger les caractères spéciaux (et les espaces)
en utilisant un '\\' lorsque vous invoquez Wine depuis
un shell, par exemple :

wine C:\\\\Program\\ Files\\\\MonProg\\\\test.exe

Il peut également s'agir d'un des exécutables Windows livrés avec Wine,
auquel cas la spécification d'un chemin complet n'est pas obligatoire,
p.ex. _wine explorer_ ou _wine notepad_.


## ENVIRONNEMENT

**wine**
passe les variables d'environnement du shell depuis lequel il
est lancé au processus Windows/DOS exécuté. Utilisez donc la syntaxe appropriée
à votre shell pour déclarer les variables d'environnement dont vous avez besoin.

* **WINEPREFIX**<br>
  Si définie, le contenu de cette variable est pris comme le nom du répertoire où
  Wine stocke ses données (la valeur par défaut est
  _\$HOME/.wine_).
  Ce répertoire est également utilisé pour identifier le socket utilisé pour
  communiquer avec
  **wineserver**.
  Tous les processus
  **wine**
  utilisant le même
  **wineserver**
  (c'est-à-dire le même utilisateur) partagent certains éléments comme la base de registre,
  la mémoire partagée et les objets du noyau.
  En donnant à
  **WINEPREFIX**
  une valeur spécifique pour différents processus
  **wine**,
  il est possible d'exécuter plusieurs sessions de
  **wine**
  totalement indépendantes.
* **WINESERVER**<br>
  Spécifie le chemin et le nom de l'exécutable
  **wineserver**.
  Si cette variable n'est pas définie, Wine essaiera de charger un
  fichier nommé « wineserver » dans le chemin système ou quelques autres
  emplacements potentiels.
* **WINEDEBUG**<br>
  Active ou désactive les messages de débogage. La syntaxe est :
  [_classe_][**+**|**-**]_canal_[,[_classe2_][**+**|**-**]_canal2_]

  La
  _classe_
  est optionnelle et peut avoir une des valeurs suivantes :
  **err**,
  **warn**,
  **fixme**
  ou
  **trace**.
  Si elle n'est pas spécifiée, tous les messages de débogage pour le canal
  associé seront activés. Chaque canal imprimera des messages à propos
  d'un composant particulier de Wine.
  Le caractère suivant peut être **+** ou **-** pour activer/désactiver
  le canal spécifié. Si aucune
  _classe_
  n'est spécifiée, le caractère **+** peut être omis. Notez que les espaces ne sont pas
  autorisées dans cette chaîne de caractères.

  Exemples :
    * WINEDEBUG=warn+all<br>
  activera tous les messages d'avertissement (recommandé pour le débogage).<br>
    * WINEDEBUG=warn+dll,+heap<br>
  activera tous messages d'avertissement sur les DLL, et tous les messages sur le tas.<br>
    * WINEDEBUG=fixme-all,warn+cursor,+relay<br>
  désactivera tous les messages FIXME, activera les messages d'avertissement sur le composant cursor et
  activera tous les messages du canal relay (appels de l'API).<br>
    * WINEDEBUG=relay<br>
  activera tous les messages du canal relay. Pour un contrôle plus fin sur l'inclusion et
  l'exclusion des fonctions et DLL des traces relay, utilisez la clé
  **HKEY_CURRENT_USER\\\\Software\\\\Wine\\\\Debug**
  de la base de registre.

  Pour plus d'informations sur les messages de débogage, référez-vous au chapitre
  _Exécution de Wine_
  du guide de l'utilisateur de Wine.

* **WINEDLLPATH**<br>
  Spécifie le(s) chemin(s) où chercher les DLL intégrées et les applications
  Winelib. C'est une liste de répertoires séparés par des « : ». En plus des
  répertoires spécifiés dans
  **WINEDLLPATH**,
  Wine utilisera aussi le répertoire d'installation.
* **WINEDLLOVERRIDES**<br>
  Définit le type de remplacement et l'ordre de chargement des DLL utilisées lors du
  processus de chargement d'une DLL. Deux types de bibliothèques peuvent actuellement
  être chargés dans l'espace d'adressage d'un processus : les DLL natives de
  Windows
  (_native_) et les DLL intégrées à Wine (_builtin_).
  Le type peut être abrégé avec la première lettre du type
  (_n_ ou _b_).
  La bibliothèque peut également être désactivée (''). Les séquences d'ordres
  doivent être séparées par des virgules.

  Chaque DLL peut avoir son ordre de chargement propre. L'ordre de chargement
  détermine quelle version de la DLL doit être chargée dans l'espace
  d'adressage. Si la première tentative échoue, la suivante est essayée et
  ainsi de suite. Plusieurs bibliothèques avec le même ordre de chargement
  peuvent être séparées par des virgules. Il est également possible de spécifier
  différents ordres de chargements pour différentes bibliothèques en séparant les
  entrées par « ; ».

  L'ordre de chargement pour une DLL 16 bits est toujours défini par l'ordre de
  chargement de la DLL 32 bits qui la contient (qui peut être identifié en
  observant le lien symbolique du fichier .dll.so 16 bits). Par exemple, si
  _ole32.dll_ est configurée comme builtin, _storage.dll_ sera également chargée comme
  builtin puisque la DLL 32 bits _ole32.dll_ contient la DLL 16 bits _storage.dll_.

  Exemples :
    * WINEDLLOVERRIDES="comdlg32,shell32=n,b"<br><br>
  Charge comdlg32 et shell32 comme des DLL windows natives, ou la version
  intégrée en cas d'échec.
    * WINEDLLOVERRIDES="comdlg32,shell32=n;c:\\\\foo\\\\bar\\\\baz=b"<br><br>
  Charge les bibliothèques windows natives comdlg32 et shell32. De plus, si une
  application demande le chargement de _c:\\foo\\bar\\baz.dll_, charge la
  bibliothèque intégrée _baz_.
    * WINEDLLOVERRIDES="comdlg32=b,n;shell32=b;comctl32=n;oleaut32="<br><br>
  Charge la bibliothèque intégrée comdlg32, ou la version native en cas
  d'échec ; charge la version intégrée de shell32 et la version native de
  comctl32 ; oleaut32 sera désactivée.

* **WINEARCH**<br>
  Spécifie l'architecture Windows à prendre en charge. Peut être
  **win32**
  (prise en charge des applications 32 bits uniquement), ou
  **win64**
  (prise en charge des applications 64 bits, et 32 bits en mode WoW64).<br>
  L'architecture prise en charge par un préfixe Wine donné est déterminée
  au moment de sa création et ne peut être modifiée ultérieurement.
  Si vous exécutez Wine avec un préfixe préexistant, il refusera de démarrer
  si
  **WINEARCH**
  ne correspond pas à l'architecture du préfixe.
* **DISPLAY**<br>
  Spécifie l'affichage X11 à utiliser.
* Variables de configuration du pilote audio OSS :<br>
* **AUDIODEV**<br>
  Définit le périphérique pour les entrées/sorties audio, par défaut
  _/dev/dsp_.
* **MIXERDEV**<br>
  Définit le périphérique pour les contrôles du mixeur, par défaut
  _/dev/mixer_.
* **MIDIDEV**<br>
  Définit le périphérique pour le séquenceur MIDI, par défaut
  _/dev/sequencer_.

## FICHIERS


* _wine_<br>
  Le chargeur de programme de Wine.
* _wineconsole_<br>
  Le chargeur de programme de Wine pour les applications en mode console (CUI).
* _wineserver_<br>
  Le serveur Wine.
* _winedbg_<br>
  Le débogueur de Wine.
* _\$WINEPREFIX/dosdevices_<br>
  Répertoire contenant le mapping des périphériques DOS. Chaque fichier dans ce
  répertoire est un lien symbolique vers le fichier périphérique Unix qui implémente
  un périphérique donné. Par exemple, si COM1 est mappé sur _/dev/ttyS0_, vous aurez un
  lien symbolique de la forme _\$WINEPREFIX/dosdevices/com1_ -\> _/dev/ttyS0_.<br>
  Les lecteurs DOS sont aussi définis à l'aide de liens symboliques ; par exemple, si le
  lecteur D: correspond au CDROM monté sur _/mnt/cdrom_, vous aurez un lien symbolique
  _\$WINEPREFIX/dosdevices/d:_ -\> _/mnt/cdrom_. Le périphérique Unix correspondant à un lecteur
  DOS peut être spécifié de la même façon, à l'exception du fait qu'il faut utiliser « :: » à
  la place de « : ». Dans l'exemple précédent, si le lecteur CDROM est monté depuis /dev/hdc,
  le lien symbolique correspondant sera _\$WINEPREFIX/dosdevices/d::_ -\> _/dev/hdc_.

## AUTEURS

Wine est disponible grâce au travail de nombreux développeurs. Pour une liste
des auteurs, référez-vous au fichier
_AUTHORS_
à la racine de la distribution des sources.

## COPYRIGHT

Wine peut être distribué selon les termes de la licence LGPL. Une copie de cette
licence se trouve dans le fichier
_COPYING.LIB_
à la racine de la distribution des sources.

## BUGS


Un rapport sur la compatibilité de nombreuses applications est disponible sur la
[**base de données d'applications de Wine**](https://appdb.winehq.org).
N'hésitez pas à y ajouter des entrées pour les applications que vous
exécutez actuellement, si nécessaire.

Les bugs peuvent être signalés (en anglais) sur le
[**système de suivi des problèmes de Wine**](https://bugs.winehq.org).

## DISPONIBILITÉ

La version publique la plus récente de Wine est disponible sur WineHQ, le
[**quartier général du développement de Wine**](https://www.winehq.org/).

## VOIR AUSSI

[**wineserver**](./wineserver),
[**winedbg**](./winedbg),<br>
[**Documentation et support de Wine**](https://www.winehq.org/help).
