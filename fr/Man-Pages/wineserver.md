---
title: wineserver - le serveur Wine
---

<small>
&nbsp;[:flag_gb: English](Man-Pages/wineserver)
&nbsp;[:flag_de: Deutsch](de/Man-Pages/wineserver)
</small>

-----


## SYNOPSIS

**wineserver**
[_options_]

## DESCRIPTION

**wineserver**
est un démon qui fournit à Wine à peu près les mêmes services
que le noyau de Windows fournit sous Windows.

**wineserver**
est normalement lancé automatiquement lorsque [**wine**](./wine) démarre, mais
il peut être utile de le démarrer explicitement avec certaines options,
détaillées ci-après.

## OPTIONS


* **-d**[_n_], **--debug**[**=**_n_]<br>
  Définit le niveau de débogage à
  _n_.
  0 signifie aucune information de débogage, 1 est le niveau normal et 2 indique
  un débogage plus important. Si
  _n_
  n'est pas spécifié, la valeur par défaut est 1. La sortie de débogage sera
  stderr. [**wine**](./wine) active automatiquement le débogage au niveau normal lorsqu'il
  démarre **wineserver** si l'option +server est indiquée dans la variable
  **WINEDEBUG**.
* **-f**, **--foreground**<br>
  Laisse le serveur au premier plan pour un débogage plus aisé, par
  exemple lorsqu'il est exécuté dans un débogueur.
* **-h**, **--help**<br>
  Affiche un message d'aide.
* **-k**[_n_], **--kill**[**=**_n_]<br>
  Termine le
  **wineserver**
  actuellement exécuté en lui envoyant facultativement le signal _n_. Si
  aucun signal n'est spécifié, un signal **SIGINT** est envoyé en premier,
  puis un signal **SIGKILL**. L'instance de **wineserver** à arrêter
  est déterminée par la variable d'environnement **WINEPREFIX**.
* **-p**[_n_], **--persistent**[**=**_n_]<br>
  Spécifie le délai de persistance de **wineserver**, c'est-à-dire le
  temps pendant lequel le serveur continuera à tourner après que tous les
  processus clients se sont terminés. Ceci évite le coût inhérent à l'arrêt
  puis au redémarrage du serveur lorsque des programmes sont lancés successivement
  à intervalle rapproché.
  Le délai d'attente _n_ est exprimé en secondes (3 secondes par défaut).
  Si _n_ n'est pas spécifié, le serveur s'exécute indéfiniment.
* **-v**, **--version**<br>
  Affiche les informations sur la version et se termine.
* **-w**, **--wait**<br>
  Attend que le
  **wineserver**
  actuellement exécuté se termine.

## ENVIRONNEMENT


* **WINEPREFIX**<br>
  Si définie, cette variable indique le nom du répertoire où
  **wineserver**
  stocke ses données (_\$HOME/.wine_ par défaut). Tous les processus
  **wine**
  utilisant le même
  **wineserver**
  (c'est-à-dire pour un même utilisateur) partagent certains éléments comme la base de registre,
  la mémoire partagée et les objets du noyau.
  En donnant à
  **WINEPREFIX**
  une valeur spécifique pour différents processus Wine, il est possible d'exécuter plusieurs
  sessions de Wine totalement indépendantes.

## FICHIERS


* **~/.wine**<br>
  Répertoire contenant les données utilisateur gérées par
  **wine**.
* **/tmp/.wine-**_uid_<br>
  Répertoire contenant le socket de serveur Unix et le fichier de verrouillage.
  Ces fichiers sont créés dans un sous-répertoire dont le nom est dérivé
  des périphérique et numéros d'inodes du répertoire **WINEPREFIX**.

## AUTEURS

L'auteur originel de
**wineserver**
est Alexandre Julliard. Beaucoup d'autres personnes ont contribué des nouvelles fonctionnalités
et des corrections de bugs. Pour une liste complète, consultez l'historique git.

## BUGS

Les bugs peuvent être signalés (en anglais) sur le
[**système de suivi des problèmes de Wine**](https://bugs.winehq.org).

## DISPONIBILITÉ

**wineserver**
fait partie de la distribution de Wine, disponible depuis WineHQ, le
[**quartier général des développeurs de Wine**](https://www.winehq.org/).

## VOIR AUSSI

[**wine**](./wine),<br>
[**Documentation et support de Wine**](https://www.winehq.org/help).
