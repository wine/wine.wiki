---
title: winemaker - générer une infrastructure de construction pour la compilation de programmes Windows sur UNIX
---

<small>
&nbsp;[:flag_gb: English](Man-Pages/winemaker)
&nbsp;[:flag_de: Deutsch](de/Man-Pages/winemaker)
</small>

-----


## SYNOPSIS

**winemaker**
[
**--nobanner** ] [ **--backup** | **--nobackup** ] [ **--nosource-fix**
]<br>
  [
**--lower-none** | **--lower-all** | **--lower-uppercase**
]<br>
  [
**--lower-include** | **--nolower-include** ]&nbsp;[ **--mfc** | **--nomfc**
]<br>
  [
**--guiexe** | **--windows** | **--cuiexe** | **--console** | **--dll** | **--lib**
]<br>
  [
**-D**_macro_[=_défn_] ] [_&nbsp;_**-I**_rép_ ]&nbsp;[ **-P**_rép_ ] [ **-i**_dll_ ] [ **-L**_rép_ ] [ **-l**_bibliothèque_
]<br>
  [
**--nodlls** ] [ **--nomsvcrt** ] [ **--interactive** ] [ **--single-target** _nom_
]<br>
  [
**--generated-files** ] [ **--nogenerated-files** ]<br>
  [
**--wine32** ]<br>
  _répertoire_de_travail_ | _fichier_projet_ | _fichier_espace_de_travail_


## DESCRIPTION


**winemaker**
est un script perl conçu pour vous aider à entamer le
processus de conversion de vos sources Windows en programmes Winelib.

À cet effet, il peut effectuer les opérations suivantes :

-&nbsp;renommer vos fichiers sources et répertoires en minuscules s'ils ont été
convertis en majuscules durant le transfert.

-&nbsp;convertir les fins de ligne DOS en fins de ligne UNIX (CRLF vers LF).

-&nbsp;parcourir les directives d'inclusion et les références aux fichiers
de ressources pour y remplacer les backslashs par des slashs.

-&nbsp;durant l'étape ci-dessus,
**winemaker**
va également effectuer une recherche insensible à la casse du fichier
référencé dans le chemin d'inclusion, et réécrire la directive d'inclusion
avec la casse correcte si nécessaire.

-&nbsp;**winemaker**
recherchera également d'autres problèmes plus exotiques comme l'emploi
de _#pragma pack_, l'utilisation de _afxres.h_ dans des projets
non MFC, etc. Quand il trouve de tels points nébuleux, il émettra des
avertissements.

-&nbsp;**winemaker**
peut également balayer un arbre de répertoires complet en une seule passe,
deviner quels sont les exécutables et bibliothèques en cours de construction,
les faire correspondre à des fichiers sources, et générer le _Makefile_
correspondant.

-&nbsp;finalement,
**winemaker**
générera un _Makefile_ global pour une utilisation classique.

-&nbsp;**winemaker**
comprend les projets de type MFC, et génère des fichiers appropriés.

-&nbsp;**winemaker**
est capable de lire des fichiers projets existants (dsp, dsw, vcproj et sln).


## OPTIONS


* **--nobanner**<br>
  Désactiver l'affichage de la bannière.
* **--backup**<br>
  Effectuer une sauvegarde préalable de tous les fichiers modifiés.
  Comportement par défaut.
* **--nobackup**<br>
  Ne pas effectuer de sauvegarde des fichiers sources modifiés.
* **--nosource-fix**<br>
  Ne pas essayer de corriger les fichiers sources (p.ex. la conversion
  DOS vers UNIX). Cela évite des messages d'erreur si des fichiers sont
  en lecture seule.
* **--lower-all**<br>
  Renommer tous les fichiers et répertoires en minuscules.
* **--lower-uppercase**<br>
  Ne renommer que les fichiers et répertoires qui ont un nom composé
  uniquement de majuscules.
  Ainsi, _HELLO.C_ serait renommé, mais pas _World.c_.
* **--lower-none**<br>
  Ne pas renommer de fichiers et répertoires en minuscules. Notez que cela
  n'empêche pas le renommage d'un fichier si son extension ne peut être traitée
  telle quelle, comme par exemple « .Cxx ». Comportement par défaut.
* **--lower-include** <br>
  Convertir en minuscules les noms de fichiers associés à des directives
  d'inclusion (ou à d'autres formes de références de fichiers pour les
  fichiers ressources) que
  **winemaker**
  n'arrive pas à trouver. Comportement par défaut.
* **--nolower-include** <br>
  Ne pas modifier la directive d'inclusion si le fichier référencé ne peut
  être trouvé.
* **--guiexe** | **--windows**<br>
  Présumer une application graphique quand une cible exécutable ou une cible d'un
  type inconnu est rencontrée.
  Comportement par défaut.
* **--cuiexe** | **--console**<br>
  Présumer une application en mode console quand une cible exécutable ou une cible d'un
  type inconnu est rencontrée.
* **--dll**<br>
  Présumer une DLL quand une cible d'un type inconnu est rencontrée (c.-à-d. si
  **winemaker**
  ne peut déterminer s'il s'agit d'un exécutable, d'une DLL ou d'une bibliothèque statique).
* **--lib**<br>
  Présumer une bibliothèque statique quand une cible d'un type inconnu est rencontrée (c.-à-d. si
  **winemaker**
  ne peut déterminer s'il s'agit d'un exécutable, d'une DLL ou d'une bibliothèque statique).
* **--mfc**<br>
  Spécifier que les cibles utilisent les MFC. Dans ce cas,
  **winemaker**
  adapte les chemins d'inclusion et des bibliothèques en conséquence,
  et lie la cible avec la bibliothèque MFC.
* **--nomfc**<br>
  Spécifier que les cibles n'utilisent pas les MFC. Cette option empêche
  l'utilisation des bibliothèques MFC même si
  **winemaker**
  rencontre des fichiers _stdafx.cpp_ ou _stdafx.h_ qui activeraient
  les MFC automatiquement en temps normal si ni **--nomfc** ni **--mfc** n'était
  spécifiée.
* **-D**_macro_[**=**_défn_]<br>
  Ajouter la définition de macro spécifiée à la liste globale des
  définitions de macros.
* **-I**_répertoire_<br>
  Ajouter le répertoire spécifié au chemin global d'inclusion.
* **-P**_répertoire_<br>
  Ajouter le répertoire spécifié au chemin global des DLL.
* **-i**_dll_<br>
  Ajouter la bibliothèque Winelib à la liste global de bibliothèques Winelib
  à importer.
* **-L**_répertoire_<br>
  Ajouter le répertoire spécifié au chemin global des bibliothèques.
* **-l**_bibliothèque_<br>
  Ajouter la bibliothèque spécifiée à la liste globale de bibliothèques à utiliser lors de l'édition des liens.
* **--nodlls**<br>
  Ne pas utiliser l'ensemble standard de bibliothèques Winelib pour les imports,
  c.-à-d. que toute DLL utilisée par votre code doit être explicitement spécifiée à l'aide d'options
  **-i**.
  L'ensemble standard de bibliothèques est : _odbc32.dll_, _odbccp32.dll_, _ole32.dll_,
  _oleaut32.dll_ et _winspool.drv_.
* **--nomsvcrt**<br>
  Définir certaines options afin que **winegcc** n'utilise pas
  msvcrt durant la compilation. Utilisez cette option si certains fichiers cpp
  incluent _\<string\>_.
* **--interactive**<br>
  Utiliser le mode interactif. Dans ce mode,
  **winemaker**
  demandera de confirmer la liste de cibles pour chaque répertoire, et ensuite
  de fournir des options spécifiques de répertoire et/ou de cible.
* **--single-target** _nom_<br>
  Spécifier qu'il n'y a qu'une seule cible, appelée _nom_.
* **--generated-files**<br>
  Générer le _Makefile_. Comportement par défaut.
* **--nogenerated-files**<br>
  Ne pas générer le _Makefile_.
* **--wine32**<br>
  Générer une cible 32 bits. Utile sur les systèmes wow64. Sans cette option,
  l'architecture par défaut est utilisée.


## EXEMPLES


Voici quelques exemples typiques d'utilisation de
**winemaker**
:

\$ winemaker --lower-uppercase -DSTRICT .

Recherche des fichiers sources dans le répertoire courant et ses
sous-répertoires. Quand un fichier ou répertoire a un nom composé
uniquement de majuscules, le renomme en minuscules. Ensuite, adapte tous
ces fichiers sources pour une compilation avec Winelib, et génère des
_Makefile_s. **-DSTRICT** spécifie que la macro **STRICT** doit
être définie lors de la compilation des sources.
Finalement, un _Makefile_ est créé.

L'étape suivante serait :

\$ make

Si vous obtenez des erreurs de compilation à ce moment (ce qui est plus que
probable pour un projet d'une taille raisonnable), vous devriez consulter
le guide de l'utilisateur de Winelib pour trouver des moyens de les résoudre.

Pour un projet utilisant les MFC, vous devriez plutôt exécuter les commandes
suivantes&nbsp;:

\$ winemaker --lower-uppercase --mfc .<br>
\$ make

Pour un fichier projet existant, vous devriez exécuter les commandes suivantes :

\$ winemaker monprojet.dsp<br>
\$ make



## LIMITATIONS / PROBLÈMES


Dans certains cas, vous devrez éditer manuellement le _Makefile_ ou les fichiers
sources.

En supposant que l'exécutable ou la bibliothèque windows est disponible, on peut
utiliser
**winedump**
pour en déterminer le type (graphique ou en mode console) et les
bibliothèques auxquelles il est lié (pour les exécutables), ou quelles fonctions
elle exporte (pour les bibliothèques). On pourrait ensuite restituer tous ces
réglages pour la cible Winelib correspondante.

De plus,
**winemaker**
n'est pas très apte à trouver la bibliothèque contenant l'exécutable : elle doit
être soit dans le répertoire courant, soit dans un des répertoires de
**LD_LIBRARY_PATH**.

**winemaker**
ne prend pas encore en charge les fichiers de messages, ni le compilateur
de messages.

Les bugs peuvent être signalés (en anglais) sur le
[**système de suivi des problèmes de Wine**](https://bugs.winehq.org).

## AUTEURS

François Gouget pour CodeWeavers<br>
Dimitrie O. Paun<br>
André Hentschel

## DISPONIBILITÉ

**winemaker** fait partie de la distribution de Wine, qui est disponible sur WineHQ, le
[**quartier général du développement de Wine**](https://www.winehq.org/).

## VOIR AUSSI

[**wine**](./wine),<br>
[**Documentation et support de Wine**](https://www.winehq.org/help).
