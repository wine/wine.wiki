---
title: Fonctionnalités de Wine
---

<small>
&nbsp;[:flag_gb: English](Wine-Features)
</small>

-----

## Compatibilité des binaires

- Charge des programmes et bibliothèques pour Windows
  9x/NT/2000/XP/Vista/7/8/10 et DOS
- Schéma de mémoire compatible Win32, gestion des exceptions, threads et
  processus
- Conçu pour les systèmes d'exploitations compatibles POSIX (ex. Linux,
  macOS et FreeBSD) et Android
- Compatibilité "bug-pour-bug" avec Windows

## Graphiques

- Affichage pour X11, permet un affichage distant sur tout terminal X
- Affichage pour macOS et Android
- X11, TrueType (.ttf/.ttc) et Polices Bitmap Windows (.fon)
- Support des jeux et applications sous DirectX (support de Direct3D
  jusqu'à DX11)
- Support des jeux et applications sous OpenGL
- Impression via le pilote PostScript de l'hôte (généralement CUPS)
- Pilotes pour les Méta-fichiers Améliorés (EMF) et Méta-fichiers
  Windows (WMF)
- Fenêtres dans un bureau virtuel ou mélangé avec les autres fenêtres
- Couche de support Windows MultiMedia (WinMM) avec des codecs intégrés

## Permet aux programmes Windows d'utiliser :

- Périphériques audios via ALSA, OSS, PulseAudio, Core Audio, etc.
- Clavier multilingues et entrées CJK via XIM
- Modems, périphériques séries
- Réseaux (TCP/IP et IPX)
- Scanners ASPI
- Tablettes via XInput (ex. Wacom)

## Couverture et portabilité de l'API

- Conçu pour la compatibilité des sources et binaires avec du code Win32
- Outils de test de l'API Win32 pour tester la compatibilité
- Compilable sur un large choix de compilateurs C
- Permet de mixé du code Win32 et POSIX
- Permet de mixé des binaires ELF (.so) et PE (.dll/exe) dans un seul
  espace mémoire
- Fichiers d'en-têtes compatible Win32
- Documentation de l'API générée automatiquement
- Compilateur de ressources
- Compilateur de messages
- Compilateur IDL
- Support extensif d'Unicode
- Internationalisation -- Wine supporte 50 langages
- Débogueur intégré et messages de traces configurables
- Support de vérificateur de mémoire externe avec Valgrind
- Programmes d'exemples
