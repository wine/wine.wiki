---
title: Pages de manuel
---

<small>
&nbsp;[:flag_gb: English](Man-Pages)
&nbsp;[:flag_de: Deutsch](de/Man-Pages)
&nbsp;[:flag_pl: Polski](pl/Man-Pages)
</small>

-----

- [msiexec](Man-Pages/msiexec) - Wine MSI Installer
- [notepad](Man-Pages/notepad) - Wine text editor
- [regedit](Man-Pages/regedit) - Wine registry editor
- [regsvr32](Man-Pages/regsvr32) - Wine DLL Registration Server
- [wineboot](Man-Pages/wineboot) - perform Wine initialization, startup, and shutdown tasks
- [winecfg](Man-Pages/winecfg) - Wine Configuration Editor
- [wineconsole](Man-Pages/wineconsole) - Wine console manager
- [winedbg](Man-Pages/winedbg) - Wine debugger
- [winefile](Man-Pages/winefile) - Wine File Manager
- [winemine](Man-Pages/winemine) - Wine Minesweeper game
- [winepath](Man-Pages/winepath) - convert Unix paths to/from Win32 paths
- [wineserver](fr/Man-Pages/wineserver) - le serveur Wine
- [widl](Man-Pages/widl) - Wine Interface Definition Language (IDL) compiler
- [wine](fr/Man-Pages/wine) - exécuter des programmes Windows sur Unix
- [winebuild](Man-Pages/winebuild) - Wine dll builder
- [winedump](Man-Pages/winedump) - Wine DLL tool
- [winegcc](Man-Pages/winegcc) - Wine C and C++ MinGW Compatible Compiler
- [winemaker](fr/Man-Pages/winemaker) - générer une infrastructure de construction pour la compilation de programmes Windows sur UNIX
- [wmc](Man-Pages/wmc) - Wine Message Compiler
- [wrc](Man-Pages/wrc) - Wine Resource Compiler
