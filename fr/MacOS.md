<small>
&nbsp;[:flag_gb: English](MacOS)
</small>

-----

## Installer les paquets WineHQ

Les paquets WineHQ pour les branches de développement et staging sont
disponibles pour macOS 10.8 et supérieur. S'il vous plaît, testez ces
paquets et rapportez tous les bugs à <https://bugs.winehq.org>.

**Prérequis :**

1.  XQuartz \>= 2.7.7
2.  Gatekeeper ne doit pas bloquer les paquets non signés.

**Installation :**

Les tarballs et fichiers .pkg sont disponibles à
<https://dl.winehq.org/wine-builds/macosx/download.html>.

Il est recommandé pour les utilisateurs non expérimentés d'utiliser le
fichier .pkg .

**Pour installer depuis un fichier .pkg**, faites un double-clic sur le
paquet et l'assistant d'installation de macOS devrait s'ouvrir. Le
processus devrait être intuitif. Il est possible d'installer le paquet
pour tous les utilisateurs (nécessite les privilèges administrateurs) ou
juste pour l'utilisateur en cours. Une fois l'installation terminée,
vous devriez trouvé une entrée "Wine Staging" ou "Wine Devel" dans votre
Launchpad. En cliquant dessus, une fenêtre de Terminal devrait
apparaître avec une petite introduction des commandes importantes dans
Wine. Vous pouvez alors directement lancer wine/winecfg/... depuis le
Terminal vu que la variable PATH est correctement indiquée. Pour le
confort des utilisateurs, le paquet associe tout les fichiers \*.exe
avec lui-même, cela signifie que vous pouvez lancer des exécutables
Windows juste en faisant un double-clic dessus.

**Pour installer depuis le tarball**, extrayez le tout simplement dans
un répertoire. Il n'est pas nécessaire de définir les variables
d'environnement DYLD_\* (tous les chemins sont relatifs), donc il
devrait fonctionner tant que la structure des répertoire est préservée
(vous pouvez éliminer le préfixe /usr en utilisant --strip-components
1).

Pour plus d'information, regardez
<https://www.winehq.org/pipermail/wine-devel/2015-December/110990.html>
et
<https://www.winehq.org/pipermail/wine-devel/2016-January/111010.html>.

## Construire Wine

Voyez [Construire Wine sur macOS](MacOS-Building)

## Désinstaller Wine

- Enlevez le répertoire des sources et les binaires.

  Avec Homebrew:

  `brew rm wine && brew rm $(join <(brew leaves) <(brew deps wine))`

  Avec MacPorts, désinstallez le paquet wine que vous aviez précédemment
  installer :

  `sudo port uninstall --follow-dependencies wine`

  Remplacer wine avec wine-devel si vous avez installé la version de
  développement.

  Avec Fink:

  `fink remove wine`

  Remplacez wine avec wine-devel si vous avez installé la version de
  développement.

  Sinon et si vous avez utilisé `sudo make install`, utilisez :

  `sudo make uninstall`

  Ensuite supprimez tout simplement le répertoire des codes sources de
  Wine :

  `rm -rf src/wine`

- Nettoyez le pseudo-périphérique C: et les entrées de registres ainsi
  que tous les programmes installés dans C:

  `rm -rf $HOME/.wine`

- Vérifiez le dossier caché `$HOME/.local/` où Wine enregistre
  certains fichiers menus du bureau et des icônes pour la compatibilité
  avec la [Fondation X.Org](https://www.x.org/) et [Free
  Desktop](https://www.freedesktop.org/wiki/).

  `rm -rf $HOME/.local`

Notes : Ces fichiers dans ce répertoire ne sont pas requis sur macOS
**sauf** si vous utilisez un gestionnaire de fenêtre UNIX ou autre
application X11 au lieu des applications natives macOS.

## Logiciels tiers

Les logiciels tiers, tels que Wineskin, Winebottler et PlayOnMac ne sont
pas supportés par WineHQ. Si vous utilisez l'un d'eux, veuillez refaire
un test avec une version normale de Wine avant de rapporter un bug,
soumettre un rapport de test sur l'AppDB ou demander de l'aide sur ce
forum ou dans IRC.

## Voir aussi

- [macOS FAQ](MacOS-FAQ)
- [macOS Building](MacOS-Building)
