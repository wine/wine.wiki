<small>
&nbsp;[:flag_gb: English](Winetricks)
&nbsp;[:flag_nl: Nederlands](nl/Winetricks)
&nbsp;[:flag_th: ภาษาไทย](th/Winetricks)
&nbsp;[:flag_kr: 한국어](ko/Winetricks)
</small>

-----

## Qu'est ce que Winetricks ?

**Winetricks** est un script d'aide qui télécharge et installe une
variété de bibliothèque d'éxécution requise pour lancer certains
programmes dans Wine. Elles peuvent être des remplacement propriétaires
à des composants de Wine.

**Note :** Bien que Winetricks peut être *très* utile pour faire
fonctionner certains programmes dans Wine, son utilisation est
supseptible de limiter les possibilités d'obtenir du support via WineHQ.
Notamment le report de bugs qui peut être impossible si vous avez
remplacé des parties de Wine. Lisez [Rapporter un bug après avoir
utiliser Winetricks](#rapporter-des-bugs-après-avoir-utilisé-winetricks) ci-dessous.

**Note :** Certains paquets listés ci-dessous risquent de ne pas
fonctionner correctement avec de vieilles versions de Wine. Nous vous
recommandons de toujours utiliser la [dernière version de
Wine](Download).

## Obtenir Winetricks

Le script est maintenu par Austin English à <https://winetricks.org>. La
dernière version est disponible à
<https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks>.
Faites un clic-droit sur ce lien puis sur 'Enregistrer sous...' pour
enregistrer une copie à jour. Vous pouvez également l'obtenir en ligne
de commande avec la commande :

```sh
$ cd $HOME/Downloads
$ wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
$ chmod +x winetricks
```

Aussi, certains "paquets" Winetricks nécessite des outils externes, à
savoir : cabextract, unzip, p7zip, wget (ou curl) et zenity (ou
kdialog). Les utilisateurs de Linux peuvent généralement les obtenirs
via le gestionnaire de paquet de leur distribution.

## Utiliser Winetricks

Une fois que avez Winetricks, vous pouvez tout simplement le lancez en
saisissant `sh winetricks` dans un terminal. Vous pouvez aussi utilisez
`./winetricks` si avez fait `chmod +x winetricks` avant. S'il est lancé
sans paramètre, Winetricks affiche une GUI avec la liste des paquets
disponibles. Si vous connaissez le nom des paquets que vous souhaitez
installer, vous pouvez les indiquer dans la commande de Winetricks, le
processus installation commence immédiatement.

```sh
$ sh winetricks corefonts vcrun6
```

va installer les paquets corefonts et vcrun6.

## Options

Texte d'aide de la version 20200412 :

```
Usage: /usr/bin/winetricks [options] [command|verb|path-to-verb] ...
Executes given verbs.  Each verb installs an application or changes a setting.

Options:
    --country=CC      Set country code to CC and don't detect your IP address
-f,  --force           Don't check whether packages were already installed
    --gui             Show gui diagnostics even when driven by commandline
    --isolate         Install each app or game in its own bottle (WINEPREFIX)
    --self-update     Update this application to the last version
    --update-rollback Rollback the last self update
-k, --keep_isos       Cache isos (allows later installation without disc)
    --no-clean        Don't delete temp directories (useful during debugging)
-q, --unattended      Don't ask any questions, just install automatically
-r, --ddrescue        Retry hard when caching scratched discs
-t  --torify          Run downloads under torify, if available
    --verify          Run (automated) GUI tests for verbs, if available
-v, --verbose         Echo all commands as they are executed
-h, --help            Display this message and exit
-V, --version         Display version and exit

Commands:
list                  list categories
list-all              list all categories and their verbs
apps list             list verbs in category 'applications'
benchmarks list       list verbs in category 'benchmarks'
dlls list             list verbs in category 'dlls'
games list            list verbs in category 'games'
settings list         list verbs in category 'settings'
list-cached           list cached-and-ready-to-install verbs
list-download         list verbs which download automatically
list-manual-download  list verbs which download with some help from the user
list-installed        list already-installed verbs
arch=32|64            create wineprefix with 32 or 64 bit, this option must be
                      given before prefix=foobar and will not work in case of
                      the default wineprefix.
prefix=foobar         select WINEPREFIX=/home/$USER/.local/share/wineprefixes/foobar
annihilate            Delete ALL DATA AND APPLICATIONS INSIDE THIS WINEPREFIX
```

**Note :** Comme toutes les commandes Wine, Winetricks utilise la
variable d'environnement `WINEPREFIX`. C'est utile quand vous voulez
utiliser Winetricks avec différents préfixes Wine. Par exemple,

```sh
$ env WINEPREFIX=~/.winetest sh winetricks mfc40
```

installe le paquet mfc40 dans le préfixe `~/.winetest`.

**Note :** Les utilisateurs avec plusieurs versions de Wine sur leur
système (par exemple, un paquet installé et une construction git à part)
peuvent spécifier la version de Wine à utiliser. Par exemple,

```sh
$ env WINE=~/wine-git/wine sh winetricks mfc40
```

installe le paquet mfc40 en utilisant le Wine dans le dossier
~/wine-git.

## Rapporter des bugs après avoir utilisé Winetricks

Merci de ne pas rapporter de bugs si vous avez utilisé Winetricks pour
des fichiers natifs (non Wine), nous ne pouvons pas supporter les DLLs
de Microsoft.

Utiliser Winetricks pour installer gecko, mono et fakeie6 et acceptable
pour reporter un bug - soyez sur de mentionner ce que vous avez fait.

Si pour une application, Winetricks est nécessaire, mentionnez le quand
vous la soumettez dans l'AppDB, liste de mailling et toute autres
ressources Wine.

## Rapporter un bug **dans** Winetricks

Winetricks à son propre traqueur de bug à <https://winetricks.org>,
utilisez celui là. Si vous ne voulez pas crée un compte pour signaler un
bug, un post sur le forum de Wine pourra éventuellement être transmis.

## Comment enlever les choses installer par Winetricks

C'est facile d'installer un préfixe Wine tout entier, donc par défault
Winetricks installe chaque application dans son propre préfixe, il offre
également un moyen facile de supprimer le préfixe et le menu qu'il a
crée.

Winetricks n'offre cependant aucun moyen pour désinstaller des
applications ou DLLs individuelles dans un préfixe Wine. Il y a
plusieurs raisons à cela, mais c'est surtout parce que la méthode
préférée pour tout désinstaller dans Wine est de simplement crée un
nouveau préfixe. (Oui, c'est vrai que ce serait bien d'avoir des
désinstallateurs pour tous, mais je n'en ai pas besoin. Les patchs sont
bienvenues.)

## Installer Winetricks

Il n'est pas nécessaire d'installer Winetricks pour l'utiliser. Vous
pouvez choisir de l'installer dans un emplacement global, ainsi vous
aurez juste à taper `winetricks` dans un terminal. Certaines
distributions Linux inclusent Winetricks dans leurs paquets Wine, ainsi
vous n'avez pas à le télécharger séparément. (Vous pourriez quand même
vouloir le faire si leur version est ancienne.)

Si vous avez téléchargé votre propre copie de Winetricks, vous pouvez
l'installer manuellement via cette commande :

```sh
$ cd $HOME/Downloads
$ wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
$ chmod +x winetricks
$ sudo cp winetricks /usr/local/bin
```

## Voir aussi

- <https://www.cabextract.org.uk/> -- cabextract est un outils pour
  extraire les fichiers cab de Microsoft dans des environnements UNIX.
