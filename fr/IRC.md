---
title: Support en direct
---

<small>
&nbsp;[:flag_gb: English](IRC)
&nbsp;[:flag_de: Deutsch](de/IRC)
&nbsp;[:flag_es: Español](es/IRC)
&nbsp;[:flag_pt: Português](pt/IRC)
&nbsp;[:flag_nl: Nederlands](nl/IRC)
&nbsp;[:flag_pl: Polski](pl/IRC)
&nbsp;[:flag_tr: Türkçe](tr/IRC)
&nbsp;[:flag_kr: 한국어](ko/IRC)
&nbsp;[:flag_cn: 简体中文](zh_CN/IRC)
</small>

-----


[Libera.​Chat](https://libera.chat/) héberge un canal IRC pour Wine (en
anglais). Vous pouvez y accéder avec un client IRC de votre choix comme
[HexChat](https://hexchat.github.io/). Utilisez les paramètres
ci-dessous.

> **Serveur :** irc.libera.chat\
> **Port :** 6697\
> **Canal :** #winehq

Si vous utilisez Firefox ou n'importe quel autre navigateur qui
supporte les URLs IRC, vous pouvez rejoindre le salon de discussion en
cliquant simplement sur [#winehq](irc://irc.libera.chat/winehq).
