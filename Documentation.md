<small>
&nbsp;[:flag_fr: Français](fr/Documentation)
&nbsp;[:flag_de: Deutsch](de/Documentation)
&nbsp;[:flag_nl: Nederlands](nl/Documentation)
&nbsp;[:flag_pl: Polski](pl/Documentation)
&nbsp;[:flag_tr: Türkçe](tr/Documentation)
&nbsp;[:flag_kr: 한국어](ko/Documentation)
</small>

-----

## Guides

- **[Wine User's Guide](Wine-User's-Guide)**

  How to use and configure Wine for running Windows applications.

- **[Winelib User's Guide](Winelib-User's-Guide)**

  How to use Wine to port Windows applications to Linux.

- **[Wine Developer's Guide](Wine-Developer's-Guide)**

  How to hack on Wine.

- **[Wine Installation and Configuration](Wine-Installation-and-Configuration)**

  A short guide for those who want to help us make things work.

## Command reference

- **[Commands](Commands)**

  Documentation about the commands available with Wine.

- **[Man Pages](Man-Pages)**

  Manual pages for the Wine commands and tools.
